#!/bin/env python
# ** MEMO **
#   source Cactus environment before execute this.

VERSION = '1.0'
'''
--------------------------------------------------------------------------------
 File:   FlashMemoryRemoteProgrammer.py

  Remotely writes the FPGA flash configuration memory through an Ip Bus connection

  Author: Fabio Montecassiano - fabio.montecassiano((a))pd.infn.it
          INFN Padova - November 2015

--------------------------------------------------------------------------------
'''

#-- Description:
#   * Coupled with FW IpBus slave v 1.0 (only reg, without FIFO)
#   * Handshaking partially handled by sw.
#   * Flash timing relies on wait/delays (open loop)
#   * Exploit the latency of Ethernet O(100us) in order to avoid Sleep() time as much as possible
# 
# TODO:
# * aggiungere logging serio
# * Verificare la possibilita' di usare il bit-mask di ipbus senza compromettere hand-shaking con fw
# * definire le COSTANTI opportune in luogo  dei comandi binari es. xxx.write("1100")

import sys, string, os

#-- Paths
CURRENT_PATH = os.getcwd()
PATH_LOC_SCRIPT,\
NAME_LOC_SCRIPT=os.path.split(sys.argv[0]) # os.path.dirname(), os.path.basename()
ABS_PATH_LOC_SCRIPT=os.path.realpath(os.path.abspath(PATH_LOC_SCRIPT))
p=os.path.join(ABS_PATH_LOC_SCRIPT ,NAME_LOC_SCRIPT)
while os.path.islink(p): p=os.path.join(os.path.dirname(p), os.readlink(p))
PATH_TO_BIN = os.path.dirname(p)
PATH_TO_LIB = os.path.join(PATH_TO_BIN,'../lib')
sys.path.append(PATH_TO_LIB)

import RemoteProgrammerLib as rpl
import bit_manipulation as bm

try:
    import uhal
    UHAL_FOUND=True
except ImportError:
    UHAL_FOUND=False
    rpl.DisplayMsg(['UHAL libraries not found !',
                   rpl.DATE_PLACEHOLDER+'Do you have the correct environment ?'],
                   '*TBDCX')

#-- Option management ----------------------------------------------------------
def myOptionParser():
    import argparse
    class SmartFormatter(argparse.HelpFormatter):
        def _split_lines(self, text, width):
            # this is the RawTextHelpFormatter._split_lines
            if text.startswith('R|'):
                return text[2:].splitlines()  
            return argparse.HelpFormatter._split_lines(self, text, width)

    defaults = {
    'AddrTable'     :      'file://'+os.path.join(PATH_TO_LIB,'IpBus_SpiProgrammer_BRIEF.xml'),
    'ConnectionFile':      "file://${DTUPY_ETC}/connections/connections-tm7.xml",
    'PAGE_DELAY'    :      0.001, # THIS IS A CRITICAL PARAMETER, change it only if needed.
    }

    parser = argparse.ArgumentParser(formatter_class=SmartFormatter, \
                        description='Remotely writes the FPGA flash configuration memory through Ip Bus connection.\n\n',
                        epilog='Comments and bug reports to fabio.montecassiano((a))pd.infn.it',
                        prog= NAME_LOC_SCRIPT)
    #-- Options
    parser.add_argument('--version', dest='DO_VERSION', action="version",
                        version='%(prog)s v. '+VERSION+
                        ' [rpl v. %s / bm v. %s]' % (rpl.VERSION, bm.VERSION) )

    parser.add_argument('-cf', '--cfile', dest='ConnectionFile', default=defaults['ConnectionFile'],
                        help='R|(%(default)s)\n'
                             'Specify the IpBus Connection File', metavar='CONNECTION_FILE')
    parser.add_argument('-ch', '--ctrlhub', dest='CtrlHub_IP', default='',
                        help='R|Establish a TCPIP connection through the Control HUB\n'
                             'with IP address CTRL_HUB_IP',
                        metavar="CTRL_HUB_IP")
    parser.add_argument('-u', '--udp', dest='DO_UDP_CONNECTION', action="store_true",
                        help='R|(%(default)s)\n'
                             'Establish a direct UDP connection to the TARGET BOARDs',
                        default=False)
    parser.add_argument('-p', '--parallel', dest='TERM_FOR_PARALLEL',
                        choices=['gnome', 'xterm' , 'konsole'],
                        help='R|(%(default)s)\n'
                             'Performs concurrent programming of all the target boards.\n'
                             '*NB* It requires a (X) terminal among those supported',
                        default='sequential', type = str.lower)

    parser.add_argument('-fm', '--forceMcs', dest='DO_FORCE_MCS_BUILD', action="store_true",
                        help='Force the build of _initial.mcs and _update.mcs files',
                        default=False)
    parser.add_argument('-fp', '--forcePro', dest='DO_FORCE_PROGRAMMER', action="store_true",
                        help='Force acquiring ownership of the Flash Programmer',
                        default=False)
    parser.add_argument('-W', '--WRITE', dest='DO_WRITE_FLASH', action="store_true", 
                        help='R|(%(default)s)\n'
                             'Do Flash memory Erase & Program.\n'
                             '*NB* THIS FLAG MUST SET IN ORDER TO REALLY WRITE THE UPDATE IMAGE.\n'
                             '     If not set, only checking of the MCS update file and \n'
                             '     the IpBus connection will be performed',
                        default=False)
    parser.add_argument('-nl', '--nolog', dest='DO_LOGFILE', action="store_false",
                        help='Disable log file',
                        default=True)

    #-- ARGUMENTS
    parser.add_argument(dest='TargetBoard_lst',  default=[], nargs='+',
                        help='R|Specifies one or more TARGET BOARDs as sequence of:\n'
                             '  * ID defined inside the CONNECTION FILE\n'
                             "  * IP address, if '-u' or '-ch' has been specified", metavar="TARGET_BOARD")
    parser.add_argument(dest='INPUT_FILE', default='', metavar='FILE.[bit|mcs]',
                        help='R|BITSTREAM (.bit) or PROM (.mcs) file\n'
                             '** NB ** File .bit file needs the Vivado environment')
    #-- Help suppressed
    parser.add_argument('-d', '--delay',dest='PAGE_DELAY', default=defaults['PAGE_DELAY'],
                        type=float, help=argparse.SUPPRESS)
                        #'R|(%(default)s)\n' 'Delay [s] for internal page writing [Advanced]')
    parser.add_argument('--noheader',  dest='DO_NOHEADER', action="store_true",
                        help=argparse.SUPPRESS, # hide this optin in the help
                        default=False )
    parser.add_argument('-v', '--verbose', dest='VERBOSE_LEV', nargs='?', type=int, const=1,
                        help=argparse.SUPPRESS,
                        default=0) #-- Activate DEBUG Mode [0=No Debug, 9=Max] --
    parser.add_argument('-lw', '--linewidth', dest='LINE_WIDTH', type=int,
                        help=argparse.SUPPRESS,
                        default=80)
    parser.add_argument('-cu', '--customuri',     dest='Custom_URI', default='',
#                        help='R|Custom URI for the target board. Examples:\n'
#                          " -cu ipbusudp-2.0://192.168.1.105:50001\n"
#                          " -cu 'chtcp-2.0://127.0.0.1:10203?target=192.168.1.105:50001'",
                        help=argparse.SUPPRESS)

    parser.add_argument('-a', '--table', dest='AddrTable', default=defaults['AddrTable'],
#                        help='R|(%(default)s)\n'
#                             'Specify the IpBus Address Table used for UDP and CTRL HUB connections')
                        help=argparse.SUPPRESS)

    args = parser.parse_args()
    return args

args =myOptionParser()
rpl.LINE_WIDTH=args.LINE_WIDTH

if not args.DO_NOHEADER:
    rpl.DisplayMsg('Remote writing of the FPGA configuration flash memory via IpBus', '=TC')
    rpl.DisplayMsg('[%s/%s/%s]' % (VERSION, rpl.VERSION, bm.VERSION), '=BR', MsgColor=rpl.Color.RESET)

if args.VERBOSE_LEV:
    rpl.DisplayMsg(['>> NAME_LOC_SCRIPT:    "'+ NAME_LOC_SCRIPT +'"',
                    '>> PATH_LOC_SCRIPT:    "'+ PATH_LOC_SCRIPT +'"',
                    '>> ABS_PATH_LOC_SCRIPT:"'+ ABS_PATH_LOC_SCRIPT +'"',
                    '>> PATH_TO_BIN:        "'+ PATH_TO_BIN +'"',
                    '>> PATH_TO_LIB:        "'+ PATH_TO_LIB +'"',
                    '>> current path:       "'+ CURRENT_PATH +'"',
#                    '** terminal_width      "'+ str(os.getenv('COLUMNS'))+'"',
                    'args: ']+str(args).split(','), '#'
                   )

#-- Basic checks --------------------------------------------------------------
#-- bit & mcs:
#-- TODO: vf .bit was given ==> check existence of VIVADO env.
if not (args.INPUT_FILE and os.path.exists(os.path.expanduser(args.INPUT_FILE))):
    rpl.DisplayMsg("ERROR: input file '%s' not found" % args.INPUT_FILE,'*TBDX')
    sys.exit(1)
#-- Generate configuration images  --------------------------------------------
theFPGAConfigurationBuilder     = rpl.FPGAConfigurationBuilder(VerboseLevel=args.VERBOSE_LEV)
##theFPGAConfigurationBuilder.PATH_TO_CALLING_SCRIPT = PATH_TO_BIN
theFPGAConfigurationBuilder.PATH_TO_LIB = PATH_TO_LIB

#-- Recover or Generate both initial and update mcs image files
MCS_UPDATE_FILE, \
MCS_INITIAL_FILE = theFPGAConfigurationBuilder.BuildMcsImages(args.INPUT_FILE, \
                        args.DO_FORCE_MCS_BUILD, PRINT_HEADER= not args.DO_NOHEADER)

#-- END HERE if images had not build  or uhal was not found --------------------
if not (os.path.exists(os.path.expanduser(MCS_UPDATE_FILE)) and
        os.path.exists(os.path.expanduser(MCS_INITIAL_FILE)) ):
    #rpl.DisplayMsg('ERROR when building the MCS images','*TBCX')
    sys.exit(1)
if not UHAL_FOUND:
    rpl.DisplayMsg('UHAL libray not found. Exiting!','-TBC',
                   MsgColor=rpl.Color.OKGREEN+rpl.Color.BOLD)
    sys.exit(1)

#-- Check options related to UHAL ----------------------------------------------
#-- Address table
AddrTable_URL, AddrTable_file = '',''
AddrTable_URL, AddrTable_file = string.split(args.AddrTable,'//',1)
if ( AddrTable_URL=='file:' and (os.path.exists(AddrTable_file)) ):
#  and (os.path.exists(os.path.expanduser(AddrTable_file))) ):  raise #TODO ? pbl con var ambiente ${} ?
    ADDR_TABLE_FOUND=True
else:
    ADDR_TABLE_FOUND=False
if not (args.DO_UDP_CONNECTION or args.CtrlHub_IP<>'' or args.Custom_URI<>'') \
   and not ADDR_TABLE_FOUND:
    rpl.DisplayMsg(['ERROR: Address table not found.',
                    "AddrTable_URL: '%s'" % AddrTable_URL,
                    "AddrTable_file:'%s'" % AddrTable_file],\
                    '*TBCX')
    exit(1)


#-- Spawn over multiple terminals..
if len(args.TargetBoard_lst)>=2 and args.Custom_URI=='' :
    #-- Multiple targets
    #TODO: remove the BUGGY tricky done here by combining sys.argv[] strings by using
    #      only argsparse features as args.TargetBoard_lst
    new_args=" ".join(sys.argv[1:])
    for t in args.TargetBoard_lst:
        new_args=string.replace(new_args, t, ' ',1)
    new_args=string.replace(new_args, ' '+args.INPUT_FILE, ' ')
    #-- remove unwanted options
    new_args=string.replace(new_args, '-fm', ' ')
    new_args_strip=string.replace(new_args, '-forceMcs', ' ')

#----- MEMO ON TERMINALs -------------------------------------------------------
#----- concurrent:
#   gnome-terminal --tab     -e "bash -c 'echo 1;read'"          --tab     -e "bash -c 'echo 2';read"
#   konsole        --new-tab -e  bash -c "echo '1';read"; konsole --new-tab -e  bash -c "echo '2'; read"
#   xterm -T "1" -e "bash -c 'echo 1; read'" &  xterm -T "2" -e "bash -c 'echo 2; read'"  &

#----- sequential: 
#   gnome-terminal -x bash -c "echo '1'; echo '2'; read -n1"
#   konsole        -e bash -c "echo '1'; echo '2'; read -n1"
#   eval "cmd1; cmd2"

    cmd_term = {
        'xterm'     : '',
        'gnome'     : 'gnome-terminal  --hide-menubar --zoom=0.85 --geometry='+\
                        str(args.LINE_WIDTH )+'x50 ' + '--working-directory='+CURRENT_PATH ,
        'konsole'   : '',
        'sequential': 'eval "' # sequential execution whitin the calling shell
    }[args.TERM_FOR_PARALLEL]

    SeqTitle= 'All targets'+'/'+os.path.basename(args.INPUT_FILE).split('/')[0]+ \
                ' ['+os.path.basename(sys.argv[0]).split('/')[0] +']'

    Target_already_seen_lst=[]
    for t in args.TargetBoard_lst:
        if not  t in Target_already_seen_lst: #- sanity check 
            Target_already_seen_lst=Target_already_seen_lst+[t]
            Title= t+'/'+os.path.basename(args.INPUT_FILE).split('/')[0]+ \
                ' ['+os.path.basename(sys.argv[0]).split('/')[0] +']'    
            cmd_term = cmd_term + \
                {'xterm': 'xterm  -T "'+ Title +'" -geometry '+str(args.LINE_WIDTH )+'x50 ' +\
                            ' -e "bash -c \'python '+ os.path.abspath(sys.argv[0])          +\
                            ' '+ new_args_strip+' --noheader '+t+' '+args.INPUT_FILE+ ' ;'  +\
                            ' echo; echo \\"Press ENTER to close...\\";' + \
                            " read ' \" &   ",
                 'gnome': ' --tab -t "'+ Title +'" -e "bash -c \'python '+ os.path.abspath(sys.argv[0]) +\
                            ' '+ new_args_strip+' --noheader '+t+' '+args.INPUT_FILE+ ' ;'              +\
                            ' echo; echo \\"Press ENTER to close...\\";'                                +\
                            ' read \'" ' ,
 
               'konsole': 'konsole --workdir '+ CURRENT_PATH + ' --new-tab  -e bash -c "python '+ os.path.abspath(sys.argv[0]) +\
                            ' '+ new_args_strip+' --noheader '+t+' '+args.INPUT_FILE+ ' ;'                                     +\
                            ' echo; echo \\"Press ENTER to close...\\";'                                                       +\
                            ' read "; ',
            'sequential': ' python '+ os.path.abspath(sys.argv[0])                        +\
                            ' '+ new_args_strip+' --noheader '+t+' '+args.INPUT_FILE+ ' ;'
                }[args.TERM_FOR_PARALLEL]

        else:
            rpl.DisplayMsg('Target: "'+ t +'" already in the TARGET list... SKIPPING',
                        MsgColor=rpl.Color.FAIL+rpl.Color.BOLD)

    cmd=cmd_term+{
        'xterm'     : '',
        'gnome'     : '',
        'konsole'   : '',
        'sequential': '"'
    }[args.TERM_FOR_PARALLEL]
    if args.VERBOSE_LEV:   rpl.DisplayMsg('EXECUTING: '+cmd, '#X')
    os.system(cmd)
    if args.TERM_FOR_PARALLEL <> 'sequential':
        rpl.DisplayMsg('Opening X terminal(s) for parallel programming.')
        rpl.DisplayMsg('','B=')
else:
    #-- 1 only Target
    TargetBoard=args.TargetBoard_lst[0]

    #-- Makes the Full Uri
    #-- Priority is: 'custom uri' -> 'chtcp-2.0' -> 'ipbusudp-2.0' -> connection file (default)
    if args.Custom_URI:
        FullUri=args.Custom_URI
        #TODO: extract IP name from the FULL URI the check it.
        TargetBoard='FullUri'
    elif args.CtrlHub_IP and TargetBoard:
        #-- Connect through CONTROL HUB (chtcp-2.0)
        FullUri=  'chtcp-2.0://'+args.CtrlHub_IP+':10203?target='+TargetBoard +':50001'
    elif args.DO_UDP_CONNECTION and TargetBoard:
        #-- Connect directly to target through UDP (ipbusudp-2.0)
        FullUri=  'ipbusudp-2.0://'+TargetBoard +':50001'
    else:
        #-- Connect through connection file
        FullUri=''

    #-- Log file for each target
    if args.DO_LOGFILE:
        rpl.LogFile_Name = \
            os.path.join(CURRENT_PATH,
                         os.path.splitext(NAME_LOC_SCRIPT)[0]+'_Log_'+TargetBoard+'.txt')
        rpl.LogFile_fd =file(rpl.LogFile_Name,'a+',1)  #append to file
    rpl.DisplayMsg('', '=T')
    rpl.DisplayMsg('TARGET Id: %s' %( TargetBoard), '-DTL')
    rpl.DisplayMsg('-- [%s/%s/%s]' % (VERSION, rpl.VERSION, bm.VERSION), '-BFR')
    if args.VERBOSE_LEV:
        rpl.DisplayMsg('INVOKED w/: \''+" ".join(sys.argv[1:])+'\'', '#LX')

    rpl.DisplayMsg(['Trying connection to the Programmer slave with:',
                    ' Update MCS file: '+MCS_UPDATE_FILE,
                    '   Log file path: '+CURRENT_PATH ],
                    '-TD')
    if FullUri<>'':
        rpl.DisplayMsg(['        Full URI: '+FullUri,
                        '   Address table: '+os.path.relpath(AddrTable_file)]) #******
    else:
        rpl.DisplayMsg(['  Connection file: '+args.ConnectionFile,
                        '     Target Board: '+TargetBoard])

    #-- classes instantiation
    TheBoardProgrammer  = rpl.IpBusRemoteProgrammer(VerboseLevel=args.VERBOSE_LEV)
    TheBoardProgrammer.DisableProgrammerWhenTerminate=True

    #-- END here if ADDR_TABLE was not found! --------------------------------------
    if not ADDR_TABLE_FOUND:
        rpl.DisplayMsg('Adress Table not found. Exiting!','-TBC',
                       MsgColor=rpl.Color.OKGREEN+rpl.Color.BOLD)
        TheBoardProgrammer.terminate(0)

    #-- Connect to the Programmer slave
    try:
        #-- Mask the errors when loading the address tables
        #uhal.disableLogging()
        uhal.setLogLevelTo(uhal.LogLevel.ERROR)
        if FullUri<>'':
            board = uhal.getDevice(TargetBoard, FullUri, args.AddrTable)
        else:
            manager = uhal.ConnectionManager(args.ConnectionFile)
            board = manager.getDevice(TargetBoard)
        
        TheBoardProgrammer.board=board
        #-- Looks for nodes from Adress Table
        #--TODO: check the 4 strings are not empty and close to what is expected
        TheBoardProgrammer.CtrlNode  = board.getNodes('.*SFP_Ctrl')[0]     #'ipbus_spi_programmer.spi_prg.SFP_Ctrl'
        TheBoardProgrammer.StatusNode= board.getNodes('.*SFP_Status')[0]   #'ipbus_spi_programmer.spi_prg.SFP_Status'
        TheBoardProgrammer.DataNode  = board.getNodes('.*SFP_Data32')[0]   #'ipbus_spi_programmer.spi_prg.SFP_Data32'

        #-- Check the existence of Programmer slave in the target board by reading fw Status
        Status=TheBoardProgrammer.DecodeStatusReg(False)
    except:
        rpl.DisplayMsg(['ERROR:',
                        'Access to the remote flash Programmer slave failed'], '*TDBLX')
        TheBoardProgrammer.DisableProgrammerWhenTerminate=False #-- it did not respond so it has not to be disabled
        raise
        #TheBoardProgrammer.terminate(1)

    SvnRev=-1
    try:#- Check the SVN revision register
        TheBoardProgrammer.SvnRev    = board.getNodes('.*SVN_REVISION')[0] #'payload.dt_sector.stat6.SVN_REVISION'
        SvnRev = board.getNode(TheBoardProgrammer.SvnRev).read(); board.dispatch()
        if SvnRev<=0: raise
        rpl.DisplayMsg('  Current SVN revision: '+ str(SvnRev))
    except:
        #-- not fatal
        rpl.DisplayMsg('SVN revision register NOT FOUND or illegal! (%s)' % str(SvnRev), '*TBC')

    if args.VERBOSE_LEV:
        rpl.DisplayMsg(['IpBus Programmer nodes:',
                        '  CtrlNode:   '+TheBoardProgrammer.CtrlNode,
                        '  StatusNode: '+TheBoardProgrammer.StatusNode,
                        '  DataNode:   '+TheBoardProgrammer.DataNode,
                        '  SvnRev:     '+TheBoardProgrammer.SvnRev], '#X')
 
    rpl.DisplayMsg(['Access to the Programmer slave SUCCESSFUL:',
                    '                    Id: '+board.id(),
                    '                   Uri: '+board.uri(),
                    '     Programmer status: '+'\n                '.join(['']+Status)],
                    '-TD')
    #---------------- BEGIN NETWORK OPERATION --------------------------------------
    #-- Check ID of the flash memory
    if TheBoardProgrammer.AcquireLockProgrammer(SHOW_MSG=True,VERBOSE=args.VERBOSE_LEV,
                                   ForceAcquireOwenrshipProgrammer=args.DO_FORCE_PROGRAMMER):
        #-- Begin to program the configuration memory
        #------ Erase
        if args.DO_WRITE_FLASH:
            TheBoardProgrammer.EraseFlashMemory(args.VERBOSE_LEV)
        else:
            rpl.DisplayMsg('ERASING not performed! '+\
                           '[DO_WRITE_FLASH='+str(args.DO_WRITE_FLASH)+']', '-TD')
        #------ Program
        try:
            if TheBoardProgrammer.ProgramFlashMemory(MCS_UPDATE_FILE, args.DO_WRITE_FLASH,\
                                                     args.PAGE_DELAY, args.VERBOSE_LEV) <> 0:
                raise
        except:
            rpl.DisplayMsg('UPDATE mcs file parsed with errors', '*TBC', MsgColor=rpl.Color.FAIL+rpl.Color.BOLD )
        #------ Verify   
        if TheBoardProgrammer.VerifyCRC(SHOW_MSG=True, VERBOSE=args.VERBOSE_LEV)==0:
            rpl.DisplayMsg(['', "S U C C E S S F U L",'',
                            ' Next boot the FPGA will run the UPDATED fw!',''],
                            FormatFlags='C', MsgColor=rpl.Color.OKGREEN+rpl.Color.BOLD )
        else:
            rpl.DisplayMsg(['', "CRC of UPDATE IMAGE IS NOT CORRECT",'',
                            'Next boot the FPGA will run the GOLDEN fw (if installed)',''],
                            FormatFlags='C', MsgColor=rpl.Color.FAIL+rpl.Color.BOLD )

        TheBoardProgrammer.terminate(0, args.VERBOSE_LEV)
    else:
        TheBoardProgrammer.terminate(1, args.VERBOSE_LEV)

