#!/bin/bash

# Sintax
#   sh TM7_powercycle.sh MCH-IP-addr slot-Nr
#
# Example of use: Reboot of TM7 on slot 5 at P5 (as done on 2Dec 2, 2015)
#   sh .\TM7_powercycle.sh mch-s1d07-41-01 5

# Conventions:
#   * MCH is housed at slot #1 of the create
#   * slot #13 houses the AMC13 which has 2 Ip numbers, one per each FPGA
#

if [[ $# -eq 0 ]] ; then
    echo "No arguments supplied"
    echo "USAGE:"
    echo "   sh $0 MCH-IP-addr slot-Nr"
    exit 1
fi

MCH=$1
let FRU=$2+4
echo "Power cycling the TM7 on SLOT #: $2 trough MCH: $MCH .....(10 s to go)"
ipmitool -P password -I lan -H $MCH picmg deactivate $FRU
sleep 10

ipmitool -P password -I lan -H $MCH picmg activate $FRU

echo "...done"
