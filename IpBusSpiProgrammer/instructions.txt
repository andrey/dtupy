=== INSTALL ===
There is a symlink in tests/scripts so after loading test/env.sh you can run 
this script from anywhere.

== Digest-CRC [optional] ==
The build of the update image *_update.mcs is a time consuming task due to the 
CRC calculation. It could be speeded up by installing on the local machine 
(where you run the script) the following perl CPAN library. The factor speed is 
around 5X in time, building of update images pass form ~10-15'to 2-3' depending 
on PC performances.
  http://search.cpan.org/dist/Digest-CRC/
The installation instruction can be found at
 http://cpansearch.perl.org/src/OLIMAUL/Digest-CRC-0.21/README
In summary,
   perl Makefile.PL
   make
   make test
   make install # requires super-user privileges.


=== USAGE ===
== Environment vars ==
  . tests/env.sh # sets up PATH and automatic connection file detection
  . /opt/Xilinx/Vivado/2014.4/settings64.sh # vivado

== mcs files description ==
[Alvaro/Andrea understanding of the situation]
When the script is run for the first time on the .bit, three .mcs files are 
created:
* filename.mcs : the bitstream modified to the structure of an mcs file
* filename_initial.mcs : the bitstream, written in both golden and non-golden
  memory positions, so that it can be written by JTAG (or ipbus?) to the flash,
  and if the non-golden gets corrupted the board will be able to start from the
  golden
* filename_update.mcs : the bitstream, written only to the non-golden memory 
  positions, to be sent by the ipbus script. This is the file used when the 
  write operation is called with the .bit as an argument
  
== Examples ==
* Generate .mcs and check without write to flash
  TM7_RemoteProgrammer.py YB0_S1 top.bit

* Program concurrently 3 TM7s using default connection file
  TM7_RemoteProgrammer.py -W -p xterm YB0_S1 YB0_S2 YB0_S3 top.mcs

* Program via UDP
  TM7_RemoteProgrammer.py -u 192.168.40.105 top.mcs -W

* Program serially 2 TM7s using default connection file
  TM7_RemoteProgrammer.py TM7_LAB_1 TM7_LAB_2 top.mcs -W

See the help for more details
  TM7_RemoteProgrammer.py -h

  
== Some notes ==
* Connections: UDP, ControlHUB and Connection file supported.
* Files are generated in the current directory
* Basic lock mechanism (cooperative) implemented for both generation of
  mcs images and programmer acquiring
* If the CACTUS environment is not set but the Xilinx one it is,
  the script generates all needed files and then exits.
* The suggested use is to perform a 1st run without '-W' option (simulation).
  If no error, then just repeat the command adding '-W' for real writing

