#-- Comment/uncomment one of the pairs of lines below, depending on whether you have the Digest::CRC
#-- Use the following two lines if you have the Digest::CRC
use Digest::CRC qw(crc32);
sub subCrc32{my $data=shift(@_);return(subFastCrc32($data));}

print "-------------------------------------------------------------------------\n";
print "--      Perl Script: $0\n";
print "--      ** Using DIGEST CRC library **                                 --\n";
print "-------------------------------------------------------------------------\n";

#-- Use the following two lines if you do NOT have the Digest::CRC
# sub crc32{return(0);}
# sub subCrc32{my $data=shift(@_);return(subSlowCrc32($data));}

# print "-------------------------------------------------------------------------\n";
# print "--      Perl Script: $0\n";
# print "--      ** Using internal CRC library **                               --\n";
# print "-------------------------------------------------------------------------\n";


##############################################################################
# Copyright � 2013, Xilinx, Inc.
# This design is confidential and proprietary of Xilinx, Inc. All Rights Reserved.
##############################################################################
#   ____  ____
#  /   /\/   /
# /___/  \  /   Vendor: Xilinx
# \   \   \/    Version: 1.00
#  \   \        Filename: MakeSpiFlashProgrammerMcsFiles.pl
#  /   /        Date Last Modified:  January 16, 2013
# /___/   /\    Date Created:        January 16, 2013
# \   \  /  \
#  \___\/\___\
#
#Devices:
#Purpose:
#Usage:
#Reference:
##############################################################################
#
#  Disclaimer: LIMITED WARRANTY AND DISCLAMER. These designs are
#              provided to you "as is." Xilinx and its licensors make and you
#              receive no warranties or conditions, express, implied,
#              statutory or otherwise, and Xilinx specifically disclaims any
#              implied warranties of merchantability, noninfringement, or
#              fitness for a particular purpose. Xilinx does not warrant that
#              the functions contained in these designs will meet your
#              requirements, or that the operation of these designs will be
#              uninterrupted or error free, or that defects in the Designs
#              will be corrected. Furthermore, Xilinx does not warrant or
#              make any representations regarding use or the results of the
#              use of the designs in terms of correctness, accuracy,
#              reliability, or otherwise.
#
#              LIMITATION OF LIABILITY. In no event will Xilinx or its
#              licensors be liable for any loss of data, lost profits, cost
#              or procurement of substitute goods or services, or for any
#              special, incidental, consequential, or indirect damages
#              arising from the use or operation of the designs or
#              accompanying documentation, however caused and on any theory
#              of liability. This limitation will apply even if Xilinx
#              has been advised of the possibility of such damage. This
#              limitation shall apply notwithstanding the failure of the
#              essential purpose of any limited remedies herein.
#
##############################################################################
# Revision History:
#   Revision (YYYY/MM/DD) - [User] Description
#   Rev 1.00 (2013/01/16) - [RMK] Created.
##############################################################################


##############################################################################
# Constants
##############################################################################
# Hex values
$hexval{"0000"} = "0";
$hexval{"0001"} = "1";
$hexval{"0010"} = "2";
$hexval{"0011"} = "3";
$hexval{"0100"} = "4";
$hexval{"0101"} = "5";
$hexval{"0110"} = "6";
$hexval{"0111"} = "7";
$hexval{"1000"} = "8";
$hexval{"1001"} = "9";
$hexval{"1010"} = "A";
$hexval{"1011"} = "B";
$hexval{"1100"} = "C";
$hexval{"1101"} = "D";
$hexval{"1110"} = "E";
$hexval{"1111"} = "F";
# Binary values
$binval{"0"} = "0000";
$binval{"1"} = "0001";
$binval{"2"} = "0010";
$binval{"3"} = "0011";
$binval{"4"} = "0100";
$binval{"5"} = "0101";
$binval{"6"} = "0110";
$binval{"7"} = "0111";
$binval{"8"} = "1000";
$binval{"9"} = "1001";
$binval{"A"} = "1010";
$binval{"B"} = "1011";
$binval{"C"} = "1100";
$binval{"D"} = "1101";
$binval{"E"} = "1110";
$binval{"F"} = "1111";
$binval{"a"} = "1010";
$binval{"b"} = "1011";
$binval{"c"} = "1100";
$binval{"d"} = "1101";
$binval{"e"} = "1110";
$binval{"f"} = "1111";


##############################################################################
# Globals
##############################################################################
@mcs  = ();
my  $optVerbose = 0;

##############################################################################
# Subroutines
##############################################################################

##############################################################################
# Print Help
sub PrintHelp()
{
  print "Perl Script:  $0\n";
  print "Description:  Takes an input MCS file containing FPGA bitstream(s).\n";
  print "              Outputs MCS file images for the SPI flash programmer.\n";
  print "Usage:  perl $0 [options] input.mcs\n";
  print " where\n";
  print "   input.mcs = File name of input MCS containing FPGA bitstream(s).\n";
  print "               MCS generated with promgen -spi -p mcs -u 0 input.bit...\n";
  print " and the options are:\n";
  print "   -verbose            = Print more messages about output files\n";
  print "   -imagesize IS       = golden/update image size in Mbits. (Default=0 [auto])\n";
  print "   -device D           = where D=\"N25Q\", \"NP5Q\" or \"Atmel\" (Default=N25Q)\n";
  print "   -quickjump          = Insert IPROG to jump over initial padding areas.\n";
  print "   -sectorsize SS      = SPI flash sector size in bytes. (Default=65536)\n";
  print "   -subsectorsize SSS  = SPI flash subsector size in bytes. (Default=4096)\n";
  print "   -simple             = Create only simple image w/CRC, but no QuickBoot header.\n";
  exit;
}

##############################################################################
# sub:          subHexToBin
# Input:        $hex = String of hexadecimal characters
#               $len = Length of output binary string
# Output:       String of equivalent binary value.
# Description:  Convert hex to binary.
sub subHexToBin
{
  my $hex = shift(@_);
  my $len = shift(@_);
  if (!$len)
  {
    die "ERROR: Illegal subHexToBin length - $len\n";
  }
  $bin  = "";
  while (length($hex))
  {
    $hexchar  = substr($hex,0,1);
    $binset   = $binval{$hexchar};
    if (!length($binset))
    {
      die "ERROR: Illegal hex character = $hexchar\n";
    }
    $bin  .= $binset;
    $hex  = substr($hex,1);
  }
  while (length($bin)<$len)
  {
    $bin  = "0" . $bin;
  }
  while (length($bin)>$len)
  {
    $bin  = substr($bin,1);
  }
  return $bin;
}

##############################################################################
# sub:          subBinToHex
# Input:        $bin = String of binary characters
# Output:       String of equivalent hex value.
# Description:  Convert binary to hex.
sub subBinToHex
{
  my $bin = shift(@_);
  $hex  = "";
  while (length($bin)%4)
  {
    $bin  = "0" . $bin;
  }
  while (length($bin))
  {
    $hex  .= $hexval{substr($bin,0,4)};
    $bin  = substr($bin,4);
  }
  return $hex;
}


##############################################################################
# sub:          subTwosComplementChecksumOfHexStringOfBytes
# Input:        String of hexadecimal characters
# Output:       8-bit 2's-complement checksum
# Description:  Calculate the 8-bit, 2's-complement checksum from the given
#               string of hexadecimal characters as bytes (hex-char-pairs).
sub subTwosComplementChecksumOfHexStringOfBytes($)
{
  my $hexString = shift(@_);

  # Check for valid hex chars
  if ($hexString !~ /^[0-9A-Fa-f]+$/)
  {
    die "ERROR:  Invalid hex string:  $hexString\n";
  }

  # Check for hexadecimal character pairs for bytes
  my $hexStringLength = length($hexString);
  my $bytes           = $hexStringLength / 2;
  if ($hexStringLength % 2)
  {
    die "ERROR:  Invalid number of hex chars for hex-pair byte values\n";
  }

  # Compute checksum
  $checksum = 0;
  for ($i=0;$i<$bytes;$i++)
  {
    my $hexByte = substr($hexString,(2*$i),2);
    $checksum += hex($hexByte);
  }

  # Compute 8-bit two's complement
  $checksum = (($checksum ^ 0xFF) + 1) & 0xFF;

  return($checksum);
}

##############################################################################
# sub:          subBinRead
# Input:        $binFileName      = Input path\file name
# Output:       String of bytes as hex char pairs.
# Description:  Subroutine to read a pure binary file.
sub subBinRead
{
  my $binFileName = shift(@_);

  my $hexData     = "";

  if (!open(BINFILEREAD, "<$binFileName"))
  {
    print "ERROR:  Cannot open bin file:  $binFileName\n";
    die;
  }
  else
  {
    binmode(BINFILEREAD);
    print "INFO:  ##### Reading BIN file:  $binFileName...\n";
  }


  while (!eof(BINFILEREAD))
  {
    $binByte  = getc(BINFILEREAD);
    $hexByte  = sprintf("%02X", ord($binByte));
    $hexData .= $hexByte;
  }
  close(BINFILEREAD);

  $hexDataLength  = length($hexData) / 2;

  if ($optVerbose==1)
  {
  printf("INFO:  Read BIN:   Data Start Address = %09d (0x%08X)\n",0,0);
  printf("INFO:  Read BIN:   Data End Address   = %09d (0x%08X)\n",($hexDataLength-1),($hexDataLength-1));
  printf("INFO:  Read BIN:   Data Length        = %09d (0x%08X)\n",$hexDataLength,$hexDataLength);
  }

  return($hexData);
}


##############################################################################
# sub:          subHexRead
# Input:        $hexFileName      = Input path\file name
# Output:       String of bytes as hex char pairs.
# Description:  Subroutine to read an ASCII hex-pair stream of bytes.
#               Whitespace is ignored.
sub subHexRead
{
  my $hexFileName = shift(@_);

  my $hexData     = "";
  my $lineNumber  = 0;

  if (!open(HEXFILEREAD, "<$hexFileName"))
  {
    print "ERROR:  Cannot open hex file:  $hexFileName\n";
    die;
  }
  else
  {
    print "INFO:  ##### Reading HEX file:  $hexFileName...\n";
  }

  while (<HEXFILEREAD>)
  {
    $lineNumber++;
    my $line        = "";
    my $inputLineLen= length($_);

    # Strip whitespace
    for (my $i=0;$i<$inputLineLen;$i++)
    {
      my $char  = substr($_,$i,1);
      if ($char =~ /[0-9A-Fa-f]/)
      {
        $line .= $char;
      }
      elsif ($char =~ /\S/)
      {
        die "ERROR:  Non-hex char ($char) on line number $lineNumber\n";
      }
    }
    my $lineLength  = length($line);

    if ($lineLength % 2)
    {
      print "WARNING:  Number of hex characters is not a multiple of 2 on line number $lineNumber\n";
    }

    $hexData  .= $line;
  }
  close(HEXFILEREAD);

  if (length($hexData) % 2)
  {
    die "ERROR:  Number of hex characters is not a multiple of 2 (Note: 2 chars/byte)\n";
  }


  $hexDataLength  = length($hexData) / 2;

  if ($optVerbose==1)
  {
  printf("INFO:  Read HEX:   Data Start Address = %09d (0x%08X)\n",0,0);
  printf("INFO:  Read HEX:   Data End Address   = %09d (0x%08X)\n",($hexDataLength-1),($hexDataLength-1));
  printf("INFO:  Read HEX:   Data Length        = %09d (0x%08X)\n",$hexDataLength,$hexDataLength);
  }

  return($hexData);
}


##############################################################################
# sub:          subMcsRead
# Input:        $mcsFileName      = Input .MCS path\file name
#               $mcsFill          = [Optional] Byte value (2-char hex pair)
#                                   to fill gaps.
#               $mcsFillFromZero  = [Optional] Byte value (2-char hex pair)
#                                   to fill a gap from address zero.
# Output:       String of bytes as hex char pairs.  If a gap is not filled,
#               a "XX<ADDR>" string can be found in the return value where
#               <ADDR> is a 32-bit (8-char) hex value that represents the byte
#               address for the first byte of data in the next set.
# Description:  Subroutine to read a .MCS file.
sub subMcsRead
{
  my $mcsFileName           = shift(@_);
  my $mcsFill               = shift(@_);
  my $mcsFillFromZero       = shift(@_);

  my $mcsDataHex            = "";
  my $mcsDataAddressStart   = -1;
  my $mcsDataAddressCurrent = 0;
  my $mcsGaps               = 0;
  my $mcsSegmentAddress     = 0;
  my $mcsStartSegmentAddress= "";
  my $mcsStartLinearAddress = "";
  my $lineNumber            = 0;
  my $eof                   = 0;

  if (!open(MCSFILEREAD, "<$mcsFileName"))
  {
    print "ERROR:  Cannot open .MCS file:  $mcsFileName\n";
    die;
  }
  else
  {
    print "INFO:  ##### Reading MCS file:  $mcsFileName...\n";
  }

  while (<MCSFILEREAD>)
  {
    $lineNumber++;
    my $errorMsg    = "";
    my $warningMsg  = "";

    # Strip trailing whitespace
    while ($_ =~ /\s$/)
    {
      chomp;
    }
    my $lineLength  = length($_);

    # Check for a valid record
    if ($_ =~ /^\s*$/)
    {
      if (!$eof)
      {
        $warningMsg .= "WARNING:  Found empty line.\n";
      }
    }
    elsif ($eof)
    {
      $warningMsg .= "WARNING:  Extra information after end of file record\n";
    }
    elsif ($_ !~ /^\:/)
    {
      $errorMsg = sprintf("ERROR:  Expected line to begin with Record Mark ':', but found: '%s'\n", substr($_,0,1));
    }
    elsif ($lineLength < 11)
    {
      $errorMsg = "ERROR:  Insufficient characters ($lineLength) on line for a valid record\n";
    }
    elsif ($_ =~ /^\:([0-9A-Fa-f]{2})([0-9A-Fa-f]{4})([0-9A-Fa-f]{2})([0-9A-Fa-f]*)([0-9A-Fa-f]{2})$/)
    {
      my $mcsLineRecLenHex      = $1;
      my $mcsLineRecLen         = hex($mcsLineRecLenHex);
      my $mcsLineLoadOffsetHex  = $2;
      my $mcsLineLoadOffset     = hex($mcsLineLoadOffsetHex);
      my $mcsLineRecTypHex      = $3;
      my $mcsLineDataHex        = $4;
      my $mcsLineDataLength     = length($mcsLineDataHex);
      my $mcsLineDataBytes      = $mcsLineDataLength / 2;
      my $mcsLineChksumHex      = $5;
      my $mcsLineChksum         = hex($mcsLineChksumHex);
      my $mcsLineCalcChksum     = subTwosComplementChecksumOfHexStringOfBytes(substr($_,1,($lineLength-3)));
      my $mcsLineCalcChksumHex  = sprintf("%02X",$mcsLineCalcChksum);

      if ($mcsLineDataLength % 2)
      {
        $errorMsg = "ERROR:  Number of hex data characters is not a multiple of 2 (2 chars/byte)\n";
      }
      elsif ($mcsLineRecLen != $mcsLineDataBytes)
      {
        $errorMsg = "ERROR:  Record Length = $mcsLineRecLen, but actual data bytes count = $mcsLineDataBytes\n";
      }
      elsif ($mcsLineRecTypHex eq "00")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }

        # Data record
        if ($mcsLineRecLen)
        {
          # Check for gaps and fill as specified
          $mcsLineByteAddress = $mcsSegmentAddress + $mcsLineLoadOffset;
          if ($mcsDataHex eq "")
          {
            if ($mcsFillFromZero ne "")
            {
              while ($mcsDataAddressCurrent < $mcsLineByteAddress)
              {
                $mcsDataHex .= $mcsFillFromZero;
                $mcsDataAddressCurrent++;
              }
              if ($mcsDataAddressStart == -1) {$mcsDataAddressStart=0;}
            }
            elsif ($mcsDataAddressCurrent < $mcsLineByteAddress)
            {
              $warningMsg .= sprintf("WARNING:  Gap from 0x%08X to 0x%08X\n",
                                     $mcsDataAddressCurrent, $mcsLineByteAddress);
              $mcsGaps++;
              $mcsDataHex .= "XX"; # Gap;  Mark new address with "XX"
              $mcsDataHex .= sprintf("%08X",$mcsLineByteAddress);
              $mcsDataAddressCurrent  = $mcsLineByteAddress;
              if ($mcsDataAddressStart == -1) {$mcsDataAddressStart=$mcsDataAddressCurrent;}
            }
            elsif ($mcsDataAddressStart == -1)
            {
              $mcsDataAddressStart=$mcsDataAddressCurrent;
            }
          }
          elsif ($mcsFill ne "")
          {
            while ($mcsDataAddressCurrent < $mcsLineByteAddress)
            {
              $mcsDataHex .= $mcsFill;
              $mcsDataAddressCurrent++;
            }
          }
          elsif ($mcsDataAddressCurrent < $mcsLineByteAddress)
          {
            $warningMsg .= sprintf("WARNING:  Gap from 0x%08X to 0x%08X\n",
                                   $mcsDataAddressCurrent, $mcsLineByteAddress);
            $mcsGaps++;
            $mcsDataHex .= "XX"; # Gap;  Mark new address with "XX"
            $mcsDataHex .= sprintf("%08X",$mcsLineByteAddress);
            $mcsDataAddressCurrent  = $mcsLineByteAddress;
          }

          # Place the data from the line
          $mcsDataHex .= $mcsLineDataHex;
          $mcsDataAddressCurrent  += $mcsLineDataBytes;
        }
      }
      elsif ($mcsLineRecTypHex eq "01")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }
        # End of file record
        if ($mcsLineRecLenHex ne "00")
        {
          $warningMsg .= "WARNING:  Expected \"00\" for record length, but found \"$mcsLineRecLenHex\"\n";
        }
        if ($mcsLineLoadOffsetHex ne "0000")
        {
          $warningMsg .= "WARNING:  Expected \"0000\" for load offset, but found \"$mcsLineLoadOffsetHex\"\n";
        }
        $eof  = 1;
      }
      elsif ($mcsLineRecTypHex eq "02")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }
        # Extended segment address record
        if ($mcsLineRecLenHex ne "02")
        {
          $errorMsg = "ERROR:  Expected \"02\" for record length, but found \"$mcsLineRecLenHex\"\n";
        }
        else
        {
          if ($mcsLineLoadOffsetHex ne "0000")
          {
            $warningMsg .= "WARNING:  Expected \"0000\" for load offset, but found \"$mcsLineLoadOffsetHex\"\n";
          }
          $mcsSegmentAddressHex = $mcsLineDataHex . "0";
          $mcsSegmentAddress    = hex($mcsSegmentAddressHex);
          if ($mcsSegmentAddress < $mcsDataAddressCurrent)
          {
            $errorMsg = "ERROR:  Extended segment address record is not sequential\n";
          }
        }
      }
      elsif ($mcsLineRecTypHex eq "03")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }
        # Start segment address record
        if ($mcsLineRecLenHex ne "04")
        {
          $errorMsg = "ERROR:  Expected \"04\" for record length, but found \"$mcsLineRecLenHex\"\n";
        }
        else
        {
          if ($mcsLineLoadOffsetHex ne "0000")
          {
            $warningMsg .= "WARNING:  Expected \"0000\" for load offset, but found \"$mcsLineLoadOffsetHex\"\n";
          }
          if (length($mcsLineStartSegmentAddress))
          {
            $warningMsg .= "WARNING:  Found more than one start segment address record.\n";
          }
          $mcsLineStartSegmentAddress = $mcsLineDataHex;
          $warningMsg .= "WARNING:  Ignoring start segment address ($mcsLineStartSegmentAddress) record.\n";
        }
      }
      elsif ($mcsLineRecTypHex eq "04")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }
        # Extended linear address record
        if ($mcsLineRecLenHex ne "02")
        {
          $errorMsg = "ERROR:  Expected \"02\" for record length, but found \"$mcsLineRecLenHex\"\n";
        }
        else
        {
          if ($mcsLineLoadOffsetHex ne "0000")
          {
            $warningMsg .= "WARNING:  Expected \"0000\" for load offset, but found \"$mcsLineLoadOffsetHex\"\n";
          }
          $mcsSegmentAddressHex = $mcsLineDataHex . "0000";
          $mcsSegmentAddress    = hex($mcsSegmentAddressHex);
          if ($mcsSegmentAddress < $mcsDataAddressCurrent)
          {
            $errorMsg = "ERROR:  Extended linear address record is not sequential\n";
          }
        }
      }
      elsif ($mcsLineRecTypHex eq "05")
      {
        if ($mcsLineCalcChksum ne $mcsLineChksum)
        {
          $warningMsg .= "WARNING:  Checksum error.  Expected \"$mcsLineCalcChksumHex\", but found \"$mcsLineChksumHex\"\n";
        }
        # Start linear address record
        if ($mcsLineRecLenHex ne "04")
        {
          $errorMsg .= "WARNING:  Expected \"04\" for record length, but found \"$mcsLineRecLenHex\"\n";
        }
        else
        {
          if ($mcsLineLoadOffsetHex ne "0000")
          {
            $warningMsg .= "WARNING:  Expected \"0000\" for load offset, but found \"$mcsLineLoadOffsetHex\"\n";
          }
          if (length($mcsLineStartLinearAddress))
          {
            $warningMsg .= "WARNING:  Found more than one start linear address record.\n";
          }
          $mcsLineStartLinearAddress  = $mcsLineDataHex;
          $warningMsg .= "WARNING:  Ignoring start linear address ($mcsLineStartLinearAddress) record.\n";
        }
      }
      else
      {
        $errorMsg = "ERROR:  Unrecognized record type '$mcsLineRecTypHex'\n";
      }
    }
    else
    {
      $errorMsg = "ERROR:  Invalid (non-hexadecimal) characters on line\n";
    }

    if (length($errorMsg)||length($warningMsg))
    {
      print "$warningMsg$errorMsg";
      print "  LINE[$lineNumber]: \"$_\"\n";
      if (length($errorMsg))
      {
        die;
      }
    }
  }
  close(MCSFILEREAD);

  my $mcsDataLength     = $mcsDataAddressCurrent - $mcsDataAddressStart;
  my $mcsDataAddressEnd = $mcsDataAddressCurrent - 1;
  if ($optVerbose==1)
  {
  printf("INFO:  Read MCS:   Data Start Address = %09d (0x%08X)\n",$mcsDataAddressStart,$mcsDataAddressStart);
  printf("INFO:  Read MCS:   Data End Address   = %09d (0x%08X)\n",$mcsDataAddressEnd,$mcsDataAddressEnd);
  printf("INFO:  Read MCS:   Data Length        = %09d (0x%08X)\n",$mcsDataLength,$mcsDataLength);
  }

  return($mcsDataHex);
}


##############################################################################
# sub:          subMcsWriteDataLine
# Input:        $mcsFile          = Output .MCS path\file handle
#               $lastAddress      = Last valid written data address
#               $lineAddress      = Start address for this line
#               $lineData         = Data for this line
# Output:       Writes an MCS file.
# Description:  Subroutine to write a line to the .MCS file.
##############################################################################
sub subMcsWriteDataLine
{
  my $mcsFile     = shift(@_);
  my $lastAddress = shift(@_);
  my $lineAddress = shift(@_);
  my $lineData    = shift(@_);

  my $line        = "";
  my $checksum    = 0;

  # Check extended address segment
  if (($lastAddress & 0xFFFF0000) ne ($lineAddress & 0xFFFF0000))
  {
    my $segmentAddress  = $lineAddress >> 16;
    $line            = sprintf("02000004%04X", $segmentAddress);
    $checksum        = subTwosComplementChecksumOfHexStringOfBytes($line);
    printf($mcsFile ":%s%02X\n", $line, $checksum);
  }

  my $lineOffset  = $lineAddress & 0xFFFF;
  my $lineBytes   = length($lineData) / 2;
  $line       = sprintf("%02X%04X00%s",$lineBytes,$lineOffset,$lineData);
  $checksum   = subTwosComplementChecksumOfHexStringOfBytes($line);
  printf($mcsFile ":%s%02X\n", $line, $checksum);
}

##############################################################################
# sub:          subMcsWrite
# Input:        $mcsFileName      = Output .MCS path\file name
#               $mcsDataHex       = Hex data to write to the MCS file.
#               $mcsStartAddr     = Start address.
#               $mcsBytesPerLine  = Number of bytes per line in the MCS file.
# Output:       Writes an MCS file.
# Description:  Subroutine to write an .MCS file.
##############################################################################
sub subMcsWrite
{
  my $mcsFileName       = shift(@_);
  my $mcsDataHex        = shift(@_);
  my $mcsStartAddr      = shift(@_);
  my $mcsBytesPerLine   = shift(@_);
  my $mcsDataHexLength  = length($mcsDataHex);

  if ($mcsStartAddr eq "")
  {
    $mcsStartAddr = 0;
  }

  if ($mcsBytesPerLine eq "")
  {
    $mcsBytesPerLine  = 16;
  }

  # Check hex data
  if ($mcsDataHexLength % 1)
  {
    print "ERROR:  Hex data length has odd number of digits.\n";
    die;
  }
  else
  {
    for ($i=0;$i<$mcsDataHexLength;$i+=2)
    {
      $byte = substr($mcsDatahex,$i,2);
      if (($byte ne "XX") && ($byte =~ /^[0-9A-Fa-f][0-9A-Fa-f]$/))
      {
        print "ERROR:  Unrecognized hex byte characters in data:  \"$byte\"\n";
        die;
      }
    }
  }

  # Prepare to write hex to MCS file
  if (!open(MCSFILE, ">$mcsFileName"))
  {
    print "ERROR:  Cannot open .MCS file:  $mcsFileName\n";
    die;
  }
  elsif ($mcsDataHexLength==0)
  {
    print "WARNING:  Empty MCS file:  $mcsFileName\n";
    close(MCSFILE);
  }
  else
  {
    if ($optVerbose==1)
    {
    print "INFO:  Writing MCS file:  $mcsFileName\n";
    #print "INFO:  Start address  = $mcsStartAddr\n";
    #print "INFO:  Bytes per line = $mcsBytesPerLine\n";
    }

    my $firstAddress= -1;
    my $lastAddress = -1;
    my $nextAddress = $mcsStartAddr;
    my $i           = 0;
    my $lineBytes   = 0;
    my $lineData    = "";
    while ($i < $mcsDataHexLength)
    {
      $byte = substr($mcsDataHex, $i, 2);
      $i += 2;

      # Check for jump to new address
      if ($byte eq "XX")
      {
        # Flush any stored bytes
        if ($lineBytes>0)
        {
          subMcsWriteDataLine(MCSFILE, $lastAddress, $nextAddress, $lineData);
          if ($firstAddress == -1) { $firstAddress = $nextAddress; }
          $nextAddress  += $lineBytes;
          $lastAddress  = $nextAddress - 1;
          $lineBytes    = 0;
          $lineData     = "";
        }

        # Get and check jump address
        if (($i+8)>$mcsDataHexLength)
        {
          print "ERROR:  MCS address error.  Insuffient address digits.\n";
          die;
        }
        my $nextAddressHex  = substr($mcsDataHex, $i, 8);
        $i += 8;
        $nextAddress        = hex($nextAddressHex);
        if ($nextAddress < $lastAddress)
        {
          print "ERROR:  MCS address error.  Next address less than prior address.\n";
          die;
        }
      }
      else
      {
        $lineData   .= $byte;
        $lineBytes  += 1;
        if ($lineBytes == $mcsBytesPerLine)
        {
          if ($firstAddress == -1) { $firstAddress = $nextAddress; }
          subMcsWriteDataLine(MCSFILE, $lastAddress, $nextAddress, $lineData);
          $nextAddress  += $lineBytes;
          $lastAddress  = $nextAddress - 1;
          $lineBytes    = 0;
          $lineData     = "";
        }
      }
    }

    if ($lineBytes>0)
    {
      if ($firstAddress == -1) { $firstAddress = $nextAddress; }
      subMcsWriteDataLine(MCSFILE, $lastAddress, $nextAddress, $lineData);
      $nextAddress  += $lineBytes;
      $lastAddress  = $nextAddress - 1;
    }


    print MCSFILE ":00000001FF\n";
    close(MCSFILE);

    my $mcsDataLength = ($lastAddress - $firstAddress + 1);
    if ($optVerbose==1)
    {
    printf(STDOUT "INFO:  Write MCS:  Data Start Address = %09d (0x%08X)\n", $firstAddress, $firstAddress);
    printf(STDOUT "INFO:  Write MCS:  Data End Address   = %09d (0x%08X)\n", $lastAddress, $lastAddress);
    printf(STDOUT "INFO:  Write MCS:  Data Length        = %09d (0x%08X)\n", $mcsDataLength, $mcsDataLength);
    }
  }
}

##############################################################################
# sub:          subPadDataHex
# Input:        $dataHex          = String of hexadecimal pairs (bytes as hex).
#               $size             = Size of memory image in bytes to pad data.
# Output:       String of bytes as hex char pairs.
# Description:  Adds padding of 0xFF bytes until $dataHex reaches $size of bytes.
##############################################################################
sub subPadDataHex
{
  my $dataHex = shift(@_);
  my $size    = shift(@_);

  my $dataHexLength   = length($dataHex);
  if (($dataHexLength % 2) == 1)
  {
    die "ERROR: Input data should be pairs of hex chars, but has one extra char.\n";
  }

  my $dataHexBytes    = $dataHexLength / 2;
  if ($dataHexBytes > $size)
  {
    die "ERROR: Input data is longer than requested size.\n";
  }

  my $padLength       = $size - $dataHexBytes;
  my $i;
  my $pad             = "";
  for ($i=0;$i<$padLength;$i++)
  {
    $pad  = $pad . "FF";
  }

  my $dataHexPadded   = $dataHex . $pad;

  return($dataHexPadded);
}

##############################################################################
# sub:          subFastCrc32
# Input:        $dataHex          = String of hexadecimal pairs (bytes as hex).
# Output:       CRC32 as hex value
# Description:  Calculate the CRC32 of the input data.
#               Inefficient implementation but matches VHDL.
##############################################################################
sub subFastCrc32
{
  my $dataHex = shift(@_);

  my $dataHexLength   = length($dataHex);
  if (($dataHexLength % 2) == 1)
  {
    die "ERROR: Input data should be pairs of hex chars, but has one extra char.\n";
  }

  my $dataBytes = "";

  # Calc CRC32 over entire data set
  my $j = 0;
  while ($j < $dataHexLength)
  {
    my  $byteHex  = substr($dataHex,$j,2);
    $dataBytes  = $dataBytes . chr(hex($byteHex));
    $j = $j + 2;
  }
  my $crc32Dec = crc32($dataBytes);
  my $crc32Rev = sprintf("%08X",$crc32Dec);
  my $crc32    = substr($crc32Rev,6,2) . substr($crc32Rev,4,2) . substr($crc32Rev,2,2) . substr($crc32Rev,0,2);

  return($crc32);
}

##############################################################################
# sub:          subSlowCrc32
# Input:        $dataHex          = String of hexadecimal pairs (bytes as hex).
# Output:       CRC32 as hex value
# Description:  Calculate the CRC32 of the input data.
#               Inefficient implementation but matches VHDL.
##############################################################################
sub subSlowCrc32
{
  my $dataHex = shift(@_);

  my $dataHexLength   = length($dataHex);
  if (($dataHexLength % 2) == 1)
  {
    die "ERROR: Input data should be pairs of hex chars, but has one extra char.\n";
  }

  my @byteBit;
  my @crc32;
  my @nextCrc32;
  my $i;

  # Preset CRC32 to 0xFFFFFFFF
  for ($i=0;$i<32;$i++)
  {
    $crc32[$i] = 1;
  }

  # Calc CRC32 over entire data set
  my $j = 0;
  while ($j < $dataHexLength)
  {
    my  $byteHex  = substr($dataHex,$j,2);
    $j = $j + 2;

    my  $byteBin  = subHexToBin($byteHex,8);
    for ($i=0;$i<8;$i++)
    {
      $byteBit[$i] = substr($byteBin,(7-$i),1);
    }

    $nextCrc32[0]   = $byteBit[7] ^ $crc32[30] ^ $byteBit[1] ^ $crc32[24];
    $nextCrc32[1]   = $byteBit[7] ^ $crc32[25] ^ $byteBit[1] ^ $crc32[31] ^ $byteBit[6] ^ $crc32[30] ^ $byteBit[0] ^ $crc32[24];
    $nextCrc32[2]   = $byteBit[7] ^ $crc32[25] ^ $byteBit[1] ^ $byteBit[5] ^ $crc32[31] ^ $byteBit[6] ^ $crc32[30] ^ $byteBit[0] ^ $crc32[26] ^ $crc32[24];
    $nextCrc32[3]   = $crc32[25] ^ $byteBit[5] ^ $crc32[31] ^ $crc32[27] ^ $byteBit[6] ^ $byteBit[0] ^ $crc32[26] ^ $byteBit[4];
    $nextCrc32[4]   = $byteBit[7] ^ $byteBit[1] ^ $byteBit[5] ^ $byteBit[3] ^ $crc32[27] ^ $crc32[30] ^ $crc32[28] ^ $crc32[26] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[5]   = $byteBit[7] ^ $crc32[25] ^ $byteBit[1] ^ $crc32[31] ^ $byteBit[3] ^ $crc32[27] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[30] ^ $crc32[28] ^ $byteBit[0] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[6]   = $crc32[25] ^ $byteBit[5] ^ $byteBit[1] ^ $crc32[31] ^ $byteBit[3] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[28] ^ $crc32[30] ^ $byteBit[0] ^ $crc32[26];
    $nextCrc32[7]   = $byteBit[7] ^ $byteBit[5] ^ $crc32[31] ^ $crc32[27] ^ $byteBit[2] ^ $crc32[29] ^ $crc32[26] ^ $byteBit[0] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[8]   = $byteBit[7] ^ $crc32[25] ^ $crc32[0] ^ $byteBit[3] ^ $crc32[27] ^ $byteBit[6] ^ $crc32[28] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[9]   = $crc32[25] ^ $byteBit[5] ^ $byteBit[3] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[1] ^ $crc32[28] ^ $crc32[26];
    $nextCrc32[10]  = $byteBit[7] ^ $byteBit[5] ^ $crc32[27] ^ $byteBit[2] ^ $crc32[29] ^ $crc32[26] ^ $crc32[2] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[11]  = $byteBit[7] ^ $crc32[25] ^ $byteBit[3] ^ $crc32[27] ^ $byteBit[6] ^ $crc32[28] ^ $crc32[3] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[12]  = $byteBit[7] ^ $crc32[4] ^ $crc32[25] ^ $byteBit[1] ^ $byteBit[5] ^ $byteBit[3] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[30] ^ $crc32[28] ^ $crc32[26] ^ $crc32[24];
    $nextCrc32[13]  = $crc32[5] ^ $crc32[25] ^ $byteBit[5] ^ $byteBit[1] ^ $crc32[31] ^ $crc32[27] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[30] ^ $byteBit[0] ^ $crc32[26] ^ $byteBit[4];
    $nextCrc32[14]  = $byteBit[5] ^ $byteBit[1] ^ $byteBit[3] ^ $crc32[31] ^ $crc32[27] ^ $crc32[28] ^ $crc32[30] ^ $crc32[26] ^ $byteBit[0] ^ $crc32[6] ^ $byteBit[4];
    $nextCrc32[15]  = $crc32[7] ^ $byteBit[3] ^ $crc32[31] ^ $crc32[27] ^ $byteBit[2] ^ $crc32[29] ^ $crc32[28] ^ $byteBit[0] ^ $byteBit[4];
    $nextCrc32[16]  = $byteBit[7] ^ $crc32[8] ^ $byteBit[3] ^ $byteBit[2] ^ $crc32[29] ^ $crc32[28] ^ $crc32[24];
    $nextCrc32[17]  = $crc32[25] ^ $crc32[9] ^ $byteBit[1] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[30];
    $nextCrc32[18]  = $crc32[10] ^ $byteBit[5] ^ $byteBit[1] ^ $crc32[31] ^ $crc32[30] ^ $crc32[26] ^ $byteBit[0];
    $nextCrc32[19]  = $crc32[11] ^ $crc32[31] ^ $crc32[27] ^ $byteBit[0] ^ $byteBit[4];
    $nextCrc32[20]  = $byteBit[3] ^ $crc32[12] ^ $crc32[28];
    $nextCrc32[21]  = $crc32[13] ^ $byteBit[2] ^ $crc32[29];
    $nextCrc32[22]  = $byteBit[7] ^ $crc32[14] ^ $crc32[24];
    $nextCrc32[23]  = $byteBit[7] ^ $crc32[25] ^ $byteBit[1] ^ $crc32[15] ^ $byteBit[6] ^ $crc32[30] ^ $crc32[24];
    $nextCrc32[24]  = $crc32[25] ^ $byteBit[5] ^ $crc32[31] ^ $byteBit[6] ^ $crc32[16] ^ $byteBit[0] ^ $crc32[26];
    $nextCrc32[25]  = $crc32[17] ^ $byteBit[5] ^ $crc32[27] ^ $crc32[26] ^ $byteBit[4];
    $nextCrc32[26]  = $byteBit[7] ^ $crc32[18] ^ $byteBit[1] ^ $byteBit[3] ^ $crc32[27] ^ $crc32[30] ^ $crc32[28] ^ $crc32[24] ^ $byteBit[4];
    $nextCrc32[27]  = $crc32[19] ^ $crc32[25] ^ $crc32[31] ^ $byteBit[3] ^ $byteBit[2] ^ $byteBit[6] ^ $crc32[29] ^ $crc32[28] ^ $byteBit[0];
    $nextCrc32[28]  = $crc32[20] ^ $byteBit[5] ^ $byteBit[1] ^ $byteBit[2] ^ $crc32[29] ^ $crc32[30] ^ $crc32[26];
    $nextCrc32[29]  = $crc32[21] ^ $byteBit[1] ^ $crc32[31] ^ $crc32[27] ^ $crc32[30] ^ $byteBit[0] ^ $byteBit[4];
    $nextCrc32[30]  = $crc32[22] ^ $byteBit[3] ^ $crc32[31] ^ $crc32[28] ^ $byteBit[0];
    $nextCrc32[31]  = $byteBit[2] ^ $crc32[23] ^ $crc32[29];

    for ($i=0;$i<32;$i++)
    {
       $crc32[$i] = $nextCrc32[$i];
    }
  }

  # complement the CRC32
  for ($i=0;$i<32;$i++)
  {
     $crc32[$i] = $crc32[$i] ^ 1;
  }

  # Swap the bit order of the CRC32
  my $sCrc32  =  $crc32[24].$crc32[25].$crc32[26].$crc32[27].$crc32[28].$crc32[29].$crc32[30].$crc32[31].
                 $crc32[16].$crc32[17].$crc32[18].$crc32[19].$crc32[20].$crc32[21].$crc32[22].$crc32[23].
                 $crc32[8].$crc32[9].$crc32[10].$crc32[11].$crc32[12].$crc32[13].$crc32[14].$crc32[15].
                 $crc32[0].$crc32[1].$crc32[2].$crc32[3].$crc32[4].$crc32[5].$crc32[6].$crc32[7];
  my $sHexCrc32 = subBinToHex($sCrc32);

  return($sHexCrc32);
}


##############################################################################
# sub:          subComplement32bits
# Input:        $data32 = 32-bit hexadecimal.
# Output:       Complement of $data32
# Description:  Complement of $data32
##############################################################################
sub subComplement32bits
{
  my $data32Hex = shift(@_);

  if (length($data32Hex) != 8)
  {
    die "ERROR: Input data32 should be 8 hex chars.\n";
  }

  my $data32Bits  = subHexToBin($data32Hex,32);
  my $i;
  my $complement32Bits1 = "";
  my $complement32Bits2 = "";
  my $complement32Bits3 = "";
  my $complement32Bits4 = "";
  my $flipped32Bits     = "";

  for ($i=0;$i<32;$i++)
  {
    if (substr($data32Bits,$i,1) eq "0")
    {
      $flipped32Bits  .= "1";
    }
    else
    {
      $flipped32Bits  .= "0";
    }
  }

  for ($i=0;$i<8;$i++)
  {
    $complement32Bits1 .= substr($flipped32Bits,(7-$i),1);
    $complement32Bits2 .= substr($flipped32Bits,(15-$i),1);
    $complement32Bits3 .= substr($flipped32Bits,(23-$i),1);
    $complement32Bits4 .= substr($flipped32Bits,(31-$i),1);
  }
  my $complement32Bits  = $complement32Bits1 . $complement32Bits2 . $complement32Bits3 . $complement32Bits4;
  my $complement32Hex   = subBinToHex($complement32Bits);

  return($complement32Hex);
}


##############################################################################
# Main
##############################################################################

my  $crc32Residue         = "C704DD7B";
my  $optImageSizeMb       = 0;
my  $optImageSizeMbPlus   = 0;
my  $optSectorSize        = 65536;
my  $optSubsectorSize     = 4096;
my  $optSubsectorSizeN25Q = 4096;
my  $optSubsectorSizeNP5Q = 64;
my  $optSubsectorSizeAtmel= 256;
my  $optPageSize          = 256;
my  $optPageSizeNP5Q      = 64;
my  $optQuickJump         = 0;
my  $optGoldenToSector    = 0;
my  $optDevice            = "N25Q";
my  $optQuickBoot2        = 0;
my  $optSimple            = 0;
my  $optTotal             = 0;

##############################################################################
# Process command-line arguments

##### No arguments or -h, then print the help
if ((@ARGV==0) || (lc($ARGV[0]) eq "-h"))
{
  PrintHelp();
}

##### Process command-line switches
while ($ARGV[0] =~ /^-/)
{
  if (lc($ARGV[0]) =~ /^-imagesize/)
  {
    shift;
    if ($ARGV[0] =~ /^\+([0-9]+)/)
    {
      $optImageSizeMbPlus = $1;
    }
    else
    {
      $optImageSizeMb = $ARGV[0];
    }
    #print("INFO:  -imagesize set to: $optImageSizeMb\n");
    #print("INFO:  -imagesizeplus set to: $optImageSizeMbPlus\n");
  }
  elsif (lc($ARGV[0]) =~ /^-verbose/)
  {
    $optVerbose = 1;
    #print("INFO:  -verbose set to print more messages about output files\n");
  }
  elsif (lc($ARGV[0]) =~ /^-sectorsize/)
  {
    shift;
    $optSectorSize = $ARGV[0];
    #print("INFO:  -sectorsize set to: $optSectorSize\n");
  }
  elsif (lc($ARGV[0]) =~ /^-subsectorsize/)
  {
    shift;
    $optSubsectorSize = $ARGV[0];
    #print("INFO:  -subsectorsize set to: $optSubsectorSize\n");
  }
  elsif (lc($ARGV[0]) =~ /^-device/)
  {
    shift;
    $optDevice = uc($ARGV[0]);
    if ($optDevice eq "N25Q")
    {
      #print("INFO:  -device N25Q = Micron N25Q device.\n");
    }
    elsif ($optDevice eq "NP5Q")
    {
      #print("INFO:  -device NP5Q = adjust for Micron NP5Q device.\n");
      $optPageSize  = $optPageSizeNP5Q;
    }
    elsif ($optDevice eq "ATMEL")
    {
      #print("INFO:  -device Atmel = adjust for Atmel DataFlash device.\n");
    }
    elsif ($optDevice eq "S3AN")
    {
      $optDevice  = "ATMEL";
      #print("INFO:  -device S3AN = adjust for Atmel DataFlash device.\n");
    }
    else
    {
      print("WARNING:  Did not recognize -device $optDevice.  Ignoring.\n");
      $optDevice  = "N25Q";
    }
  }
  elsif (lc($ARGV[0]) =~ /^-quickjump/)
  {
    $optQuickJump = 1;
    #print("INFO:  -quickjump = Insert IPROG jump over padding areas\n");
  }
  elsif (lc($ARGV[0]) =~ /^-quickboot2/)
  {
    $optQuickBoot2 = 1;
    #print("INFO:  -quickboot2 = Support 2 updatable areas\n");
  }
  elsif (lc($ARGV[0]) =~ /^-simple/)
  {
    $optSimple = 1;
    #print("INFO:  -simple = Create simple image\n");
  }
  else
  {
    print("WARNING:  Did not recognize command-line option: $ARGV[0]\n");
  }
  shift;
}

if ($optDevice eq "NP5Q")
{
  $optSubsectorSize = $optSubsectorSizeNP5Q
}
elsif ($optDevice eq "ATMEL")
{
  $optSubsectorSize = $optSubsectorSizeAtmel
}

my  $fileNameMcsInput   = $ARGV[0];
if ($optVerbose==1)
{
print("INFO:  Input file name set to: $fileNameMcsInput\n");
}
my  $fileNameType       = lc(substr($fileNameMcsInput,-4,4));
if ($fileNameType eq ".mcs")
{
  # OK.  MCS is the default
}
elsif ($fileNameType eq ".bin")
{
}
else
{
  die "ERROR: Input file $fileNameMcsInput is not a .mcs or .bin file\n";
}

#OK#---- ORIGINALE -------------------------------------------------------------
#OK# my  $fileNameRoot       = substr($fileNameMcsInput,0,-4);
#OK#---- ORIGINALE -------------------------------------------------------------
#-- [FM - Dec 2015] ------------------------------------------------------------
use File::Basename;
my  $fileNameRoot       = substr( basename($fileNameMcsInput) ,0,-4);
#-------------------------------------------------------------------------------

if (length($fileNameRoot) == 0)
{
  die "ERROR: Invalid input file $fileNameMcsInput\n";
}

my  $fileNameMcsInitial = $fileNameRoot . "_initial.mcs";
my  $fileNameMcsTotal   = $fileNameRoot . "_total.mcs";
my  $fileNameMcsGolden  = $fileNameRoot . "_golden.mcs";
my  $fileNameMcsUpdate  = $fileNameRoot . "_update.mcs";
my  $fileNameMcsUpdate2 = $fileNameRoot . "_update2.mcs";
my  $fileNameMcsSimple  = $fileNameRoot . "_simple.mcs";

my $mcsDataHex = "";
if ($fileNameType eq ".mcs")
{
  $mcsDataHex = subMcsRead($fileNameMcsInput);
}
else
{
  $mcsDataHex = subBinRead($fileNameMcsInput);
}
# Search input file for unexpected gaps.  Expect contiguous, starting from addr 0
if (substr($mcsDataHex,0,2) eq "XX")
{
  die "ERROR:  Input MCS data does not start at address zero.\n";
}
if (index($mcsDataHex,"XX") > 0)
{
  die "ERROR:  Input MCS data has gaps in its defined data space.\n";
}
my $mcsDataLength = length($mcsDataHex) / 2;
if ($optVerbose==1)
{
print("INFO:  Input data length = $mcsDataLength bytes\n");
}

if ($optImageSizeMb==0)
{
  # Auto set image size. Round to next megabit (128Kbytes)
  $optImageSizeMb = int(($mcsDataLength+131071)/131072);
  $bits = $mcsDataLength*8;
  if ($optImageSizeMbPlus!=0)
  {
    $optImageSizeMb += $optImageSizeMbPlus;
  }
}
if ($verbose==1)
{
  print "INFO:  Image size = $optImageSizeMb Mb\n";
}

# Define switch word and warm boot sequences
my  $switchWordHex    = "AA995566";
my  $iprogToSwitchWordHexFormat = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000BB11220044FFFFFFFFFFFFFFFFAA9955662000000030020001%08X300080010000000F20000000200000002000000020000000";
my  $iprogToUpdateHexFormat     = "2000000030020001%08X300080010000000F200000002000000020000000";

# Compute switch word and warm boot addresses
my  $addrSwitch   = $optSubsectorSize - 4;
if ($optQuickJump==1)
{
  $addrSwitch   += $optSubsectorSize;
  $iprogToSwitchWordHex=sprintf($iprogToSwitchWordHexFormat,$addrSwitch);
}

my $imageSizeBytes  =   $optImageSizeMb * 131072;

if ($optQuickBoot2==1)
{
  $addrSwitch2  = (3 * $optSubsectorSize) - 4;
  if ($optQuickJump==1)
  {
    $addrSwitch2  += $optSubsectorSize;
    $iprogToSwitchWordHex2=sprintf($iprogToSwitchWordHexFormat,$addrSwitch2);
  }
  $updateStartAddr2 = $imageSizeBytes;
  $updateEndAddr2   = $updateStartAddr2 + $imageSizeBytes;
  $updateStartAddr  = $updateEndAddr2;
  $updateEndAddr    = $updateStartAddr + $imageSizeBytes;
  $iprogToUpdateHex2= sprintf($iprogToUpdateHexFormat,$updateStartAddr2);
}
else
{
  $updateStartAddr  = $imageSizeBytes;
  $updateEndAddr    = $updateStartAddr + $imageSizeBytes;
}

$iprogToUpdateHex   = sprintf($iprogToUpdateHexFormat,$updateStartAddr);

# build the QuickBoot header
my  $header = "";
if ($optQuickJump==1)
{
  $header .= $iprogToSwitchWordHex;
}
$header = subPadDataHex($header,$addrSwitch);
$header .= $switchWordHex;
$header .= $iprogToUpdateHex;

if ($optQuickBoot2==1)
{
  if ($optQuickJump==1)
  {
    $header .= $iprogToSwitchWordHex2;
  }
  $header = subPadDataHex($header,$addrSwitch2);
  $header .= $switchWordHex;
  $header .= $iprogToUpdateHex2;
}

my $goldenBitSpace = $imageSizeBytes - (length($header) / 2);
if ($mcsDataLength > $goldenBitSpace)
{
  die "ERROR:  Input MCS data length ($mcsDataLength bytes) exceeds golden bitstream capacity ($goldenBitSpace bytes).\n";
}

#my $testValue = "AA995566";
#my $testCrc32 = subCrc32($testValue);
#my $testValueCrc  = $testValue . $testCrc32;
#my $testResidue  = subComplement32bits(subCrc32($testValueCrc));
#print("TEST value=$testValue, CRC32=$testCrc32, Residue=$testResidue\n");

if ($optSimple==1)
{
  $updateEndAddr  = $imageSizeBytes;
  print ("INFO:  ##### Creating simple MCS file...\n");
  printf("INFO:  Simple image start address = 0x00000000\n");
  printf("INFO:  Simple image end+1 address = 0x%08X\n", $updateEndAddr);
  printf("INFO:  Generating files...This can take several minutes....\n");
  my $simpleData  = subPadDataHex($mcsDataHex, $updateEndAddr-4);
  my $crc32 = subCrc32($simpleData);
  if ($optVerbose==1)
  {
  print ("INFO:  CRC32 = $crc32\n");
  }
  my $simpleDataCrc  = $simpleData . $crc32;
  my $residue = subComplement32bits(subCrc32($simpleDataCrc));
  if ($residue eq $crc32Residue)
  {
    if ($optVerbose==1)
    {
    print ("INFO:  Residue = $residue ($crc32Residue expected)\n");
    }
  }
  else
  {
    print ("ERROR:  Residue $residue does not match expected $crc32Residue value\n");
  }
  subMcsWrite($fileNameMcsSimple, $simpleDataCrc, 0);
  printf("INFO:  Output Simple MCS file name = $fileNameMcsSimple\n");
}
else
{
  my $minSpiFlashSize = int(((8*$updateEndAddr)+1048575)/1048576);
  my $addrWidth       = 24;
  if ($minSpiFlashSize>128)
  {
    $addrWidth  = 32;
  }
  printf("INFO:  SPI Device Type             = $optDevice\n");
  printf("INFO:  SPI flash (minimum) size    = %d Mbits\n",$minSpiFlashSize);
  printf("INFO:  SPI address width           = %d bits\n", $addrWidth);
  printf("INFO:  SPI sector size             = %08X (%d) bytes\n", $optSectorSize, $optSectorSize);
  printf("INFO:  SPI page size               = %08X (%d) bytes\n", $optPageSize, $optPageSize);
  printf("INFO:  Update image start address  = 0x%08X\n", $updateStartAddr);
  printf("INFO:  Update image end+1 address  = 0x%08X\n", $updateEndAddr);
  printf("INFO:  Update switch word address  = 0x%08X\n", $addrSwitch);
  if ($optQuickBoot2==1)
  {
    printf("INFO:  Update2 image start address = 0x%08X\n", $updateStartAddr2);
    printf("INFO:  Update2 image end+1 address = 0x%08X\n", $updateEndAddr2);
    printf("INFO:  Update2 switch word address = 0x%08X\n", $addrSwitch2);
  }
  if ($optVerbose==1)
  {
  printf("INFO:  SPI subsector size          = %d bytes\n", $optSubsectorSize, $optSubsectorSize);
  }
  printf("INFO:  Switch word                 = 0x%s\n", $switchWordHex);
  if ($optVerbose==1)
  {
  printf("INFO:  Golden bitstream capacity   = $goldenBitSpace bytes\n");
  }
  printf("INFO:  Generating files...This can take several minutes....\n");
  if ($optVerbose==1)
  {
  print ("INFO:  ##### Creating update MCS file...\n");
  printf("INFO:  Update image start address = 0x%08X\n", $updateStartAddr);
  printf("INFO:  Update image end+1 address = 0x%08X\n", $updateEndAddr);
  }
  my $updateData  = subPadDataHex($mcsDataHex, $updateStartAddr-4);
  my $crc32 = subCrc32($updateData);
  if ($optVerbose==1)
  {
  print ("INFO:  CRC32 = $crc32\n");
  }
  my $updateDataCrc  = $updateData . $crc32;
  my $residue = subComplement32bits(subCrc32($updateDataCrc));
  if ($residue eq $crc32Residue)
  {
    if ($optVerbose==1)
    {
    print ("INFO:  Residue = $residue ($crc32Residue expected)\n");
    }
  }
  else
  {
    print ("ERROR:  Residue $residue does not match expected $crc32Residue value\n");
  }
  subMcsWrite($fileNameMcsUpdate, $updateDataCrc, $updateStartAddr);
  printf("INFO:  Output Update MCS file name = $fileNameMcsUpdate\n");

  if ($optQuickBoot2==1)
  {
    subMcsWrite($fileNameMcsUpdate2, $updateDataCrc, $updateStartAddr2);
    printf("INFO:  Output Update2 MCS file name= $fileNameMcsUpdate2\n");
  }

  if ($optVerbose==1)
  {
  print ("INFO:  ##### Creating Initial MCS file...\n");
  printf("INFO:  Initial image start address = 0x00000000\n");
  printf("INFO:  Initial image end+1 address = 0x%08X\n", $updateEndAddr);
  }
  my $initialData  = $header . $mcsDataHex;
  $initialData  = subPadDataHex($initialData, $imageSizeBytes) . $updateDataCrc;
  if ($optQuickBoot2==1)
  {
    $initialData  .= $updateDataCrc;
  }
  subMcsWrite($fileNameMcsInitial, $initialData, 0);
  printf("INFO:  Output Initial MCS file name= $fileNameMcsInitial\n");

  if ($optTotal==1)
  {
    if ($optVerbose==1)
    {
    print ("INFO:  ##### Creating total MCS file...\n");
    printf("INFO:  total image start address = 0x00000000\n");
    printf("INFO:  total image end+1 address = 0x%08X\n", $updateEndAddr);
    }
    my $totalData  = substr($initialData, 0, -8);
    $crc32  = subCrc32($totalData);
    if ($optVerbose==1)
    {
    print ("INFO:  CRC32 = $crc32\n");
    }
    my $totalDataCrc = $totalData . $crc32;
    $residue = subComplement32bits(subCrc32($totalDataCrc));
    if ($residue eq $crc32Residue)
    {
      if ($optVerbose==1)
      {
      print ("INFO:  Residue = $residue ($crc32Residue expected)\n");
      }
    }
    else
    {
      print ("ERROR:  Residue $residue does not match expected $crc32Residue value\n");
    }
    subMcsWrite($fileNameMcsTotal, $totalDataCrc, 0);
    printf("INFO:  Output Total MCS file name  = $fileNameMcsTotal\n");
  }
}






