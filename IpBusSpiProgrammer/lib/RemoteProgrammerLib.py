#-------------------------------------------------------------------------------
#-- Classes and methods for the IpBus Remote Programmer
#-- Fabio Montecassiano - INFN Pd
#-- v 0.2 - November 2015
#-------------------------------------------------------------------------------

VERSION = '1.0'

import sys, time
import os, subprocess
import datetime as dt
import bit_manipulation as bm

#===============================================================================
# http://stackoverflow.com/questions/15580303/python-output-complex-line-with-floats-colored-by-value
# http://stackoverflow.com/questions/287871/print-in-terminal-with-colors-using-python
# https://www.npmjs.com/package/ansi-codes
class Color:
    DEFAULT = '\033[99m'
    WHITE   = '\033[97m'
    CYAN    = '\033[96m'
    HEADER  = '\033[95m'    # pink/magenta
    OKBLUE  = '\033[94m'
    WARNING = '\033[93m'    # yellow
    OKGREEN = '\033[92m'
    FAIL    = '\033[91m'    # red
    DARKGREY= '\033[90m'

    LIGHTGREY='\033[37m'
    CYAN2   = '\033[36m'
    PURPLE  = '\033[35m'
    CYAN    = '\033[96m'
    PINK    = '\033[95m'
    LIGHTBLUE='\033[94m'
    BLUE =    '\033[94m'
    YELLOW =  '\033[93m'
    LIGHTGREEN='\033[92m'
    GREEN =   '\033[92m'
    LIGHTRED= '\033[91m'
    RED =     '\033[91m'

    STRIKETHROUGHOFF='\033[29m'
    STRIKETHROUGH='\033[09m'
    INVISABLE   = '\033[08m'
    REVERSEOFF  ='\033[27m'
    REVERSE     ='\033[07m'
    #BLINK      = '\033[5m'
    UNDERLINEOFF='\033[24m'
    UNDERLINE   ='\033[4m'
    ITALICOFF   ='\033[23m'
    ITALIC      ='\033[3m'
    BOLDOFF     ='\033[21m'
    BOLD        ='\033[1m'
    RESET       ='\033[0m'



#-------------------------------------------------------------------------------
#-- Class Data members 
LogFile_TmpName = None
LogFile_Name    = None
LogFile_fd = None
LINE_WIDTH = 80 # default. Can be set from the calling script
DATE_PLACEHOLDER=' '*22

#-------------------------------------------------------------------------------
def unique_file(file_name):
    counter = 1
    file_name_parts = os.path.splitext(file_name) # returns ('/path/file', '.ext')
    while counter < 999: #TODO: remove tricky
        try:
            fd = os.open(file_name, os.O_CREAT | os.O_EXCL | os.O_RDWR)
            return os.fdopen(fd), file_name
        except OSError:
            pass
        file_name = file_name_parts[0] + '_' + str(counter) + file_name_parts[1]
        counter += 1
    else:
        print "[unique_file] ERROR: naming limit reached"

def unique_file_2(file_name):
    import tempfile
    dirname, filename = os.path.split(file_name)
    prefix, suffix = os.path.splitext(filename)

    fd, filename = tempfile.mkstemp(suffix, prefix+"_", dirname)
#    return os.fdopen(fd)#, filename
    return fd
#-------------------------------------------------------------------------------

def DisplayMsg(msgs, FormatFlags='', MsgColor=''):
    #-- FormatFlags
    # 'D': date
    # 'T': top          'B': bottom
    # 'L': left         'C': center     'R': right
    # 'X': extended lines
    # '*': Error        '=': Header     '-': Info       '#': Debug
    
    # 'S': only on screen               'F': only on file     // 
    if type(msgs) == type(''):
        msg_list=msgs.split('\n') if msgs <> '' else []
    elif type(msgs) == type([]):
        msg_list=msgs
    else:
        raise ValueError("[DisplayMsg] uncorrect msgs type")
    FormatFlags=FormatFlags.upper()
    if (msg_list <> [] and 'D' in FormatFlags):
        msg_list[0]= '[%s] ' % dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+msg_list[0]

    if   '*' in FormatFlags:
            C={'border':Color.FAIL , 'msg':Color.BOLD+Color.BOLD,'clear':Color.RESET}
            CharFiller='*'
    elif '=' in FormatFlags: 
            C={'border':Color.BOLD , 'msg':Color.BOLD+Color.DARKGREY,'clear':Color.RESET}
            CharFiller='='
    elif '#' in FormatFlags:
            C={'border':Color.BOLD , 'msg':Color.OKBLUE,'clear':Color.RESET}
            CharFiller='#'
    else:
            C={'border':'', 'msg':'','clear':''}
            CharFiller='-'

    if MsgColor<>'':
        C['clear']=Color.RESET
        C['msg']  =MsgColor

    if not 'F' in FormatFlags and 'T' in FormatFlags:
        print C['border'] +(CharFiller*LINE_WIDTH)[:LINE_WIDTH] +C['clear']
    if not 'S' in FormatFlags and 'T' in FormatFlags and LogFile_fd:
        print >> LogFile_fd, (CharFiller*LINE_WIDTH)[:LINE_WIDTH]
    for msg in msg_list:
        ms=msg.split('\n')
        for m in ms:
          #-- splits lines longer then LINE_WIDTH
          while len(m)>0:
            if   'C' in FormatFlags and \
                  not 'X' in FormatFlags: mm = m.center(LINE_WIDTH-6)[:LINE_WIDTH-6]
            elif 'R' in FormatFlags: mm = m.rjust(LINE_WIDTH-6)[:LINE_WIDTH-6]
            elif 'L' in FormatFlags: mm = m.ljust(LINE_WIDTH-6)[:LINE_WIDTH-6]
            else: mm = m.ljust(LINE_WIDTH-6)[:LINE_WIDTH-6]
            if not 'F' in FormatFlags:
                print ( (C['border'] + CharFiller*2 + C['clear'])+
                         C['msg']    +    " %s "    + C['clear'] +
                        (C['border'] + CharFiller*2 + C['clear']) ) % mm

            if not 'S' in FormatFlags and LogFile_fd:
                print >> LogFile_fd, (CharFiller*2 + " %s " + CharFiller*2) % mm

            m=m[LINE_WIDTH-6:] if ('X' in FormatFlags) else ''
    if not 'F' in FormatFlags and 'B' in FormatFlags:
        print C['border'] +(CharFiller*LINE_WIDTH)[:LINE_WIDTH] +C['clear']
    if not 'S' in FormatFlags and 'B' in FormatFlags and LogFile_fd:
            print >> LogFile_fd, (CharFiller*LINE_WIDTH)[:LINE_WIDTH]

#===============================================================================
class IpBusRemoteProgrammer(object):
    '''
    Program the FPGA flash configuration memory through IpBus.
    '''
    #------------------- Configuration constants -------------------------------
    WAIT_READY_BUSYB = False #-- Activates a more stric hand-shaking.
                             #-- Default is "False" because of Ethernet latency
    DO_USE_INTELHEX  = False #-- IntelHex library: useful for debugging mcs files
    try:
        import uhal
        UHAL_FOUND=True
    except ImportError:
        UHAL_FOUND=False
        #raise ImportError("'uhal' libraries not found ! Did you source the correct environment ?")

    def __init__(self, board=None, CtrlNode=None, StatusNode=None, DataNode=None, VerboseLevel=0):
        self.board = board
        self.CtrlNode=CtrlNode
        self.StatusNode=StatusNode
        self.DataNode=DataNode
# NODE: TOP.ipbus_spi_Programmer.spi_prg

        self.VerboseLevel=VerboseLevel

    #-- Set the default for Constants
        #-- TODO: remap Flags in a more human readable fashion
        #--       i.e. w/ tuple:  0: ('Ready_BusyB', 'Ready'), ('Done, 'Program DONE!'), ...
        self.Flags={
           0: 'Ready_BusyB',        # The module drives this signal High when it is ready to accept the next data word.
           1: 'Done',               # The module drives outDone High when programming is done or an error is detected.
           2: 'Error',              # The module drives outError High when any error is detected. See following,
           3: 'ErrorIdcode',        # Active-High check ID error signal (AD_ERR)
           4: 'ErrorErase',         # Active-High erase error signal    (AD_ERR)
           5: 'ErrorProgram',       # Active-High program error signal  (AD_ERR)
           6: 'ErrorTimeOut',       # Active-High erase or program timeout error signal (AD_ERR)
           7: 'ErrorCrc',           # Active-High verify CRC error signal               (AD_ERR)
           8: 'Started',            # Active-High module started procedure signal               (AD_PRG)
           9: 'InitializeOK',       # Active-High module completed initialization stage signal  (AD_PRG)
          10: 'CheckIdOK',          # Active-High module successfully completed check ID stage signal (AD_PRG)
          11: 'EraseSwitchWordOK',  # Active-High module successfully completed erase switch word stage signal (AD_PRG)
          12: 'EraseOK',            # Active-High module successfully completed update image erase stage signal     (AD_PRG)
          13: 'ProgramOK',          # Active-High module successfully completed update image program stage signal   (AD_PRG)
          14: 'VerifyOK',           # Active-High module successfully completed verify stage signal                 (AD_PRG)
          15: 'ProgramSwitchWordOK' # Active-High module successfully completed switch word programming stage signal(AD_PRG)
        }
        self.ExpectedFlagsDuringWriting=[
                  'Ready_BusyB', 'Started', 'InitializeOK',
                  'CheckIdOK', 'EraseSwitchWordOK', 'EraseOK']
        self.LockProgrammerAcquired=False
        self.DisableProgrammerWhenTerminate=True #-- Default: Stop Programmer when terminate
    #---------------------------------------------------------------------------
    def DecodeStatusReg(self, DeassertedAsBlank=True):
        '''
          Decode the status register read from SpiFlashProgrammer and report the asserted flags.
          Arguments:
             DeassertedAsBlank: When True, the not asserted flags will be reported as BLANKs strings.
          Returns a list.
        '''
        AssertedBits=[]
        Offsets= self.Flags.keys()
        Offsets.sort()
        #print "---> Ordered offsets:", Offsets

        #-- read the status register from fw
        StatusReg_intType= self.board.getNode(self.StatusNode).read();
        self.board.dispatch();

        for offset in  Offsets:
            if bm.testBit(StatusReg_intType, offset):
                AssertedBits.append(self.Flags[offset])
            elif DeassertedAsBlank:
                AssertedBits.append(' '*len(self.Flags[offset]))
        return AssertedBits

    #-------------------------------------------------------------------------------
    def WaitForStatus(self, WaitedFwStatus, Node, TimeStep=1, TimeOut=0, Verbose=0):
        CurrentFwStatus=self.DecodeStatusReg(DeassertedAsBlank=False);
        PreviousFwStatus=CurrentFwStatus;
        step=0
        if (TimeOut<=0): TimeOut= 1*60*60  # if not already set, thaen set it to 1 hour
        MaxSteps=TimeOut//TimeStep+1

        while (WaitedFwStatus not in CurrentFwStatus) and (step < MaxSteps):
            CurrentFwStatus=self.DecodeStatusReg(DeassertedAsBlank=False)
            if CurrentFwStatus == PreviousFwStatus: 
                RETURN='\r'
            else:
               #RETURN='\n\r'
                RETURN='\r'
                PreviousFwStatus=CurrentFwStatus
            if Verbose:
                sys.stdout.write( ('-- [%4.1fs] Current FW status:' +
                                   str(CurrentFwStatus)+RETURN) % (step*TimeStep) )
            else:
                sys.stdout.write( ('-- [%4.1fs] ...' +RETURN) % (step*TimeStep) )
            sys.stdout.flush()
            time.sleep(TimeStep)
            step+=1

        return (step if step<MaxSteps else -1)

    #--------------------------------------------------------------------------------
    def AcquireLockProgrammer(self, SHOW_MSG=False, VERBOSE=0, ForceAcquireOwenrshipProgrammer=False):
        '''
        Check if the Programmer is NOT  already in use  by someone other and try to acquire the cooperative LOCK.
        '''
        # TODO: This procedure is not safe!
        # TODO  There is no ATOMIC operation which grantsthe LOCK to ONE ONLY client!
        # TODO: It has to be fully revisited!
        #-- check if Programmer is already lock by someone other
        if SHOW_MSG: DisplayMsg('Checking the availability of Flash Programmer....', '-TD')
        if ForceAcquireOwenrshipProgrammer:
             DisplayMsg('WARNING: Forcing acquiring of the Programmer LOCK!',
                         MsgColor=Color.PURPLE+Color.BOLD)
        SpiCtrl_intTyp=self.board.getNode(self.CtrlNode).read(); self.board.dispatch()
        if  bm.testBit(SpiCtrl_intTyp, 4) and not ForceAcquireOwenrshipProgrammer:
            DisplayMsg('WARNING: Programmer is already locked ...', MsgColor=Color.PURPLE+Color.BOLD)
            DisplayMsg("Set the specific option to force the acquiring of the ownership",
                        MsgColor=Color.DARKGREY+Color.BOLD)
            return False # exit because lock already set
        else:
            #-- Acquire lock by activating the Programmer for CheckID
            SpiCtrl_intTyp=int('1'+'1001',2);
            self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
            if (self.WaitForStatus('Done', self.StatusNode, 0.2, 10, False)<0):
                #-- TIMEOUT
                DisplayMsg('TIMEOUT when trying to acquire LOCK of Programmer', '*BT')
                # disabling Programmer
                SpiCtrl_intTyp=int('0'+'0000',2);
                self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
                return(False) # error
            else:
                #-- Check flags
                StatusReg = self.DecodeStatusReg(DeassertedAsBlank=False)
                if SHOW_MSG and VERBOSE:
                    DisplayMsg('Firmware STATUS:'+'\n   '.join(['']+StatusReg), '#X')
                if ('CheckIdOK' in StatusReg):
                    if SHOW_MSG: DisplayMsg( "... Programmer (weak) LOCK acquired!")
                    self.LockProgrammerAcquired=True
                    SpiCtrl_intTyp=int('1'+'0000',2);
                    self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
                    return(True)
                else:
                    DisplayMsg( "ERROR: when trying to acquire LOCK of Programmer", '*TB')
                    # disabling Programmer
                    SpiCtrl_intTyp=int('0'+'0000',2);
                    self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
                    return(False) # error

    #--------------------------------------------------------------------------------
    def ReleaseLockProgrammer(self, SHOW_MSG=False, VERBOSE=0, ForceDisableOfProgrammer=False):
        '''
        Release the programmer LOCK
        '''
        if self.LockProgrammerAcquired:  # self.board and
            if SHOW_MSG or VERBOSE: DisplayMsg('Releasing LOCK on Flash Programmer....', '#TD')
            SpiCtrl_intTyp=self.board.getNode(self.CtrlNode).read(); self.board.dispatch()
            if  bm.testBit(SpiCtrl_intTyp, 4):
                # disabling Programmer
                SpiCtrl_intTyp=int('0'+'0000',2);
                self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
                if SHOW_MSG and VERBOSE: DisplayMsg('.... Programmer LOCK released','#')
                return True
            else:
                # it was not enabled as expected
                DisplayMsg('WARNING: Flash Programmer was not locked but it has should be',
                            MsgColor=Color.PURPLE+Color.BOLD)

    #--------------------------------------------------------------------------------
    def VerifyCRC(self, SHOW_MSG=False, VERBOSE=0):
        '''
        Verify the CRC of the UPDATE image in the flash configuration memory
        '''
        if SHOW_MSG:
            DisplayMsg(['','VERIFYING the CRC of the UPDATE area in the Flash Memory...'], '-TD')
        ValueToReturn=None
        if  self.LockProgrammerAcquired:
            SpiCtrl_intTyp=self.board.getNode(self.CtrlNode).read(); self.board.dispatch()
            #-- prepare VERIFY CRC
            SpiCtrl_intTyp=int('1'+'0010',2); self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
            time.sleep(0.1)
            #-- enable SFP and do VERIFY CRC
            SpiCtrl_intTyp=int('1'+'1010',2); self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
            if (self.WaitForStatus('Done', self.StatusNode, 0.2, 10, False)<0):  # VERIFYING last ~5s ...
                DisplayMsg('TIMEOUT during Verifying CRC of the Flash Memory', '*BT')
                # self.terminate(1)
                ValueToReturn=1 # error
            else:
                #-- Check flags
                StatusReg = self.DecodeStatusReg(DeassertedAsBlank=False) # without blank strings
                if ('VerifyOK' in StatusReg):
                    if SHOW_MSG: DisplayMsg("... CRC is CORRECT!")
                    ValueToReturn=0
                else:
                    DisplayMsg(['ERROR: CRC is NOT CORRECT!'], '*TB')
                    ValueToReturn=1 # error
                if SHOW_MSG and VERBOSE:
                    DisplayMsg('Firmware STATUS:'+'\n   '.join(['']+StatusReg),'#X')
            #-- disabling
            SpiCtrl_intTyp=int('1'+'0000',2); self.board.getNode(self.CtrlNode).write(SpiCtrl_intTyp); self.board.dispatch()
        else:
            DisplayMsg('WARNING: have tried to VERIFY the memory without having the LOCK of Programmer',
                        MsgColor=Color.PURPLE+Color.BOLD)
        return ValueToReturn

    #---------------------------------------------------------------------------
    def EraseFlashMemory(self, VERBOSE=0 ):
        '''
        Erase the UPDATE area of the flash configuration memory
        '''
        # TODO: aggiungere gestione del LOCK Programmer
        DisplayMsg("Erasing FLASH memory... (will last ~3')", '-TD')
        # disable SFP (whenever enabled)
        SpiCtrl=int('1'+'0000',2); self.board.getNode(self.CtrlNode).write(SpiCtrl); self.board.dispatch()

        # enable SFP for uploding image
        SpiCtrl=int('1'+'1000',2); self.board.getNode(self.CtrlNode).write(SpiCtrl); self.board.dispatch()

        TimeStep=0.5
        TimeOut=200 # [s] 
        steps=self.WaitForStatus('EraseOK', self.StatusNode, TimeStep, TimeOut, Verbose=VERBOSE)
        if steps > 0: 
            DisplayMsg("................ completed in %5.1f [s]" % (steps*TimeStep))
        elif steps==0:
            DisplayMsg("WARNING: it looks like memory was already erased %5.1f [s]" % (steps*TimeStep))
        else:
            DisplayMsg("TIMEOUT after %3.2f [s] when waiting for the ERASE of FLASH memory.\n Exiting!" % TimeOut, '*TB')
            self.terminate(1)

    #---------------------------------------------------------------------------
    def ProgramFlashMemory(self, MCS_UPD_FILE, DO_WRITE_FLASH=False, PAGE_WRITE_DELAY=0.001, VERBOSE=0):
        '''
        Write the UPDATE bitstream in the UPDATE area of the FPGA configuration memory
        '''
    #-- Assumes: Programmer ENABLED + Flash ERASED
        # TODO: aggiungere gestione del LOCK Programmer
        if IpBusRemoteProgrammer.DO_USE_INTELHEX:
            try:
                from intelhex import IntelHex
                DisplayMsg('Reading FILE with IntelHex ....', '-TD')
                ih = IntelHex(MCS_UPDATE_FILE)
                DisplayMsg('........(IntelHex end)', '-B')
            except :
                pass
                DisplayMsg(['IntelHex library is NOT INSTALLED or FAILED to read the MCS file',
                                '!Disabling IntelHex!'],'*TB')
                IpBusRemoteProgrammer.DO_USE_INTELHEX=0

        DataLine_cnt=0
        AllLine_cnt=0
        line_bytesNr=16 # 32 data nibbles per linea

        AddressNextByteToRead=0

        #-- Counters
        DataLine_cnt    = 0      # Nr. of Current DATA line under processing
        Bytes_cnt       = 0      # Nr of bytes already written
        EndOfFile_cnt   = 0
        AddressChange_cnt = 0
        Unknown_cnt     = 0

        Errors_cnt      = 0
        Warnings_cnt    = 0

        if DO_WRITE_FLASH:
            DisplayMsg('Programming FLASH. (DO_WRITE_FLASH='+str(DO_WRITE_FLASH)+')' , '-TD')
        else:
            DisplayMsg([('No changes to the FLASH memory will be done!'),
                        DATE_PLACEHOLDER+'Just checking MCS update file and IpBus connection'],'-TD')
                        
        #-- Text  header -------------------------------------------------------
        if self.VerboseLevel  >=8 and IpBusRemoteProgrammer.DO_USE_INTELHEX:
            DisplayMsg('Datas extracted from MCS file','=TDC')
            DisplayMsg('[addr]'.center(8)+'  '+'from direct reading'.center(33) +
                       'from IntelHex class'.center(33), '=')  

        if VERBOSE: DisplayMsg("Opening FILE '%s'" % MCS_UPD_FILE, '#X')
        f = open(MCS_UPD_FILE, 'r')

        StartTime=time.time()
        #-- StartDTime: base ref. for T since beginning when checking the status of the SpiRemoteProgramming client
        StartDTime=dt.datetime.now() 
        #-- StartDeltaDTime: base ref for DELTA T when checking the status of the SpiRemoteProgramming client 
        StartDeltaDTime=dt.datetime.now();
        for lineX in f:
                line=lineX.strip()
                AllLine_cnt += 1

                #INTEL HEX file FMT: ($_ =~ /^\:([0-9A-Fa-f]{2})([0-9A-Fa-f]{4})([0-9A-Fa-f]{2})([0-9A-Fa-f]*)([0-9A-Fa-f]{2})$/)
                # TODO: check the First char, should be  ":"
                ByteCount_HexStr= line[1:3]
                Address_HexStr=   line[3:7]
                RecordType_HexStr=line[7:9]
                Data_HexStr=      line[9:-2]
                Checksum_HexStr=  line[-2:]

                #-- Parsing of the row 
                if RecordType_HexStr == '00':  # Data
                    DataLine_cnt += 1

                    #[TODO] Sanity check on the 'Address_HexStr'
                    if IpBusRemoteProgrammer.DO_USE_INTELHEX:
                        aLineOfBytes_HexStr=""
                        for i in range(0, int(ByteCount_HexStr, 16)):
                            aLineOfBytes_HexStr += "%02x" % (ih[AddressNextByteToRead+i])
                        #-- print out both readings decoded
                        if self.VerboseLevel  >=8:
                            DisplayMsg( ("[%05x]:  " % AddressNextByteToRead).center(8)+Data_HexStr.center(33)+aLineOfBytes_HexStr.center(33), '=')
                        #-- compare the two decodings
                        if (Data_HexStr.upper() <> aLineOfBytes_HexStr.upper()):
                            DisplayMsg(
                                ["\n",
                                "**[RecordType: '00']**  Lettura DATI discorde alla linea %06d: '%s'" % (AllLine_cnt, line),
                                "**                  **  [Linea DATA corrente: %06d, AddressNextByteToRead: 0x%06x]" %
                                                                               (DataLine_cnt, AddressNextByteToRead)], '*')
                            Errors_cnt +=1

                    #-- assume continuita' negli indirizzi dei bytes. Controllo eseguito nel seguito
                    AddressNextByteToRead+=line_bytesNr
                    if len(Data_HexStr) <> 2*int(ByteCount_HexStr, 16) or len(Data_HexStr) % 2*4:
                        #-- both the algo for mcs reading must report the same length as multiple of 32bit words
                        DisplayMsg(
                            ["\n",
                            "**[RecordType: '00']**  Linea MCS file con lunghezza DATI inconsistente o non multiplo intero",
                            "                        di 4 byte( word da 32 bit) [linea %06d]: '%s'" % (AllLine_cnt, line)], '*')
                        Errors_cnt +=1

                    if len(Data_HexStr) / (2*4) <> 4:
                        DisplayMsg(
                            ["\n",
                            "**[RecordType: '00']**WARNING**  Linea MCS file con lunghezza DATI inferiore a 4 words",
                            "                                 da 32 bit [linea %06d]: '%s'" % (AllLine_cnt, line)], '*')
                        Warnings_cnt +=1

                    #-- Send data to be written to the Programmer
                    # TODO: check consistenza: 4bytes, multiple Page, ... + CRC
                    for j in range(len(Data_HexStr) / (2*4) ):
                        HexWord32ToBeWritten = Data_HexStr[j*8:(j+1)*8]
                        #-- prepare DATA reg 32b and Write to 'inData32'
                        SpiData=int(HexWord32ToBeWritten,16);
                        if self.VerboseLevel >=9 : print '  HexWord32ToBeWritten: %8s' % HexWord32ToBeWritten, '-> %8x' % SpiData

                        if DO_WRITE_FLASH:
                            #-- Send 32 bits to the Programmer 
                            self.board.getNode(self.DataNode).write(SpiData);
                            if IpBusRemoteProgrammer.WAIT_READY_BUSYB:
                                #-- Wait for 'Ready_BusyB' HIGH
                                #--   Default is "WAIT_READY_BUSYB=False" because Ethernet latency let to skip this.
                                #--   
                                if self.WaitForStatus('Ready_BusyB', self.StatusNode, 0.001, 0.02, Verbose=self.VerboseLevel) < 0:
                                    # Timeout!
                                    DisplayMsg("TIMEOUT during writing of a word. Exiting!", '*TB')
                                    self.terminate(1)

                            #-- Write 'inDataWriteEnable=HIGH'
                            #--  tentativo di creare un single-clk pulse via IpBus....
                            self.board.getNode(self.CtrlNode).write(int('1'+'1100',2)); # write 'inDataWriteEnable=HIGH'
                            self.board.getNode(self.CtrlNode).write(int('1'+'1000',2)); # write 'inDataWriteEnable=LOW'
                            #-- send the packet
                            self.board.dispatch()
                            #-- No wait -------------------------------------------
                            #-- In principle here it should wait a total of 40 ipbus cycles (=32bit+8)
                            #-- for serialization of data (~32ns*40=1.28us) but Ethernet latency is much
                            #-- more, around O(100us)
                        Bytes_cnt +=4 # 32bit written

                    #-- wait for Page writing
                    # DO_WRITE_FLASH and
                    if (DataLine_cnt % 16)==0: # 1 page (256 bytes) was written (over ~54K) #-- ( Bytes_cnt % 256)==0
                        steps=0
                        if DO_WRITE_FLASH:
                            #-- Page writing --------------------------------------
                            #-- Internal page writing needs a delay O(5ms). Lower values O(1ms) seems to work, probably
                            #--  due to the Ethernet latency.
                            #   
                            #-- Alternatives to 'time.sleep()' to be checked
                            #--   #1#   for j in range(156500):  # wait a total of 5ms=156500 cycles
                            #--             self.board.getNode(self.CtrlNode).write(int('1'+'1000',2)); # write 'inDataWriteEnable=LOW'
                            #--   #2#   steps=self.WaitForStatus('Ready_BusyB', self.StatusNode, 0.004, 0.02, 0)
                            time.sleep(PAGE_WRITE_DELAY)

                        if (DataLine_cnt % (16*500) )==0: #-- each 500 pages 
                            StartDeltaDTime=dt.datetime.now()
                            #-- read the status from fw...
                            StatusReg = self.DecodeStatusReg(DeassertedAsBlank=False) # False=> with blank strings.
                            #-- check for errors
                            if DO_WRITE_FLASH and     \
                                ( ('Error' in StatusReg) or \
                                    not (set(self.ExpectedFlagsDuringWriting).issubset(StatusReg) )
                                ):
                                DisplayMsg(['ERROR during writing of FLASH memory. Exiting!\n',
                                                'Asserted Flags:\n  '+'\n   '.join(['']+StatusReg),
                                                '\n',
                                                'Expected Flags:\n  '+'\n   '.join(['']+self.ExpectedFlagsDuringWriting)],
                                                '*TB')
                                self.terminate(1)
                            if VERBOSE:
                                sys.stdout.write( '[%3.1d/%4d][%5d.%2d] Write PAGEs: %s. [Steps: %3d] [Status: %s]\r'  % \
                                                 ( (dt.datetime.now()-StartDeltaDTime).seconds,                          \
                                                   (dt.datetime.now()-StartDTime).seconds, DataLine_cnt / 16,            \
                                                    DataLine_cnt % 16, bool(DO_WRITE_FLASH), steps,                      \
                                                                           str(self.DecodeStatusReg(DeassertedAsBlank=False)) )  )
                            else:
                                # TODO: remove the tricky with fix # 54800!
                                sys.stdout.write( '%4d [s] - %3.1f%%   (Write PAGEs: %s)\r'  %  \
                                                ( (dt.datetime.now()-StartDTime).seconds,       \
                                                  int(1000.0*DataLine_cnt / 16.0/54800.0)/10.0,  bool( DO_WRITE_FLASH) )  )
                            sys.stdout.flush()

                elif RecordType_HexStr == '01': # End of file
                    EndOfFile_cnt += 1
                    if line <> ":00000001FF":
                        DisplayMsg(['\n',
                                        "** [RecordType: '01']**  Possibile errore 'END OF FILE' alla linea %06d: '%s'",
                                        "**                   **  [Linea DATA corrente: %06d, AddressNextByteToRead: 0x%06x]" % (DataLine_cnt, AddressNextByteToRead)],
                                        '*TB')
                        Errors_cnt +=1

                elif RecordType_HexStr == '04':
                    AddressChange_cnt += 1
                    #-- The first address discontinuity before the 1 row of data is accepted as normal behaviour.
                    #-- Instead any further discontinuity, that is when 'DataLine_cnt>0', will be reported.
                    if ( int(Data_HexStr, 16) *65536 <> int(AddressNextByteToRead) ):
                        New_AddressNextByteToRead=int(Data_HexStr, 16) *65536
                        if (DataLine_cnt>0):
                          DisplayMsg(
                            ['\n',
                                "**[RecordType: '04']**  ADDRESS discontinuity at line %06d: '%s'" % (AllLine_cnt, line),
                                "**                  **  [int(Data_HexStr, 16)*65536=0x%06x  <> int(AddressNextByteToRead)=0x%06x]" % \
                                                    (int(Data_HexStr, 16) *65536 , int(AddressNextByteToRead)),
                                "**                  **  [DATA lines processed so far: %06d, AddressNextByteToRead: 0x%06x]" %        \
                                                    (DataLine_cnt, AddressNextByteToRead),
                                "**                  **   !! AddressNextByteToRead CHANGED as int(Data_HexStr, 16) *65536=0x%06x" %    \
                                                    int(New_AddressNextByteToRead)],
                                    '*TB')
                          Warnings_cnt+=1
                        AddressNextByteToRead=New_AddressNextByteToRead
                else:
                    Unknown_cnt+=1
                    Errors_cnt +=1
                    DisplayMsg("\n**[                  ]**  ERRORE alla linea %06d: '%s' [Linea DATA corrente: %06d]" \
                                        % (AllLine_cnt, line, DataLine_cnt), '*TB')
        #[END]: for lineX in f:

        EndTime=time.time()
        DisplayMsg(['.... Bitstream sent in %d [s]'%(EndTime-StartTime)])

        if VERBOSE:
            DisplayMsg(['',
             '== Counters =====================================================',
             '==    Erros:           %06d'     % Errors_cnt,
             '==    Warnings:        %06d'     % Warnings_cnt,
             '==    -----------------------------------------\n',
             '==    All Lines:       %06d'     % AllLine_cnt,
             '==    Data Lines:      %06d'     % DataLine_cnt,
             '==    Address Changes: %06d' % AddressChange_cnt,
             '==    End Of File:     %06d'     % EndOfFile_cnt,
             '==    Unknown:         %06d'     % Unknown_cnt,
             '==',
             '==    Data lines + Address Changes + End Of File: %06d' % int(DataLine_cnt+AddressChange_cnt+EndOfFile_cnt),
             '=================================================================',''], '#X')
        return Errors_cnt
    #---------------------------------------------------------------------------
    def terminate(self, ExitCondition=None, VERBOSE=0):
        '''
        Exit from script releasing the Programmer LOCK if acquired..
        '''
        if self.DisableProgrammerWhenTerminate: self.ReleaseLockProgrammer(VERBOSE=VERBOSE)
        DisplayMsg('[END]','-TD')
        if VERBOSE: DisplayMsg("Exiting with '"+str(ExitCondition)+"'", '#B')
        else:                                   DisplayMsg("", '-B')
        if LogFile_fd: LogFile_fd.close()
        sys.exit(ExitCondition)

#===============================================================================

class FPGAConfigurationBuilder(object):
    def __init__(self, VerboseLevel=0):
        self.PATH_TO_LIB=None
        self.VerboseLevel=VerboseLevel

    def MakeMcsFromBit(self, InpFile, McsFile=None):
        '''
        Converts Xilinx FPGA bitstream given in .bit format into IntelHex .mcs file format.
        '''
        #-- TODO instead of 'subprocess.Popen' try 'os.execve(...)'
        import tempfile
        FileNameBase, FileNameExt =os.path.splitext(os.path.basename(InpFile))
        FileDirName=os.path.dirname(InpFile)
        #if FileDirName<>'': FileDirName=FileDirName+'/'
        if FileNameExt.lower()<>'.bit':
            print "Input file must have .bit extension"
            return (-1)
        if not McsFile:
            McsFile=os.path.join(FileDirName,FileNameBase+".mcs")

        if self.VerboseLevel:
            DisplayMsg(["FileNameBase:" + FileNameBase, "FileNameExt: "+FileNameExt,
                        "FileDirName: "+FileDirName,"McsFile: "+ McsFile], '#' )
        #-- write_cfgmem -format MCS -size 256 -interface SPIx1 -loadbit "up 0x0 ../images/top_Nov12.bit" top
        with tempfile.NamedTemporaryFile(delete=False) as temp:
            temp.write('write_cfgmem -force -format MCS -size 32 -interface SPIx1 -loadbit "up 0x0 '+InpFile+'"'+
                       ' -file '+ McsFile+'\n')
            temp.flush()
            try:
                process = subprocess.Popen('vivado -nolog -nojournal -mode batch -source '+\
                                            temp.name, shell=True, stdout=subprocess.PIPE)
                process.wait()
            except:
                DisplayMsg('ERROR when building MCS file from .bit input file','*TD')
                return 1
        return 0



    #---------------------------------------------------------------------------
    def BuildMcsImages(self, INPUT_FILE, DO_FORCE_GENERATION=False, Verbose=0, PRINT_HEADER=True):

    #    TODO: controllare bene il meccaniscmo di LOCK
    #          serve anche un lock non eaclusivo ?...

        '''
        Generate or recover already generated and still valid initial and update mcs image files.
        Accept Xilinx bitstream i both .mcs or .bit format.
        If .bit file is given, the Xilinx VIVADO environment must already be set.
        Return the file name of update image.
        '''
#        #-----------------------------------------------------------------------
#        def AcquireLockMCSbuild_OK(LockFile, TimeStep=1, TimeOut=0, Verbose=1):
#            if (TimeOut<=0):  TimeOut= 60*25  # if not already set then set it to 25'  
#            MaxSteps=TimeOut//TimeStep+1
#            step=0
#            RETURN='\r'
#            if os.path.exists(LockFile):
#                DisplayMsg(
#                    ['WARNING: Have found the LOCK FILE: '+LockFile,
#                     '         This means that some other process is building the MCS files or',
#                     '         the LOCK FILE was not removed in the previous run.',
#                     '         YOU CAN REMOVE IT MANUALLY IF YOU ARE SURE THERE ARE NO OTHERS.'],
#                            MsgColor=Color.OKBLUE+Color.BOLD )
#            LockAcquired=False
#            while (not LockAcquired) and (step < MaxSteps):
#                try:
#                    os.mknod(LockFile)
#                    LockAcquired=True
#                except:
#                    if Verbose:
#                        sys.stdout.write( ('-- Waiting for LOCK releasing. Still %4.0f [s] before time out...' +RETURN) % (TimeOut-step*TimeStep) )
#                        sys.stdout.flush()
#                    time.sleep(TimeStep)
#                    step+=1
#            return (step if step<MaxSteps else -1)
#        #-----------------------------------------------------------------------
#        def ReleaseLockMCSbuild_OK(LockFile=''):
#            if os.path.exists(LockFile):
#                try:
#                    os.remove(LockFile)
#                except:
#                    DisplayMsg("Error during lock removing: '"+LockFile+"'",
#                               '*', 'T')

        #-----------------------------------------------------------------------

        import fcntl
        def AcquireLockMCSbuild(LockFile, TimeStep=1, TimeOut=0, Verbose=0):
            ''' Acquire cooperative lock file access '''
            # https://docs.python.org/2/library/fcntl.html#fcntl.lockf
            # http://stackoverflow.com/questions/4843359/python-lock-a-file
            #   locked_file_descriptor = open(LockFile, 'w+')
            #   fcntl.lockf(locked_file_descriptor, fcntl.LOCK_EX)
            #   return locked_file_descriptor


            #TODO: it seems that the lock is 'manage' by the sheel: it fails (get the lock when should not)
            #       
            if (TimeOut<=0):  TimeOut= 60*25  # if not already set then set it to 25'  
            MaxSteps=TimeOut//TimeStep+1
            step=0
            RETURN='\r'
            LockAcquired=False
            locked_file_descriptor = open(LockFile, 'w+')

            try:
                fcntl.lockf(locked_file_descriptor, fcntl.LOCK_EX|fcntl.LOCK_NB)
                LockAcquired=True
            except:
                DisplayMsg(
                    ['WARNING: Building of MCSs file is LOCKED by some other process',
                     '         (LOCK File is: '+ LockFile+')'],
                            MsgColor=Color.OKBLUE+Color.BOLD )

            while (not LockAcquired) and (step < MaxSteps):
                try:
                    fcntl.lockf(locked_file_descriptor, fcntl.LOCK_EX|fcntl.LOCK_NB)
                    LockAcquired=True
                except:
                    if Verbose:
                        sys.stdout.write( 
                            ('-- Waiting for LOCK releasing. Still %4.0f [s] before time out...' +RETURN)\
                             % (TimeOut-step*TimeStep) )
                        sys.stdout.flush()
                    time.sleep(TimeStep)
                    step+=1

            return (step if step<MaxSteps else -1), locked_file_descriptor
        #-----------------------------------------------------------------------
        def ReleaseLockMCSbuild(LockFile='', locked_file_descriptor=None):
            ''' Release cooperative lock file access '''
            locked_file_descriptor.close()
            if os.path.exists(LockFile):
                try:
                    os.remove(LockFile)
                except:
                    DisplayMsg("Error during lock removing: '"+LockFile+"'", '*TD')
        #-----------------------------------------------------------------------

        if PRINT_HEADER:
             DisplayMsg(['Building MCS images',
                         '    Input file: '+INPUT_FILE  ],'-TD')

        FileNameBase, FileNameExt =os.path.splitext(os.path.basename(INPUT_FILE))
        FileDirName=os.path.dirname(INPUT_FILE)
        if FileDirName<>'': FileDirName=FileDirName+'/'

        #-- Any new generated file will be build into the current path
        if FileNameExt.lower()=='.bit':
            MCS_FILE = FileNameBase+'.mcs'
        else:
            MCS_FILE = FileDirName +FileNameBase+'.mcs'
        MCS_UPD_FILE=    FileNameBase+'_update.mcs'
        MCS_INI_FILE=    FileNameBase+'_initial.mcs'
        LOCK_FILE_for_WR=FileNameBase+'_LockFile.txt'      # TODO: needed for lock when making mcs files

#       MCS_FILE=        FileDirName+FileNameBase+".mcs"
#       MCS_UPD_FILE=    FileDirName+FileNameBase+"_update.mcs"
#       MCS_INI_FILE=    FileDirName+FileNameBase+"_initial.mcs"
#       LOCK_FILE_for_WR=FileDirName+FileNameBase+'_LockFile.txt'

        #-----------------------------------------------------------------------
        #-- LOCK in the case of 'others' are making the mcs files -- TIMEOUT=25'
        #-- TODO: still to be checked carefully!
        #-----------------------------------------------------------------------
        TS=1 # [s]
        WaitedSteps, locked_file_descriptor =AcquireLockMCSbuild(LOCK_FILE_for_WR, TimeStep=TS, TimeOut=25*60, Verbose=0)
        if WaitedSteps==0:
            # there was no lock already set. Now I got the Lock
            if Verbose: DisplayMsg('No LOCK for MCS file has been found  --> LOCK  got!', '#X')
        elif WaitedSteps<0:
            # timeout occured
            DisplayMsg('TIMEOUT when waiting for MCS LOCK.', '*BT')
            DisplayMsg('Check that no other is building the same MCS files and eventually',
                       'remove the file: '+LOCK_FILE_for_WR, '*BX')
            return '',''  # error
        else:
            # I got the lock after waiting
            # There was a lock file but now it has been removed -> the 'other process finished...
            DisplayMsg('Lock removed after having wait for '+str(WaitedSteps*TS)+' [s]. Continuing...')
        #-----------------------------------------------------------------------
        #-- At that point the LOCK has been got
        #-----------------------------------------------------------------------

        if self.VerboseLevel:
            DisplayMsg(["MCS_FILE:        "+MCS_FILE,
                        "MCS_UPD_FILE:    "+MCS_UPD_FILE,
                        "MCS_INI_FILE:    "+MCS_INI_FILE,
                        "LOCK_FILE_for_WR:"+LOCK_FILE_for_WR], '#' )

        if not DO_FORCE_GENERATION and (os.path.exists(MCS_INI_FILE) and os.path.exists(MCS_UPD_FILE)) \
           and ( os.path.getmtime(INPUT_FILE) < os.path.getmtime(MCS_INI_FILE) ) \
           and ( os.path.getmtime(INPUT_FILE) < os.path.getmtime(MCS_UPD_FILE) ):
            #-- assumes both MCS_INI_FILE and MCS_UPD_FILE are still valid then it skips generation
            if PRINT_HEADER:
                DisplayMsg(['Found a valid GOLDEN image file for local initial programming:',
                            "    '%s'" % MCS_INI_FILE,
                            'and a valid UPDATE image file for remote re-programming:',
                            "    '%s'\n" % MCS_UPD_FILE])
                DisplayMsg( 'Set the specific option if you want to force the building of images','C',
                             MsgColor=Color.DARKGREY+Color.BOLD)

        else:
        #-- No valid MCS files was found, than make them!
            if FileNameExt.lower()=='.bit':
            #-- Generate MCS_FILE from .bit
                try:
                    #----------- Require XILINX VIVADO environment   -----------
                    #-- strict follow the source order:
                    # source ~/bin/SetCactusEnv.csh
                    # source /soft/Xilinx/Vivado/2014.4/settings64.csh

                    DisplayMsg(">>  Making MCS file from '%s'  <<" % INPUT_FILE)
                    if os.path.exists(MCS_FILE):  os.remove(MCS_FILE)
                    self.MakeMcsFromBit(INPUT_FILE, MCS_FILE)
                    if not os.path.exists(MCS_FILE):
                        raise # ValueError("Generating of mcs image FAILED!")
                    DisplayMsg("... done")
                except:
                    DisplayMsg(['ERROR: generation of MCS image from .bit file FAILED !',
                                'Is the VIVADO environment set ?'],
                                '*TB')
                   #ReleaseLockMCSbuild(LOCK_FILE_for_WR)
                    ReleaseLockMCSbuild(LOCK_FILE_for_WR, locked_file_descriptor)

                    return '',''  # error
             
            #-- remove old INITIAL and UPDATE files
            if (os.path.exists(MCS_INI_FILE)):
                DisplayMsg("Removing old '%s' image file" % MCS_INI_FILE)
                os.remove(MCS_INI_FILE)
            if (os.path.exists(MCS_UPD_FILE)):
                DisplayMsg("Removing old '%s' image file" % MCS_UPD_FILE)
                os.remove(MCS_UPD_FILE)
            DisplayMsg("Making GOLDEN and UPDATE images from '%s'" % MCS_FILE)
            #-- check if the perl module "Digest::CRC" is installed
            try:
                #OK# retcode=subprocess.call(["perl", "-MDigest::CRC", "-e 1"])
                child = subprocess.Popen('perl -MDigest::CRC -e 1', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                #OK#streamdata = child.communicate()[0]
                output, error = child.communicate()
                retcode = child.returncode
                if self.VerboseLevel:
                    DisplayMsg(["'perl -MDigest::CRC -e 1' has returned '"+str(retcode)+"'"],'#')
                if retcode<>0: raise
                else: LIBRARY_CRC='DIGEST_CRC'
            except:
                DisplayMsg(["WARNING: Looks like the Perl 'Digest::CRC' module is not installed!!",
                                "         The production of GOLDEN and UPDATE images will last longer"],
                                '*TBX')
                LIBRARY_CRC='Internal'
            #-- Generate images
            try:
        #---- TODO: Remove the ugly tricky in calling the perl script.
        #           HERE I SHOULD USE 1 ONLY SCRIPT FOR BOTH LIBRARIES!!
        #       PerlCmd=["perl ", os.path.join(self.PATH_TO_LIB, "MakeSpiFlashProgrammerMcsFiles_both.pl"), " -",LIBRARY_CRC, " ",MCS_FILE]
        #       DisplayMsg(''.join(PerlCmd), '-' 'TB')
        #       process = subprocess.Popen(PerlCmd, stdout=subprocess.PIPE)
        #       process.wait()

                if (LIBRARY_CRC == 'DIGEST_CRC'):
                    DisplayMsg("*** Using the faster DIGEST CRC library. It will last ~5' ***")
                    cmd=' '.join(["perl", os.path.join(self.PATH_TO_LIB,
                                          "MakeSpiFlashProgrammerMcsFiles_Digest-CRC.pl"), MCS_FILE])
                else:
                    DisplayMsg("*** Using the slower internal CRC library. It will last up to ~20' ***")
                    cmd=' '.join(["perl", os.path.join(self.PATH_TO_LIB,
                                          "MakeSpiFlashProgrammerMcsFiles_int-CRC.pl"),MCS_FILE])


                if Verbose: DisplayMsg('EXECUTING: '+cmd, '#X')
                child = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                output, error = child.communicate()
                retcode = child.returncode
                #process.wait()

                #-- check the files have been generated!
                if not (os.path.exists(MCS_INI_FILE) and os.path.exists(MCS_UPD_FILE)):
                    raise # ValueError("Generating of GOLDEN and UPDATE mcs images FAILED!")
                DisplayMsg("... done")
            except:
                DisplayMsg('ERROR: Generation of MCS images files FAILED !!', '*TB')
                ReleaseLockMCSbuild(LOCK_FILE_for_WR, locked_file_descriptor)
                return '',''  # error
            DisplayMsg(['Created the GOLDEN image file for local initial programming:',
                            '    '+MCS_INI_FILE,
                            'and the UPDATE image for remote re-programming',
                            '    '+MCS_UPD_FILE])

        ReleaseLockMCSbuild(LOCK_FILE_for_WR, locked_file_descriptor)
        return MCS_UPD_FILE, MCS_INI_FILE
