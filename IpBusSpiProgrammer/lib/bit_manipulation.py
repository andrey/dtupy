#-------------------------------------------------------------------------------
#-- Collection of methods for bit manipulation, testing and visualization
#-- found somewhere or written.
#--
#-- Fabio Montecassiano - INFN Pd (Fabio_DOT_Montecassiano_AT_pd.infn.it)
#-- September 2015
#-------------------------------------------------------------------------------
VERSION = '1.0'

def bin(x):
     """
     [for older version of python less then 2.5]
     bin(number) -> string
     Stringifies an int or long in base 2.
     """
     if x < 0:
         return '-' + bin(-x)
     out = []
     if x == 0:
         out.append('0')
     while x > 0:
         out.append('01'[x & 1])
         x >>= 1
         pass
     try:
         return '0b' + ''.join(reversed(out))
     except NameError, ne2:
         out.reverse()
     return '0b' + ''.join(out)

#-------------------------------------------------------------------------------
# --  a further definition for bin()
def bin_v1(a):
    '''
    [for older version of python less then 2.5]
    bin(number) -> string
    '''
    s=''
    t={'0':'000','1':'001','2':'010','3':'011',
        '4':'100','5':'101','6':'110','7':'111'}
    for c in oct(a)[1:]:
            s+=t[c]
    return s
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-- def bitLen(int_type):
#-------------------------------------------------------------------------------
def bitLen(int_type):
    '''
    return the actual bit length of a Python integer, that is, the number of the
    highest non-zero bit plus 1
    '''
    length = 0
    while (int_type):
        int_type >>= 1
        length += 1
    return(length)
#------- Example ---------------------------------------------------------------
#-- for i in range(17):
#--     print(bitLen(i))
#-- 
#-- # results: 0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-- def bitLenCount(int_type):
#-------------------------------------------------------------------------------
def bitLenCount(int_type):
    '''
    Return the number of set (1) bits
    '''
    length = 0
    count = 0
    while (int_type):
        count += (int_type & 1)
        length += 1
        int_type >>= 1
    return(length, count)
#------- Example ---------------------------------------------------------------
#-- >>> bitLenCount(65535)
#-- (16, 16)
#-- >>> bitLenCount(65534)
#-- (16, 15)
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-- def bitCount(int_type):
#-------------------------------------------------------------------------------
def bitCount(int_type):
    '''
    Return the count of bits set
    '''
    count = 0
    while(int_type):
        int_type &= int_type - 1
        count += 1
    return(count)
#------- Example ---------------------------------------------------------------
#-- >>> bitCount(128)
#-- 1
#-- >>> bitCount(127)
#-- 7
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-- def parityOf(int_type):
#-------------------------------------------------------------------------------
def parityOf(int_type):
    '''
    Calculates the parity of an integer, returning 0 if there are an even number
    of set bits, and -1 if there are an odd number. 
    '''
    parity = 0
    while (int_type):
        parity = ~parity
        int_type = int_type & (int_type - 1)
    return(parity)
#------- Example ---------------------------------------------------------------
#-- >>> parityOf(8)
#-- -1
#-- >>> parityOf(12)
#-- 0
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#--  lowestSet(int_type):
#-------------------------------------------------------------------------------
def lowestSet(int_type):
    '''
    To determine the bit number of the lowest bit set in an integer, in
    twos-complement notation i & -i zeroes all but the lowest set bit. 
    '''
    low = (int_type & -int_type)
    lowBit = -1
    while (low):
        low >>= 1
        lowBit += 1
    return(lowBit)
#------- Example ---------------------------------------------------------------
#-- >>> lowestSet(int('00111000',2))
#-- 3
#-- >>> lowestSet(int('00111010',2))
#-- 1
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#--  def testBit(int_type, offset):
#-------------------------------------------------------------------------------
def testBit(int_type, offset):
    '''
    Return a non  zero result, 2**offset, if the bit at 'offset' is 1
    '''
    mask= 1 << offset
    return (int_type & mask)
#------- Example ---------------------------------------------------------------
#-- 
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#--  def setBit(int_type, offset):
#-------------------------------------------------------------------------------
def setBit(int_type, offset):
    '''
    setBit() returns an integer with the bit at 'offset' set to 1.
    '''
    mask = 1 << offset
    return(int_type | mask)
#------- Example ---------------------------------------------------------------
#-- >>> setBit(16,4)
#-- 16
#-- >>> setBit(16,0)
#-- 17
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#--  def clearBit(int_type, offset):
#-------------------------------------------------------------------------------
def clearBit(int_type, offset):
    '''
    clearBit() returns an integer with the bit at 'offset' cleared.
    '''
    mask = ~(1 << offset)
    return(int_type & mask)
#------- Example ---------------------------------------------------------------
#-- >>> clearBit(int('1010',2), 3)
#-- 2
#-- >>> clearBit(16,3)
#-- 16
#-- >>> clearBit(17,4)
#-- 1
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#--  def toggleBit(int_type, offset):
#-------------------------------------------------------------------------------
def toggleBit(int_type, offset):
    '''
    toggleBit() returns an integer with the bit at 'offset' inverted, 0 -> 1 and 1 -> 0.
    '''
    mask = 1 << offset
    return(int_type ^ mask)
#------- Example ---------------------------------------------------------------
#-- >>> toggleBit(16,3)
#-- 24
#-- >>> toggleBit(int('1010',2), 3)
#-- 2
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# ** bitfield manipulation **
#    from http://code.activestate.com/recipes/113799/
#-- Example
#-- >>> k=bit_manipulation_test.bf()
#-- >>> k[3:7]=5
#-- >>> print int(k)
#-- 40
#-- >>> print k[3]
#-- 1
#-- >>> k[7]=1
#-- >>> print int(k)
#-- 168
#-- >>> print k[4:8]
#-- 10

class bf(object):
    def __init__(self,value=0):
        self._d = value

    def __getitem__(self, index):
        return (self._d >> index) & 1 

    def __setitem__(self,index,value):
        value    = (value&1L)<<index
        mask     = (1L)<<index
        self._d  = (self._d & ~mask) | value

    def __getslice__(self, start, end):
        mask = 2L**(end - start) -1
        return (self._d >> start) & mask

    def __setslice__(self, start, end, value):
        mask = 2L**(end - start) -1
        value = (value & mask) << start
        mask = mask << start
        self._d = (self._d & ~mask) | value
        return (self._d >> start) & mask

    def __int__(self):
        return self._d


