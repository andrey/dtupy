function pathadd() {
  # TODO add check for empty path
  # and what happens if $1 == $2
  # Copy into temp variables
  PATH_NAME=$1
  PATH_VAL=${!1}
  if [[ ":$PATH_VAL:" != *":$2:"* ]]; then
    PATH_VAL="$2${PATH_VAL:+":$PATH_VAL"}"
    echo "- $1 += $2"

    # use eval to reset the target
    eval "$PATH_NAME=$PATH_VAL"
  fi

}


AMC13WF=~/amc13/amc13tool2_wf
echo "- AMC13WF = $AMC13WF"
CACTUS_ROOT=/opt/cactus
echo "- CACTUS_ROOT = $CACTUS_ROOT"

DTUPY_ROOT=$( readlink -f $(dirname $BASH_SOURCE)/ )
echo "- DTUPY_ROOT = $DTUPY_ROOT"
DTUPY_ETC=$( readlink -f ${DTUPY_ROOT}/etc)
echo "- DTUPY_ETC = $DTUPY_ETC"

# add default cactus library path
pathadd LD_LIBRARY_PATH "${CACTUS_ROOT}/lib"

# add to path
pathadd PATH "${DTUPY_ROOT}/scripts"

# add python path
pathadd PYTHONPATH "${DTUPY_ROOT}/pkg"



export AMC13WF CACTUS_ROOT DTUPY_ETC DTUPY_ROOT LD_LIBRARY_PATH PYTHONPATH 
