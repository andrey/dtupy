### Board definitions
uros      address-table       file:///opt/cactus/etc/uros/addrtab/uros_infra.xml
amc13     address-table_T1    file:///opt/cactus/etc/amc13/AMC13XG_T1.xml
amc13     address-table_T2    file:///opt/cactus/etc/amc13/AMC13XG_T2.xml

pos_01  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-01:50001
pos_02  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-02:50001
pos_03  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-03:50001
pos_04  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-04:50001
pos_05  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-05:50001
pos_06  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-06:50001
pos_07  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-07:50001
pos_08  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-08:50001
pos_09  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-09:50001
pos_10  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-10:50001
pos_11  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-11:50001
pos_12  uri        chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-12:50001
pos_13  uri_T1     chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-13-t1:50001
pos_13  uri_T2     chtcp-2.0://bridge-s1d15-22:10203?target=amc-s1d15-22-13-t2:50001
                   
neg_01  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-01:50001
neg_02  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-02:50001
neg_03  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-03:50001
neg_04  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-04:50001
neg_05  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-05:50001
neg_06  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-06:50001
neg_07  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-07:50001
neg_08  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-08:50001
neg_09  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-09:50001
neg_10  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-10:50001
neg_11  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-11:50001
neg_12  uri        chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-12:50001
neg_13  uri_T1     chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-13-t1:50001
neg_13  uri_T2     chtcp-2.0://bridge-s1d14-22:10203?target=amc-s1d14-22-13-t2:50001

zero_01  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-01:50001
zero_02  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-02:50001
zero_03  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-03:50001
zero_04  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-04:50001
zero_05  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-05:50001
zero_06  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-06:50001
zero_07  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-07:50001
zero_08  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-08:50001
zero_09  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-09:50001
zero_10  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-10:50001
zero_11  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-11:50001
zero_12  uri       chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-12:50001
zero_13  uri_T1    chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-13-t1:50001
zero_13  uri_T2    chtcp-2.0://bridge-s1d14-38:10203?target=amc-s1d14-38-13-t2:50001

slice_01  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-01:50001
slice_02  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-02:50001
slice_03  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-03:50001
slice_04  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-04:50001
slice_05  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-05:50001
slice_06  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-06:50001
slice_07  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-07:50001
slice_08  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-08:50001
slice_09  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-09:50001
slice_10  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-10:50001
slice_11  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-11:50001
slice_12  uri      chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-12:50001
slice_13  uri_T1   chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-13-t1:50001
slice_13  uri_T2   chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-13-t2:50001

pos_13    fed      1371
neg_13    fed      1369
zero_13   fed      1370
slice_13  fed      1368


## Mapping
pos_01    slotMap  5
pos_02    slotMap  11
pos_03    slotMap  1
pos_04    slotMap  7
pos_05    slotMap  2
pos_06    slotMap  8
pos_09    slotMap  9
pos_10    slotMap  3
pos_11    slotMap  10
pos_12    slotMap  4

neg_01    slotMap  11
neg_02    slotMap  5
neg_03    slotMap  7
neg_04    slotMap  1
neg_05    slotMap  8
neg_06    slotMap  2
neg_09    slotMap  3
neg_10    slotMap  9
neg_11    slotMap  4
neg_12    slotMap  10

zero_01   slotMap  5
zero_03   slotMap  1
zero_05   slotMap  2
zero_10   slotMap  3
zero_12   slotMap  4

slice_01  slotMap 0
slice_02  slotMap 0
slice_03  slotMap 0
slice_04  slotMap 0
slice_05  slotMap 0
slice_06  slotMap 0
slice_07  slotMap 0
slice_08  slotMap 0
slice_09  slotMap 0
slice_10  slotMap 0
slice_11  slotMap 0
slice_12  slotMap 0
