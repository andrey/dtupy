### Parameters

amc13     resyncCmd                   0x14 # was 4
amc13     ocrCmd                      0x20 # was 8
amc13     localTTC                    0 
amc13     bcnOffset                   0  # was 3541 
amc13     slinkMask                   1
      
uros      fw_version                  32 
uros      tm7BcnIncr                  0x18 # was 1
uros      tts_threshold_0             400 
uros      tts_threshold_1             200 
uros      tts_threshold_2             800 
uros      tts_threshold_3             600 
uros      max_inevent_idles           40 
uros      max_words_per_ch            200 
uros      max_words_per_ev            4000 
uros      max_hdr_delay               8192 
uros      rob_evn_inc                 1 
uros      enforce_bxn                 0 
uros      dteb_status_mask            4095 
uros      dummydata_mul               0 
uros      robff_af_thres              512 
uros      big_evt_size_ctr_threshold  200
uros      userData                    0
uros      boardID                     0x1234


uros      pd_inertia                  5 
uros      pd_decay                    64 
uros      idelay                      0 

### Per-channel mtp-grouped disables
uros      disable_mtp                 0 # default value for all disable_mtp_X regs for X in 1..6 is not disabled ( = enabled )

neg_01   disable_mtp_1                0
neg_01   disable_mtp_2                0xFFF
neg_01   disable_mtp_3                0xFFF
neg_01   disable_mtp_4                0xFFF
neg_01   disable_mtp_5                0xFFF
neg_01   disable_mtp_6                0xFFF

neg_02   disable_mtp_1                0
neg_02   disable_mtp_2                0xFFF
neg_02   disable_mtp_3                0xFFF
neg_02   disable_mtp_4                0xFFF
neg_02   disable_mtp_5                0xFFF
neg_02   disable_mtp_6                0xFFF

zero_01  disable_mtp_1                0
zero_01  disable_mtp_2                0xFFF
zero_01  disable_mtp_3                0xFFF
zero_01  disable_mtp_4                0xFFF
zero_01  disable_mtp_5                0xFFF
zero_01  disable_mtp_6                0xFFF

pos_01   disable_mtp_1                0
pos_01   disable_mtp_2                0xFFF
pos_01   disable_mtp_3                0xFFF
pos_01   disable_mtp_4                0xFFF
pos_01   disable_mtp_5                0xFFF
pos_01   disable_mtp_6                0xFFF

pos_02   disable_mtp_1                0
pos_02   disable_mtp_2                0xFFF
pos_02   disable_mtp_3                0xFFF
pos_02   disable_mtp_4                0xFFF
pos_02   disable_mtp_5                0xFFF
pos_02   disable_mtp_6                0xFFF


