## Each line contains the name of the crate, and then a list of 12 tokens
## that correspond to the 12 uROS slots.
## Each token can be either:
##   - the number of the slot (which means the slot is included in the partition)
##   - "x" (which means the slot is not included in the partition)
##
## The AMC13 board is automatically included in the partition without needing to 
## specify it because otherwise the crate cannot be operated



slice x x x 4 5 6 x x x x x x
