var maps = {};
var errtxts = {};
var warntxts = {};

var init_data = {};
var data = {};
var partial_lists = {'general-errwarns':{},'inputs-errs':{},'inputs-warns':{} };
var inputmetric2unid = {'hits2daq_ctr':'', 'hits2daq_ctr_recent':'', 'evs_lost_ratio':'', 'evs_lost_ratio_recent':'' };
var leds = {0:'silver',1:'limegreen',2:'orange',3:'red'}



function content_update(result) {
  data = result;
  //t0 = (new Date()).getTime();
  // Run state and number
  $("#run_number").html( result['run_number'] );
  $("#run_type").html( result['runType'] );
  $("#beam_mode").html( result['beamMode'] );
  $("#part_key").html( result['partKey'] );
  $("#config_key").html( result['configKey'] );
  $("#run_state").html( result['run_state'] );
  $("#num_events").html( result['num_events'] );
  $("#L1Arate").html( result['L1Arate'] );
  $("#ev_size").html( result['avgEvtSize'] );
  $("#ev_sizes").html( result['avgEvtSizeFED'] );
  $("#ev_size_recent").html( result['avgEvtSizeRecent'] );
  $("#ev_sizes_recent").html( result['avgEvtSizeRecentFED'] );
  $("#backpressures").html( result['backPressureFED'] );
  
  if('warn' in result) $('#warnBox').html( result['warn'] ).show();
  else $('#warnBox').hide();
  
  //t1 = (new Date()).getTime();
  
  // Fill diagram and robleds on hover summary
  for (var unid in maps) // modify the summary for the unid's of which we have a map
    data['summary'][unid] = maps[unid].uros + ' ' + maps[unid].str + 
      (data['summary'][unid].indexOf('\n')>=0?data['summary'][unid].slice(data['summary'][unid].indexOf('\n')):"");
  //t2 = (new Date()).getTime();

  // Creation of the partial error and warning lists
  for (var listname in partial_lists) {
    var listdata = result[listname];
    for (var m=0; m<listdata.length; m++){
      if (listdata[m].id.indexOf(".Inputs.")>=0) {
        var parentChannelUnid = listdata[m].unid.slice(0,listdata[m].unid.lastIndexOf('-'));
        if (parentChannelUnid in maps) listdata[m].map = maps[parentChannelUnid].str;
      }
      if (listdata[m].unid in errtxts) listdata[m].errtxt = errtxts[listdata[m].unid];
      if (listdata[m].unid in warntxts) listdata[m].warntxt = warntxts[listdata[m].unid];
    }
    partial_lists[listname] = {"id":listname, 'doMap':(listname.indexOf('inputs')>=0), "subsAreFinal": true, "subs": listdata };
    
  }
  
  //t3 = (new Date()).getTime();
  
  // Update diagram colors
  updateDiagrams();
  //t4 = (new Date()).getTime();
  
  updateRegList();
  
  //t5 = (new Date()).getTime();
  //console.log('times spent',t1-t0, t2-t1, t3-t2, t4-t3, t5-t4, 'total time', t5 - t0);
}

function content_init(result) {
  init_data = result;
  
  // Draw crates diagram
  var templ_crateleds = $('#templ_crateleds').html();
  $('#crateleds').html( Mustache.render(templ_crateleds, result ) );
  
  // Fill and enable first dropdown selector
  for (var i=0; i<result.subs.length; i++)
    $('#sel0').append( $('<option></option>').attr("value",i).text(result.subs[i].id) );
  for (var name in partial_lists)
    $('#sel0').append( $('<option></option>').attr("value",name).text(name) );
  $('#sel0').attr('disabled', false);
  
  // Fill the maps dict
  maps = {};
  errtxts = {};
  warntxts = {};
  var physmap = {}
  for (wh = -2; wh<3; wh++){
    physmap[wh]= {};
    for (sec = 1; sec<15; sec++){
      physmap[wh][sec]={};
      for (st = 1; st<5; st++) {
        physmap[wh][sec][st]={};
        for (rob = 0; rob<(st==3?7:6); rob++)
          physmap[wh][sec][st][rob]="";
      }
    }
  }
  for (var c = 0; c<result.subs.length; c++) {
    var crate = result.subs[c];
    for (var b = 0; b<crate.subs.length; b++) {
      var board = crate.subs[b];
      for (var bl = 0; bl<board.subs.length; bl++) {
        var block = board.subs[bl];
        if (block.subsAreFinal){
          for (var m = 0; m<block.subs.length; m++) {
            var metric = block.subs[m];
            if ('errtxt' in metric) errtxts[metric.unid] = metric.errtxt;
            if ('warntxt' in metric) warntxts[metric.unid] = metric.warntxt;
          }
        }
        else{
          for (var ch=0; ch<block.subs.length; ch++) {
            var channel = block.subs[ch];
            if ('map' in channel){
              maps[channel.unid] = channel.map
              physmap[channel.map.phys[0]][channel.map.phys[1]][channel.map.phys[2]][channel.map.phys[3]] = channel.unid;
            }
            for (var m = 0; m<channel.subs.length; m++) {
              var metric = channel.subs[m];
              if ('errtxt' in metric) errtxts[metric.unid] = metric.errtxt;
              if ('warntxt' in metric) warntxts[metric.unid] = metric.warntxt;
            }
          }
        }
      }
    }
  }
  var physmapdata = {'rows':[], 'wh':[{'n':'-2'},{'n':'-1'},{'n':'0'},{'n':'+1'},{'n':'+2'}], 'sec':[]};
  for (sec = 1; sec<15; sec++) physmapdata['sec'].push({'n':(sec<10?"0":"")+sec.toString()/*.toString(16)*/, 'br':sec==14});
  for (st = 1; st<5; st++){
    var numRobs = (st==3?7:6);
    for (rob = 0; rob<numRobs; rob++){
      physmapdata['rows'].push({'cells':[]});
      if (rob==numRobs-1) physmapdata['rows'][physmapdata['rows'].length-1]['drawLine']=true;
      for (wh = -2; wh<3; wh++) {
        for (sec = 1; sec<15; sec++) {
          unid = physmap[wh][sec][st][rob];
          physmapdata['rows'][physmapdata['rows'].length-1]['cells'].push({'unid':unid,'br':sec==14});
        }
      }
    }
  }
  
  // Draw inputs diagram
  var templ_robleds = $('#templ_robleds').html();
  $('#robleds').html( Mustache.render(templ_robleds, physmapdata ) );
  
  // Extract the unid information needed for later finding metrics needed for robleds diagram color maps
  inputmetric2unid = {'hits2daq_ctr':'', 'hits2daq_ctr_recent':'', 'evs_lost_ratio':'', 'evs_lost_ratio_recent':'' };
  label_toploop:
  for (var c = 0; c<result.subs.length; c++) {
    var crate = result.subs[c];
    for (var b = 0; b<crate.subs.length; b++) {
      var board = crate.subs[b];
      for (var bl = 0; bl<board.subs.length; bl++) {
        var block = board.subs[bl];
        if (block.id == 'Inputs'){
          for (var ch=0; ch<block.subs.length; ch++) {
            var channel = block.subs[ch];
            for (var m = 0; m<channel.subs.length; m++) {
              var metric = channel.subs[m];
              if (metric.id in inputmetric2unid)
                inputmetric2unid[metric.id] = metric.unid.slice(metric.unid.lastIndexOf('-'));
            }
            break label_toploop;
          }
        }
      }
    }
  }
}

function showReg(unid) {
  if (unid.indexOf('-')<0) return; //not needed anymore?
  var tokens = unid.split('-').slice(1);
  for (var i = 0; i < tokens.length; i++){
    $('#sel'+i).val(tokens[i]);
    newSelection(i);
  }
}

function newSelection(sel){
  var selBlock = init_data;
  var preamble = '';
  for (var i=0; i<4; i++) {
    if (i<=sel)
      selBlock = selBlock.subs[$('#sel'+i).val()];
    else
      $('#sel'+i).attr('disabled', true).find('option:gt(0)').remove();
    if (i>0 && i<sel)
      preamble += selBlock.id + '.';
  }
  
  if (selBlock && selBlock.subsAreGroup){
    $('#sel'+(sel+1)).attr('disabled', false);
    for (var i=0; i<selBlock.subs.length; i++)
      if (selBlock.subs[i].AMCexists != false) // Can also be none
        $('#sel'+(sel+1)).append( $('<option></option>').attr("value",i).text(selBlock.subs[i].id) );
  }
  if (selBlock && selBlock.subsAreFinal){
    var templ_reglist = $('#templ_reglist').html();
    $('#reglist').html( Mustache.render(templ_reglist, selBlock ) );
    $('#reglist_title').html( preamble + $('#reglist_title').html() );
  } // Can also be selBlock = none if any of the partial_lists is selected

  updateRegList();
}

function updateColorWithEW() {
  unid = $(this).attr('id').slice($(this).attr('id').indexOf('-'));
  $( this ).css('background-color', leds[ data['colors'][unid] ] );
}

var takeAsGood = Math.log(1e-4);
var colorMapConfig;
var maxHits = {};
function updateColorMap() {
  var unid = $(this).attr('id').slice($(this).attr('id').indexOf('-'));
  var rgbin = {};
  var rgb = {};
  for (c in colorMapConfig)
    rgbin[c] = colorMapConfig[c]['f']( unid,  data['values'][ unid + inputmetric2unid[colorMapConfig[c]['m']] ], colorMapConfig[c]['m']  );
  for (c in colorMapConfig)
    rgb[c] = Math.min(1, 1+ 2*rgbin[c] - rgbin.r - rgbin.g - rgbin.b);
  for (c in colorMapConfig) if(rgb[c]<0) {
    var desv = rgb[c]/2;
    rgb[c] -= 3*desv;
    for (c in colorMapConfig) rgb[c] += desv;
  }
  var colorString = 'rgb('  + Math.floor(256*rgb.r) + ','
                            + Math.floor(256*rgb.g) + ','
                            + Math.floor(256*rgb.b) + ')';
  $( this ).css('background-color', colorString );
}

function updateDiagrams(){
  $('#crateleds').find('[id^="diag"]').each( updateColorWithEW );
  
  var robleds_option = $('#sel_rob').val();
  colorMapConfig = {'r':{'f':function(){return 0;}, 'm':''}, 'g':{'f':function(){return 0;}, 'm':''}, 'b':{'f':function(){return 0;}, 'm':''} };
  
  var rlopt_hits_metrics = { 'HITS': ['hits2daq_ctr'], 'HITSR': ['hits2daq_ctr_recent'], 'HITSM': ['hits2daq_ctr', 'hits2daq_ctr_recent'] };

  if (robleds_option == 'EW')
    $('#robleds').find('[id^="diag"]').each( updateColorWithEW );
  else{
    if (robleds_option == 'ELR'){
      colorMapConfig.b.m = 'evs_lost_ratio';
      colorMapConfig.b.f = function(unid,value,m){ return (1 - Math.min(1,Math.log(Math.min(1,Math.max(1e-10,value)))/takeAsGood)) }
    }
    else if (robleds_option == 'ELRR'){
      colorMapConfig.r.m = 'evs_lost_ratio_recent';
      colorMapConfig.r.f = function(unid,value,m){ return (1 - Math.min(1,Math.log(Math.min(1,Math.max(1e-10,value)))/takeAsGood)) }
    }
    else if (robleds_option == 'ELRM'){
      colorMapConfig.b.m = 'evs_lost_ratio';
      colorMapConfig.b.f = function(unid,value,m){ return (1 - Math.min(1,Math.log(Math.min(1,Math.max(1e-10,value)))/takeAsGood)) }
      colorMapConfig.r.m = 'evs_lost_ratio_recent';
      colorMapConfig.r.f = function(unid,value,m){ return (1 - Math.min(1,Math.log(Math.min(1,Math.max(1e-10,value)))/takeAsGood)) }
    }
    else if (robleds_option in rlopt_hits_metrics){
      //var maxHits = {};

      for (var i=0; i<rlopt_hits_metrics[robleds_option].length; i++) {
        hitmet = rlopt_hits_metrics[robleds_option][i];
        var hitsList = [];
        for (unid in data['values'])
          if (unid.split('-').length==6 && parseInt(unid.split('-')[2])<13 && unid.split('-')[5]==inputmetric2unid[hitmet].slice(1)) {
            var chunid = unid.slice(0,unid.lastIndexOf('-'));
            var hitsperwire = parseInt(data['values'][unid]) / maps[chunid]['numwires'];
            hitsList.push(hitsperwire) ;
            var hitstr = '\n hits'+hitmet.slice(12)+'/evt/wire = '+(hitsperwire/Math.max(1,data.num_events)).toExponential(2);
            data['summary'][chunid] = (data['summary'][chunid]+'\n').replace('\n', hitstr+'\n').slice(0,-1);
          }
        hitsList.sort(function(a, b){return a - b});
        hitsList = hitsList.slice(-20,-15); 
        maxHits[hitmet] = hitsList.reduce(function(a, b) { return a + b; }) / hitsList.length ;
        if(maxHits[hitmet]==0) maxHits[hitmet] = 1;

        colorMapConfig[hitmet.indexOf('recent')>=0?'r':'b'].m = hitmet;
        colorMapConfig[hitmet.indexOf('recent')>=0?'r':'b'].f = function(unid,value,m){ return 1 + Math.max(-1, Math.log(Math.min(1,.2*value/maps[unid]['numwires']/maxHits[m]))/7) }
      }
    }
    
    $('#robleds').find('[id^="diag"]').each( updateColorMap );
  }

  // Write to DOM the summaries of both diagrams
  for (var unid in data['summary']) // assign all the summaries to titles
    $('#diag'+unid).prop( 'title', data['summary'][unid]);
  
}

function updateRegList(){
  var topSel = $('#sel0 option:selected').text();
  if( topSel in partial_lists ){
    var templ_reglist = $('#templ_reglist').html();
    var newcode = Mustache.render(templ_reglist, partial_lists[topSel] );
    newcode = newcode.replace(new RegExp('beginwhite', 'g'),'<span style="color:white">');
    newcode = newcode.replace(new RegExp('endwhite', 'g'),'</span>');
    $('#reglist').html( newcode );
  }
  $('#reglist [id^="led"]').each( function() {
    unid = $(this).attr('id').slice($(this).attr('id').indexOf('-'));
    $( this ).css('color', leds[ data['colors'][unid] ] );
  });
  $('#reglist [id^="value"]').each( function() {
    unid = $(this).attr('id').slice($(this).attr('id').indexOf('-'));
    $( this ).html( data['values'][unid] );
  });
}
