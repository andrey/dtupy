#!/usr/bin/python 


import cgi
import cgitb; cgitb.enable()
import cx_Oracle
import zlib, json
import datetime
import traceback
import sys
import socket

username = 'cms_dt_elec_conf'
service = 'cms_omds_adg' if 'cern.ch' in socket.gethostname() else 'cms_omds_lb'
password = 'cmsonr2008'

try:
  form = cgi.FieldStorage()
  #import pprint,os
  #sys.stderr.write(pprint.pformat(os.environ))
  getLive = form.getvalue("getLive", None)
  dayRunList = form.getvalue("dayRunList", None)
  getRun = form.getvalue("getRun", None)
  getSnapshot = form.getvalue("getSnapshot", None)
  
  con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
  con.autocommit = 1
  cur = con.cursor()
  
  if getLive != None:
    # get the live data
    cur.execute( 'select DATA_CONTENT from W_MON_LIVE where DATA_NAME = :dataName', {'dataName':getLive} )
    row = cur.fetchone()
    body =  zlib.decompress(row[0].read())

  elif dayRunList != None:
    minDate = datetime.datetime.strptime(dayRunList, '%Y-%m-%d')
    maxDate = minDate + datetime.timedelta(days=1)
    
    cur.execute( 
      'select distinct RUN from W_MON_DATA where TIME_STAMP >= :minDate and TIME_STAMP < :maxDate order by RUN asc' , 
      {'minDate':minDate, 'maxDate':maxDate })
    rows = cur.fetchall()
    runList = [row[0] for row in rows]
    
    body = json.dumps({'runList':runList}, separators=(',', ': '), allow_nan=False)
  
  elif getRun != None:
    run = int(getRun)
    
    # get the init data for that run
    cur.execute( 'select CONTENT_FILES, INIT_DATA from W_MON_RUNS where RUN=:runNum', {'runNum':run} )
    content_files, init_data = map( lambda x: json.loads(zlib.decompress(x.read())), cur.fetchone() )
    
    # get the latest snapshot for that run
    cur.execute( 'select TIME_STAMP, WEB_DATA from W_MON_DATA where RUN = :runNum order by TIME_STAMP desc', {'runNum':run} )
    row = cur.fetchmany(3)[1]
    ts = str(row[0])
    web_data = json.loads(zlib.decompress(row[1].read()))
    
    # get the the complete timestamp information
    cur.execute( 'select TIME_STAMP from W_MON_DATA where RUN = :runNum order by TIME_STAMP asc', {'runNum':run} )
    timestamps = [str(row[0]) for row in cur.fetchall()]
    
    result = {'content_files':content_files, 'init_data':init_data,
              'web_data':web_data, 'ts':ts,
              'timestamps': timestamps}
    
    body =  json.dumps(result, separators=(',', ': '), allow_nan=False)
  
  elif getSnapshot != None:
    # get the latest snapshot for that run
    ts = datetime.datetime.strptime(getSnapshot, '%Y-%m-%d %H:%M:%S')
    cur.execute( 'select WEB_DATA from W_MON_DATA where TIME_STAMP = :ts order by TIME_STAMP desc', {'ts':ts} )
    row = cur.fetchone()
    web_data = json.loads(zlib.decompress(row[0].read()))
    
    body =  json.dumps({'web_data':web_data}, separators=(',', ': '), allow_nan=False)
    
    
    
    
  else:
    body = json.dumps("jarl", separators=(',', ': '), allow_nan=False)
  
  cur.close()
  con.close()

except:
  body = json.dumps({'alarm': traceback.format_exc()}, separators=(',', ': '), allow_nan=False)

  
  #
  ## get the next to last run number
  #cur.execute( 'select distinct RUN from W_MON_DATA order by RUN desc' )
  #run = cur.fetchmany(2)[1][0] 
  #
  #
  ## get the init data for that run
  #cur.execute( 'select CONTENT_FILES, INIT_DATA from W_MON_RUNS where RUN=:runNum', {'runNum':run} )
  #content_files, init_data = map( lambda x: json.loads(zlib.decompress(x.read())), cur.fetchone() )
  #
  #
  ## get the latest snapshot for that run
  #cur.execute( 'select TIME_STAMP, WEB_DATA from W_MON_DATA where RUN = :runNum order by TIME_STAMP desc', {'runNum':run} )
  #row = cur.fetchmany(3)[2]
  #ts = row[1]
  #web_data = json.loads(zlib.decompress(row[1].read()))
  #
  #body =  json.dumps({'content_files':content_files, 'init_data':init_data, 'web_data':web_data}, separators=(',', ': '), allow_nan=False)



print "Status: 200 OK"
print "Content-Type: application/json"
print "Length:", len(body)
print ""
print body


#sys.stdout.write('Status: 200 OK\n')
#sys.stdout.write('Content-type: application/octet-stream\n')
#sys.stdout.write('Content-Encoding: gzip\n\n')
#sys.stdout.flush()
#sys.stdout.write(zlib.compress(body))
#