#!/usr/bin/env python

import cx_Oracle
import zlib
import json
import math
import datetime

username = 'cms_dt_elec_conf'
service = 'cms_omds_lb'
password = 'cmsonr2008'

con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
con.autocommit = 1
cur = con.cursor()
cur.execute( 'select DATA_CONTENT,TIME_STAMP from W_MON_LIVE where DATA_NAME = :dataName', {'dataName':'dataDcs.json'} )
row = cur.fetchone()
dataDcs =  json.loads(zlib.decompress(row[0].read()))
td = datetime.datetime.now() - row[1]
age = (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6 # this is instead of td.total_seconds() because the host pc runs python 2.6
cur.close()
con.close()


def genColor(rgbin):
  rgb = {}
  for c in rgbin:
    rgb[c] = min(1, 1+ 2*rgbin[c] - sum([rgbin[col] for col in rgbin]));
  for c in rgbin:
    if rgb[c]<0:
      desv = rgb[c]/2
      rgb[c] -= 3*desv
      for col in rgbin:
        rgb[col] += desv
  return 'rgb(%i,%i,%i)'%(256*rgb['r'],256*rgb['g'],256*rgb['b'])

unid2map = dataDcs['unid2map']
unid2metrics = dataDcs['unid2metrics']
  
hitsList = sorted([1. * unid2metrics[unid]['hits2daq_ctr'] / unid2map[unid]['numwires'] for unid in unid2map])[-20:-15]
maxHits = sum( hitsList ) / len(hitsList)
maxHits = maxHits if maxHits else 1

hitsListR = sorted([1. * unid2metrics[unid]['hits2daq_ctr_recent'] / unid2map[unid]['numwires'] for unid in unid2map])[-20:-15]
maxHitsR = sum( hitsListR ) / len(hitsListR)
maxHitsR = maxHitsR if maxHitsR else 1


evs_lost = {}
hits_deliv = {}

for unid in unid2map:
  [wh,sec,mb,rob] = unid2map[unid]['phys']
  evslostrec = unid2metrics[unid]['evs_lost_ratio_recent']
  r = 1 - min(1,math.log(min(1,max(1e-10,evslostrec)))/math.log(1e-4))
  evslosttot = unid2metrics[unid]['evs_lost_ratio']
  b = 1 - min(1,math.log(min(1,max(1e-10,evslosttot)))/math.log(1e-4))
  evs_lost[(wh, sec, mb, rob)] = genColor({'r':r,'g':0,'b':b})
  
  hitsdelivrec = unid2metrics[unid]['hits2daq_ctr_recent']
  r = 1 + max(-1, math.log(max(1e-10,min(1,.2*hitsdelivrec/unid2map[unid]['numwires']/maxHitsR)))/7)
  hitsdeliv = unid2metrics[unid]['hits2daq_ctr']
  b = 1 + max(-1, math.log(max(1e-10,min(1,.2*hitsdeliv/unid2map[unid]['numwires']/maxHits)))/7)
  hits_deliv[(wh, sec, mb, rob)] = genColor({'r':r,'g':0,'b':b})

body = '''
<style>
.table_robleds {
  border-collapse: collapse;
  width:100%;
  outline-style:solid;
  outline-width:1px;
}
table.table_robleds th {
  border: 1px solid;
  text-align:center;
}

table.table_robleds td {
  border: 1px solid;
  border-bottom-color:white;
}

table.table_robleds td.border_right, table.table_robleds th.border_right {
  border-right:2px solid black;
}

tr.border_bottom td, tr.border_bottom th {
  border-bottom-color:black;
}
</style>
'''


def genTableWith(colorMap):
  table =  '<table class="table_robleds">\n  <tr class="border_bottom">\n'
  for wh in ['-2','-1','0','+1','+2']:
    table += '    <th colspan="14" class="border_right">YB%s</th>\n'%wh
  table += '  </tr>\n  <tr style="font-size:8px" class="border_bottom">'
  for wh in ['-2','-1','0','+1','+2']:
    table += '\n    '
    for sec in range(1,15):
      table += '<td%s>%02i</td>'%(' class="border_right"' if sec==14 else '',sec)
  table += '\n  </tr>\n'
  for mb in range(1,5):
    for rob in range(6 if mb!=3 else 7):
      table += '  <tr%s>\n'%(' class="border_bottom"' if (rob==5 and mb!=3) or (rob==6 and mb==3) else '')
      for wh in ['-2','-1','0','+1','+2']:
        for sec in range(1,15):
          table += '    <td style="background-color: %s;" height="5"%s></td>\n'%(
                          colorMap[(int(wh),sec,mb,rob)] if (int(wh),sec,mb,rob) in colorMap else 'silver',' class="border_right"' if sec==14 else '' )
      table += '  </tr>\n'
  table += '</table>\n'
  return table

if age > 20:
  body += '<h3 style="color:red;">The available information seems to be too old: %.1f seconds</h3>'%age
  
body += "Blue = full run; Red = recent; pink/magenta = blue+red"
body += "<h3>Events Lost</h3>"
body += "Intense = 100% events lost; white = <0.01% events lost"
body += genTableWith(evs_lost)
body += "<br/><h3>Hits Delivered</h3>"
body += genTableWith(hits_deliv)
body += '<br/><br/><br/>'

  
  
print "Status: 200 OK"
print "Content-Type: application/json"
print "Length:", len(body)
print ""
print body

