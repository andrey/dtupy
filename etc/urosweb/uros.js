const updateInterval = 5;

var dataAge = {}
var intervals = {'getLiveData':0,'freshness':0}

var live_timeOfCreation = 0;

var runHistTimeStamps = [];
var runHistNum = 0;

var state = "";

//////////////////////////////////////////////
///  Live Content
//////////////////////////////////////////////

function loadLiveContent( statusMessage = 'Initializing content...' ) {
  $('#statusMessage').html( statusMessage ).removeClass('btn-danger btn-warning btn-success').addClass('btn-warning');
  $("#tsForm").hide();
  dataAge = {'received':0, 'newReceived':0, 'timestamp':0}
  clearInterval(intervals['getLiveData']);
  clearInterval(intervals['freshness']);
  
  state = "live-ing";

  var init_data = {};
  $.when(
    $.ajax("content.htm" , { cache:false, success: function(result){
      if(state=='live-ing') $( "#content" ).html( result );
    }}),

    $.ajax("content.css" , { cache:false, success: function(result){
      if(state=='live-ing') $( "head" ).append("<style>" + result + "</style>");
    }}),

    $.getScript("content.js"), // cache false is automatic here
    
    $.ajax("dbretrieve.py?getLive=init_data.json" , { cache:false, dataType: 'json', success: function(result){
      if(state=='live-ing') init_data = result;
    }})
  ).then(function(){
    if(state=='live-ing') {
      state = '';
      if ('alarm' in init_data) alarm( result.alarm );
      else{
        noalarm();
        content_init(init_data);
        live_timeOfCreation = init_data.timeOfCreation;
        $('#statusMessage').html( 'Loading data...' );
        intervals['getLiveData'] = setInterval(getLiveData, updateInterval*1000);
        intervals['freshness'] = setInterval(freshness, 1000);
        getLiveData();
      }
    }
  });
}

function getLiveData(){
  state = 'live-updating';
  $.ajax("dbretrieve.py?getLive=data.json" , { cache:false, dataType: 'json', success: function(result){
    if(state=='live-updating'){
      state = '';

      // Control of the age of the data received
      dataAge['received'] = Math.floor(new Date().getTime()/1000);
      if ('timestamp' in result) {
        if ( dataAge['timestamp'] != result['timestamp'] ) dataAge['newReceived'] = dataAge['received'];
        dataAge['timestamp'] = result['timestamp'];
      }

      if ('alarm' in result) alarm( result.alarm );
      else {
        noalarm();
        if( live_timeOfCreation == result.timeOfCreation ) content_update(result);
        else loadLiveContent('Reloading init data (monitoring process has reloaded the system)...');
      }
    }
  }});
}

function freshness() {
  if(dataAge['received']){
    var timenow = Math.floor(new Date().getTime()/1000)
    var dataage = timenow - dataAge['received'];
    var changeddataage = timenow - dataAge['newReceived'];
    var timestampage = timenow - dataAge['timestamp'];
    var newBtnClass = 'btn-danger'
    if (dataage > 20)                 $("#statusMessage").html( "<b>Lost communication to web server</b>" );
    else if (changeddataage > 20)     $("#statusMessage").html( "<b>Server sending static data (> 20 s)</b>" );
    else if (timestampage > 120)      $("#statusMessage").html( "<b>Server sending old data (> 2 minutes)</b>" );
    else {
      $("#statusMessage").html( 'Live <span style="font-size:10px">(data ' + changeddataage.toString() + 's old)</span>' );
      newBtnClass = 'btn-success';
    }
    $("#statusMessage").removeClass('btn-danger btn-warning btn-success').addClass( newBtnClass );
  }
}


//////////////////////////////////////////////
///  Historic Content
//////////////////////////////////////////////

/// Loads run selector
function runSelector(){
  clearInterval(intervals['getLiveData']);
  clearInterval(intervals['freshness']);
  $("#statusMessage").html( 'History' ).removeClass('btn-danger btn-warning btn-success').addClass('btn-warning' );

  $("#tsForm").hide();
    
  $('#content').html( $('#templ_runSelect').html() ); 
  var now = new Date();
  $("#dateInput").val( now.getFullYear() + "-" + ("0"+(now.getMonth()+1)).slice(-2)+ "-" + ("0"+now.getDate()).slice(-2) );
  
  updateRunList();
}

/// dateInput (part of run selector) blurs on enter to prevent reload
$(document).on("keypress", "#dateInput", function(event) {
  if (event.keyCode == 13) $(this).blur();
  return event.keyCode != 13;
});

/// runInput (part of run selector) gets a new run on enter
$(document).on("keypress", "#runInput", function(event) {
  if (event.keyCode == 13) getRun('runInput');
  return event.keyCode != 13;
});


/// loads the list of runs for a give day
function updateRunList(){
  $("#runSelectBox").html( '' ).attr( 'class','loader' );

  var date2get = $("#dateInput").val()
  state = date2get;
  
  $.ajax("dbretrieve.py?dayRunList="+date2get , { cache:false, dataType: 'json', success: function(result){
    if(state==date2get){
      state = '';
      if ('alarm' in result) alarm( result.alarm );
      else{
        noalarm();
        if ('runList' in result){
          if (!result.runList.length ){
            $("#runSelectBox").html( 'No runs for selected date'  ).attr( 'class','alert alert-warning' );
          }else{
            var runs = []
            for (i=0; i<result.runList.length; i++){
              runs.push({'number':result.runList[i]});
            }
            $("#runSelectBox").html( Mustache.render($('#templ_runList').html(), {'runs':runs} ) ).attr( 'class','' );
          }
        }
      }
    }
  }});
}

/// loads a run (usually the server will give the final snapshot)
function getRun(what){
  var number = (what=="runInput") ? $("#runInput").val() : what;
  
  state=number;
  
  $.ajax("dbretrieve.py?getRun="+number , { cache:false, dataType: 'json', success: function(result){
    if(state==number){
      state='';
      if ('alarm' in result) alarm( result.alarm );
      else{
        noalarm();
        runHistNum = what;
        $( "#content" ).html( result['content_files']['content.htm'] );
        $( "head" ).append("<style>" + result['content_files']['content.css'] + "</style>");
        $.globalEval(result['content_files']['content.js'])
        content_init(result['init_data']);
        content_update(result['web_data']);
        $('#tsRange').attr('max', result['timestamps'].length-1).val(result['timestamps'].length-2);
        runHistTimeStamps = result['timestamps'];
        $("#tsForm").show();
        $('#statusMessage').html( 'History: run '+runHistNum+', '+result['ts'] );
      }
    }
  }});
}

/// loads a different snapshot for the currently-loaded run
function getSnapshot(dir){
  if (dir!=0) $('#tsRange').val(Number($('#tsRange').val())+dir);
  
  var newts = runHistTimeStamps[$('#tsRange').val()]
  state = newts;
  $.ajax("dbretrieve.py?getSnapshot="+newts , { cache:false, dataType: 'json', success: function(result){
    if(state==newts){
      state = '';
      if ('alarm' in result) alarm( result.alarm );
      else{
        noalarm();
        content_update(result['web_data']);
        $('#statusMessage').html( 'History: run '+runHistNum+', '+newts );
      }
    }
  }});
}

//////////////////////////////////////////////
///  Alarm box
//////////////////////////////////////////////

function alarm(txt){
  console.log("Printing received alarm in the console:");
  console.log(txt);
  $('#alarmBtn').show();
  $('#alarmBox').modal('show');
  $('#alarmContent').html(txt);
}

function noalarm(){
  $('#alarmBtn').hide();
  $('#alarmBox').modal('hide');
}

