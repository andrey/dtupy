################################################################################
## The AB7  driver wrapper plus some helpers and the transitions
################################################################################

import time, os
import uros, tm7common
from dtupy.bcolors import bcolors
from dtupy.mocomanager import obdt_connectors


class AB7(uros.UROSController):
  isDAQsource = True
  fw_type_identifier = 0x04
  
  read                      = tm7common.read
  write                     = tm7common.write
  getNode                   = tm7common.getNode
  reg_num2name              = tm7common.reg_num2name
  gth_debuginfo             = tm7common.gth_debuginfo
  amc13link_reset_duration  = tm7common.amc13link_reset_duration
  ttc_err_ctr_clear         = tm7common.ttc_err_ctr_clear
  ttc_ctr_clear             = tm7common.ttc_ctr_clear
  measfreq                  = tm7common.measfreq
  ttc_stats                 = tm7common.ttc_stats
  verifyBoardTypeVersion    = tm7common.verifyBoardTypeVersion
  
  def __init__(self, device):
    super(AB7, self).__init__(device)
    self.storedReads = {}

  def dispatch(self):
    self.hw().dispatch()
    
  def debugModeSetup(self, mode = 'inject', action = 'enable', value = 1 ):
    allowed = [ ('inject','enable'), ('inject', 'select'), ('snapshot', 'enable')]
    if (mode, action) not in allowed:
      raise Exception('mode/action parameters invalid for debugModeSetup function: %s/%s'%(mode, action))
    self.write( 'hit_preprocessor.csr.conf.%sMode_%s'%(mode, action), 1 if value else 0 )
  
  def debugMode_inject_mem(self, hits, bx_time_convert_ena = 1):
    hitsmem_data = []
    for hit in hits:
      if bx_time_convert_ena:
        if 'bx' not in hit or 'fine' not in hit:
          hit['bx'] = hit['ns'] /25
          hit['fine'] = int ( (hit['ns'] % 25) *30. / 25. )  + 1 # int() truncates towards 0
        hitsmem_data += [ (1<<31) + (hit['ch']<<17) + (hit['bx']<<5) + (hit['fine']<<0) ]
      else:
        if 'ns' not in hit:
          hit['ns'] = int(  25*(hit['bx'] + (hit['fine']-1)/30.)  ) # int() truncates towards 0
        hitsmem_data += [ (1<<31) + (hit['ch']<<17) + (hit['ns']<<0) ]
      hitsmem_data += [( (hit['orbit']<<12) + (hit['arrivalBX']<<0)   )]
    if len(hits) > self.getNode('hit_preprocessor.hits_mem').getSize():
      raise Exception( 'Too many hits to write in the injection memory' )
    if len(hits) < self.getNode('hit_preprocessor.hits_mem').getSize():
      hitsmem_data += [0, 0xFFF] # this hit has arrivalBX = 0xFFF, which will never, happen, thus stopping the injection.
    self.getNode( 'hit_preprocessor.hits_mem' ).writeBlock( hitsmem_data )
    self.dispatch()

  def debugMode_readout_mem(self, prims = True):
    (mem, words_per_data, decode_f) = ('prims',8, decode_tp_slv) if prims else ('hits',2,decode_hits_slv)
    ctr = self.read('hit_preprocessor.csr.stat.%smem_ctr'%mem)
    ctr = min( ctr, self.getNode( 'hit_preprocessor.%s_mem'%mem ).getSize() / words_per_data - 2 )
    raw_readout = self.getNode( 'hit_preprocessor.%s_mem'%mem ).readBlock( ctr*words_per_data )
    self.dispatch()
    raw_readout = map(int, raw_readout )
    data_slv = []
    for i in range(len(raw_readout)):
      if i%words_per_data == 0: data_slv += [ 0 ]
      data_slv[-1] += raw_readout[i]<<(32*(i%words_per_data))
    data_decoded = map( decode_f, data_slv )
    return data_decoded
    
  def remap_config(self, params_override = {}):
    params = self.getParams(params_override)
    
    if 'remap_lut' in params:
      with open(os.path.expandvars('${DTUPY_ETC}/luts/'+params['remap_lut']), 'r') as f:
        values = [int(line.strip(),16) for line in f.readlines() if line.strip()]
    else:
      values = [0xFFFFFFFF]*256
      for conn in 'abcdefghijkmno':
        conn_mask = params['remap_%s_enable'%conn] if 'remap_%s_enable'%conn in params else 0
        conn_offset = params['remap_%s_offset'%conn] if 'remap_%s_offset'%conn in params else 0
        for i in range(16):
          if (conn_mask>>i)&1:
            values[ obdt_connectors[conn][i] ] = conn_offset + i
    self.hw().getNode('hit_preprocessor.channel_remapping_lut').writeBlock(values)
    self.write( 'hit_preprocessor.csr.conf.remap_enable', params['remap_enable'] )

  
  ################################################################################
  ## board-level functions for cell operation
  ################################################################################
  
  def configureCmd(board, params_override = {}):
    """Runs the configure transition"""
    params = board.getParams(params_override)
    
    FWvers = board.verifyBoardTypeVersion( params )
    
    board.reset( 'external' )
    
    board.write( 'readout.conf.userData',                params['userData'] )
    board.write( 'readout.conf.bcn_incr',                params['tm7BcnIncr'])
    board.write( 'readout.conf.tts_thresholds.rdy2ofw',  params['tts_threshold_0'] )
    board.write( 'readout.conf.tts_thresholds.ofw2rdy',  params['tts_threshold_1'] )
    board.write( 'readout.conf.tts_thresholds.ofw2bsy',  params['tts_threshold_2'] )
    board.write( 'readout.conf.tts_thresholds.bsy2ofw',  params['tts_threshold_3'] )
    
    for prefix in ['hits','tps']:
      for suffix in ['disable', 'latency', 'window', 'startpointer_cooldown', 'max_disorder', 'evb_timeout', 'evb_keepfuture']:
        board.write( 'readout.conf.%s.%s'%(prefix,suffix),    params[ prefix+'_'+suffix ] )
    board.write( 'readout.conf.debug_payload', params['debug_enable'] )

    board.write( 'technical_trigger.conf.bx_time_tolerance',   params['bx_time_tolerance']  )
    board.write( 'technical_trigger.conf.chisqr_threshold',    params['chisqr_threshold']  )
    board.write( 'technical_trigger.conf.bx_time_convert_ena', params['bx_time_convert_ena'] )
    if 'readout_bx_delay' in params:
      board.write( 'readout.conf.ttgen.bx_delay',                  params['readout_bx_delay'] )
    
    board.write( 'gbt.conf.resetgbtfpga',1  )
    board.write( 'gbt.conf.resetgbtfpga',0  )

    time.sleep(1)


    board.write( 'hit_preprocessor.csr.conf.gbt_unlock_cooldown',     params['gbt_unlock_cooldown'] )
    board.write( 'hit_preprocessor.csr.conf.wrong_pattern_cooldown',  params['wrong_pattern_cooldown'] )
    
    gbt_unlock_cool = board.read( 'hit_preprocessor.csr.stat.gbt_unlock_cool' )
    wrong_pattern_cool = board.read( 'hit_preprocessor.csr.stat.wrong_pattern_cool' )
    
    #print "gbt_unlock_cool   "  , gbt_unlock_cool
    #print "wrong_pattern_cool"  , wrong_pattern_cool
    
    if not gbt_unlock_cool: print bcolors.red + 'board id=%s has its gbt link unlocked'%( board.hw().id() ) + bcolors.reset
    if not wrong_pattern_cool: print bcolors.red + 'board id=%s gbt data is incorrectly formatted'%( board.hw().id() ) + bcolors.reset
    
    if gbt_unlock_cool and wrong_pattern_cool and 'sx5_retiming_meas_time' in params and 'sx5_retiming_offset_goal' in params and params['sx5_retiming_meas_time']!=0:
      t0 = time.time()
      print 'Measuring the bx difference between local bunch counter and sx5 hit data for %i seconds'%params['sx5_retiming_meas_time']
      #bxdiffs = set([]) # for set-based calibration
      bxdiffs = []  # for list-based calibration
      prevBXdiff = board.read('hit_preprocessor.csr.stat.lastBXdiff')
      while time.time() < t0 + params['sx5_retiming_meas_time']:
        lastBXdiff = board.read('hit_preprocessor.csr.stat.lastBXdiff')
        if lastBXdiff != prevBXdiff:
          prevBXdiff = lastBXdiff
          #bxdiffs.update([board.read('hit_preprocessor.csr.stat.lastBXdiff')&0xFFF]) # for set-based calibration
          bxdiffs += [lastBXdiff&0xFFF] # for list-based calibration
      if bxdiffs:
        bxoffset = ( sum(bxdiffs) / len(bxdiffs) - params['sx5_retiming_offset_goal'] ) % 3564
        bxdiffs = { key:bxdiffs.count(key) for key in range(3564) if key in bxdiffs} # for list-based calibration
        bxdiffs = ', '.join(['%i:%i'%(key,bxdiffs[key]) for key in sorted(bxdiffs.keys(), key=lambda k:bxdiffs[k], reverse=True) ])
        print 'Measured differences: ', bxdiffs
        print 'Writing a retiming BXoffset of', bxoffset
        board.write( 'hit_preprocessor.csr.conf.BXoffset', bxoffset )
      else:
        print bcolors.red + 'Not a single hit was received... not possible to calibrate bxoffset' + bcolors.reset

    board.remap_config()    

    board.write( 'hit_preprocessor.csr.conf.age_filter_enable', params['age_filter_enable'] )
    board.write( 'hit_preprocessor.csr.conf.age_filter', params['age_filter'] )
    board.write( 'hit_preprocessor.csr.conf.injectMode_select', params['injectMode_select'] )
    
  def startCmd(board):
    """Runs the start transition"""
    
    #board.write( 'readout.conf.ctr_rst', 1)
    #board.write( 'readout.conf.rob.ctr_ena', 1)
    #board.write( 'readout.conf.ctr_rst', 0)
    
  def stopCmd(board):
    """Runs the stop transition"""
    
    #board.write( 'readout.conf.rob.ctr_ena', 0)
  

  
  
  
  
  
  
################################################################################
## Functions to decode ab7's injectMode and snapshotMode data
################################################################################
from bitfield import bf

two_comp = lambda val,size:val - ((2*val)&(1<<size))

def dtTime_from_slv(slv):
  return {  'valid' : slv[0],
            'tdc_time' : slv[1:18] }

def dtHit_from_slv(slv):
  return {  'super_layer_id' : slv[0:2],
            'layer_id' : slv[2:4],
            'channel_id' : slv[4:11],
            'dt_time' : dtTime_from_slv(slv[11:29])  }

def hitsSegment_from_slv(slv):
  return [ dtHit_from_slv( slv[29*i:29*(i+1)] ) for i in range(4) ] #HitsSegment_t'length=4, DTHIT_SIZE=29

def dtSegMainParams_from_slv(slv):
  return {  'bx_time' : slv[0:17],
            'quality' : slv[17:20],
            'laterality_combination' : bin(16+slv[20:24])[3:], # 16 + forces bin to fill with 0s and then the 1 corresponding to 16 is deleted.
            'hits' : hitsSegment_from_slv(slv[24:140])  }   #DTSEG_MAINPARAMS_SIZE=140

def dtSegment_from_slv(slv):
  return {  'bx_id' : slv[0:12],
            'chi_square' : two_comp(slv[12:36],24),
            'horizontal_position' : two_comp(slv[36:52],16),
            'phi_tangent_x4096' : two_comp(slv[52:67],15),
            'main_params' : dtSegMainParams_from_slv(slv[67:207])  } #DTSEG_MAINPARAMS_SIZE=207

def decode_tp_slv(slv): # information added by hit_preprocessor
  slv = bf(slv)
  tp = dtSegment_from_slv(slv[24:231])
  tp['arrivalBX'] = slv[0:12]
  tp['arrivalOC'] = slv[12:24]
  return tp


def decode_hit_slv(slv):
  return {  'tdc'     : slv[0:5],
            'bx'      : slv[5:17],
            'ch'      : slv[17:26],
            #'SL'      : slv[26:28],
            #'St'      : slv[28:30],
            #'valid'   : slv[31],
            }
  
def decode_hits_slv(slv):
  slv = bf(slv)
  hit = decode_hit_slv(slv[0:32])
  hit['arrivalBX'] = slv[32:44]
  hit['arrivalOC'] = slv[44:56]
  return hit
