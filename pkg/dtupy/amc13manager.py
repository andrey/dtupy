################################################################################
## The AMC13 driver wrapper plus some helpers and the transitions
################################################################################

import uhalaux
import amc13
import os
import unpacker

    
class AMC13(amc13.AMC13):
  def __init__(self, connection_file):
    super(AMC13, self).__init__(connection_file)
    self.storedReads = {}
    
  def __init__(self, uri1, addtab1, uri2, addtab2):
    super(AMC13, self).__init__(uri1, addtab1, uri2, addtab2)
    self.storedReads = {}
    
  def __init__(self, t1, t2):
    super(AMC13, self).__init__(t1, t2)
    self.storedReads = {}

  def freezeStats(self, value):
    pass
  
  def activeInputs(self):
    return self.s.hw[self.context['crate']]['slots']

  def statsFromChan(self, channel):
    self.readingFromChan = channel
  
  def dispatch(self):
    self.getT1().dispatch()
    self.getT2().dispatch()

  def getNode(self, reg, T2=False):
    if len(reg)>2 and reg[:3] in ['T1.','T2.']:
      T2 = (reg[:3] == 'T2.')
      reg = reg[3:]
    if '%' in reg: reg = reg%self.readingFromChan
    return (self.getT2() if T2 else self.getT1()).getNode(reg)
    
  def read(self, reg, mask = 0xFFFFFFFF, nodispatch = False, fresh = True, storeSuffix = '', T2=False):
    """IPBus read operation in register "reg", which may be a textual register or an address.
    Optional argument mask is only used with a numerical address
    If register is textual and it starts with "T1." or "T2." it will be applied to the corresponding Tongue,
    otherwise the tongue is controlled by the value of input parameter T2"""
    if isinstance(reg,str) and len(reg)>2 and reg[:3] in ['T1.','T2.']:
      T2 = (reg[:3] == 'T2.')
      reg = reg[3:]
    reg_suffixed = ('T2.' if T2 else 'T1.') + str(reg) + storeSuffix
    if isinstance(reg,str) and '%' in reg: reg = reg%self.readingFromChan
    if fresh or not reg_suffixed in self.storedReads:
      self.storedReads[reg_suffixed] = uhalaux.read(self.getT2() if T2 else self.getT1(), reg, mask, nodispatch)
    return self.storedReads[reg_suffixed]

  def write(self, reg, data, mask = 0xFFFFFFFF, T2 = False):
    """IPBus write operation in register "reg" of T1 (or T2 if T2=True), which may be a textual register name or a numeric address.
    Optional argument mask is only used with a numerical address"""
    return uhalaux.write(self.getT2() if T2 else self.getT1(), reg, data, mask)

  writeT1 = lambda self, reg, data, mask = 0xFFFFFFFF: self.write(reg, data, mask, False)
  writeT2 = lambda self, reg, data, mask = 0xFFFFFFFF: self.write(reg, data, mask, True)

  def action(self, action):
    """Issues an action-type write command to the amc13, corresponding to "ACTION.<user's 'action' parameter>"
    Additionally, some aliases are defined for quicker access:
      'rg' =  'RESETS.GENERAL'
      'rc' = 'RESETS.COUNTER'
      'lt' = 'burst' = 'stop' = 'LOCAL_TRIG.SEND_BURST'
      'continuous' = 'LOCAL_TRIG.CONTINUOUS'
      'ecr' = 'LOCAL_TRIG.SEND_ECR'
      'ocr' = 'LOCAL_TRIG.SEND_OCR'
      'custom_bgo' sends 1 custom bgo (reg 0x0 mask 0x100)"""
    alias = {'rc':'RESETS.COUNTER',
             'rg': 'RESETS.GENERAL',
             'lt':'LOCAL_TRIG.SEND_BURST',
             'burst':'LOCAL_TRIG.SEND_BURST',
             'stop': 'LOCAL_TRIG.SEND_BURST',
             'continuous':'LOCAL_TRIG.CONTINUOUS',
             'ecr':'LOCAL_TRIG.SEND_ECR',
             'ocr':'LOCAL_TRIG.SEND_OCR'}
    if action in alias: action = alias[action]
    if action == 'custom_bgo': self.writeT1(0x0,1,0x100) # (this is necessary because this field is new and yet unnamed in amc13tool release 1.1.2...)
    else: uhalaux.singlebitaction(self.getT1(),'ACTION.%s'%action)
    print action + ' action issued'

  #some alias functions for still faster access to actions
  ecr = lambda self: self.action('ecr')
  ocr = lambda self: self.action('ocr')
  lt = lambda self: self.action('burst')
  customBGO = lambda self: self.action('custom_bgo')

  def reg_num2name(self, regnum, mask = 0xFFFFFFFF, T2 = False):
    return uhalaux.reg_num2name(self.getT2() if T2 else self.getT1(), regnum, mask)
    
  def loctrigconf(self, data = None):
    """Queries (data = None) or sets the local trigger configuration.
    data is a dict containing values for the keys: 'TYPE','RULES','RATE','NUM_TRIG'
      * in the case of TYPE, value may be numeric (0-3) or an alias: 'perorbit','invalid','perbx','random'
      * in the case of RULES, value may be numeric (0-3) or an alias: 'allrules','rules123','rules12','rule1'
      * for RATE and NUM_TRIG, the value corresponds to the AMC13's value (functional value minus 1)"""
    keys = ['TYPE','RULES','RATE','NUM_TRIG']
    alias = {'TYPE': ['perorbit','invalid','perbx','random'],
             'RULES': ['allrules','rules123','rules12','rule1']}
    if not data:
      data = {}
      for key in keys:
        data[key] = self.read('T1.CONF.LOCAL_TRIG.%s'%key)
        if key in ['TYPE','RULES']: data[key] = alias[key][data[key]]
      return data
    else:
      for key in data:
        self.writeT1('CONF.LOCAL_TRIG.%s'%key, data[key] if key not in ['TYPE','RULES'] else alias[key].index(data[key]))
      print 'Local trigger configuration OK'

  def ttcsource(self, combo = None):
    """Queries (combo=None) or sets the TTC source in AMC13
    Possible combos are:
      'internal':     FAKE_TTC_ENABLE = 1, ENABLE_INTERNAL_L1A = 1
      'external':     FAKE_TTC_ENABLE = 0, ENABLE_INTERNAL_L1A = 0
      'extTTCintL1A': FAKE_TTC_ENABLE = 0, ENABLE_INTERNAL_L1A = 1
      """
    combos = {'invalid':(1,0), 'internal':(1,1), 'external':(0,0), 'extTTCintL1A':(0,1)}
    if not combo:
      ttsisttc = self.read('T1.CONF.DIAG.FAKE_TTC_ENABLE')
      intL1A = self.read('T1.CONF.TTC.ENABLE_INTERNAL_L1A')
      for combo in combos:
        if combos[combo] == (ttsisttc,intL1A): return combo
    elif combo in combos:
      self.writeT1('CONF.DIAG.FAKE_TTC_ENABLE',combos[combo][0])
      self.writeT1('CONF.TTC.ENABLE_INTERNAL_L1A',combos[combo][1])
    else: return 'invalid combo string'
    print combo + ' -> OK'

  def customBGO_conf(self, custom_slot, data = None):
    """Queries (data = None) or sets the custom bgo configuration.
    data is a dict containing values for the keys:
      'ENABLE_SINGLE','ENABLE','LONG_CMD','COMMAND','BX','ORBIT_PRESCALE0'"""
    keys = ['ENABLE_SINGLE','ENABLE','LONG_CMD','COMMAND','BX','ORBIT_PRESCALE',]
    if custom_slot not in range(4): return "Invalid custom_slot parameter. Should be 0 to 3"
    if not data:
      data = {}
      # (this is necessary because this field is new and yet unnamed in amc13tool release 1.1.2...)
      addr = self.getT1().getNode('CONF.TTC.BGO%i.ENABLE'%custom_slot).getAddress()
      mask = self.getT1().getNode('CONF.TTC.BGO%i.ENABLE'%custom_slot).getMask() << 1
      data['ENABLE_SINGLE'] = self.read(addr, mask, T2=False)

      for key in keys[1:]:
        data[key] = self.read('T1.CONF.TTC.BGO%i.%s'%(custom_slot,key))
      return data
    else:
      for key in data:
        if key == 'ENABLE_SINGLE': # (this is necessary because this field is new and yet unnamed in amc13tool release 1.1.2...)
          addr = self.getT1().getNode('CONF.TTC.BGO%i.ENABLE'%custom_slot).getAddress()
          mask = self.getT1().getNode('CONF.TTC.BGO%i.ENABLE'%custom_slot).getMask() << 1
          self.writeT1(addr, data['ENABLE_SINGLE'], mask)
        else:
          self.writeT1('CONF.TTC.BGO%i.%s'%(custom_slot,key), data[key])
      print 'Custom BGO configuration OK'

  def ttcstatus(self):
    """Prints the information from all the relevant TTC registers in T1 and T2."""
    print '\n\n--------------------------\n---- AMC13 TTC STATUS ----\n--------------------------'
    print '-----------INFO FROM T2-----------'
    for node in self.getT2().getNodes('STATUS\.TTC\..*'):
      print node.split('.')[-1] + ' = ', self.read(node, T2=True)
    print '-----------INFO FROM T1-----------'
    for node in self.getT1().getNodes('STATUS\.TTC\..*'):
      print node.split('.')[-1] + ' = ', self.read(node, T2=False)

  def amcEnableMask(self, mask = None):
    """Queries (mask=None) or sets the AMC enable mask in AMC13.
    mask is a 12-bit integer in which each bit corresponds to an AMC slot."""
    if mask == None:
      return hex(self.read('T1.CONF.AMC.ENABLE_MASK'))
    else:
      return self.writeT1('CONF.AMC.ENABLE_MASK',mask)

  def conf_dump(self):
    """Returns a dict containing the dump of the configuration registers of the AMC13, both T1 and T2"""
    return {'T1':{'CONF':regs_dump(self.getT1(),'CONF')},'T2':{'CONF':regs_dump(self.getT2(),'CONF')}}

  def conf_compare_current_conf_to(self, conf1):
    """Compares the current AMC13 configuration registers to the provided configuration dump dictionary."""
    conf_compare(conf1,self.conf_dump())

  def st(self, detail=1):
        """Produces a report with given detail level by calling the C bindings"""
        self.getStatus().Report(detail)

  def TT_lut(self, params_override = {}):
    params = self.getParams(params_override)
    if 'LUT' not in params:
      return
    with open(os.path.expandvars('${DTUPY_ETC}/luts/'+params['LUT']), 'r') as f:
      values = [int(line.strip(),16) for line in f.readlines()]
    for i in range(len(values)):
      self.writeT2(0x200 + i, values[i])

  def set_TT_status(self, status = None, params_override = {}):
    if status == None:
      params = self.getParams(params_override)
      status = ('TT_enable' in params and params['TT_enable'])
    self.writeT2('CONF.DTTRIG.ENABLE', 1 if status else 0)

  def readUnpackPrintEvent(self, slotTypes = None, raw = False):
    data = self.readEvent()
    if not data:
      print 'Event data empty'
    else:
      if raw:
        print '+--------------+------------------+'
        for i in range(len(data)):
          print '|% 10i    | %s |'%(i,('% 17x'%data[i]).replace(' ','0')[1:])
        print '+--------------+------------------+'
      unpacked = unpacker.unpack(data, slotTypes)
      unpacker.printUnpacked(unpacked, slotTypes)
      
      
      
  ################################################################################
  ## amc13-level commands that are defined in
  ##  - Swatch's AMC13Manager (AMC13Commands, ...)
  ##  - TwinMux Swatch Cell's AMC13Interface
  ################################################################################

  def resetCmd(self):
    """Equivalent to Swatch's ResetCommand from
    cactuscore/swatch/amc13/src/common/selfCommands.cpp"""
    
    self.reset(self.Board(1))
    self.reset(self.Board(0))
    self.endRun()
    self.sfpOutputEnable(0)
    self.AMCInputEnable(0)
    self.ttsDisableMask(0x0)
    self.localTtcSignalEnable(False)
    self.fakeDataEnable(False)
    self.monBufBackPressEnable(False)
    self.enableAllTTC()
  
  def configTTCCmd(self, params_override = {}):
    """Equivalent to Swatch's ConfigureTTCCommand from
    cactuscore/swatch/amc13/src/common/selfCommands.cpp"""
    
    params = self.getParams(params_override)
    self.setOcrCommand(params['ocrCmd'], 0)
    self.setResyncCommand(params['resyncCmd'])
    if params['localTTC']:
      self.localTtcSignalEnable(True)
    self.enableAllTTC()
    
  def configDAQCmd(self, amclist = [], fedid = 0, params_override = {}):
    """Equivalent to Swatch's ConfigureTTCCommand from
    cactuscore/swatch/amc13/src/common/selfCommands.cpp"""
    
    params = self.getParams(params_override)
    if not amclist:
      amclist = [ proc.context['slot'] for proc in self.s.hw[self.context['crate']]['processors'] if proc.isDAQsource ]
    bitmask = sum(map(lambda x:2**(x-1), amclist))
    if not fedid: fedid = params['fed']
    
    self.AMCInputEnable(bitmask)
    self.setFEDid(fedid)
    self.sfpOutputEnable(params['slinkMask'])
    self.daqLinkEnable(params['slinkMask'] != 0)
    self.setBcnOffset(params['bcnOffset'])
    self.resetCounters()
    self.reset(self.Board(1))
    
  def cfgTT_Cmd(self):
    """Equivalent to TwinMux Swatch Cell's cfgTT_Cmd"""
    print 'Remember to fill the TT configuration routine'
  
  def cfgPostCmd(self, amclist = []):
    """Equivalent to TwinMux Swatch Cell's selfPostConfigure"""
  
    if not amclist:
      amclist = [ proc.context['slot'] for proc in self.s.hw[self.context['crate']]['processors'] if proc.isDAQsource ]
    bitmask = sum(map(lambda x:2**(x-1), amclist))
    self.ttsDisableMask( (~bitmask)&0xFFF )
    self.writeT1("CONF.EVB.MON_FULL_OVERWRITE", 0)
    self.writeT1(0xF0, 0x1F)
  
    
  def startCmd(self):
    """Equivalent to Swatch's StartCommand from
    cactuscore/swatch/amc13/src/common/selfCommands.cpp"""
    
    self.startRun()
    
  def stopCmd(self):
    """Equivalent to Swatch's StopCommand from
    cactuscore/swatch/amc13/src/common/selfCommands.cpp"""
    
    self.endRun()
