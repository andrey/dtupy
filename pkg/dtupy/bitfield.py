#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# bitfield manipulation (to access integers bitwise, vhdl-like style)
#

class bf(long):
  def __getitem__( self, index ):
    return (self >> index) & 1

  #def __setitem__( self, index, value ):
  #  value = (value&1L)<<index
  #  mask   = (1L)<<index
  #  self  = (self & ~mask) | value

  def __getslice__( self, start, end ):
    mask = 2L**(end - start) -1
    return bf( (self >> start) & mask )

  #def __setslice__( self, start, end, value ):
  #  mask = 2L**(end - start) -1
  #  value = (value & mask) << start
  #  mask = mask << start
  #  self = (self & ~mask) | value
  #  return bf( (self >> start) & mask )

  def __int__(self):
    return super(bf, self).__int__()
  
  def __repr__(self):
    if self.bit_length() > 64:
      return super(bf, self).__repr__()
    else:
      return int(self).__repr__()
  