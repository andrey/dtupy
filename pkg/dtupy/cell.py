from dtupy.systembuilder import loadkey
from dtupy.bcolors import bcolors

################################################################################
## Generic Cell Class definition
################################################################################

class Cell(object):
  def __init__(self, key):
    self.s = loadkey(key)

  def configure(self):
    print bcolors.cyan + 'Executing Configure transition' + bcolors.reset

    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = sorted( [proc for crate in s.hw for proc in s.hw[crate]['processors'] ], key=lambda x:x.__class__.__name__, reverse=True)
    
    for amc13 in amc13s: amc13.resetCmd()
    for amc13 in amc13s: amc13.configTTCCmd()
    for amc13 in amc13s: amc13.TT_lut()
    for tm7 in tm7s: tm7.configureCmd()
    for amc13 in amc13s: amc13.configDAQCmd()
    #for amc13 in amc13s: amc13.cfgPostCmd()

    print bcolors.cyan + 'Executed Configure transition' + bcolors.reset
    
  def start(self):
    print bcolors.cyan + 'Executing Enable (Start) transition' + bcolors.reset

    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for tm7 in tm7s: tm7.startCmd()
    for amc13 in amc13s: amc13.startCmd()
    for amc13 in amc13s: amc13.set_TT_status()

    print bcolors.cyan + 'Executed Enable (Start) transition' + bcolors.reset

  def stop(self):
    print bcolors.cyan + 'Executing Disable (Stop) transition' + bcolors.reset

    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for tm7 in tm7s: tm7.stopCmd()
    for amc13 in amc13s: amc13.set_TT_status(0)
    for amc13 in amc13s: amc13.stopCmd()

    print bcolors.cyan + 'Executed Disable (Stop) transition' + bcolors.reset
