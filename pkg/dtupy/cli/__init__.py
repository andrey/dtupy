from dtupy.bcolors import bcolors

def interactive(sp):
  # function to be run
  def run(args):
    import os
    os.system('python -i $DTUPY_ROOT/scripts/interactive.py %s'%args.key)

  # configure the command line parser
  p = sp.add_parser('i', help='run an interactive python console on a crate')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.set_defaults(func=run)

def enableAmc13clk(sp):
  # function to be run
  def run(args):
    from dtupy.systembuilder import loadkey
    import time, uhal
    s = loadkey(args.key)
    doesnotchange = False
    for crate in s.hw:
      t0 = time.time()
      clk_readings = []
      while (time.time()-t0 < args.stuck_meas_time):
        clk_readings += [s.hw[crate]['amc13'].read('STATUS.TTC.CLK_FREQ',T2=True)]
      if len(set(clk_readings))==1:
        print 'It looks like the amc13 in crate %s may need rebooting'%crate
        doesnotchange = True
    if doesnotchange:
      if raw_input('Do you want to reboot all AMC13s in the system? (y/n) ') in ['Y','y']:
        for crate in s.hw:
          print 'Rebooting AMC13 in crate %s...'%crate
          s.hw[crate]['amc13'].getFlash().loadFlash()
        itsalive = { crate:False for crate in s.hw }
        t0 = time.time()
        print 'Waiting for AMC13 boards to reload FW...'
        while time.time()-t0 < args.timeout and not all([itsalive[crate] for crate in itsalive]):
          time.sleep(0.3)
          for crate in itsalive:
            if not itsalive[crate]:
              try:
                uhal.setLogLevelTo(uhal.LogLevel.FATAL)
                s.hw[crate]['amc13'].read(0,T2=False)
                s.hw[crate]['amc13'].read(0,T2=True)
                itsalive[crate] = True
                print 'AMC13 from crate %s has awaken after %.1f s'%(crate, time.time()-t0)
              except:
                pass
              uhal.setLogLevelTo(uhal.LogLevel.ERROR)
        if not all([itsalive[crate] for crate in itsalive]): 
          print 'Not all AMC13 boards in system managed to come back alive after the reboot, timeout = %.0f'%args.timeout
          return
    
    for crate in s.hw:
      print 'Enabling TTC to all AMC slots in %s...'%crate,
      s.hw[crate]['amc13'].enableAllTTC()
      measfreq = s.hw[crate]['amc13'].read('STATUS.TTC.CLK_FREQ',T2=True)*50
      print '   the measured clk frequency is %.03f %s MHz'%(measfreq/1e6, ' (Maybe ICI/PI not enabled?)' if not measfreq else '')
      

  # configure the command line parser
  p = sp.add_parser('amc13clk', help='enable AMC13s clock to amc slots')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.add_argument('-n', '--stuck_meas_time', default=2, type=int, help='the time in seconds it spends reading amc13 clk to see if it needs reconfiguration; default=2')
  p.add_argument('-t', '--timeout', default=120, type=float, help='the wait for wakeup timeout in seconds; default=120')
  p.set_defaults(func=run)

#def unpack(sp):
#  # function to be run
#  def run(args):
#    import dtupy.unpacker as unpacker
#    import os
#    #a = unpacker.counters_comparison(args.filename)
#    a = unpacker.load_amc13_dump(args.filename)
#
#  # configure the command line parser
#  p = sp.add_parser('unpack', help='run the unpacker in file')
#  p.add_argument('filename', help='the amc13 binary dump file')
#  p.set_defaults(func=run)

def transition(sp):
  # function to be run
  def run(args):
    from dtupy.cell import Cell
    cell = Cell(args.key)
    transitions = {
      'c':cell.configure,
      'e':cell.start,
      'd':cell.stop
      }
    if any([ch not in transitions for ch in args.transitions.lower()]):
      print bcolors.red + 'ERROR: the first positional argument to "dtu.py t" should be a combination of the characters "%s"'%(''.join(transitions.keys())) + bcolors.reset
      return
    
    for ch in args.transitions.lower():
      transitions[ch]()

  # configure the command line parser
  p = sp.add_parser('t', help='runs one or more transitions in the specified system')
  p.add_argument('transitions', default='', help='the transition(s) to run, may be any combination of the characters in "ced", which stand for configure, enable and disable')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.set_defaults(func=run)
  
def liveEventView(sp):
  # function to be run
  num_flush_events = 1000
  def run(args):
    from dtupy.systembuilder import loadkey
    import time, uhal
    s = loadkey(args.key)
    
    if args.flush:
      print '-'*80 + '\nFlushing amc13 buffer by doing %s reads\n'%num_flush_events
      for crate in s.hw:
        for i in range(num_flush_events):
          s.hw[crate].amc13.readEvent()
    
    for crate in s.hw:
      print '-'*80 + '\nLoading, unpacking and printing %i events from crate %s\n'%(args.num_events, crate) + '-'*80
      crate = s.hw[crate]
      slotTypes = {board.context['slot']:board.getParams()['hw-type'] if 'hw-type' in board.getParams() else 'uros' for board in crate.processors}
      for i in range(args.num_events):
        print 'Event number %i\n'%(i+1) + '-'*14
        crate.amc13.readUnpackPrintEvent(slotTypes, raw = args.raw)
        print '-'*60

  # configure the command line parser
  p = sp.add_parser('lieview', help='live event view: fetches event(s) from amc13, unpacks and prints it')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.add_argument('-n', '--num_events', default=1, type=int, help='the number of events to load and display')
  p.add_argument('-f', '--flush', action='store_true', default=False, help='Flushes the amc13 buffer to make room for new events by reading (not printing) %i events prior to performing the live event view'%num_flush_events)
  p.add_argument('-r', '--raw', action='store_true', default=False, help='Prints the raw data from the event in addition to the unpacked data')
  p.set_defaults(func=run)
  
def readTM7fw(sp):
  # function to be run
  def run(args):
    from dtupy.systembuilder import loadkey
    import time, uhal

    s = loadkey(args.key, maskCreationException = True)

    print '\n\nWaiting for the boards to respond'
    t0 = time.time()
    allboards = [(crate, slot) for crate in s.hw.keys() for slot in s.hw[crate]['slots']]
    doneboards = []
    while time.time() - t0 < args.timeout:
      for (crate,slot) in allboards:
        if (crate,slot) not in doneboards:
          try:
            uhal.setLogLevelTo(uhal.LogLevel.FATAL)
            print 'board %s.slot%i SUCCESS: fw version = %i (time to wake = %.0f s)'%(crate, slot, s.hw[crate]['slot%i'%slot].read('ctrl.id.algorev') & 0xFFFFFF , time.time()-t0)
            doneboards += [(crate,slot)]
            uhal.setLogLevelTo(uhal.LogLevel.ERROR)
          except:
            uhal.setLogLevelTo(uhal.LogLevel.ERROR)
      if len(doneboards) == len(allboards):
        print 'All boards successfully contacted'
        break
      time.sleep(1)
    for (crate, slot) in allboards:
      if (crate, slot) not in doneboards:
        print 'board %s.slot%i TIMED OUT (%i s)'%(crate,slot,args.timeout)
      
  # configure the command line parser
  p = sp.add_parser('readTM7fw', help='attempt to read the fw version from all TM7 boards in the key')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.add_argument('-t', '--timeout', default=120, type=float, help='the wait for wakeup timeout in seconds; default=120')
  p.set_defaults(func=run)

def reboot_tm7(sp):
  # function to be run
  def run(args):
    from dtupy.systembuilder import loadkey
    import time

    s = loadkey(args.key, maskCreationException = True)
    
    if args.ipmi_export: print 'Please run the following commands in a separate shell'
    for crate in s.hw.keys():
      for slot in s.hw[crate]['slots']:
        if args.ipmi_export: print s.hw[crate].ipmi_deactivate_str[slot]
        else:
          print 'Deactivating %s.slot%i'%(crate,slot)
          s.hw[crate].ipmi_deactivate(slot)
    if args.ipmi_export: raw_input('Please press enter when the ipmi commands have been run')
    print '\nCountdown to activation...'
    for i in range(9,0,-1):
      print i
      time.sleep(1)
    print
    if args.ipmi_export: print 'Please run the following commands in a separate shell'
    for crate in s.hw.keys():
      for slot in s.hw[crate]['slots']:
        if args.ipmi_export: print s.hw[crate].ipmi_activate_str[slot]
        else:
          print 'Activating %s.slot%i'%(crate,slot)
          s.hw[crate].ipmi_activate(slot)
    if args.ipmi_export: raw_input('Please press enter when the ipmi commands have been run')

  # configure the command line parser
  p = sp.add_parser('rebootTM7s', help='reboot the TM7 boards in the key')
  p.add_argument('-k','--key', default='active', help='the key to load')
  p.add_argument('-x', '--ipmi_export', action='store_true', default=False, help='Instead or running ipmi commands, it prints them for you to run in a different shell')
  p.set_defaults(func=run)


  
COMMANDS = []

COMMANDS += [interactive]
COMMANDS += [enableAmc13clk]
#COMMANDS += [unpack]
COMMANDS += [transition]
COMMANDS += [liveEventView]
COMMANDS += [reboot_tm7]
COMMANDS += [readTM7fw]


import twinmux_dbmonitoring
COMMANDS += [twinmux_dbmonitoring.tm_dbmonitor]

import twinmux_rates
COMMANDS += [twinmux_rates.tm_rates]
COMMANDS += [twinmux_rates.tm_tts]

import twinmux_maskstations
COMMANDS += [twinmux_maskstations.tm_mask_stations]

import twinmuxcell_cli
COMMANDS += [twinmuxcell_cli.tm_config]
COMMANDS += [twinmuxcell_cli.tm_start]

import uroscell_cli
COMMANDS += [uroscell_cli.uros_config]
COMMANDS += [uroscell_cli.uros_start]
COMMANDS += [uroscell_cli.uros_stop]
COMMANDS += [uroscell_cli.uros_links_status]
COMMANDS += [uroscell_cli.uros_configuration_checker]

try: # loads oracle that may not be present in every system
  import traceback, urosmonitor
  COMMANDS += [urosmonitor.uros_monitor]
except ImportError:
  print '[WARN] Import error when loading urosmonitor: probably due to missing cx_Oracle library'
except:
  print "[WARN] Exception when trying to load urosmonitor module:"
  traceback.print_exc()

import uros_sql
COMMANDS += [uros_sql.uros_genSQL]
