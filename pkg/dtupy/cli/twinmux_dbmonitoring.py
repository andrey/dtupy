def tm_dbmonitor(sp):
    # function to be run
    def run(args):
        from dtupy.systembuilder import loadkey
        from dtupy.twinmux.dbmonitoring import regdump
        import gzip
        
        s = loadkey(args.key)
        txt = ''
        for crate in s.hw:
            txt+= '#'*80 + '\n CRATE %s\n'%crate + '#'*80 + '\n'
            for proc in s.hw[crate]['processors']:
                try: txt += proc.hw().id() +'\n' + regdump(proc, True) + '\n'
                except: pass
        
        
        f = gzip.open(args.filename,'w')
        f.write(txt)
        f.close()
            
        
            

    # configure the command line parser
    p = sp.add_parser('tmhddmon', help='TwinMux system HDD-based monitoring (no amc13s)')
    p.add_argument('key', help='the key to load')
    p.add_argument('filename', help='the filename to save the data')
    p.set_defaults(func=run)
