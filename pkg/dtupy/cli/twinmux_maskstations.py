def tm_mask_stations(sp):
    # function to be run
    def run(args):
        from dtupy.systembuilder import loadkey
        from dtupy.bcolors import bcolors
        import time
        s = loadkey(args.key)
        print '\n\n'

        wheels = ['-2','-1','0','+1','+2']
        masked = {}
        for a in args.stations:
            if a[:-1] in ['YB%s_S%i_MB'%(wheels[n/12],n%12+1) for n in range(60)] and a[-1] in ['%i'%n for n in range(1,5)]:
                wheel = a[:a.find('_')]
                sector = a[a.find('S'):a.find('_MB')]
                station = int(a[-1])
                if wheel not in masked: masked[wheel] = {}
                if sector not in masked[wheel]: masked[wheel][sector]=0
                masked[wheel][sector] |= 1<<(station-1)
            else:
                print bcolors.red + 'ERROR: unrecognized format for station identifier "%s"'%a + bcolors.reset + '\n'
                return
        if masked:
            while True:
                for wheel in masked:
                    for sector in masked[wheel]:
                        proc = s.hw['crate_%s'%wheel[2:]]['slot%s'%sector[1:]]
                        print 'Setting %s mask register to 0x%X'%(proc.id(), masked[wheel][sector]), 
                        try:
                            proc.write("payload.dt_sector.ctrl4.mask_input", masked[wheel][sector])
                            print bcolors.green + 'SUCCESS' + bcolors.reset
                        except: print bcolors.red + 'FAILED' + bcolors.reset
                time.sleep(args.period)

    # configure the command line parser
    p = sp.add_parser('tm7maskst', help='TwinMux individual station continuous mask')
    p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
    p.add_argument('-s', '--stations', nargs='*', help='the stations to mask in the format YBxx_Syy_MBz, e.g. YB+1_S1_MB2')
    p.add_argument('-p', '--period', default=2, type=float, help='the register write period in seconds; default=2')
    p.set_defaults(func=run)
