def reflow(txt, lines):
    import os
    rows, columns = map(int,os.popen('stty size', 'r').read().split())
    tokens, tokens2 = txt.split('\n'), []
    if len(tokens) > rows-5:
        for i in range(len(tokens)):
            if (i/lines)%2 : continue
            tokens2 += [tokens[i] + '\t\t\t\t' + (tokens[i+lines] if i+lines < len(tokens) else '')]
        return '\n'.join(tokens2)
    return txt
    

def tm_rates(sp):
    # function to be run
    def run(args):
        from dtupy.systembuilder import loadkey
        from dtupy.bcolors import bcolors
        import os, select, sys, time
        
        s = loadkey(args.key)
        if 'all' in args.crates: args.crates = s.hw.keys()
        
        maxstations = 5 if (args.rate == 'dt') else 4
        if args.rate == 'dt':       reg = 'payload.dt_sector.stat3_st%i.trigger_rate'
        elif args.rate == 'rpc':    reg = 'super_sector.super_sector.stat0_st%i.rpc_rate'
        elif args.rate == 'output': reg = 'super_sector.super_sector.stat0_st%i.super_rate'
            
        def rate(st):
            val = str(s.hw[crate][slot].read(reg%st))
            if (s.hw[crate][slot].read("payload.dt_sector.ctrl4.mask_input") & (1 << min(st,3))):
                val = bcolors.yellow + bcolors.bold + val + bcolors.resetolor
            return val
        
        while not select.select([sys.stdin,],[],[],0.0)[0]:
            txt = ''
            for crate in args.crates:
                txt += bcolors.green + crate+ ' %s trigger rate\t\t\t'%args.rate + bcolors.reset + '\n'
                txt += bcolors.purple + 'BOARDS\t\t' + '\t'.join(['MB1', 'MB2', 'MB3', 'MB4', 'MB4bis'][:maxstations]) + bcolors.reset + '\n'
                for slot in ['slot%i'%i for i in range(1,13)]:
                    if not slot in s.hw[crate]:
                        txt += slot + '   \t' + bcolors.red + 'failed during script launch\t' + bcolors.reset + '\n'
                    else:
                        stations = maxstations if slot[-1] in ['4','0'] else 4
                        txt += s.hw[crate][slot].id() + '  \t'
                        try:
                            txt += '\t'.join([rate(st) if st<stations else '' for st in range(maxstations)]) + '\n'
                        except:
                            txt += bcolors.red + 'failed fetching values\t\t' + bcolors.reset + '\n'

            txt = reflow(txt,14)
            _=os.system("clear")
            print txt[:-1]
            
            if bcolors.yellow in txt: 
                print '\n\n' + bcolors.yellow + 'Please be aware that we are running with some stations masked, which are marked in this table in YELLOW' + bcolors.reset
            
            time.sleep(args.period)

    # configure the command line parser
    p = sp.add_parser('tm7rates', help='TwinMux system rate monitoring')
    p.add_argument('-r', '--rate', default='dt', choices=['dt', 'rpc', 'output'], help='Which rate (dt, rpc, output) to monitor; default=dt')
    p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
    p.add_argument('-c', '--crates', default=['all'], nargs='*', help='the crate(s) to monitor; default=all')
    p.add_argument('-p', '--period', default=2, type=float, help='the monitoring period in seconds; default=2')
    p.set_defaults(func=run)

def tm_tts(sp):
    # function to be run
    def run(args):
        from dtupy.systembuilder import loadkey
        from dtupy.bcolors import bcolors
        import os, select, sys, time
        
        s = loadkey(args.key)
        if 'all' in args.crates: args.crates = s.hw.keys()
        
        while not select.select([sys.stdin,],[],[],0.0)[0]:
            txt = ''
            for crate in args.crates:
                runtime = s.hw[crate]['amc13'].read('STATUS.GENERAL.RUN_TIME_LO') +           (s.hw[crate]['amc13'].read('STATUS.GENERAL.RUN_TIME_HI') << 32)
                rdytime = s.hw[crate]['amc13'].read('STATUS.GENERAL.READY_TIME_LO') +         (s.hw[crate]['amc13'].read('STATUS.GENERAL.READY_TIME_HI') << 32)
                bsytime = s.hw[crate]['amc13'].read('STATUS.GENERAL.BUSY_TIME_LO') +          (s.hw[crate]['amc13'].read('STATUS.GENERAL.BUSY_TIME_HI') << 32)
                ofwtime = s.hw[crate]['amc13'].read('STATUS.GENERAL.OF_WARN_TIME_LO') +       (s.hw[crate]['amc13'].read('STATUS.GENERAL.OF_WARN_TIME_HI') << 32)
                bptime =  s.hw[crate]['amc13'].read('STATUS.SFP.SFP0.LSC.BACKP_COUNTER_LO') + (s.hw[crate]['amc13'].read('STATUS.SFP.SFP0.LSC.BACKP_COUNTER_HI') << 32)
                
                amcbsy = [s.hw[crate]['amc13'].read('STATUS.AMC%02i.STATE.BSY_STATE_TIME_LO'%i) + (s.hw[crate]['amc13'].read('STATUS.AMC%02i.STATE.BSY_STATE_TIME_HI'%i) << 32) for i in range(1,13)]
                amcofw = [s.hw[crate]['amc13'].read('STATUS.AMC%02i.STATE.OFW_STATE_TIME_LO'%i) + (s.hw[crate]['amc13'].read('STATUS.AMC%02i.STATE.OFW_STATE_TIME_HI'%i) << 32) for i in range(1,13)]
                
                txt += bcolors.green + crate+ ' (%i) TTS metrics\t\t\t\t'%s.hw[crate]['amc13'].read('CONF.ID.SFP0.SOURCE_ID') + bcolors.reset + '\n'
                txt += 'Run Time         \t0x% 12X\t\t\t\n'%runtime
                if  runtime:
                    txt += 'RDY Time         \t0x% 12X\t(%.2f %%)\t\n'%(rdytime, 100.0*rdytime/runtime)
                    txt += 'BSY Time         \t0x% 12X\t(%.2f %%)\t\n'%(bsytime, 100.0*bsytime/runtime)
                    txt += 'OFW Time         \t0x% 12X\t(%.2f %%)\t\n'%(ofwtime, 100.0*ofwtime/runtime)
                    txt += 'BackPressure Time\t0x% 12X\t(%.2f %%)\t\n'%(bptime, 100.0*bptime/runtime)
                    txt += 'AMCs BSY Time    \t0x%X - 0x%X\t(%.2f %% - %.2f %%)\n'%(max(amcbsy),sum(amcbsy), 100.0*max(amcbsy)/runtime, 100.0*sum(amcbsy)/runtime)
                    txt += 'AMCs OFW Time    \t0x%X - 0x%X\t(%.2f %% - %.2f %%)\n'%(max(amcofw),sum(amcofw), 100.0*max(amcofw)/runtime, 100.0*sum(amcofw)/runtime)
                
                txt += '\n'

            txt = reflow(txt,9)
            
            _=os.system("clear")
            print txt[:-1]
            print bcolors.yellow + 'For AMCs, as there are 12 values, a range between minimum possible (times overlap between AMCs) and maximum (times stack up)' + bcolors.reset               
            time.sleep(args.period)

    # configure the command line parser
    p = sp.add_parser('tm7tts', help='TwinMux TTS and backpressure diagnostics')
    p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
    p.add_argument('-c', '--crates', default=['all'], nargs='*', help='the crate(s) to monitor; default=all')
    p.add_argument('-p', '--period', default=2, type=float, help='the monitoring period in seconds; default=2')
    p.set_defaults(func=run)
