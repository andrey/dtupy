from dtupy.twinmuxcell import TWINMUXCell

def tm_config(sp):
    # function to be run
    def run(args):
      TWINMUXCell(args.key).configure()

    # configure the command line parser
    p = sp.add_parser('tm7config', help='Run TwinMux system configure transition')
    p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
    p.set_defaults(func=run)

def tm_start(sp):
    # function to be run
    def run(args):
      TWINMUXCell(args.key).start()

    # configure the command line parser
    p = sp.add_parser('tm7start', help='Run TwinMux system start transition (aligns links)')
    p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
    p.set_defaults(func=run)
