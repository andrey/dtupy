def drop_create(table):
  return 'DROP TABLE %s;\nCREATE TABLE %s'

def uros_genSQL(sp):
  # function to be run
  def run(args):
    from dtupy.systembuilder import loadkey
    from dtupy.bcolors import bcolors
    
    print bcolors.blue + 'uROS database automatic SQL generator. Initialization...' + bcolors.reset
    s = loadkey(args.key)
    
    boardset = {'amc13':  [s.hw[crate]['amc13'] for crate in s.hw],
                'uros':   [proc for crate in s.hw for proc in s.hw[crate]['processors'] ] }

    columnlist = {key:[] for key in boardset}
    for boardtype in boardset:
      for board in boardset[boardtype]:
        for param in board.filterparams():
          if param not in columnlist[boardtype]: columnlist[boardtype] += [param]
          ### put a test on allowed characters!!!

    sqlcode = []
    for boardtype in ['uros','amc13']:
      table = 'UROS_%sCONF'%boardtype.upper()
      # Destroy table
      sqlcode += [ 'DROP TABLE %s'%table ]
      # Create table
      sqlcode += ['CREATE TABLE %s('%table ]
      sqlcode[-1] += ' BOARDID NUMERIC'
      for i in [1,2] if boardtype == 'amc13' else [1]:
        sqlcode[-1] += ', URI%s VARCHAR2(1024)'%('T%i'%i if boardtype == 'amc13' else '')
        sqlcode[-1] += ', ADDRESSTABLE%s VARCHAR2(1024)'%('T%i'%i if boardtype == 'amc13' else '')
      for column in sorted(columnlist[boardtype]):
        sqlcode[-1] += ', %s NUMERIC'%column.upper()
      sqlcode[-1] +=')'
      # Insert rows
      for board in boardset[boardtype]:
        sqlcode += [ 'INSERT INTO %s VALUES ( %s'%(table, board.context['id'] ) ]
        if boardtype == 'uros': sqlcode[-1] += ", '%s', '%s'"%(board.hw().uri(), board.context['addressTable'])
        else: sqlcode[-1] += ", '%s', '%s', '%s', '%s'"%(board.getT1().uri(), board.context['addressTableT1'], board.getT2().uri(), board.context['addressTableT2'])
        filteredparams = board.filterparams()
        for column in sorted(columnlist[boardtype]):
          sqlcode[-1] += ', %i'%( filteredparams[column] if column in filteredparams else 0)
        sqlcode[-1]+= ')'
    
    if not args.write:
      txt1 = '*'*10 + ' SQL CODE: ONE LINE = ONE COMMAND ' + '*'*10
      txt2 = ' END OF SQL CODE '
      txt2 = '*'*( (len(txt1)-len(txt2))/2 ) + txt2 + '*'*( (1+len(txt1)-len(txt2))/2 )
      print bcolors.blue + '*'*len(txt1) + '\n' + txt1 + '\n' + '*'*len(txt1) + bcolors.reset
      for line in sqlcode:
        print line
        print bcolors.blue + '*'*len(txt2) + bcolors.reset
      print bcolors.blue + txt2 + '\n' + '*'*len(txt2) + bcolors.reset

    if args.write:
      username = 'cms_dt_elec_conf'
      service = 'cms_omds_lb'

      from getpass import getpass
      txt1 = '!'*10 +' STARTING WRITE OF DATA TO THE SQL DATABASE ' + '!'*10
      print bcolors.blue + '!'*len(txt1) + '\n' + txt1 + '\n' + '!'*len(txt1) + bcolors.reset
      password = getpass( 'password for %s/%s: '%(username,service) )
      
      import cx_Oracle
      con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
      print '%sConnection stablished, version %s%s'%(bcolors.blue,con.version,bcolors.reset)
      con.autocommit = 1
      cur = con.cursor()
      cur = con.cursor()
      
      for line in sqlcode:#['select * from UROS_AMC13CONF','select * from UROS_UROSCONF']:
        print bcolors.blue + 'About to run the SQL command...' + bcolors.reset
        print repr(line)
        #s = raw_input(bcolors.blue +'...do you want to?) '%(username,service) + bcolors.reset')
        cur.execute(line)
      
      cur.close()
      con.close()

    
    
    
  # configure the command line parser
  p = sp.add_parser('sql', help='Generate SQL code that destroys and recreates the uROS database tables')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-w', '--write', action='store_true', default=False)
  p.set_defaults(func=run)