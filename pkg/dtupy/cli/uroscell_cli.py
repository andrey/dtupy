from dtupy.uroscell import UROSCell
from dtupy.bcolors import bcolors
import time

def uros_config(sp):
  # function to be run
  def run(args):
    UROSCell(args.key, args.db_partKey, args.db_configKey, args.db_configVers).configure()
      
  # configure the command line parser
  p = sp.add_parser('urosconfig', help='Run uROS system configure transition')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-p', '--db_partKey', default=0, type=int, help='the partition key to load from database; default=0')
  p.add_argument('-c', '--db_configKey', default=0, type=int, help='the config key to load from database; default=0')
  p.add_argument('-v', '--db_configVers', default=0, type=int, help='the config key\'s version to load from database; default=0')
  p.set_defaults(func=run)

def uros_start(sp):
  # function to be run
  def run(args):
    UROSCell(args.key, args.db_partKey, args.db_configKey, args.db_configVers).start()
      
  # configure the command line parser
  p = sp.add_parser('urosstart', help='Run uROS system start transition')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-p', '--db_partKey', default=0, type=int, help='the partition key to load from database; default=0')
  p.add_argument('-c', '--db_configKey', default=0, type=int, help='the config key to load from database; default=0')
  p.add_argument('-v', '--db_configVers', default=0, type=int, help='the config key\'s version to load from database; default=0')
  p.set_defaults(func=run)

def uros_stop(sp):
  # function to be run
  def run(args):
    UROSCell(args.key, args.db_partKey, args.db_configKey, args.db_configVers).stop()
      
  # configure the command line parser
  p = sp.add_parser('urosstop', help='Run uROS system stop transition')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-p', '--db_partKey', default=0, type=int, help='the partition key to load from database; default=0')
  p.add_argument('-c', '--db_configKey', default=0, type=int, help='the config key to load from database; default=0')
  p.add_argument('-v', '--db_configVers', default=0, type=int, help='the config key\'s version to load from database; default=0')
  p.set_defaults(func=run)

def uros_configuration_checker(sp):
  # function to be run
  def run(args):
    uc = UROSCell(args.key)
    hw = uc.s.hw
    params = uc.s.params
    proclist = [ board.context['id'] for crate in hw for board in hw[crate].processors ]
    amc13list = [ hw[crate].amc13.context['id'] for crate in hw ]
    
    bad_scopes = []
    for scope in params:
      if scope not in ['daqttcs', 'processors'] and ( '_' not in scope or scope.split('_')[1] not in ['%02i'%i for i in range(1,14)] ):
        bad_scopes += [scope]
    used_scopes = ['daqttcs', 'processors'] + proclist + amc13list
    unused_scopes = [scope for scope in params if scope not in used_scopes]
    
    for scope in sorted(bad_scopes):
      print bcolors.red + 'Scope found in parameters is not allowed: "%s"'%scope + bcolors.reset
    for scope in sorted(unused_scopes):
      print bcolors.yellow + 'Scope found in parameters is not used: "%s"'%scope + bcolors.reset
    if not bad_scopes and not unused_scopes:
      print bcolors.green + 'No bad or unused scopes found in parameters' + bcolors.reset
      
  # configure the command line parser
  p = sp.add_parser('uroscheck', help='Check configuration and partition files for inconsistencies')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.set_defaults(func=run)
  
def uros_links_status(sp):
  # function to be run
  def run(args):
    from dtupy import dtromap
    uc = UROSCell(args.key)
    hw = uc.s.hw
    params = uc.s.params
    proclist = [ board for crate in hw for board in hw[crate].processors ]
    
    metrics ={'unlock':     {'reg': 'readout.stat.rob.unlock12_ctr'         },
              'startstop':  {'reg': 'readout.stat.rob.startstoperr_ctr'     },
              'parity':     {'reg': 'readout.stat.rob.parityerr_total_ctr'  }}
    for metric in metrics: metrics[metric]['doit'] = 'all' in args.metrics or metric in args.metrics
    
    readConcurrentlyException = Exception('Another process is reading from the uros boards concurrently. This can lead to corruption of its data.')
    
    data = {}
    def readData():
      for board in proclist:
        if board.read('readout.conf.stat_freeze'): raise readConcurrentlyException
        board.write('readout.conf.stat_freeze',1)
        if not board.context['id'] in data: data[board.context['id']] = {}
        for channel in board.activeInputs():
          if not channel in data[board.context['id']]: data[board.context['id']][channel] = {}
          board.write("readout.conf.rob.stat_mux",channel)
          for m in metrics:
            if metrics[m]['doit']:
              if m not in data[board.context['id']][channel]:
                if m=='unlock':
                  #normally would be 0, but making it dependent on the initial unlock status, it ensures that if initial or final condition is nowlock=0 the count will be >0
                  data[board.context['id']][channel][m] = 1 - board.read('readout.stat.rob.nowlock')  
                else:
                  data[board.context['id']][channel][m] = 0
              data[board.context['id']][channel][m] = board.read(metrics[m]['reg']) - data[board.context['id']][channel][m]
        board.write('readout.conf.stat_freeze',0)
    
    readData()
    time.sleep(args.time)
    readData()
    
    maps = {  (board.context['id'], channel):
                (dtromap.decode_uros_map(board.s.hw[board.context['crate']]['amc13'].getParams()['fed'], 
                                        board.getParams()['slotMap'], channel )['str'] 
                if 'slotMap' in board.getParams() else '')
              for board in proclist for channel in board.activeInputs()}
    
    txt = '# board.channel\tdecoded map'
    for m in sorted(metrics):
      if metrics[m]['doit']:
        txt += '\t' + m
    txt+='\n'
    for board in proclist:
      for channel in data[board.context['id']]:
        txt += board.context['id'] + '.' + str(channel) + '\t' + maps[(board.context['id'], channel)]
        for m in sorted(metrics):
          if metrics[m]['doit']:
            txt += '\t' + str(data[board.context['id']][channel][m])
        txt += '\n'
    
    with open(args.file,'w') as f:
      f.write(txt)
    
    print 'Writing of metrics to file done successfully'
      
  # configure the command line parser
  p = sp.add_parser('uroslinkstatus', help='Writes link reception quality metrics to a file')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-f', '--file', default='uroslinkstatuslog.txt', help='the output file; default=uroslinkstatuslog.txt')
  p.add_argument('-t', '--time', default=1, type=float, help='the measurement time in seconds; default=1')
  p.add_argument('-m', '--metrics', choices=['unlock', 'startstop', 'parity', 'all'], default=['all'], nargs='+', help='the metrics to monitor; default=all')
  p.set_defaults(func=run)
  