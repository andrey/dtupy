from dtupy.uroscell import UROSCell
from dtupy.bcolors import bcolors
import os, sys, time, datetime, random
import traceback, cx_Oracle
import zlib, json
import requests
import xml.etree.ElementTree as ET
import socket
import getpass
import pprint
import threading


username = 'cms_dt_elec_conf'
service = 'cms_omds_lb'
password = 'cmsonr2008'

def log_message(message):
  try:
    message = str(datetime.datetime.now()) + '\t' + message
    reportfile = 'monitor_report.txt'
    if reportfile in os.listdir('.'):
      datereportfile = datetime.date.fromtimestamp( os.path.getmtime(reportfile) )
      if datereportfile != datetime.date.today():
        newreportfile = reportfile.replace('.','_%s.'%datereportfile)
        newreportfilecontent = ''
        with open(newreportfile, 'a') as fout, open(reportfile, 'r') as fin:
          newreportfilecontent = fin.read()
          fout.write(newreportfilecontent)
        with open(reportfile, 'w') as f:
          f.write('')
          
        try:
          def worker(text):
            import externalcommands
            reload(externalcommands)
            externalcommands.log_day(text)
          threading.Thread(target=worker, args=(newreportfilecontent,)).start()
        except: pass
        
    with open(reportfile,'a') as f:
      f.write( message + '\n' )
  except:
    print 'Exception when writing to log file, error redirected to stderr while trying to log the message:'
    print message
    print 'This is the exception itself'
    traceback.print_exc()
    
  try:
    def worker(text):
      import externalcommands
      reload(externalcommands)
      externalcommands.log_item(text)
    threading.Thread(target=worker, args=(message,)).start()
  except: pass
    
def keepgoing(info):
  try:
    firstTime = 'id' not in info
    if firstTime:
      info['id'] = '%s_%s_%i_%06i'%( socket.gethostname(), getpass.getuser(), os.getpid(), int(random.random()*1e6) )
    
    stopCause = ''
    
    con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
    con.autocommit = 1
    cur = con.cursor()
    
    cur.execute( 'select DTHOSTNAME from HOST2RESOURCES where UROS = 1' )
    rows = cur.fetchall()
    dbhost = rows[0][0] if len(rows)==1 else ''

    cur.execute( 'select TIME_STAMP, DATA_NAME, SCRIPT_ID from W_MON_LIVE' )
    mon_live_rows = { row[1]:{'t':row[0], 'id':row[2] } for row in cur.fetchall() }
    
    cur.close()
    con.close()

    actualhost = socket.gethostname()
    info['host'] = actualhost

    now = datetime.datetime.now()
    for entry in mon_live_rows:
      mon_live_rows[entry]['ago'] = (now-mon_live_rows[entry]['t']).total_seconds()
      
    if socket.gethostbyname(dbhost) != socket.gethostbyname(actualhost): #to account for the possibility of dbhost being an alias
      stopCause = 'running script in a host not designated by host2resources'
    elif 'monitor_disable' in os.listdir('.'):
      stopCause = 'monitor_disable file found in tmp folder'
    elif 'disable' in mon_live_rows:
      stopCause = 'found a "disable" entry in W_MON_LIVE'
    elif any([(mon_live_rows[entry]['ago'] < 30) and (mon_live_rows[entry]['ago'] > 0) and (mon_live_rows[entry]['id']!=info['id']) for entry in mon_live_rows]): 
      stopCause = 'another instance of monitor script already running'
      
    if stopCause:
      if not firstTime: # if the script had already run once, this will cause it to stop --> report it
        log_message( '[INFO] Exiting session of the monitor script (#%s) because an exit condition was met (%s)'%(info['id'], stopCause) )
    else:
      if firstTime:
        log_message( '[INFO] Starting monitor script with id#%s'%info['id'] )
      dbstore_live('onduty', '', info['id'], False)

    return stopCause == ''
  except:
    log_message('[ERROR] Exception raised in method keepgoing(); Stopping monitor script\n' + traceback.format_exc() )
    return False
    
runTypes = {'Global':40000, 'Local':41000}
def getinfo(info):
  ## Get the status from the xdaq application by SOAP
  headers = {
    'content-type': 'text/xml', 
    'Content-Description': 'SOAP Message', 
    'SOAPAction': 'urn:xdaq-application:class=SOAPStateMachine,instance=0' }

  body = '''
  <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Header>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
      <xdaq:WhatsYourStatus xmlns:xdaq="urn:xdaq-soap:3.0"/>
    </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>
  '''
  
  commErrCounter = 0
  for runType in runTypes:
    try:
      url = 'http://%s:%i'%(info['host'], runTypes[runType])
      response = requests.post(url,data=body, headers=headers)
      response = ET.fromstring(response.content)[1][0].tag
      response = response.split('WhatsYourStatusResponse_')[1].split('_')
      response = { response[i]:response[i+1] for i in range(0,len(response),2) }
      for key in response:
        try: response[key] = int(response[key])
        except: pass

      if any([key not in info or response[key]!=info[key] for key in response]):
        response['State_change']=time.time()
      
      response['runType'] = runType
      info.update(response)
      break
    except:
      commErrCounter += 1

  if commErrCounter == len(runTypes):
    info['State']='CommError'
    info['runType']='N/A'
  
  ## Get the beamMode from the database
  con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
  con.autocommit = 1
  cur = con.cursor()
  
  cur.execute( 'select VALUE from CMS_LHC_BEAM_COND.LHC_BEAMMODE order by DIPTIME desc' )
  beamMode = cur.fetchone()[0]

  cur.close()
  con.close()

  info['beamMode'] = beamMode
  
  if info['beamMode'] == 'STABLE BEAMS' and info['runType'] != 'Global': 
    log_message('[ERROR] Stable beams detected but not possible to communicate with Global instance of xdaq application' )

  
  foldercontents = os.listdir('.')
  info['write_files'] = 'write_files' in foldercontents
  info['exitOnStop'] = 'exitOnStop' in foldercontents
  info['simulateDegraded'] = 'simulateDegraded' in foldercontents
  info['logDegradedAnalysis'] = 'logDegradedAnalysis' in foldercontents

def dbstore_run(runNumber, init_data):
  try:
    webfolder = os.path.expandvars('${DTUPY_ETC}/urosweb')
    filesDict = {}
    for filename in os.listdir(webfolder):
      if 'content' in filename:
        with open(webfolder + '/' + filename,'r')as f:
          filesDict[filename] = f.read()
    
    files_zlib = zlib.compress(json.dumps(filesDict, sort_keys=True, indent=2, separators=(',', ': '), allow_nan=False),9)
    data_zlib = zlib.compress(json.dumps(init_data, sort_keys=True, indent=2, separators=(',', ': '), allow_nan=False),9)

    con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
    con.autocommit = 1
    cur = con.cursor()

    filesBlob = cur.var(cx_Oracle.BLOB)
    filesBlob.setvalue(0,files_zlib)

    initDataBlob = cur.var(cx_Oracle.BLOB)
    initDataBlob.setvalue(0,data_zlib)

    cur.setinputsizes(contentFiles = cx_Oracle.BLOB)
    cur.setinputsizes(initData = cx_Oracle.BLOB)
    cur.execute( '''
      merge into W_MON_RUNS R 
        using (select :runNum as RUN from dual) A 
        on(A.RUN = R.RUN)
      when matched then 
        update set R.CONTENT_FILES = :contentFiles, R.INIT_DATA = :initData
      when not matched then 
        insert (RUN, CONTENT_FILES, INIT_DATA) values ( :runNum, :contentFiles, :initData )''',
      {'runNum':runNumber, 'contentFiles': filesBlob, 'initData':initDataBlob} )
    cur.close()
    con.close()
  except:
    log_message('[ERROR] Exception raised in method dbstore_run; Continuing...\n' + traceback.format_exc() )

def dbstore_snapshot(runNumber, monitorData, monitorDataWeb):
  try:
    mdw_zlib = zlib.compress(json.dumps(monitorDataWeb, sort_keys=True, indent=2, separators=(',', ': '), allow_nan=False),9)

    con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
    con.autocommit = 1
    cur = con.cursor()

    mdwBlob = cur.var(cx_Oracle.BLOB)
    mdwBlob.setvalue(0,mdw_zlib)

    rawClob = cur.var(cx_Oracle.CLOB)
    rawClob.setvalue(0,monitorData)

    cur.setinputsizes(raw_data = cx_Oracle.CLOB)
    cur.setinputsizes(web_data = cx_Oracle.BLOB)
    cur.execute( 'INSERT INTO W_MON_DATA VALUES ( :runNum, :datenow, :raw_data, :web_data )', 
      {'runNum':runNumber, 'datenow': datetime.datetime.now(), 'raw_data':rawClob, 'web_data':mdwBlob } )

    cur.close()
    con.close()
  except:
    log_message('[ERROR] Exception raised in method dbstore_snapshot; Continuing...\n' + traceback.format_exc() )

def dbstore_live(name, data, id, writeFiles):
  try:
    data_zlib = zlib.compress(json.dumps(data, sort_keys=True, indent=2, separators=(',', ': '), allow_nan=False),9)

    con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
    con.autocommit = 1
    cur = con.cursor()

    dataBlob = cur.var(cx_Oracle.BLOB)
    dataBlob.setvalue(0,data_zlib)

    cur.setinputsizes(dataContent = cx_Oracle.CLOB)
    cur.execute( '''
      merge into W_MON_LIVE R 
        using (select :dataName as DATA_NAME from dual) A 
        on(A.DATA_NAME = R.DATA_NAME)
      when matched then 
        update set R.TIME_STAMP = :datenow, R.DATA_CONTENT = :dataContent, R.SCRIPT_ID = :id
      when not matched then 
        insert (TIME_STAMP, DATA_NAME, DATA_CONTENT, SCRIPT_ID) values ( :datenow, :dataName, :dataContent, :id )''',
      {'datenow': datetime.datetime.now(), 'dataName':name, 'dataContent':dataBlob, 'id':id } )

    cur.close()
    con.close()
    
    if writeFiles:
      with open(name, 'w') as f:
        f.write( json.dumps(data, sort_keys=True, indent=2, separators=(',', ': '), allow_nan=False) )

  except:
    log_message('[ERROR] Exception raised in method dbstore_live; Continuing...\n' + traceback.format_exc() )

def degradedChange(info, degraded):
  try:
    headers = {
      'content-type': 'text/xml', 
      'Content-Description': 'SOAP Message', 
      'SOAPAction': 'urn:xdaq-application:class=SOAPStateMachine,instance=0' }

    body = '''
    <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
      <SOAP-ENV:Header>
      </SOAP-ENV:Header>
      <SOAP-ENV:Body>
        <xdaq:%sAlert xmlns:xdaq="urn:xdaq-soap:3.0"/>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>
    '''%('' if degraded else 'Remove')
    
    url = 'http://%s:%i'%(info['host'], runTypes[info['runType']])
    response = requests.post(url,data=body, headers=headers)

  except:
    log_message('[ERROR] Exception raised in method degradedChange(); Continuing...\n' + traceback.format_exc() )

def degradedAllowed(info):
  #return info['State'] == 'Running' # use this line when testing in local and not stable beams
  return info['beamMode'] == 'STABLE BEAMS' and info['runType']== 'Global' and info['State'] == 'Running'
  
def uros_monitor(sp):
  # function to be run
  def run(args):
    sys.path.append(os.getcwd()) # needs to be run once; allows log to import module from local files
    # Run information
    info = {  'runNumber' : 0, 'State' : 'Unknown', 'runType':'N/A', 'partKey' : 0, 'configKey' : 0,
              'old_State':None, 'State_change' : 0, 'beamMode': 'N/A', 'degraded': None,
              'write_files': False, 'exitOnStop': False, 'simulateDegraded':False, 'logDegradedAnalysis':False,
              'last_dbstore_snapshot':0, 'last_dbstore_run':0}
    
    while keepgoing(info):
      t0 = time.time()

      try:
        getinfo(info)
      except:
        log_message('[ERROR] Exception raised in method getinfo(); Re-trying...\n' + traceback.format_exc() )
        alarm = '[ERROR] Exception raised in method getinfo(); Re-trying...\n' + traceback.format_exc()
        dbstore_live('data.json', {'alarm':alarm, 'timestamp':int(time.time())}, info['id'], args.writewebfiles or info['write_files'])
        while t0+args.period > time.time(): pass
        continue
      
      try:
        if info['old_State'] == None or ( info['old_State'] not in ['Running','Configured'] and info['State'] == 'Configured' ):
          log_message( '[INFO] Init/Configure: (fethcing configuration, )intializing drivers and structure...' )
          if 'partKey' in info: log_message( '[INFO] Partition key: ' + str(info['partKey']) )
          if 'configKey' in info: log_message( '[INFO] Config key: ' + str(info['configKey']) )
          uc = UROSCell(
            args.key, 
            db_partKey=(int(info['partKey']) if 'partKey' in info else 0),
            db_configKey=(int(info['configKey']) if 'configKey' in info else 0) )
          uc.resetPersistentMonitoringData()
          init_data = uc.fetchMonitorData(onlyStructure=True)
          dbstore_live('init_data.json', init_data, info['id'], args.writewebfiles or info['write_files'])
          unid2map = uc.prepareDcsInitData(init_data)
        if info['old_State'] == 'Configured' and info['State'] == 'Running':
          log_message( '[INFO] Start detected: clearing persistent monitor data...' )
          uc.resetPersistentMonitoringData()
          info['degraded'] = False
          info['last_dbstore_snapshot'] = 0 #force immediate dbstore_snapshot
        if info['State'] == 'Running' and info['last_dbstore_run'] != info['runNumber']:
          dbstore_run(info['runNumber'], init_data)
          info['last_dbstore_run'] = info['runNumber']

        # retrieve monitoring data from boards
        monitorData = uc.fetchMonitorData()
      except:
        alarm = ''
        if degradedAllowed(info) and not info['degraded']:
          log_message( '[WARN] Reporting Running Degraded due to exception in hw access' )
          alarm += '[WARN] Reporting Running Degraded due to exception in hw access\n'
          degradedChange(info, True)
          info['degraded'] = True
        log_message( '[ERROR] Exception in hw access\n' + traceback.format_exc() )
        alarm += '[ERROR] Exception in hw access\n' + traceback.format_exc()
        dbstore_live('data.json', {'alarm':alarm, 'timestamp':int(time.time())}, info['id'], args.writewebfiles or info['write_files'])
        while t0+args.period > time.time(): pass
        continue
      
      try:
        # evaluate running degraded
        degraded, degraDetails = uc.isRunningDegraded(monitorData, unid2map)
        
        if info['logDegradedAnalysis']:
          log_message(  '[INFO] Forced logging of degraded analysis; Result = %s; Details... \n%s'%(
                        'True' if degraded else 'False', pprint.pformat(degraDetails)) )
        
        #print degradedAllowed(info), degraded, degraDetails, info['degraded']
        if degradedAllowed(info):
          if degraded and not info['degraded']:
            log_message( '[WARN] Reporting Running Degraded due to monitoring condition' )
            info['degraded'] = True
            degradedChange(info, True)
          elif not degraded and info['degraded']:
            log_message( '[INFO] Removing Running Degraded' )
            info['degraded'] = False
            degradedChange(info, False)
        elif info['beamMode'] != 'STABLE BEAMS' and info['State'] == 'Running':
          if info['simulateDegraded'] and not info['degraded']:
            log_message( '[WARN] Reporting Running Degraded triggered by file simulateDegraded' )
            info['degraded'] = True
            degradedChange(info, True)
          elif not info['simulateDegraded'] and info['degraded']:
            log_message( '[INFO] Removing Running Degraded' )
            info['degraded'] = False
            degradedChange(info, False)
        
        # convert and store data structure for web interface
        monitorDataWeb = uc.prepareWebData(monitorData)
        
        if info['degraded']:
          monitorDataWeb['warn'] = 'Running Degraded reported to Run Control due to number of problematic ROB links during a STABLE BEAMS run'
        
        monitorDataWeb['run_number'] = info['runNumber']
        monitorDataWeb['runType'] = info['runType']
        monitorDataWeb['partKey'] = info['partKey']
        monitorDataWeb['configKey'] = info['configKey']
        monitorDataWeb['beamMode'] = info['beamMode']
        timediff = time.time()-info['State_change']
        statusSince = ( ' (' +
                        ('%i h '%(timediff/3600) if timediff/3600 else '') + 
                        ('%i min '%((timediff%3600) / 60) ) + 
                        (' since %s')%(datetime.datetime.fromtimestamp(info['State_change']).strftime('%H:%M:%S' ))
                        + ')' )
        monitorDataWeb['run_state'] = info['State'] + (statusSince if info['State'] == 'Running' else '')
        
        if info['old_State'] == 'Running' and info['State'] != 'Running':
          dbstore_snapshot(info['old_runNumber'], uc.prepareLogData(info['old_monitorData']), info['old_monitorDataWeb'])
          time.sleep(1)
          dbstore_snapshot(info['old_runNumber'], uc.prepareLogData(monitorData), monitorDataWeb)
        elif (time.time()-info['last_dbstore_snapshot'] > 120) and info['State'] == 'Running':
          dbstore_snapshot(info['runNumber'], uc.prepareLogData(monitorData), monitorDataWeb)
          info['last_dbstore_snapshot'] = time.time()
        
        dbstore_live('data.json', monitorDataWeb, info['id'], args.writewebfiles or info['write_files'])
        
        # convert and store data for DCS diagram
        monitorDataDcs = { 'unid2map':unid2map, 'unid2metrics':uc.prepareDcsData(monitorData) }
        dbstore_live('dataDcs.json', monitorDataDcs, info['id'], args.writewebfiles or info['write_files'])
        
        if info['exitOnStop'] and info['old_State'] == 'Running' and info['State'] not in ['Running', 'Paused']:
          log_message( '[INFO] Exiting session of the monitor script (#%s) because an exit condition was met (exitOnStop)'%(info['id']) )
          break

        info['old_State'] = info['State']
        info['old_monitorData'] = monitorData
        info['old_monitorDataWeb'] = monitorDataWeb
        info['old_runNumber'] = info['runNumber']
      except:
        log_message( '[ERROR] Exception in urosmonitor main loop (continuing): \n' + traceback.format_exc() )
        alarm = '[ERROR] Exception in urosmonitor main loop (continuing): \n' + traceback.format_exc()
        dbstore_live('data.json', {'alarm':alarm, 'timestamp':int(time.time())}, info['id'], args.writewebfiles or info['write_files'])
        while t0+args.period > time.time(): pass
        continue

      while t0+args.period > time.time(): pass
    
  # configure the command line parser
  p = sp.add_parser('urosmonitor', help='Start the monitoring service')
  p.add_argument('-k', '--key', default='active', help='the key to load; default=active')
  p.add_argument('-p', '--period', default=2, type=float, help='the monitoring period in seconds; default=2')
  p.add_argument('-w', '--writewebfiles', action='store_true', default=False, help='Write the web monitoring data to json text files')
  p.set_defaults(func=run)
