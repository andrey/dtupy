def mtp2ros(sector, mtp, ch): 
  # returns ( sector, MBx, ROB ) according to ROS mapping, where sector 1-12, MBx 1-4 and rob 0-5 (0-6 for MB3)
  if mtp == 1:    return ( sector, ch/6 + 1, ch%6 )
  elif mtp == 2:  return ( sector, ch/7 + 3, ch%7 )
  else:           return ( ch+1,      4,        5)

def ros_mb_024(station, channel):
  if station != 3:    return (station - 1) * 6 + channel
  elif channel != 3:  return (station - 1) * 6 + channel - channel /4
  else:               return 24
def ros_024_mb(channel):
  channel = channel if channel<15 else 15 if channel == 24 else channel + 1
  return ((1, channel   ) if channel < 6  else
          (2, channel-6 ) if channel < 12 else
          (3, channel-12) if channel < 19 else
          (4, channel-19))

def ros2phys(sector, mbx, rob):
  # returns ( sector, station, rob ) according to real (physical) mapping
  remap = {(4,4,0):(4,4,2), (4,4,1):(4,4,3), (4,4,2):(4,4,4), (4,4,3):(13,4,2), (4,4,4):(13,4,3), (4,4,5):(13,4,4),
           (9,4,3):(13,4,0), (9,4,4):(13,4,1), (9,4,5):(10,4,3),
           (10,4,0):(14,4,0), (10,4,1):(14,4,1), (10,4,2):(14,4,2), (10,4,3):(10,4,0), (10,4,4):(10,4,1), (10,4,5):(10,4,2),
           (11,4,3):(4,4,0), (11,4,4):(4,4,1), (11,4,5):(14,4,3)}
  tup = (sector, mbx, rob) 
  return tup if tup not in remap else remap[tup]

def physwires(wheel, sector, station, rob):
  # station   phirobs(SL1/3) thetarobs (SL2)
  # mb1         4 (-> 49)      2 (-> 57)
  # mb1c        4 (-> 49)      2 (-> 48)   ---> W-1 S3, W+1 S4
  # mb2         4 (-> 60)      2 (-> 57)
  # mb2c        4 (-> 60)      2 (-> 48)   ---> W-1 S3, W+1 S4
  # mb3         5 (-> 72)      2 (-> 57)
  # mb3c        5 (-> 72)      2 (-> 48)   ---> W-1 S3, W+1 S4
  # mb4         6 (-> 96)
  # mb4(8,12)   6 (-> 92)
  # mb4(4,13)   5 (-> 72)
  # mb4(10,14)  4 (-> 60)
  # mb4(9,11)   3 (-> 48)
  weird_phis = {8:92,12:92,4:72,13:72,10:60,14:60,9:48,11:48}
  if station == 4 and sector in weird_phis: max_phi = weird_phis[sector]
  else: max_phi = ['nostation0',49,60,72,96][station]
  
  weird_thetas = [(-1,3), (+1,4)]
  if station == 4 : max_theta = 0
  elif (wheel,sector) in weird_thetas : max_theta = 48
  else: max_theta = 57
  
  phirobs  = (max_phi-1) / 16 + 1
  SL = ['1/3', '2'][rob / phirobs]

  SLrob = rob % phirobs
  wiresPerROB = 16 * (1 + rob / phirobs)
  max_wires = [max_phi, max_theta][rob / phirobs]
  wire_min = SLrob * wiresPerROB + 1
  wire_max = min( (SLrob + 1)*wiresPerROB , max_wires)
  
  return (SL, wire_min, wire_max)

def slotMap2mtp(crate, slotMap, input):
  # transforms a uROS-type mapping to a ofcu-type mapping
  wheel = 0 if crate =='0' else (-2 if crate == '-' else +1) + (slotMap-1)/6
  sector = ((slotMap-1)%6)*3 + (input-1)/2 + 1 if (slotMap-1)%6 < 4 else 0 if ((slotMap-1)%6 == 4 and input == 1) else None
  mtp = (input-1)%2 + 1 if (slotMap-1)%6 < 4 else 3 if ((slotMap-1)%6 == 4 and input == 1) else None
  return (wheel, sector, mtp)

sliceMap = {
  (3, 1): ('0', 5, 1),
  (3, 2): ('-', 11, 1),
  (3, 3): ('-', 5, 1),
  (4, 1): ('0', 4, 3),
  (4, 2): ('0', 4, 4),
  (4, 3): ('-', 10, 3),
  (4, 4): ('-', 10, 4),
  (4, 5): ('-', 4, 3),
  (4, 6): ('-', 4, 4),
  (5, 1): ('0', 4, 1),
  (5, 2): ('0', 4, 2),
  (5, 3): ('-', 10, 1),
  (5, 4): ('-', 10, 2),
  (5, 5): ('-', 4, 1),
  (5, 6): ('-', 4, 2),
  (6, 1): ('0', 3, 5),
  (6, 2): ('0', 3, 6),
  (6, 3): ('-', 9, 5),
  (6, 4): ('-', 9, 6),
  (6, 5): ('-', 3, 5),
  (6, 6): ('-', 3, 6),
  }
def sliceDecode(slot, channel):
  input = channel/12 + 1
  if (slot, input) in sliceMap: return sliceMap[(slot,input)]
  else: return None, None, None
  
fed2uroscrate = {1369:'-',1370:'0',1371:'+'}
def decode_uros_map(fed, slotMap, channel, slot):
  if fed in fed2uroscrate:
    crate = fed2uroscrate[fed]
    input = channel /12 + 1
  elif fed == 1368:
    crate, slotMap, input = sliceDecode(slot, channel)
    if crate  == None: return None
  else: return None
  
  ( wheel, sector, mtp ) = slotMap2mtp(crate, slotMap, input)
  if sector==None or mtp==None: return None
  mtp_ch = channel % 12
  ( ROS_sector, ROS_station, ROS_rob ) = mtp2ros( sector, mtp, mtp_ch )
  ( PHYS_sector, PHYS_station, PHYS_rob )  = ros2phys( ROS_sector, ROS_station, ROS_rob )
  ( SL, wire_min, wire_max ) = physwires( wheel, PHYS_sector, PHYS_station, PHYS_rob )
  
  mapStr = ('yb%i [s%i mtp%i.%i mb%i.%i] '+                 '[s%i st%i.%i (SL%s w%i-%i)]')%(
            wheel,ROS_sector,mtp,mtp_ch, ROS_station,ROS_rob, PHYS_sector, PHYS_station, PHYS_rob, SL, wire_min, wire_max )
  return {'str':mapStr, 'phys':[wheel, PHYS_sector, PHYS_station, PHYS_rob], 'numwires':(4 if SL=='2' else 8)*(1+wire_max-wire_min)}