################################################################################
## Functions to interact with the GBTX chip and SCA chip through the GBT protocol
################################################################################

import time


##########
## GBTX ##
##########

def configure_gbtx(self,filepath,GBTx_addr=1):
  """Configure the GBTx with the register map file given by filepath."""
  with open(filepath,'r') as f:
    file_data = f.readlines() ##read the map register
  for i in range(len(file_data)):
    self.gbtx_write_register (reg,file_data[i],0xff,GBTx_addr)
      
def gbtx_write_register(self,reg,value,mask=0xFF,GBTx_addr=1):
   """Write data on a register"""
   value_masked = (int(self.gbtx_read_register(reg)[0][1]) & (0xFF - mask)) | (value & mask)
   self.write('gbt.conf.ic_data',value_masked)        ## writes value at the "IC_data" register
   self.write('gbt.conf.gbtx_addr',GBTx_addr)  ## writes GBTx_addr at the "GBTx address" register
   self.write('gbt.conf.ic_rd_wr',1)           ## selects "write" operation
   self.write('gbt.conf.reg',reg)              ## writes reg at the "GBTx internal address" register and asserts a "send write" pulse

def gbtx_read_register(self,reg,mem_val_format='d',GBTx_addr=1,num_of_words=1):
  """Read data from a register"""
  # Configure IP
  self.write('gbt.conf.nb_to_read',num_of_words)  ## writes GBTx_addr at the "GBTx address" register

  # Send read command to GBTx
  self.write('gbt.conf.gbtx_addr',GBTx_addr)    ## writes GBTx_addr at the "GBTx address" register
  self.write('gbt.conf.ic_rd_wr',0)		## selects "read" operation
  self.write('gbt.conf.reg',reg)                ## writes reg at the "GBTx internal address" and asserts a "send read" pulse

  time.sleep(0.1)
  ic_empty = self.read('gbt.stat.rx_empty')     ## reads the status of the rx_empty register
  while ic_empty == 1:
    print ("FIFO empty")
    ic_empty = self.read('gbt.stat.rx_empty')
  mem_addr = self.read('gbt.stat.reg')
  self.write('gbt.conf.read_fifo',1)      ## sends a rd pulse
  self.write('gbt.conf.read_fifo',0)
  registers=[]
  formatter='{:d}'
  if mem_val_format=='h': formatter='{:02x}'
  elif mem_val_format=='b': formatter='{:08b}'
  while ic_empty == 0:
    ic_empty = self.read('gbt.stat.rx_empty')
    mem_val = self.read('gbt.stat.ic_data')
    registers.append([mem_addr,formatter.format(mem_val)])
    mem_addr += 1
    self.write('gbt.conf.read_fifo',1)      ## sends a rd pulse
    self.write('gbt.conf.read_fifo',0)

  return registers

def export_gbtx_conf (self,filepath,num_registers=366):
  """Saves the values of the registers in a file"""
  registers = []
  for i in range(num_registers+1):
    print "Reading register ",i
    if i != 124 and i != 125 :    #registers 124 and 125 don't respond, at least with thid OBDT, so they must be masked
      registers.append(format(int(self.gbtx_read_register(i)[0][1]),'02x'))
    else:
      registers.append("00")
    registers.append('\n')
  print registers

  with open(filepath,'w') as f:
    f.writelines(registers)

def gbtx_id(self):
   """Retunrs the chip ID"""
   return (int(self.gbtx_read_register(368)[0][1])<<8) + int(self.gbtx_read_register(367)[0][1])


#########
## SCA ##
#########


def reset_gbtsc (self):
  """Sends a command to reset gbt_sc module"""
  self.write('gbt.conf.reset_gbt_sc',1)
  self.write('gbt.conf.reset_gbt_sc',0)

def sca_send_reset (self):
  """Sends a HDLC send_reset command"""
  self.write('gbt.conf.send_reset',1)
  self.write('gbt.conf.send_reset',0)

def sca_send_connect (self):
  """Sends a HDLC send_connect command"""
  self.write('gbt.conf.send_connect',1)
  self.write('gbt.conf.send_connect',0)

def sca_write_command (self,channel,length,command,data,timeout = 1):
  """Writes data in a register of the SCA chip given by channel & command"""
  self.write('gbt.conf.id',1)
  self.write('gbt.conf.ch',channel)
  self.write('gbt.conf.len',length)
  self.write('gbt.conf.comm',command)
  self.write('gbt.conf.ec_data',data)

  t0 = time.time()
  while self.read('gbt.stat.reply_flag') != 1:
    if time.time()-t0 > timeout:
      print "[ERROR] timeout on waiting for gbt reply flag"
      return

  error_flag = self.read('gbt.stat.error')
  error_bits = format(error_flag,'08b')[::-1]
  error_msg = ('Generic error flag','Invalid channel request','Invalid command request','Invalid transaction number request','Invalid lenght','Channel not enabled','Channel currently busy','Command in treatment') 
  if error_flag != 0:
     for i in range(7):
	if error_bits[i] == '1':
	   print "[ERROR]",error_msg[int(i)]

def sca_masked_write (self,channel,length,command,data,mask):
   """Writes data in a register of the SCA chip only on bits given by mask"""
   data_in_reg = self.sca_read_command(channel,1,command+1,0)
   self.sca_write_command (channel,length,command,(data_in_reg & (0xFFFFFFFF - mask)) | (data & mask))

def sca_read_command (self,channel,length,command,data):
  """Reads data of a channel in the SCA chip """
  sca_write_command (self,channel,length,command,data)
  rx_data = self.read('gbt.stat.ec_data')
  return rx_data

def sca_enable_channel(self,channel=None,enable=1):
  """Enables/disables DAC channel"""
  channels = ['SPI','GPIO','JTAG','ADC','DAC']
  regs = [0,0,2,2,2]
  if channel == None:
     print "===INFO: CHANNEL NUMBERS==="
     print "SPI   : Channel 1"
     print "GPIO  : Channel 2"
     print "JTAG  : Channel 3"
     print "ADC   : Channel 4"
     print "DAC   : Channel 5"
     print "I2C_n : Channel 0x1n\n"
  elif channel == 1: self.sca_masked_write(0,1,2,0xFFFFFFFF*enable,0x02000000)
  elif channel == 2: self.sca_masked_write(0,1,2,0xFFFFFFFF*enable,0x04000000)
  elif channel == 3: self.sca_masked_write(0,1,6,0xFFFFFFFF*enable,0x08000000)
  elif channel == 4: self.sca_masked_write(0,1,6,0xFFFFFFFF*enable,0x10000000)
  elif channel == 5: self.sca_masked_write(0,1,6,0xFFFFFFFF*enable,0x20000000)
  elif channel >= 0x10 and channel <= 0x14: self.sca_masked_write(0,4,2,0xFFFFFFFF*enable,2**(3+channel-0x10)<<24)
  elif channel >= 0x15 and channel <= 0x1C: self.sca_masked_write(0,4,4,0xFFFFFFFF*enable,2**(channel-0x15)<<24)
  elif channel >= 0x1C and channel <= 0x1F: self.sca_masked_write(0,4,6,0xFFFFFFFF*enable,2**(channel-0x1C)<<24)
  else: print "Invalid channel number. Please, run command without arguments for INFO"
  enabled = ['disabled','enabled']
  reg = []
  regI2C = ''
  for i in range(3): reg.append(format(self.sca_read_command(0,1,2*i+3,0),'032b')[0:8])
  for i in range(5): print channels[i],'\t',enabled[int(reg[regs[i]][6-i])]
  regI2C = reg[2][5:]+reg[1]+reg[0][:5]
  for i in range(16): print "I2C",i,'\t',enabled[int(regI2C[15-i])]   

def sca_dac(self,output=None,value=0):
  """Sets the analog voltage level on the DAC at the given output. Value must be a number between 0 and 1 V."""
  if output == None:
     for i in range (4): print "Output",i+1,":",format(((self.sca_read_command(0x15,1,16*(i+1)+1,0))>>24)/255.00,'.4f'),"V"
  else:
     if value >= 0 and value <= 1:
        self.sca_write_command(0x15,4,16*output,int(value*255*(0x1000000)))
        print "DAC ",output," set to ",value
     else:
	print "Value must be between 0 and 1"

def sca_gpio(self,channel=None,value=None):
   """When no arguments given, displays the value of all the 11 gpio channels. Else, writes the value at the given channel"""
   direction = 0b00011001111
   tags = ['DEVRST_N','FF_EXIT_N','IO_CFG_INTF','SPI_EN\t','IntL\t','ModPrsL','ResetL\t','LPMode\t','LOS\t','MOD_ABS','TX_FAULT']
   self.sca_masked_write(2,4,0x10,0xFFFFFFFF,0x41)  # the bit 0 of dataout register must be '1' before setting it to output. 
                                                    # In other case, the fpga will be reset
     					            # the bit 6 must be '1' to enable the QSFP+
   self.sca_write_command(2,4,0x20,direction)
   if self.sca_read_command(2,1,0x21,0) != direction: 
      print 'Unable to write direction register. Try again'
   else:
      if channel == None: pass
      elif channel <= 11 and format(direction,'011b')[10-channel] == '1':
            self.sca_masked_write(2,4,0x10,0xFFFFFFFF*value,1<<channel)
      else: print "[ERROR]: Trying to write on an input or invalid channel"
      values = format(self.sca_read_command(2,1,1,0),'011b')
      print "===INPUTs==="
      for i in range(11):
         if format(direction,'011b')[10-i] == '0':
            print "Ch.",i,":",tags[i],'\t<=',values[10-i]
      print "===OUTPUTs==="
      for i in range(11):
         if format(direction,'011b')[10-i] == '1':
            print "Ch.",i,":",tags[i],'\t=>',values[10-i]
        

def sca_adc(self,channel):
   """Reads the ADC Voltage (0V to 1V)"""
   R1 = [20,20,39,0,51,22,0,22,13,13,3.3,10,20,22,39,51,10,100,100,0,0]
   for i in range(11): R1.append(0)
   R2 = 10
   self.sca_write_command(0x14,4,0x50,channel) 	# ADC_W_MUX
   self.sca_write_command(0x14,4,0x02,1)	# ACD_GO
   return self.read('gbt.stat.ec_data')/4095.00*(R1[channel]+R2)/R2

def sca_chip_id(self,SCA_version=2):
   """Reads the SCA chip ID.Default SCA version = 2"""
   print "SCA Chip ID: ",self.sca_read_command(0x14,1,(SCA_version-1)*64+145,0)  

def sca_i2c_read(self,channel,reg):
   """Reads the value of the given register of the device at the i2c channel"""
   dev_addr = [0x00,0x00,0x50]
   data_to_write = (dev_addr[channel]<<24)|reg<<16
   self.sca_write_command(channel+3,4,0x82,data_to_write)
   self.sca_write_command(channel+3,4,0x86,dev_addr[channel]<<24)
   return (self.read('gbt.stat.ec_data')>>16)&0xFF

def sca_temp(self,show=None):
   """Shows the temperature of a sensor"""
   self.sca_masked_write(0x14,4,0x60,0xFFFFFFFF,0x180000)  # enables current source for ADC channels 19 & 20
   coef = [[23484,-220.83],[25952,-243.01],[-578.3,409.28],[0.0035,2.1859]]
   tags = ["Power temp:\t","Optics temp:\t","SCA temp:\t","QSFP+ temp:\t"]
   o= u'\N{DEGREE SIGN}'
   temp=[]
   for i in range(2): temp.append('{:.02f}'.format(self.sca_adc(19+i)*coef[i][0]+coef[i][1]))
   temp.append('{:.02f}'.format(self.sca_adc(31)*coef[2][0]+coef[2][1]))
   temp.append('{:.02f}'.format((self.sca_i2c_read(2,22)<<8|self.sca_i2c_read(2,23))*coef[3][0]+coef[3][1]))
   if show == None: 
      for i in range(4): print tags[i],temp[i],o+"C"
   else: 
      return temp
