################################################################################
## The MOCO driver wrapper plus some helpers and the transitions
################################################################################

import time
import uros, tm7common, gbtsc
from dtupy.bcolors import bcolors

obdt_connectors = {
  'o':[234,36,50,48,44,46,41,204,220,217,219,233,214,237,15,169],
  'a':[222,45,49,52,43,209,53,51,215,235,238,28,30,31,20,17],
  'b':[14,16,108,13,221,89,90,57,88,198,42,38,37,39,186,0],
  'i':[6,47,207,54,208,205,206,212,225,18,101,12,110,111,126,125],
  'j':[112,188,187,154,170,179,180,182,183,199,195,193,189,191,203,190],
  'g':[4,211,210,228,230,224,236,25,21,106,103,127,131,172,85,151],
  'h':[218,150,158,160,56,86,75,87,173,174,178,200,202,201,5,26],
  'e':[33,27,10,231,232,109,213,239,226,223,7,227,2,8,114,113],
  'f':[229,139,141,143,161,59,63,65,58,60,175,171,177,181,185,184],
  'k':[104,35,98,11,1,3,24,9,121,102,123,107,138,128,129,130],
  'l':[216,55,91,93,66,72,79,76,81,78,74,64,70,176,162,69],
  'c':[119,115,96,29,32,19,34,22,142,105,137,120,124,152,149,165],
  'd':[40,62,73,153,148,155,156,157,159,166,61,82,77,68,146,147],
  'm':[122,117,23,99,100,116,118,134,132,136,135,144,145,163,164,167],
  'n':[140,92,94,95,97,67,71,80,83,168,196,192,194,84,133,197],
  }

class MOCO(uros.UROSController):
  isDAQsource = False
  fw_type_identifier = 0x04

  read                      = tm7common.read
  write                     = tm7common.write
  getNode                   = tm7common.getNode
  reg_num2name              = tm7common.reg_num2name
  gth_debuginfo             = tm7common.gth_debuginfo
  amc13link_reset_duration  = tm7common.amc13link_reset_duration
  ttc_err_ctr_clear         = tm7common.ttc_err_ctr_clear
  ttc_ctr_clear             = tm7common.ttc_ctr_clear
  measfreq                  = tm7common.measfreq
  ttc_stats                 = tm7common.ttc_stats
  verifyBoardTypeVersion    = tm7common.verifyBoardTypeVersion
  
  configure_gbtx            = gbtsc.configure_gbtx
  gbtx_write_register       = gbtsc.gbtx_write_register
  gbtx_read_register        = gbtsc.gbtx_read_register
  export_gbtx_conf          = gbtsc.export_gbtx_conf
  gbtx_id                   = gbtsc.gbtx_id
  
  reset_gbtsc               = gbtsc.reset_gbtsc
  sca_send_reset            = gbtsc.sca_send_reset
  sca_send_connect          = gbtsc.sca_send_connect
  sca_write_command         = gbtsc.sca_write_command
  sca_read_command          = gbtsc.sca_read_command
  sca_masked_write          = gbtsc.sca_masked_write
  sca_dac                   = gbtsc.sca_dac
  sca_enable_channel        = gbtsc.sca_enable_channel
  sca_gpio                  = gbtsc.sca_gpio
  sca_chip_id               = gbtsc.sca_chip_id
  sca_adc                   = gbtsc.sca_adc
  sca_i2c_read              = gbtsc.sca_i2c_read
  sca_temp                  = gbtsc.sca_temp
  
  def __init__(self, device):
    super(MOCO, self).__init__(device)
    self.storedReads = {}

  def dispatch(self):
    self.hw().dispatch()

  def tdchistogram(self, channel = 0, meastime = 5, filename = None):
    self.write('histogram.conf.mux',channel)
    self.write('histogram.conf.valid_pattern',1)
    self.write('histogram.conf.enable',0)
    self.write('histogram.conf.reset',1)
    self.write('histogram.conf.reset',0)
    t0 = time.time()
    self.write('histogram.conf.enable',1)
    while time.time() < t0 + meastime: pass
    self.write('histogram.conf.enable',0)
    

    histo = []
    for i in range(32):
      self.write('histogram.conf.select_bin',i)
      histo += [self.read('histogram.stat.selected_bin_value')]
      print i, histo[-1]
    
    if not filename:
      filename = 'output_ch%i_t%i.txt'%(channel, meastime)
    with open(filename, 'w') as f:
      f.write('\n'.join(map(str,histo)))

  def gbtlink_reset(self, reg = 'resetgbtfpga', cooltime = 1):
    '''Argument reg should be resetgbtfpga (default), manualResetTx, manualResetRx '''
    if reg not in ['resetgbtfpga', 'manualResetTx', 'manualResetRx' ]:
      raise Exception('invalid argument for gbtlink_reset: %s'%reg)
    self.write('gbt.conf.%s'%reg,1)
    self.dispatch()
    self.write('gbt.conf.%s'%reg,0)
    time.sleep(cooltime)

  def obdt_enable_elink_serial(self):
    self.write('gbt.conf.gbt_data_group_0', 0x600D)
    self.write('gbt.conf.gbt_data_group_1', 0x5C15)
    self.write('gbt.conf.elink_serial_fire', 0)
    self.write('gbt.conf.elink_serial_fire', 1)
    self.write('gbt.conf.elink_serial_fire', 0)


  def obdt_enable_chans(self,chanlist=None, cables=None):
    enable_regs = [0]*15
    enablelist = set([])
    if isinstance(chanlist, list):
      enablelist.update(chanlist)
    if isinstance(cables, str):
      for cable in cables:
        enablelist.update( obdt_connectors[cable] )
    print 'enabling channels:', sorted( enablelist )
    for chan in enablelist:
      enable_regs[chan/16] |= (1<<(chan%16))
    for i in range(15):
      self.obdt_write_conf(i, enable_regs[i])

  def obdt_write_conf(self, reg, value):
    self.write('gbt.conf.gbt_data_group_0',value)
    self.write('gbt.conf.gbt_data_group_1', 0x5700 + reg )
    self.write('gbt.conf.elink_serial_fire',0)
    self.write('gbt.conf.elink_serial_fire',1)
    self.write('gbt.conf.elink_serial_fire',0)
    

  ################################################################################
  ## board-level functions for cell operation
  ################################################################################
  
  def configureCmd(board, params_override = {}):
    """Runs the configure transition"""
    params = board.getParams(params_override)
    
    FWvers = board.verifyBoardTypeVersion( params )
    
    board.reset( 'external' )
    board.gbtlink_reset()
 
    print 'Enabling ipbus interface for gbtx slow control in moco'
    board.write('gbt.conf.gbtsc_intf',1)

    #print 'Enabling the gbtx chip watchdog'
    #board.gbtx_write_register(50,7)

    print 'Reset of the polarfire tdc module and then of the polarfire gbt module'
    board.write('gbt.conf.gbt_data_group_4',0x2)
    board.write('gbt.conf.gbt_data_group_4',0x0)
    board.write('gbt.conf.gbt_data_group_4',0x4)
    board.write('gbt.conf.gbt_data_group_4',0x0)

    print 'Enabling the elink serial interface in the polarfire'
    board.obdt_enable_elink_serial()
 
    print 'Enable testpulse output'
    board.obdt_write_conf(16,1)

    if 'obdt_cables' in params:
      print 'Enable the connected channels: %s'%params['obdt_cables']
      board.obdt_enable_chans( cables=params['obdt_cables'] )
  
  def startCmd(board):
    """Runs the start transition"""
    
  def stopCmd(board):
    """Runs the stop transition"""
  

