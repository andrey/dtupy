import os
import xml.etree.ElementTree as ET
from collections import OrderedDict

################################################################################
## General declarations and utilities
################################################################################

# Class AttrDict inherits from dict and allows to access attrDictObject[key] as attrDictObject.key
class AttrDict(dict):
  def __init__(self, *args, **kwargs):
    super(AttrDict, self).__init__(*args, **kwargs)
    self.__dict__ = self
    
def filterparams(params_by_scope, scopes, params_override = {}):
  params = {}
  for params_src in [params_by_scope[scope] for scope in scopes if scope in params_by_scope] + [params_override]:
    for p in params_src:
      params[p] = params_src[p]
  return params

## Function to be included as an amc13/processor function
def getParams(board, params_override = {}):
  '''Returns a dict containing the parameters applicable to this board, 
  overridden by the given parameter set, if given as argument'''
  return filterparams(board.s.params, board.context['paramfilters'], params_override)

################################################################################
## Creation of system
################################################################################
import sys
from dtupy.bcolors import bcolors

not_loaded = []

try:
  import uhal
  uhal.setLogLevelTo(uhal.LogLevel.ERROR)
except ImportError: not_loaded += ['uhal']
try:
  from amc13manager import AMC13
  AMC13.getParams = getParams
except ImportError: not_loaded += ['AMC13']
try:
  import urosmanager
  urosmanager.UROS.getParams = getParams
except ImportError: not_loaded += ['urosmanager']
try:
  import twinmuxmanager
  twinmuxmanager.TM7.getParams = getParams
except ImportError: not_loaded += ['twinmuxmanager']
try:
  import ab7manager
  ab7manager.AB7.getParams = getParams
except ImportError: not_loaded += ['ab7manager']
try:
  import mocomanager
  mocomanager.MOCO.getParams = getParams
except ImportError: not_loaded += ['mocomanager']

if ('uhal' in not_loaded) or ('AMC13' in not_loaded) or ('urosmanager' in not_loaded and 'twinmuxmanager' in not_loaded):
  print bcolors.red + 'Libraries not found: ' + ', '.join(not_loaded) + bcolors.reset


def make_board(hw, params, crate, slot):
  global UROS, TM7, urosmanager
  
  id = hw[crate][slot]
  
  if slot == 13: 
    boardParams = filterparams( params, ['daqttcs', id] )
    [t1, t2] = [uhal.getDevice(T, boardParams['uri_'+T], boardParams['address-table_'+T]) for T in ['T1', 'T2'] ]
    board = AMC13( t1, t2 )
    board.context = {'paramfilters': ['daqttcs', id] }
    
  else:
    boardParams = filterparams( params, ['processors',id] )
    hwtype = boardParams['hw-type'].lower() if 'hw-type' in boardParams else 'uros'
    boardParams = filterparams( params, ['processors', hwtype, id] )
    device  = uhal.getDevice(id, boardParams['uri'], boardParams['address-table'])
    
    if hwtype == 'tm7':
      if 'twinmuxmanager' in not_loaded: raise Exception( '[ERROR] failed importing twinmuxmanager module (is tm7controller installed?)' )
      else: board = twinmuxmanager.TM7(device)
    elif hwtype == 'ab7':
      if 'ab7manager' in not_loaded: raise Exception( '[ERROR] failed importing ab7manager module (is uroscontroller installed?)' )
      else: board = ab7manager.AB7(device)
    elif hwtype == 'moco':
      if 'mocomanager' in not_loaded: raise Exception( '[ERROR] failed importing mocomanager module (is uroscontroller installed?)' )
      else: board = mocomanager.MOCO(device)
    elif hwtype == 'uros':
      if 'urosmanager' in not_loaded: raise Exception( '[ERROR] failed importing urosmanager module (is uroscontroller installed?)' )
      else: board = urosmanager.UROS(device)
    else:
      raise Exception('Unknown hw-type parameter %s'%hwtype)
    board.context = {'paramfilters': ['processors', hwtype, id] }
  board.context.update( {'crate':crate, 'slot':slot, 'id':id } )
  return board

def make_system(hw, params, maskCreationException = False):
  s = AttrDict( {'params': params, 'hw':AttrDict(OrderedDict()) } ) # this makes sure iterator on hw keys always returns the same order
  for crate in hw:
    if crate in s.hw : raise Exception('Crate name "%s" is not allowed'%(crate))
    s.hw[crate] = AttrDict( {'slots':[], 'processors':[]} )
    
    try: # create ipmi helpers
      ipmistr = 'ipmitool -P password -I lan -H %s picmg %s %i'
      s.hw[crate]['mch'] = 'mch-%s-01'%params[hw[crate][13]]['uri_T1'].split('=amc-')[1].split('-13-t1')[0]
      s.hw[crate]['ipmi_activate_str'] =    { slot: ipmistr%(s.hw[crate]['mch'], 'activate'  , slot+4) for slot in range(1,14)}
      s.hw[crate]['ipmi_deactivate_str'] =  { slot: ipmistr%(s.hw[crate]['mch'], 'deactivate', slot+4) for slot in range(1,14)}
      s.hw[crate]['ipmi_activate'] =        (lambda crate: (lambda slot: os.system(s.hw[crate]['ipmi_activate_str'][slot])  ) )(crate)
      s.hw[crate]['ipmi_deactivate'] =      (lambda crate: (lambda slot: os.system(s.hw[crate]['ipmi_deactivate_str'][slot])) )(crate)
    except Exception as e:
      print "[WARN] Exception raised when trying to create ipmi control strings for crate %s"%crate
    
    for slot in hw[crate]:
      try:
        if slot != 13: s.hw[crate]['slots'] += [ slot ]
        board = make_board(hw, params, crate, slot )
        board.s = s
        if slot != 13:
          s.hw[crate]['processors'] += [board]
          s.hw[crate]['slot%i'%( slot )] = board
        else:
          s.hw[crate]['amc13'] = board
      except:
        print "[ERROR] Exception raised when trying to create board %s"%hw[crate][slot]
        if not maskCreationException: raise
      
  return s
  
################################################################################
## Loading system hw/params from TXT
################################################################################

txt_hw_filename = 'partition.txt'

def txt_load_cleanup_tokenize(filename, txt):
  if not txt and filename:
    with open(filename, 'r') as f:
      txt = f.read()
  txt = txt.replace('\t',' ') # convert tab to space
  while '  ' in txt: txt = txt.replace('  ',' ') # reduce separators to one space
  txt = txt.split('\n') #converts to list of lines
  txt = [line.split('#')[0].strip() for line in txt] # removes comments and leading/trailing spaces
  txt = [line.split(' ') for line in txt if line] # converts each non-empty line to list of info tokens
  return txt


def parse_hw_txt(hw, filename='', txt=''):
  for line in txt_load_cleanup_tokenize(filename, txt):
    if len(line)!= 13: raise Exception("Loadkey: each line in a txt partition should have 13 tokens")
    crate = line[0]
    hw[crate] = {}
    for slot in range(1,14):
      if slot==13 or line[slot] in ['%i'%slot, '%02i'%slot]: # amc13 always created, processors if token is the slot number
        hw[crate][slot] = '%s_%02i'%(crate,slot)
      elif line[slot] not in ['X','x']:
        raise Exception("Loadkey: each slot token in a txt partition should be either 'x' or the slot number")

def parse_params_txt(params, filename='', txt=''):
  for line in txt_load_cleanup_tokenize(filename, txt):
    if len(line)!= 3: raise Exception("Loadkey: each line in a txt params should have 3 tokens")
    [scope, parameter, value] = line
    scope = 'daqttcs' if scope == 'amc13' else scope.lower()
    if scope not in params: params[scope] = {}
    try:
      params[scope][parameter] = int(value,0) # stores the value in the dict parsing decimal/hex number
    except ValueError as e:
      params[scope][parameter] = value # or as string if the integer conversion failed

################################################################################
## Loading system hw/params from XML
################################################################################

xml_hw_filename = 'init.json'

param_converters = {
  'uint': lambda txt: int(txt,0),
  'int':int,
  'bool': {'true':True,'false':False}.__getitem__, # raises exception otherwise
  'float':float,
  'string': str,
  'vector:uint':  lambda txt: [int(item.strip()) for item in txt.split(',')],
  'vector:int':   lambda txt: [int(item.strip()) for item in txt.split(',')],
  'vector:float':   lambda txt: [float(item.strip()) for item in txt.split(',')],
  'vector:string':  lambda txt: [str(item.strip()) for item in txt.split(',')],
#  'table':,
  }

def parse_params_xml(d, filename='', txt=''):
  tag0 = ET.parse(filename).getroot()
  if tag0.tag not in ['algo','infra']:
    print 'Loadkey: skipping file "%s" because root tag is not "algo" or "infra", but "%s"'%(filename,tag0.tag)
    return
  if tag0.attrib != {'id':'TwinMux'}:
    raise Exception('XML file "%s" root tag should have unique attribute id="TwinMux"'%(filename))
  for tag1 in tag0:
    if tag1.tag != 'context':
      raise Exception('XML file "%s" secondary tags must be "context", not "%s"'%(filename,tag1.tag))
    if not 'id' in tag1.attrib:
      raise Exception('XML file "%s" context tag must have an id attribute'%(filename))
    context = tag1.attrib['id']
    if not context in d: d[context] = {}
    for tag2 in tag1:
      if tag2.tag != 'param':
        raise Exception('XML file "%s" children of a context tag must be "param", not "%s"'%(filename,tag2.tag))
      if not all([key in tag2.attrib for key in ['id','type'] ]):
        raise Exception('XML file "%s" param tag must have an id attribute'%(filename))
      param = tag2.attrib['id']
      if not tag2.attrib['type'] in param_converters:
        raise Exception( 'XML file "%s" param tag type "%s" is of unknown kind'%(filename, tag2.attrib['type']) )
      try:
        value = param_converters[tag2.attrib['type']]( tag2.text.strip() )
      except Exception as e:
        print '\n\n'+'#'*80+'\nXML file "%s": error parsing param id "%s" of type "%s" with text "%s"'%(filename, param, tag2.attrib['type'], tag2.text)
        raise e
      if param in context:
        raise Exception('XML file "%s": trying to incorporate param "%s" already existing in context "%s"'%(filename, param, context))
      d[context][param] = value


def parse_hw_xml(d, filename='', txt=''):
  tag0 = ET.parse(filename).getroot()
  if tag0.tag != 'system':
    raise Exception('XML file "%s" root tag "%s" is of "system" type'%(filename,tag0.tag))
  if tag0.attrib != {'id':'TwinMux'}:
    raise Exception('XML file "%s" root tag should have unique attribute id="TwinMux"'%(filename))
  for tag1 in tag0:
    if tag1.tag not in ['creator','crates', 'processors','daqttc-mgrs', 'links', 'connected-feds', 'excluded-boards']:
      raise Exception('XML file "%s" secondary tag "%s" not recognized'%(filename,tag1.tag))
    if tag1.tag in d:
      raise Exception('XML file "%s" more than one instance of secondary tag "%s" found'%(filename,tag1.tag))
    if tag1.tag == 'creator': d['creator'] = tag1.text.strip()
    elif tag1.tag == 'crates':
      d['crates'] = {}
      for tag2 in tag1:
        if ( tag2.tag != 'crate' or 'id' not in tag2.attrib or len(tag2) !=2 or 
          not all( [key in [tag3.tag for tag3 in tag2] for key in ['description','location'] ] ) ):
          raise Exception('XML file "%s" problem parsing tag "%s", child of crates'%(filename,tag2.tag))
        d['crates'][tag2.attrib['id']] = {}
        for tag3 in tag2:
          d['crates'][tag2.attrib['id']][tag3.tag] = tag3.text.strip()
    elif tag1.tag == 'daqttc-mgrs':
      d['daqttc-mgrs'] = {}
      for tag2 in tag1:
        if ( tag2.tag != 'daqttc-mgr' or 'id' not in tag2.attrib or len(tag2) !=9 or 
          not all( [key in [tag3.tag for tag3 in tag2] for key in ['creator','role','crate','slot','uri','address-table','fed-id'] ] ) ):
          raise Exception('XML file "%s" problem parsing tag "%s", child of daqttc-mgrs'%(filename,tag2.tag))
        d['daqttc-mgrs'][tag2.attrib['id']] = {}
        for tag3 in tag2:
          if tag3.tag in ['slot', 'fed-id']: 
            d['daqttc-mgrs'][tag2.attrib['id']][tag3.tag] = int(tag3.text.strip())
          elif tag3.tag in ['uri','address-table']:
            if not tag3.tag in d['daqttc-mgrs'][tag2.attrib['id']]: 
              d['daqttc-mgrs'][tag2.attrib['id']][tag3.tag] = {}
            d['daqttc-mgrs'][tag2.attrib['id']][tag3.tag][tag3.attrib['id']] = tag3.text.strip()
          else:
            d['daqttc-mgrs'][tag2.attrib['id']][tag3.tag] = tag3.text.strip()
    elif tag1.tag == 'processors':
      d['processors'] = {}
      for tag2 in tag1:
        if ( tag2.tag != 'processor' or 'id' not in tag2.attrib or 
          not all( [tag3.tag in ['creator','hw-type','role','uri','address-table','crate','slot', 'rx-port','tx-port'] for tag3 in tag2] ) or
          not all( [key in [tag3.tag for tag3 in tag2] for key in ['creator','hw-type','role','uri','address-table','crate','slot'] ] ) ):
          raise Exception('XML file "%s" problem parsing tag "%s", child of processors'%(filename,tag2.tag))
        d['processors'][tag2.attrib['id']] = {}
        for tag3 in tag2:
          if tag3.tag == 'slot': 
            d['processors'][tag2.attrib['id']][tag3.tag] = int(tag3.text.strip())
          elif 'port' in tag3.tag:
            continue # following code was causing problems with HO ports, and we don't use ports in python cell at all
            if not tag3.tag in d['processors'][tag2.attrib['id']]: 
              d['processors'][tag2.attrib['id']][tag3.tag] = {}
            nameprefix = tag3.attrib['name'].split('[')[0]
            nameleft = int(tag3.attrib['name'].split('[')[1].split(':')[0])
            nameright = int(tag3.attrib['name'].split('[')[1].split(':')[1].split(']')[0])
            pidleft = int(tag3.attrib['pid'].strip('[]').split(':')[0])
            pidright = int(tag3.attrib['pid'].strip('[]').split(':')[1])
            if (pidright-pidleft != nameright-nameleft): 
              raise Exception('XML file "%s" width of port does not match: "%s" vs "%s"'%(tag3.attrib['name'],tag3.attrib['pid']))
            for i in range (nameleft, nameright):
              d['processors'][tag2.attrib['id']][tag3.tag][nameprefix+'%02i'%i] = pidleft + i - nameleft
          else:
            d['processors'][tag2.attrib['id']][tag3.tag] = tag3.text.strip()
    elif tag1.tag == 'excluded-boards':
      d['excluded-boards'] = []
      for tag2 in tag1:
        if ( tag2.tag != 'exclude' or 'id' not in tag2.attrib ):
          raise Exception('XML file "%s" problem parsing tag "%s", child of excluded-boards'%(filename,tag2.tag))
        d['excluded-boards'] += [tag2.attrib['id']]

def flattenXMLdata(xmlhw,xmlparams):
  hw, params = {}, xmlparams
  for crate_xml in xmlhw['crates']:
    crate = crate_xml.replace('-','m').replace('+','p') # this is to be able to use the AttrDict completion
    hw[crate] = {}
    for board_type in [bt for bt in ['daqttc-mgrs', 'processors'] if bt in xmlhw]: 
      for id in xmlhw[board_type]:
        if xmlhw[board_type][id]['crate'] == crate_xml:
          slot = xmlhw[board_type][id]['slot']
          hw[crate][slot] = id
          if not id in params: params[id]={}
          if board_type == 'daqttc-mgrs':
            params[id]['fed'] = xmlhw[board_type][id]['fed-id']
          else:
            params[id]['hw-type'] = xmlhw[board_type][id]['hw-type']
          for hwparam in ['address-table','uri']:
            if board_type == 'processors':
              params[id][hwparam] = xmlhw[board_type][id][hwparam]
            else:
              params[id][hwparam+'_T1'] = xmlhw[board_type][id][hwparam]['t1']
              params[id][hwparam+'_T2'] = xmlhw[board_type][id][hwparam]['t2']
  
  return hw, params

  
################################################################################
## Load the appropriate files, call the system building function with the data
################################################################################

  
def loadkey(key = 'active', db_partKey=0, db_configKey=0, db_configVers=0, maskCreationException = False):
  hw, params = {}, {}
  
  if (db_partKey>2300 and db_configKey>2300):
    parse_hw, parse_params, parse_posptro = parse_hw_txt, parse_params_txt, lambda hw,params:(hw,params)
  
    import cx_Oracle
    username = 'cms_dt_elec_conf'
    service = 'cms_omds_lb'
    password = 'cmsonr2008'
    con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
    con.autocommit = 1
    cur = con.cursor()
    # Fetch hw CLOB
    cur.execute( 'select DATA_CLOB from UROS_PARTITION where DAQPARTITIONID = :pk', {'pk':db_partKey} )
    hwclob = cur.fetchone()[0].read()
    parse_hw(hw, txt=hwclob)
    print 'Partition retrieved from database, DAQPARTITIONID = %i'%db_partKey
    
    # Fetch DAQCONFIGID
    if db_configVers:
      cur.execute( 'select DAQCONFIGID from DAQCONFIGSVERSIONSET where DAQCONFIGKEY = :ck and DAQCONFIGKEYVERSION = :kv', {'ck':db_configKey,'kv':db_configVers} )
    else:
      cur.execute( 'select DAQCONFIGID from DAQCONFIGSVERSIONSET where DAQCONFIGKEY = :ck order by DAQCONFIGKEYVERSION desc', {'ck':db_configKey } )
    db_configId = cur.fetchone()[0]
    # Fetch params CLOB and MAPID
    cur.execute( 'select DATA_CLOB,MAPID from UROS_CONF where DAQCONFIGID = :cid', {'cid':db_configId} )
    row = cur.fetchone()
    paramsclob = row[0].read()
    parse_params(params, txt=paramsclob)
    db_mapId = row[1]
    print 'Configuration retrieved from database, DAQCONFIGID = %i'%db_configId
    # Fetch map CLOB
    cur.execute( 'select DATA_CLOB from UROS_MAP where ID = :mid', {'mid':db_mapId} )
    mapclob = cur.fetchone()[0].read()
    parse_params(params, txt=mapclob)
    
    cur.close()
    con.close()

  else:
    folder = os.path.expandvars('${DTUPY_ETC}/keys/%s'%key)
    
    if xml_hw_filename in os.listdir(folder): # it is a twinmux-style (xml) key
      hw_filename, usual_files, skipfiles = xml_hw_filename, [], ['config.xml']
      parse_hw, parse_params, parse_posptro = parse_hw_xml, parse_params_xml, flattenXMLdata
    elif txt_hw_filename in os.listdir(folder): # it is a TXT style key
      hw_filename, usual_files, skipfiles = txt_hw_filename, ['map.txt', 'params.txt'], []
      parse_hw, parse_params, parse_posptro = parse_hw_txt, parse_params_txt, lambda hw,params:(hw,params)
    else:
      raise Exception( 'Neither "%s" nor "%s" files found in the key folder'%(xml_hw_filename, txt_hw_filename) )
    
    parameter_files = []
    for filename in os.listdir(folder):
      if filename == hw_filename:
        print '[INFO] Loadkey: parsing file "%s" as hw definition'%hw_filename
        parse_hw(hw, folder +'/'+ hw_filename)
      elif filename in skipfiles or filename[0]=='.':
        print 'Loadkey: skipping file "%s" as not a parameters definition file'%filename
      else:
        rarefilename = True if usual_files and not filename in usual_files else False
        parameter_files += [(bcolors.yellow if rarefilename else '')+filename+(bcolors.reset if rarefilename else '')]
        parse_params(params, folder +'/'+ filename)
    print '[INFO] Loadkey: parsing as parameters definition the files %s'%(', '.join(parameter_files))
  
  hw,params = parse_posptro(hw,params)
  return make_system(hw, params, maskCreationException)




