import pprint

regdump_version = 2 # Change this when cpp version changes

def testandadd(name,t,reglist):
    if name in t.hw().getNodes() and name not in reglist:
        reglist += [name]

def create_reglist(t):
    reglist = []
    reglist += ['data_frame.data_frame.stat0.tm7_mc_delay']
    reglist += ['payload.dt_sector.stat0.locked']
    reglist += ['payload.dt_sector.stat0.d_locked']
    for i in range(4):
        for j in range(5):
            testandadd('payload.dt_sector.stat%i_st%i.delay_done'%(i,j), t, reglist)
            testandadd('payload.dt_sector.stat%i_st%i.word_done'%(i,j), t, reglist)
            testandadd('payload.dt_sector.stat%i_st%i.parity_up_errors'%(i,j), t, reglist)
            testandadd('payload.dt_sector.stat%i_st%i.parity_down_errors'%(i,j), t, reglist)
            testandadd('payload.dt_sector.stat%i_st%i.trigger_rate'%(i,j), t, reglist)
            for k in range(8):
                testandadd('payload.dt_sector.stat%i_st%i.tap_out_%i'%(i,j,k), t, reglist)
                testandadd('payload.dt_sector.stat%i_st%i.bitslip_cnt_%i'%(i,j,k), t, reglist)
                testandadd('payload.dt_sector.stat%i_st%i.wordslip_cnt_%i'%(i,j,k), t, reglist)
    for i in range(5):
        testandadd('super_sector.super_sector.stat0_st%i.super_rate'%(i), t, reglist)
        testandadd('super_sector.super_sector.stat0_st%i.rpc_rate'%(i), t, reglist)
    for name in t.hw().getNodes('readout.stat.*'):
        if not t.hw().getNode(name).getNodes():
            testandadd(name, t, reglist)
    for name in t.hw().getNodes('ttc\.cmd_ctrs\..*'):
        testandadd(name, t, reglist)
    
    reglist += ['ttc.csr.stat1.evt_ctr'] # minus 1
    reglist += ['ttc.csr.stat0.bunch_ctr']
    reglist += ['ttc.csr.stat2.orb_ctr']
    reglist += ['ttc.csr.stat3.single_biterr_ctr']
    reglist += ['ttc.csr.stat3.double_biterr_ctr']
    reglist += ['ttc.csr.stat0.bc0_lock']
    reglist += ['ctrl.csr.stat.clk40_lock']
    
    return reglist

def generate_cpp(t, reglist):
    txt = '/// Retrieval of monitoring parameters for db storage\n'
    txt += '/// Automatically generated file: do not edit\n\n'
    txt += '#include "tm7/TM7Controller.hpp"\n\n'
    #txt += 'void tm7::TM7Controller::dumpRegs(REGISTER_DUMP *rd)\n{\n'
    txt += 'void tm7::TM7Controller::dumpRegs(std::ostream & stream)\n{\n'
    #txt += '  rd->version = %i;\n\n'%regdump_version
    txt += '  uhal::ValWord<uint32_t> valwords[%i];\n\n'%len(reglist)
    i = 0
    for r in sorted(reglist):
        txt += '  valwords[%i] = getNode("%s").read();\n'%(i,r)
        i+=1
    txt += '\n  dispatch();\n\n'
    #txt += '  for (int i=0;i<%i;i++) rd->dump[i] = (uint32_t)valwords[i];\n'%len(reglist);
    txt += '  char tempOutString[12];\n'
    txt += '  sprintf( tempOutString, "%%08x", %i );\n'%regdump_version
    txt += '  stream << "Dump format version: " << tempOutString << std::endl;\n'
    txt += '  for ( uint32_t i = 0; i < 198; i++ ){\n'
    txt += '    sprintf( tempOutString, "%08x ", (uint32_t)valwords[i] );\n'
    txt += '    stream << tempOutString;\n'
    txt += '  }\n'
    txt += '}\n'
    
    return txt

reglist = []

def regdump(board, verbose = True):
    # if the reglist doesn't exist, create it so that it can be reused in this same session
    global reglist
    if not reglist: reglist = create_reglist(board)
    # create the dump
    dump = '%08x\n'%regdump_version
    values = [board.read(r, nodispatch = True) for r in sorted(reglist)[:-1]]
    values += [board.read(sorted(reglist)[-1])] #latest value with dispatch()
    for i in range(len(values)):
        dump += '%08x '%int(values[i])
        if verbose: dump += sorted(reglist)[i] + '\n'
    return dump


    
if __name__ == '__main__':
    from dtupy import systembuilder
    hw. params = systembuilder.loadkey()
    
    reglist = create_reglist(hw.crate_0.slot1)

    for name in sorted(reglist):
        print bin(tm7s[1].hw().getNode(name).getMask()).count('1'), '\t',name

    print generate_cpp(tm7s[1], reglist)
