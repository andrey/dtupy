from dtupy.systembuilder import loadkey
import math

################################################################################
## TwinMux Cell Class definition
################################################################################

class TWINMUXCell(object):
  def __init__(self, key):
    self.s = loadkey(key)

  def configure(self):
    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for amc13 in amc13s: amc13.resetCmd()
    for amc13 in amc13s: amc13.configTTCCmd()
    for tm7 in tm7s: tm7.configureCmd()
    for amc13 in amc13s: amc13.configDAQCmd()
  
  def start(self):
    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for tm7 in tm7s: tm7.armCmd()
    for tm7 in tm7s: tm7.requestMcSyncCmd()
    for amc13 in amc13s: amc13.startCmd()

