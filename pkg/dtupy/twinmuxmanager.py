import tm7, tm7common

class TM7(tm7.TM7Controller):
  isDAQsource = True
  
  read                      = tm7common.read
  write                     = tm7common.write
  getNode                   = tm7common.getNode
  reg_num2name              = tm7common.reg_num2name
  gth_debuginfo             = tm7common.gth_debuginfo
  amc13link_reset_duration  = tm7common.amc13link_reset_duration
  ttc_err_ctr_clear         = tm7common.ttc_err_ctr_clear
  ttc_ctr_clear             = tm7common.ttc_ctr_clear
  measfreq                  = tm7common.measfreq
  ttc_stats                 = tm7common.ttc_stats
  
  def __init__(self, device):
    super(TM7, self).__init__(device)
    self.storedReads = {}

  ################################################################################
  ## uros-level functions for cell operation
  ################################################################################
  

  def configureCmd(driver, params_override = {}):
    """Runs the configure transition"""
    params = driver.filterparams(params_override)
    
    # reset driver
    driver.reset(params['clockSource'])
    
    alignParams = { key:params[key] for key in ['skip_align_l','skip_align_h',
      'skip_check_l','skip_check_h','station_delay','maskAlignLsb','maskAlignMsb',
      'dtRoDelay'] }
    for i in range(5):
      for pname in ['etaDelay', 'shift', 'filter', 'bitSlip', 'wordSlip']:
        alignParams[pname + '_%i'%i] = params[pname][i]
      for j in range(8):
        alignParams['tapOff_%i_%i'%(i,j)] = params['tapOff_%i'%i][j]
    driver.configureDtInputLinks( alignParams );
  
    configReadoutParams = { key:params[key] for key in ['tm7BcnIncr','dt_disable',
      'dt_latency','dt_window','rpc_disable','rpc_latency','rpc_bxMin','rpc_bxMax',
      'ho_disable','ho_latency','ho_window'] }
    for i in range(4): configReadoutParams['tts_thresholds_%i'%i] = params['tts_thresholds'][i]
    driver.configureReadout( configReadoutParams );
  
    configAlgoParams = { key:params[key] for key in ['super_sectorDelay','super_en',
      'super_rpcOnly','super_rpcQual','super_rpcWinPhi','super_rpcWinPhiB',
      'super_dtQual','super_rpcOnlyRoEn','super_rpcOnlyOnly'] }
    driver.configureAlgo( configAlgoParams, params['super_arctgLut'] );
  
    configOutParams = { key:params[key] for key in ['noDataStartBXbmtf','noDataStopBXbmtf',
      'noDataStartBXomtf','noDataStopBXomtf','BC0Mask','valid_int'] }
    driver.configureOutputs( configOutParams );
  
    driver.maskOutputs(params["station_mask"]);
  
    configTTParams = { key:params[key] for key in ['tt_mask','tt_delay','tt_en'] }
    driver.configureTT( configTTParams );
  
  def armCmd(driver):
    """Arms the DT input links"""
    driver.armDtInputLinks()
    
  def requestMcSyncCmd(driver, params_override = {}):
    """Requests Minicrate Sync patterns"""
    params = driver.filterparams(params_override)
    
    if params['requestMcSyncDTwide']:
      #print 'TwinMux requested DT-wide sync command from minicrates'
      driver.requestSyncDT( params['mcSyncServer'], params['mcSyncPort'] )
    elif params['requestMcSyncSector']:
      #print 'TwinMux requested sector sync command from minicrates'
      driver.requestSyncSector( params['mcSyncServer'], params['mcSyncPort'] )
    #else: print "TwinMux didn't request sync command from minicrates"
  
  def TTenableCmd(driver):
    """Enable TT"""
    driver.setTTenable(1)
  
  def TTdisableCmd(driver):
    """Enable TT"""
    driver.setTTenable(0)
  
  def stationMaskCmd(driver, params_override = {}):
    """Writes station mask"""
    params = driver.filterparams(params_override)
    driver.maskOutputs(params['station_mask'])
