import struct
from bitfield import bf
from bcolors import bcolors

def printUnpacked(evt, slotTypes):
  print 'EVid = %i, BXid = %i, OrN = %i(0x%X), size = %i'%(evt['LV1_id'],evt['BX_id'],evt['OrN'],evt['OrN'],evt['Evt_lgth'])
  if slotTypes:
    if isinstance(slotTypes,str):
      slotTypes = { n: slotTypes for n in range(1,13) }
    for slot in [amc['AmcNo'] for amc in evt['AMCs']]:
      if slotTypes[slot].lower() == 'ab7':
        ab7print( evt , slot)
      elif slotTypes[slot].lower() == 'uros':
        urosprint( evt , slot)


def unpack(evt, slotTypes = None):
  evt = unpack_amc13(evt)
  if slotTypes:
    if isinstance(slotTypes,str):
      slotTypes = { n: slotTypes for n in range(1,13) }
    for slot in [amc['AmcNo'] for amc in evt['AMCs']]:
      if slotTypes[slot].lower() == 'ab7':
        ab7unpack( evt , slot)
      elif slotTypes[slot].lower() == 'uros':
        urosunpack( evt , slot)
  
  return evt

roberr = {                                                   
  0 : 'Hit lost in group 0 from read-out fifo overflow',
  1 : 'Hit lost in group 0 from L1 buffer overflow',
  2 : 'Hit error have been detected in group 0',
  3 : 'Hit lost in group 1 from read-out fifo overflow',
  4 : 'Hit lost in group 1 from L1 buffer overflow',
  5 : 'Hit error have been detected in group 1',
  6 : 'Hit data lost in group 2 from read-out fifo overflow',
  7 : 'Hit lost in group 2 from L1 buffer overflow',
  8 : 'Hit error have been detected in group 2',
  9 : 'Hit lost in group 3 from read-out fifo overflow',
  10: 'Hit lost in group 3 from L1 buffer overflow',
  11: 'Hit error have been detected in group 3',
  12: 'Hits rejected because of programmed event size limit',
  13: 'Event lost (trigger fifo overflow)',
  14: 'Internal fatal chip error has been detected',
  }
def urosunpack( evt , slot ):
  for amc in evt['AMCs']:
    if amc['AmcNo'] == slot:
      amc['tts'] = amc['UserData'] & 0xF
      amc['global_flags'] = (amc['UserData']>>4) & 0xFFF
      amc['slotMap'] = (amc['BoardID']) & 0xF
      amc['fwvers'] = (amc['BoardID']>>8) & 0xFF
      
      hits = []
      errors = []
      mtpflags = {}
      gstatus = []
      for w in amc['payload']:
        if w[61:64] == 1:
          for offset in [0,32]:
            if w[offset:offset+29] == 0x1FFFFFFF:
              pass
            elif w[offset+28] == 0:
              hits += [{'data':w[offset:offset+28]}]
              hits[-1]['robid'] = hits[-1]['data'][21:28]
              hits[-1]['tdcid'] = hits[-1]['data'][19:21]
              hits[-1]['chid'] = hits[-1]['data'][14:19]
              hits[-1]['time'] = hits[-1]['data'][0:14]
              
            else:
              errors += [{'data':w[offset:offset+28]}]
              errors[-1]['robid'] = errors[-1]['data'][21:28]
              errors[-1]['tdcid'] = errors[-1]['data'][19:21]
              errors[-1]['flag'] = errors[-1]['data'][0:15]
              errors[-1]['txt'] = []
              for i in range(15):
                if (errors[-1]['flag']>>i)&1:
                  errors[-1]['txt'] += [roberr[i]]
        elif w[60:64] in range(8,14):
          for i in range(12):
            mtpflags[12*(w[60:64]-8)+i] = w[i*5:(i+1)*5]
        elif w[60:64] == 0xF:
          gstatus += [w]

      amc['hits'] = hits
      amc['errors'] = errors
      amc['mtpflags'] = mtpflags
      amc['gstatus'] = gstatus

def urosprint( evt , slot ):
  for amc in evt['AMCs']:
    if amc['AmcNo'] == slot:
      print 'AMC%i, BXid = %i, tts = %i, global_flags = %i, slotMap = %i, fw_version = %i'%(amc['AmcNo'], amc['BX_id'], amc['tts'], amc['global_flags'], amc['slotMap'], amc['fwvers'])
      if amc['hits']:
        print '  Hits:'
        for hit in amc['hits']:
          print '    - robid=%i, tdcid=%i, chid=%i, time=%i'%(hit['robid'], hit['tdcid'], hit['chid'], hit['time'])
      if amc['errors']:
        print '  ROB Errors:'
        for err in amc['errors']:
          print '    - robid=%i, tdcid=%i, flag=0x%X (%s)'%(err['robid'], err['tdcid'], err['flag'], '|'.join(err['txt']))
      #print '  MTP Flags:'
      #for mtpf in amc['mtpflags']:
      #  print '    - ch=%i, flag=%i'%(mtpf, amc['mtpflags'][mtpf])
      #print '  Global Status:'
      #for gs in amc['gstatus']:
      #  print '    - 0x%X'%(int(gs))
  
def ab7unpack( evt , slot ):
  for amc in evt['AMCs']:
    if amc['AmcNo'] == slot:
      amc['lasttpbx'] = amc['UserData'] & 0xFFF
      amc['tptimer'] = (amc['UserData']>>12) & 0xFFFF
      amc['fwvers'] = (amc['BoardID']>>7) & 0x1FF
      
      hits = []
      tps = []
      debug = []
      for w in amc['payload']:
        if w[60:64] == 1:
          for offset in [30,0]:
            if w[offset:offset+30] == 0x3FFFFFFF:
              hits += [ {'str':'fill semiword'} ]
            else:
              hit = {'data':w[offset:offset+30]}
              hit['ch_id'] = (hit['data']>>17)&0x1FF
              hit['bx'] = (hit['data']>>5)&0xFFF
              hit['fine'] = (hit['data']>>0)&0x1F
              hit['bxdiff'] = hit['bx']-evt['BX_id']
              if hit['bxdiff'] >= 0: hit['bxdiff'] -= 3564
              hit['str'] = 'bx=%i.%i (%ins) wrtEvBx=%i ch=%i (%i.%i) '%(hit['bx'], hit['fine'], int(25*(hit['bx'] + (hit['fine']-1)/30.)), hit['bxdiff'], hit['ch_id'], hit['ch_id']%4, hit['ch_id']/4 )

              hits += [hit]
        elif w[62:64] == 2:
          tps += [{'word1':w}]
          tps[-1]['st']        = tps[-1]['word1'][60:62]
          tps[-1]['sl']        = tps[-1]['word1'][58:60]
          tps[-1]['time']      = tps[-1]['word1'][41:58]
          tps[-1]['quality']   = tps[-1]['word1'][35:41]
          tps[-1]['slope']     = 1.*tps[-1]['word1'][16:35]/4096
          tps[-1]['position']  = 1.*tps[-1]['word1'][0:16]/4
        elif w[60:64] == 0xC:
          tps[-1]['word2'] = w

          tps[-1]['chi2']      = tps[-1]['word2'][0:5]
          tps[-1]['index']     = tps[-1]['word2'][5:8]
          tps[-1]['lateral']   = [ tps[-1]['word2'][ 8 +i*1 : 8 +i*1+1 ] for i in range(4) ]
          tps[-1]['hitsused']  = [ tps[-1]['word2'][ 12+i*1 : 12+i*1+1 ] for i in range(4) ]
          tps[-1]['hittimes']  = [ tps[-1]['word2'][ 16+i*4 : 16+i*4+4 ] for i in range(4) ]
          tps[-1]['hitchs']    = [ tps[-1]['word2'][ 32+i*7 : 32+i*7+7 ] for i in range(4) ]
          
          tps[-1]['str'] = 'time=%i ns (bx %i)'%(tps[-1]['time'], tps[-1]['time']/25)
          tps[-1]['str'] += '    q=%i, slope=%.05f, position=%.02f, chi2=%i'%(tps[-1]['quality'], tps[-1]['slope'], tps[-1]['position'], tps[-1]['chi2'])
          tps[-1]['str'] += '\nlaterality=%s, hits_used=%s, hit_times=%s, hit_chs=%s'%(tps[-1]['lateral'], tps[-1]['hitsused'], str(tps[-1]['hittimes']), str(tps[-1]['hitchs']))
        elif w[60:64] == 0xf:
          debug += [{}]
          if len(debug) < 65: # Trigger Primitives debug words
            debug[-1]['bx_id'] = w[0:12]
            debug[-1]['tp_time'] = w[12:29]
            debug[-1]['str'] = '[time=%i(%i), bx_id=%i]'%(debug[-1]['tp_time'], debug[-1]['tp_time']/25, debug[-1]['bx_id'])
            debug[-1]['arrivalBX'] = w[29:41]
            debug[-1]['arrivalOrbit'] = w[41:49]
            debug[-1]['counter'] = w[49:60]
            debug[-1]['str']+= '@%X.%i, counter=%i'%(debug[-1]['arrivalOrbit'], debug[-1]['arrivalBX'], debug[-1]['counter'])
          else: # Hits debug words
            debug[-1]['tdc'] = w[0:5]
            debug[-1]['bx'] = w[5:17]
            debug[-1]['ch'] = w[17:25]
            debug[-1]['str'] = '[%i.%i,ch=%i(%i.%i)]'%(debug[-1]['bx'], debug[-1]['tdc'], debug[-1]['ch'], debug[-1]['ch']%4, debug[-1]['ch']/4)
            debug[-1]['arrivalBX'] = w[25:37]
            debug[-1]['arrivalOrbit'] = w[37:45]
            debug[-1]['counter'] = w[45:60]
            debug[-1]['str']+= '@%X.%i, counter=%i'%(debug[-1]['arrivalOrbit'], debug[-1]['arrivalBX'], debug[-1]['counter'])
        

      amc['hits'] = hits
      amc['tps'] = tps
      amc['debug'] = debug
    
def ab7print( evt , slot ):
  for amc in evt['AMCs']:
    if amc['AmcNo'] == slot:
      print 'AMC%i, OrN=%i(0x%X), BXid = %i, tptimer = %i, lastTPbx = %i, fw_version = %i'%(amc['AmcNo'], amc['OrN'], amc['OrN'], amc['BX_id'], amc['tptimer'],amc['lasttpbx'], amc['fwvers'])
      print '  Hits:'
      for hit in amc['hits']:
        print '    - ' + hit['str']
      print '  Trigger primitives:'
      for tp in amc['tps']:
        print '    - '+ tp['str'].replace('\n','\n      ')
      print '  Debug words:'
      for dw in amc['debug']:
        print '    - '+ dw['str']

def load_amc13_dump(filename, skip_length_report = False):
  """Loads an AMC13's event dump file and returns a list of events, each containing a list of bytes (human-readable endian)"""
  print 'Loading event(s) from file "%s" ...'%filename,
  words = []
  with open(filename,'rb') as f:
    while True:
      try: words += [struct.unpack('Q',f.read(8))[0]]
      except struct.error: break
  print 'done, loaded %i words'%len(words)
  
  evts = []
  for w in words:
    if w==0xbadc0ffeebadcafe:
      evts+=[[]]
    else:
      evts[-1]+=[w]
  print 'Found %i divider words (0xBADC0FEEBADCAFE)'%len(evts),
  if not skip_length_report: print 'lengths:',
  for evt in evts:
    if len(evt)!= evt[0]+1:
      raise Exception('event size (%i) different than reported (%i)'%(len(evt)-1,evt[0]))
    if not skip_length_report: print '%i, '%(len(evt)-1),
  print
  evts = [evt[1:] for evt in evts]
  return evts

def unpack_amc13(evt):
  """Accepts an event formatted as a list of 64-bit words, and returns a dict with the info from the amc13 fields extracted"""
  status = ''
  AMC_sizes = []
  unpev = {}
  for i in range(len(evt)):
    w = bf(evt[i])
    if i==0: #### CDF Header
      if w[60:64]!=5:
        print bcolors.red + 'CDF Header first hex digit different than 0x5' + bcolors.reset
        #raise Exception('CDF Header first hex digit different than 0x5')
      unpev['LV1_id'] = w[32:56]
      unpev['BX_id'] = w[20:32]
      unpev['Source_id'] = w[8:20]
      unpev['FOV'] = w[4:8]
    elif i==1: #### CDF Header follow-up
      unpev['uFOV'] = w[32:56]
      unpev['nAMC'] = w[52:56]
      unpev['OrN'] = w[4:36]
      unpev['uFOV'] = w[60:64]
      unpev['AMCs'] = []
      nAMC = w[52:56]
    elif i in range(2,2+nAMC):  #### summary of AMCs payload
      unpev['AMCs'] += [{'AmcNo':w[16:20], 'size':w[32:56], 'BoardID':w[0:16]}]
      AMC_sizes += [w[32:56]]
      in_AMC = 1
    elif i==len(evt)-2: #### Payload block trailer
      unpev['Blk_No'] = w[20:28]
      unpev['LV1_id_trl'] = w[12:20]
      if unpev['LV1_id_trl'] != (unpev['LV1_id'] & 0xFF):
        print bcolors.red + 'LV1_id different in CDF header and Block trailer' + bcolors.reset, unpev['LV1_id_trl'], (unpev['LV1_id'] & 0xFF)
        #raise Exception('LV1_id different in CDF header and Block trailer')
      unpev['BX_id_trl'] = w[0:12]
      if unpev['BX_id_trl'] != unpev['BX_id']:
        print bcolors.red + 'BX_id different in CDF header and Block trailer' + bcolors.reset, unpev['BX_id_trl'], unpev['BX_id']
        #raise Exception('BX_id different in CDF header and Block trailer')
    elif i==len(evt)-1: #### CDF Trailer
      if w[60:64]!=0xA:
        print bcolors.red + 'CDF Trailer first hex digit different than 0xA' + bcolors.reset
        #raise Exception('CDF Trailer first hex digit different than 0xA')
      unpev['Evt_lgth'] = w[32:56]
      unpev['TTS'] = w[4:7]
    elif in_AMC <= nAMC and i == 2 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header word
      unpev['AMCs'][in_AMC-1]['AmcNo (payload)'] = w[56:60]
      unpev['AMCs'][in_AMC-1]['LV1_id'] = w[32:56]
      unpev['AMCs'][in_AMC-1]['BX_id'] = w[20:32]
      unpev['AMCs'][in_AMC-1]['Data_lgth_hdr'] = w[0:20]
      BX_id = w[20:32]
    elif in_AMC <= nAMC and i == 3 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header follow-up
      unpev['AMCs'][in_AMC-1]['UserData'] = w[32:64]
      unpev['AMCs'][in_AMC-1]['OrN'] = w[16:32]
      unpev['AMCs'][in_AMC-1]['BoardID'] = w[0:16]
      unpev['AMCs'][in_AMC-1]['payload'] = []
    elif in_AMC <= nAMC and i == 1 + nAMC + sum(AMC_sizes[:in_AMC]): #### DAQLink Trailer
      unpev['AMCs'][in_AMC-1]['LV1_id_trl'] = w[24:32]
      unpev['AMCs'][in_AMC-1]['Data_lgth_trl'] = w[0:20]
      in_AMC+=1
    elif in_AMC <= nAMC: #### Payload
      unpev['AMCs'][in_AMC-1]['payload'] += [ w ]
      wordtype = w[60:64]
  return unpev

def counters_comparison(filename, events = None, amc = -1):
  """Prints a list of the EvN/OrN/BxN in the amc13 and one of the amcs for each event in the file (or the filtered ones)"""
  evts = load_amc13_dump(filename, skip_length_report=True)
  if events == None: events = range(len(evts))
  elif type(events) == int: events = [events]
  summaries = []
  last_i = None
  for i in events:
    summaries += [unpack_amc13(evts[i])]
    print 'Evt#% 4i:'%i,
    print 'EvN %X/%X/%i'%(  summaries[-1]['LV1_id'],  summaries[-1]['AMCs'][amc]['LV1_id'], summaries[-1]['AMCs'][amc]['LV1_id']-summaries[-1]['LV1_id']),
    print 'OrN %X/%X/%i'%(  summaries[-1]['OrN'],     summaries[-1]['AMCs'][amc]['OrN'],    summaries[-1]['AMCs'][amc]['OrN']-summaries[-1]['OrN']),
    print 'BxN %X/%X/%i'%(  summaries[-1]['BX_id'],   summaries[-1]['AMCs'][amc]['BX_id'],  summaries[-1]['AMCs'][amc]['BX_id']-summaries[-1]['BX_id']),
    if last_i != None and last_i == i-1:
      print 'Delta EvN AMC13 = %i'%(summaries[-1]['LV1_id']-summaries[-2]['LV1_id']),
      print 'Delta EvN TM7 = %i'%(summaries[-1]['AMCs'][amc]['LV1_id']-summaries[-2]['AMCs'][amc]['LV1_id']),
    print
    last_i = i

  
#####################################################
## Starting here is old. obsolete code
#####################################################
  
  
  
  
# Summarize event dump file
# loads events and prints for each one evn, orn and bxn of amc13 and one of the amc slots
# uses _summarize_evt_simple
def summarize_amc13_dump(self, filename, events = None, amc = -1):
  evts = self.amc13.load_events_from_file(filename, skip_length_report=True)
  if events == None: events = range(len(evts))
  elif type(events) == int: events = [events]
  summaries = []
  last_i = None
  for i in events:
    summaries += [self._summarize_evt_simple(evts[i])]
    print 'Evt#% 3i:'%i,
    print 'EvN %i/%i/%i'%(summaries[-1]['evn_amc13'],summaries[-1]['amcs'][amc]['evn'],summaries[-1]['amcs'][amc]['evn']-summaries[-1]['evn_amc13']),
    print 'OrN %i/%i/%i'%(summaries[-1]['orn_amc13'],summaries[-1]['amcs'][amc]['orn'],summaries[-1]['amcs'][amc]['orn']-summaries[-1]['orn_amc13']),
    print 'BxN %i/%i/%i'%(summaries[-1]['bxn_amc13'],summaries[-1]['amcs'][amc]['bxn'],summaries[-1]['amcs'][amc]['bxn']-summaries[-1]['bxn_amc13']),
    if last_i != None and last_i == i-1:
      print 'Delta EvN AMC13 = %i'%(summaries[-1]['evn_amc13']-summaries[-2]['evn_amc13']),
      print 'Delta EvN TM7 = %i'%(summaries[-1]['amcs'][amc]['evn']-summaries[-2]['amcs'][amc]['evn']),
    print
    last_i = i

# quickly extract some headers/trailers info from the events and stores it in a dict
# QUITE redundant with _unpacker, which has already been passed
def _summarize_evt_simple(self, evt):
  AMC_sizes = []
  summary = {'amcs':[]}
  for i in range(len(evt)):
    w = bf(evt[i])
    if i==0: #### CDF Header
      summary['evn_amc13'] = int(w[32:56])
      summary['bxn_amc13'] = int(w[20:32])
    elif i==1: #### CDF Header follow-up
      nAMC = int(w[52:56])
      summary['orn_amc13'] = int(w[4:36])
      AMC_offsets = [2+nAMC]
      in_AMC = 1
    elif i in range(2,2+nAMC):  #### summary of AMCs payload
      AMC_sizes += [int(w[32:56])]
    elif in_AMC <= nAMC and i == 2 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header word
      summary['amcs']+=[{'amcno':int(w[56:60]), 'evn':int(w[32:56]), 'bxn':int(w[20:32])}]
    elif in_AMC <= nAMC and i == 3 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header follow-up
      summary['amcs'][-1]['orn']=int(w[16:32])
      status = 'trigger data'
    elif in_AMC <= nAMC and i == 1 + nAMC + sum(AMC_sizes[:in_AMC]): #### DAQLink Trailer
      in_AMC+=1
      pass
  return summary

# Load and analyze event dump file, printing detailed interpretation of each event according to twinmux's format
# uses _analyze_evt_simple for each event's print and _qualstats to print statistics on qualities recorded
def analyze_amc13_dump(self, filename, events = None, skip_all = False, skip_booring = True, skip_bxids = [], skip_amcs = [], skip_payload = False, skip_length_report = False, qualitystats = False):
  """Grabs the dump file in this TM7's AMC13's work folder, and does an screen dupm of the selected events, parsed.
  By default, it analyzes all events. 
  If events is an integer, it analizes that single event; if it's a list of integers, it analizes those events.
  skip_booring parameter prevents printing empty data lines
  skip_bxids is a list of bxids that prevent the event to be printed
  skip_amcs is a list of the amcs to exclude
  skip_payload boolean to not print the payload data (but it does write the F-word)
  skip_length_report True to skip printing the lengths of all the events found at the beginning
  quality stats prints some stats on the qualities of the dump"""
  evts = self.amc13.load_events_from_file(filename, skip_length_report)
  if events == None: events = range(len(evts))
  elif type(events) == int: events = [events]
  all_qualities = []
  for i in events:
    print bcolors.bold + bcolors.red + '-'*10 + ' Analysis of event #%i '%i + '-'*10 + bcolors.reset
    all_qualities += self._analyze_evt_simple(evts[i], skip_all, skip_booring, skip_bxids, skip_amcs, skip_payload)
  if qualitystats:
    self._qualstats(all_qualities)

# analyzes one event with twinmux's format, printing results according to some parameter settings, 
# and returns a list of dicts containing info on each twinmux quality reported
def _analyze_evt_simple(self, evt, skip_all, skip_booring, skip_bxids, skip_amcs, skip_payload):
  explic = ['']*len(evt)
  status = ''
  AMC_sizes = []
  TEB_fw_vers = 0
  all_qualities = []
  for i in range(len(evt)):
    w = bf(evt[i])
    if i==0: #### CDF Header
      explic[i]+=bcolors.red
      explic[i]+='CDF Header = 0x%X'%int(w[60:64])+(', ' if w[60:64]==5 else ' (but should be "5"), ')
      explic[i]+='LV1_id = 0x%06x, '%int(w[32:56])
      explic[i]+='BX_id = 0x%03x, '%int(w[20:32])
      explic[i]+='Source_id = 0x%03x, '%int(w[8:20])
      explic[i]+='FOV = 0x%x'%int(w[4:8])
      explic[i]+=bcolors.reset
    elif i==1: #### CDF Header follow-up
      nAMC = int(w[52:56])
      explic[i]+='\t'
      explic[i]+=bcolors.YELLOW
      explic[i]+='uFOV = 0x%x, '%int(w[60:64])
      explic[i]+='nAMC = %i, '%nAMC
      explic[i]+='OrN = 0x%08x'%int(w[4:36])
      explic[i]+=bcolors.reset
      AMC_offsets = [2+nAMC]
    elif i in range(2,2+nAMC):  #### summary of AMCs payload
      explic[i]+='\t'
      explic[i]+=bcolors.YELLOW
      explic[i]+='AMC%i_size = %i, '%(int(w[16:20]),int(w[32:56]))
      explic[i]+='AmcNo = %i, '%int(w[16:20])
      explic[i]+='BoardID = 0x%04x'%int(w[0:16])
      explic[i]+=bcolors.reset
      AMC_sizes += [int(w[32:56])]
      in_AMC = 1
    elif i==len(evt)-2: #### Payload block trailer
      explic[i]+='\t'
      explic[i]+=bcolors.YELLOW
      explic[i]+='Blk_No = %i, '%int(w[20:28])
      explic[i]+='LV1_id = 0x%02x, '%int(w[12:20])
      explic[i]+='BX_id = 0x%03x'%int(w[0:12])
      explic[i]+=bcolors.reset
    elif i==len(evt)-1: #### CDF Trailer
      explic[i]+=bcolors.red
      explic[i]+='CDF Trailer = 0x%X'%int(w[60:64])+(', ' if w[60:64]==0xA else ' (but should be "A"), ')
      explic[i]+='Evt_lgth = %i, '%int(w[32:56])
      explic[i]+='TTC = 0x%x'%w[4:7]
      explic[i]+=bcolors.reset
    elif in_AMC <= nAMC and i == 2 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header word
      explic[i]+='\t\t'
      explic[i]+=bcolors.BLUE
      explic[i]+='AMC Header: AmcNo = %i, '%int(w[56:60])
      explic[i]+='LV1_id = 0x%06x, '%int(w[32:56])
      explic[i]+='BX_id = 0x%03x, '%int(w[20:32])
      if int(w[0:20]) == 0xFFFFF:
        explic[i]+='Data_lgth = undefined'
      else:
        explic[i]+='Data_lgth = %i'%int(w[0:20])
      explic[i]+=bcolors.reset
      BX_id = int(w[20:32])
    elif in_AMC <= nAMC and i == 3 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header follow-up
      explic[i]+='\t\t'
      explic[i]+=bcolors.BLUE
      if int(w[32:64]) == 0xcafecafe:
        explic[i]+='User = 0x%08x (), '%int(w[32:64])
      else:
        explic[i]+='UserData {FW_vers=%i,win=%i,lat=%i}, '%(int(w[50:64]),int(w[41:50]),int(w[32:41]))
        TEB_fw_vers = int(w[50:64])
      explic[i]+='OrN = 0x%04x, '%int(w[16:32])
      explic[i]+='BoardID = 0x%04x'%int(w[0:16])
      explic[i]+=bcolors.reset
      status = 'trigger data'
    elif in_AMC <= nAMC and i == 1 + nAMC + sum(AMC_sizes[:in_AMC]): #### DAQLink Trailer
      in_AMC+=1
      explic[i]+='\t\t'
      explic[i]+=bcolors.BLUE
      explic[i]+='AMC Trailer: LV1_id = 0x%02x, '%int(w[24:32])
      explic[i]+='Data_lgth = %i'%int(w[0:20])
      explic[i]+=bcolors.reset
    elif status == 'trigger data':
      explic[i]+='\t\t\t'
      wordtype = int(w[60:64])
      if wordtype==0xF: #### Error word
        explic[i]+=bcolors.GREEN
        explic[i]+='Error word (F) = '
        flagstr = bin(w[46:56])[2:].zfill(10)
        explic[i]+=bcolors.reset
        status = ''
      elif wordtype == 4: #### BX header
        explic[i]+=bcolors.GREEN
        explic[i]+='BX header (4), '
        bxdif = int(w[48:60]-BX_id)
        if bxdif not in range(-3564/2,3564/2): bxdif += 3564 * (1 if bxdif < 0 else -1)
        explic[i]+='BX_id=0x%03x(%i) '%(int(w[48:60]),bxdif)
        if w[0:48] == 0:
          explic[i]+='All zeros. Boooooring'
        else:
          explic[i]+='Had_L1A = %i, '%int(w[23])
          for mb in range(1,5):
            base=((4-mb)/2)*32 + ((mb-1)%2)*8
            explic[i]+='MB%i{'%mb
            explic[i]+='C,' if w[base+7] else ''
            explic[i]+='TR,' if w[base+6] else ''
            explic[i]+='THQ,' if w[base+5] else ''
            explic[i]+='TH,' if w[base+4] else ''
            explic[i]+='FS,' if w[base] else ''
            explic[i]+='Q=%i'%int(w[base+1:base+4])
            explic[i]+='}, '
            all_qualities += [{'amc':in_AMC, 'bx':bxdif, 'mb':mb, 'q':int(w[base+1:base+4])}]
          explic[i]=explic[i][:-2]
        explic[i]+=bcolors.reset
      elif wordtype in [1,2]: #### input Phi data (keeps code for parsing output)
        explic[i]+='\t'
        explic[i]+='Phi '+('in   ' if wordtype <3 else 'out  ')+'(%X), '%(wordtype)
        if w[0:60] == 0x1C0000001C00000:
          explic[i]+='All zeros. Boooooring'
        else:
          for mbmod in [1,2]:
            mb = (mbmod + 2) if wordtype in [2,0xC] else (mbmod)
            base = (2-mbmod)*32
            explic[i]+='MB%i{'%mb
            explic[i]+='P=%i,'%w[base+27]
            explic[i]+='FS,' if w[base+26] else ''
            explic[i]+='Q=%i,'%int(w[base+22:base+(25 if wordtype <3 else 26)])
            explic[i]+='PhiB=0x%03x,'%int(w[base+12:base+22]) if mb!=3 else 'PhiB=-----,'
            explic[i]+='Phi=0x%03x'%int(w[base:base+12])
            explic[i]+='}, '
          explic[i]=explic[i][:-2]
      elif wordtype == 3: #### input theta data (keeps code for parsing output)
        explic[i]+='\t'
        explic[i]+='Theta '+('in ' if wordtype==3 else 'out')+'(%X), '%(wordtype)
        if w[0:60] == 0:
          explic[i]+='All zeros. Boooooring'
        else:
          bases = [48,40,32,16,8,0]
          for mb in range(1,4):
            explic[i]+='MB%i{'%mb
            explic[i]+='etaHL=0x%02x,'%w[bases[mb*2-2]:bases[mb*2-2]+8]
            explic[i]+='eta=0x%02x'%w[bases[mb*2-1]:bases[mb*2-1]+8]
            explic[i]+='}, '
          explic[i]=explic[i][:-2]
      elif wordtype in [0xB,0xC,0xD]: #
        explic[i]+='\t'
        if wordtype == 0xB:
          explic[i]+='BX_id raw: 0x%03x'%int(w[48:60])
        elif wordtype==0xC:
          explic[i]+='EVN:0x%06x'%(int(w[0:24]))

  if skip_all: return all_qualities
  skip_this_AMC = False
  this_bx_is_completely_boring = False
  for i in range(len(evt)):
    # Skip the event completely if it matches one of the skip_bxids list
    if skip_bxids and 'CDF Header' in explic[i] and True in ['BX_id = 0x%03x'%bxid in explic[i] for bxid in skip_bxids]:
      print 'Event discarded due to BX_id matching one of the excluded BX_ids'
      break
    # Skip AMCs contained in the skip_amcs list
    if 'AMC Trailer' in explic[i]: skip_this_AMC = False
    if skip_this_AMC: continue
    if 'AMC Header' in explic[i]: skip_this_AMC = True in ['AmcNo = %i'%amc in explic[i] for amc in skip_amcs]

    # Skip boring data or all payload
    if skip_payload and ('\t\t\t\t' in explic[i] or 'BX header (4)' in explic[i]):
      continue
    if 'BX header (4)' in explic[i]:
      this_bx_is_completely_boring = not False in ['Boooooring' in explic_i for explic_i in explic[i+1:i+4]]
      if this_bx_is_completely_boring and skip_booring:
        continue
    elif '\t\t\t\t' in explic[i] and skip_booring and (this_bx_is_completely_boring or 'All zeros. Boooooring' in explic[i]):
      continue

    # If reached here, you can print (and format) the line
    print '% 3i:%016X %s'%(i,evt[i],explic[i].replace('\t','|'))

  return all_qualities

# twinmux-specific: analyzes a list of dicts in order to print a list of how many events have certain qualities
# for certain position in the twinmux read window
def _qualstats(self,qs):
  # {'amc':in_amc, 'bx':int(w[48:60], 'mb':mb, 'q':int(w[base+1:base+4])}
  q_for_each_bx = {}
  for d in qs:
    if not d['bx'] in q_for_each_bx: q_for_each_bx[d['bx']] = {}
    if not d['q'] in q_for_each_bx[d['bx']]: q_for_each_bx[d['bx']][d['q']] = 0
    q_for_each_bx[d['bx']][d['q']] += 1
  pprint.pprint(q_for_each_bx)
  
# it reads all events, upacks them (_unpacker) and extracts statistics about qualities (_qualstats2)
def qualities_amc13_dump(self, filename, events = None, skip_length_report = False):
  evts = self.amc13.load_events_from_file(filename, skip_length_report)
  if events == None: events = range(len(evts))
  elif type(events) == int: events = [events]
  qstats = {}
  for i in events:
    print bcolors.bold + bcolors.red + '-'*10 + ' Analysis of event #%i '%i + '-'*10 + bcolors.reset
    self._qualstats2(qstats, self._unpacker(evts[i]))
  pprint.pprint(qstats)
    
## unpacks an event and returns a dict containing all the info it has inside (twinmux format)
## COPIED to the working part except for the trigger payload analysis
def _unpacker(self, evt):
  status = ''
  AMC_sizes = []
  unpev = {}
  for i in range(len(evt)):
    w = bf(evt[i])
    if i==0: #### CDF Header
      if w[60:64]!=5: unpev['CDF Header wrong'] = True
      unpev['LV1_id'] = int(w[32:56])
      unpev['BX_id'] = int(w[20:32])
      unpev['Source_id'] = int(w[8:20])
      unpev['FOV'] = int(w[4:8])
    elif i==1: #### CDF Header follow-up
      unpev['uFOV'] = int(w[32:56])
      unpev['nAMC'] = int(w[52:56])
      unpev['OrN'] = int(w[4:36])
      unpev['uFOV'] = int(w[60:64])
      unpev['AMCs'] = []
      nAMC = int(w[52:56])
      AMC_offsets = [2+nAMC]
    elif i in range(2,2+nAMC):  #### summary of AMCs payload
      unpev['AMCs'] += [{'AmcNo':int(w[16:20]), 'size':int(w[32:56]), 'BoardID':int(w[0:16])}]
      AMC_sizes += [int(w[32:56])]
      in_AMC = 1
    elif i==len(evt)-2: #### Payload block trailer
      unpev['Blk_No'] = int(w[20:28])
      unpev['LV1_id_trl'] = int(w[12:20])
      unpev['BX_id'] = int(w[0:12])
    elif i==len(evt)-1: #### CDF Trailer
      if w[60:64]!=0xA: unpev['CDF Trailer wrong'] = True
      unpev['Evt_lgth'] = int(w[32:56])
      unpev['TTS'] = int(w[4:7])
    elif in_AMC <= nAMC and i == 2 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header word
      unpev['AMCs'][in_AMC-1]['AmcNo (payload)'] = int(w[56:60])
      unpev['AMCs'][in_AMC-1]['LV1_id'] = int(w[32:56])
      unpev['AMCs'][in_AMC-1]['BX_id'] = int(w[20:32])
      unpev['AMCs'][in_AMC-1]['Data_lgth'] = int(w[0:20])
      BX_id = int(w[20:32])
    elif in_AMC <= nAMC and i == 3 + nAMC + sum(AMC_sizes[:in_AMC-1]): #### DAQLink Header follow-up
      unpev['AMCs'][in_AMC-1]['UserData'] = {'FW_vers':int(w[50:64]),'win':int(w[41:50]),'lat':int(w[32:41])}
      unpev['AMCs'][in_AMC-1]['OrN'] = int(w[16:32])
      unpev['AMCs'][in_AMC-1]['BoardID'] = int(w[0:16])
      unpev['AMCs'][in_AMC-1]['bxs'] = []
      status = 'trigger data'
    elif in_AMC <= nAMC and i == 1 + nAMC + sum(AMC_sizes[:in_AMC]): #### DAQLink Trailer
      unpev['AMCs'][in_AMC-1]['LV1_id_trl'] = int(w[24:32])
      unpev['AMCs'][in_AMC-1]['Data_lgth_trl'] = int(w[0:20])
      in_AMC+=1
    elif status == 'trigger data':
      wordtype = int(w[60:64])
      if wordtype==0xF: #### Error word
        status = ''
      elif wordtype == 4: #### BX header
        bxdif = int(w[48:60]-BX_id)
        if bxdif not in range(-3564/2,3564/2): bxdif += 3564 * (1 if bxdif < 0 else -1)
        unpev['AMCs'][in_AMC-1]['bxs'] += [{'bx':int(w[48:60]), 'bxdif':bxdif, 'mbs':{}}]
        unpev['AMCs'][in_AMC-1]['bxs'][-1]['Had_L1A'] = int(w[23])
        for mb in range(1,5):
          base=((4-mb)/2)*32 + ((mb-1)%2)*8
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb] = {}
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['C'] = int(w[base+7])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['TR'] = int(w[base+6])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['THQ'] = int(w[base+5])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['TH'] = int(w[base+4])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['FS'] = int(w[base])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['Q'] = int(w[base+1:base+4])
      elif wordtype in [1,2]: #### input Phi data (keeps code for parsing output)
        for mbmod in [1,2]:
          mb = (mbmod + 2) if wordtype in [2,0xC] else (mbmod)
          base = (2-mbmod)*32
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['FS'] = int(w[base+26])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['PhiB'] = int(w[base+12:base+22])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['Phi'] = int(w[base:base+12])
      elif wordtype == 3: #### input theta data (keeps code for parsing output)
        bases = [48,40,32,16,8,0]
        for mb in range(1,4):
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['Eta'] = int(w[bases[mb*2-1]:bases[mb*2-1]+8])
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['mbs'][mb]['EtaHL'] = int(w[bases[mb*2-2]:bases[mb*2-2]+8])
      elif wordtype in [0xB,0xC,0xD]: #
        if wordtype == 0xB:
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['BX_id raw'] = int(w[48:60])
        elif wordtype==0xC:
          unpev['AMCs'][in_AMC-1]['bxs'][-1]['EVN raw'] = int(w[0:24])
  return unpev

# given a dict containing all de info of an event in twinmux format, 
# it updates a given dict which contains statistics on the qualities of all contained events
def _qualstats2(self, qstats, unpev):
  for AMC in unpev['AMCs']:
    for bx in AMC['bxs']:
      for mb in bx['mbs']:
        MB = bx['mbs'][mb]
        if MB['Q'] <7:
          if 'total' not in qstats: qstats['total'] = {}
          if AMC['AmcNo'] not in qstats: qstats[AMC['AmcNo']] = {}
          if bx['bxdif'] not in qstats['total']: qstats['total'][bx['bxdif']] = {}
          if bx['bxdif'] not in qstats[AMC['AmcNo']]: qstats[AMC['AmcNo']][bx['bxdif']] = {}
          if MB['Q'] not in qstats['total'][bx['bxdif']]: qstats['total'][bx['bxdif']][MB['Q']] = 0
          if MB['Q'] not in qstats[AMC['AmcNo']][bx['bxdif']]: qstats[AMC['AmcNo']][bx['bxdif']][MB['Q']] = 0
          qstats['total'][bx['bxdif']][MB['Q']] += 1
          qstats[AMC['AmcNo']][bx['bxdif']][MB['Q']] += 1
        
