from dtupy.systembuilder import loadkey
from dtupy.bcolors import bcolors
from dtupy import dtromap
import time
import traceback


################################################################################
## Parameter definitions
################################################################################
complexMetricsHistoryTimeout = 60

################################################################################
## Metrics definitions
################################################################################

std_tts_states = {0x1:'OFW',0x4:'BSY',0x8:'RDY',0x2:'OOS',0xC:'ERR',0xF:'DIS',0xFF:'UNK'}
int_tts_states = {0x0:'RDY',0x1:'OFW',0x2:'BSY',0x4:'OOS',0x8:'ERR',0x10:'DIS'}

amc13metrics = {
  'root':[                                                     
    {'n':'fwVersionT1', 'r':'T1.STATUS.FIRMWARE_VERS'              , 'postpro':hex },
    {'n':'fwVersionT2', 'r':'T2.STATUS.FIRMWARE_VERS'              , 'postpro':hex },
    {'n':'fedId'      , 'r':'T1.CONF.ID.SOURCE_ID'                 },
    {'n':'temperature', 'r':'T1.STATUS.FPGA.DIE_TEMP'              , 'postpro':lambda x: x/10.   },
    {'n':'anySfpDown' , 'r':'T1.STATUS.SFP.ANY_DOWN'                , 'err':'==1'},
  ],
  'evb':[                                         
    {'n':'warningOverflow', 'r':'T1.STATUS.EVB.OVERFLOW_WARNING'   , 'err':'==1'},
    {'n':'outOfSync'      , 'r':'T1.STATUS.EVB.SYNC_LOST'          , 'err':'==1'},
    {'n':'amcsTts'        , 'r':'T1.STATUS.AMC_TTS_STATE'          , 'err':'==ERR', 'warn':'!=RDY', 'postpro' : lambda x: int_tts_states[x] if x in int_tts_states else 'N/A' },
    {'n':'t1tts'          , 'r':'T1.STATUS.T1_TTS_STATE'           , 'err':'==ERR','warn':'!=RDY', 'postpro' : lambda x: std_tts_states[x] if x in std_tts_states else 'N/A' },
    {'n':'l1aCount'       , 'r':'T1.STATUS.GENERAL.L1A_COUNT'      , 'expand':1 },
    {'n':'l1a_inOFW'      , 'r':'T1.STATUS.GENERAL.L1A_WHEN_OFW'   , 'expand':1 },
    {'n':'l1a_inBSY'      , 'r':'T1.STATUS.GENERAL.L1A_WHEN_BSY'   , 'expand':1 },
    {'n':'l1a_inSYN'      , 'r':'T1.STATUS.GENERAL.L1A_WHEN_SYN'   , 'expand':1 },
    {'n':'runTime'        , 'r':'T1.STATUS.GENERAL.RUN_TIME'       , 'expand':1 },
    {'n':'readyTime'      , 'r':'T1.STATUS.GENERAL.READY_TIME'     , 'expand':1 },
    {'n':'busyTime'       , 'r':'T1.STATUS.GENERAL.BUSY_TIME'      , 'expand':1 },
    {'n':'oosTime'        , 'r':'T1.STATUS.GENERAL.SYNC_LOST_TIME' , 'expand':1 },
    {'n':'warnTime'       , 'r':'T1.STATUS.GENERAL.OF_WARN_TIME'   , 'expand':1 },
    {'n':'runTimeSecs'    },
    {'n':'readyTimeSecs'  },
    {'n':'busyTimeSecs'   },
    {'n':'oosTimeSecs'    },
    {'n':'warnTimeSecs'   },
    {'n':'L1Arate'        , 'r':'T1.STATUS.GENERAL.L1A_RATE_HZ'    },
  ],                                              
  'SLinkExpress':[                                
    {'n':'sfp0_absent'            , 'r':'T1.STATUS.SFP0.SFP_ABSENT_MASK'         , 'err':'==1' },
    {'n':'sfp0_LOS'               , 'r':'T1.STATUS.SFP0.RX_SIG_LOST_MASK'        , 'err':'==1' },
    {'n':'sfp0_txFault'           , 'r':'T1.STATUS.SFP0.TX_FAULT_MASK'           , 'err':'==1' },
    {'n':'sfp0_liveTimePerCent'   , 'r':'T1.STATUS.GENERAL.SFP0.LIVE_TIME_PCT'   },
    {'n':'revision'               , 'r':'T1.STATUS.SFP.SFP0.LSC.VERSION'         },
    {'n':'initialized'            , 'r':'T1.STATUS.SFP.SFP0.LSC.INITIALIZED'     , 'err':'==0'},
    {'n':'linkUp'                 , 'r':'T1.STATUS.SFP.SFP0.LSC.LINK_UP'         , 'warn':'==0'},
    {'n':'backPressure'           , 'r':'T1.STATUS.SFP.SFP0.LSC.NO_BACKPRESSURE' , 'postpro':lambda x:1-x, 'warn':'==1'},
    {'n':'backPressureTime'       , 'r':'T1.STATUS.SFP.SFP0.LSC.BACKP_COUNTER'   , 'expand':1 },
    {'n':'backPressureTimeSecs'   },
    {'n':'backPressureTimeRecent' },
    {'n':'events'                 , 'r':'T1.STATUS.LSC.SFP0.BUILT_EVENT_COUNT'   , 'rollover': True },
    {'n':'eventsSent'             , 'r':'T1.STATUS.SFP.SFP0.LSC.EVENTS_SENT'     , 'rollover': True },
    {'n':'words'                  , 'r':'T1.STATUS.LSC.SFP0.WORD_COUNT'          , 'rollover': True },
    {'n':'wordsSent'              , 'r':'T1.STATUS.SFP.SFP0.LSC.WORDS_SENT'      , 'rollover': True },
    {'n':'packetsSent'            , 'r':'T1.STATUS.SFP.SFP0.LSC.PACKETS_SENT'    , 'rollover': True },
    {'n':'packetsReceived'        , 'r':'T1.STATUS.SFP.SFP0.LSC.PACKETS_RCVD'    , 'rollover': True },
    {'n':'averageEventSize'       },
    {'n':'averageEventSizeRecent' },
  ],                                              
  'TTC':[                                         
    {'n':'ttc_sfp_absent'   , 'r':'T1.STATUS.SFP.TTS_SFP_ABSENT'           , 'err':'==1' },
    {'n':'ttc_sfp_LOS'      , 'r':'T1.STATUS.SFP.TTS_LOS_LOL'              , 'err':'==1' },
    {'n':'ttc_sfp_txFault'  , 'r':'T1.STATUS.SFP.TTS_TX_FAULT'             , 'err':'==1' },
    {'n':'t2_clkFreq'       , 'r':'T2.STATUS.TTC.CLK_FREQ'                 , 'err':'LHCFREQ','postpro' : lambda x:x*50 },
    {'n':'t2_bc0Counter'    , 'r':'T2.STATUS.TTC.BC0_COUNTER'              },
    {'n':'t2_bc0Errors'     , 'r':'T2.STATUS.TTC.BCNT_ERROR'               },
    {'n':'t2_SglBitErr'     , 'r':'T2.STATUS.TTC.SBIT_ERROR'               , 'err':'>0' },
    {'n':'t2_DblBitErr'     , 'r':'T2.STATUS.TTC.MBIT_ERROR'               , 'err':'>0' },
    {'n':'t1_resyncCount'   , 'r':'T1.STATUS.TTC.RESYNC_COUNT'             , 'expand':1  },
    {'n':'t1_TTCnotRdy'     , 'r':'T1.STATUS.TTC.NOT_READY'                , 'err':'==1' },
    {'n':'t1_BcntError'     , 'r':'T1.STATUS.TTC.BCNT_ERROR'               , 'err':'==1' },
    {'n':'t1_SglBitErr'     , 'r':'T1.STATUS.TTC.SGL_BIT_ERROR'            , 'err':'==1' },
    {'n':'t1_DblBitErr'     , 'r':'T1.STATUS.TTC.MULT_BIT_ERROR'           , 'err':'==1' },
  ],                                                            
  'AMCPorts':[                                                  
    {'n':'amcRevision'      , 'r':'T1.STATUS.AMC%02i.LINK.AMC_LINK_VER'                  },
    {'n':'revisionWrong'    , 'r':'T1.STATUS.AMC%02i.LINK_VERS_WRONG_MASK'               , 'err':'==1'},
    {'n':'ready'            , 'r':'T1.STATUS.AMC%02i.AMC_LINK_READY_MASK'                , 'err':'==0'},
    {'n':'ok'               , 'r':'T1.STATUS.AMC%02i.LOSS_OF_SYNC_MASK'                  , 'err':'==0'},
    {'n':'amc13Revision'    , 'r':'T1.STATUS.AMC%02i.LINK.AMC13_LINK_VER'                },
    {'n':'amc_tts'          , 'r':'T1.STATUS.AMC%02i.LINK.AMC_TTS'                       , 'err':'==ERR', 'warn':'!=RDY', 'postpro' : lambda x: std_tts_states[x] if x in std_tts_states else 'N/A'},
    {'n':'evbEvents'        , 'r':'T1.STATUS.AMC%02i.COUNTERS.AMC13_EVB_EVENTS'          , 'expand':1  },
    {'n':'amcEvents'        , 'r':'T1.STATUS.AMC%02i.COUNTERS.RECEIVED_EVENT_COUNTER'    , 'expand':1  },
    {'n':'amcHeaders'       , 'r':'T1.STATUS.AMC%02i.COUNTERS.HEADERS_AT_LINK_INPUT'     , 'expand':1  },
    {'n':'amcTrailers'      , 'r':'T1.STATUS.AMC%02i.COUNTERS.TRAILERS_AT_LINK_INPUT'    , 'expand':1  },
    {'n':'amcWords'         , 'r':'T1.STATUS.AMC%02i.COUNTERS.WORDS_AT LINK_INPUT'       , 'expand':1  },
    {'n':'amc13Events'      , 'r':'T1.STATUS.AMC%02i.COUNTERS.AMC13_AMC_EVENTS'          , 'expand':1  },
    {'n':'amc13Words'       , 'r':'T1.STATUS.AMC%02i.COUNTERS.WORD_COUNTER'              , 'expand':1  },
    {'n':'amcBcnMismatch'   , 'r':'T1.STATUS.AMC%02i.COUNTERS.BCN_MISMATCH_COUNTER'      , 'expand':1  },
    {'n':'amcOrnMismatch'   , 'r':'T1.STATUS.AMC%02i.COUNTERS.ORN_MISMATCH_COUNTER'      , 'expand':1  },
    {'n':'amc13BcnMismatch' , 'r':'T1.STATUS.AMC%02i.COUNTERS.AMC13_BCN_MISMATCH'        , 'expand':1  },
    {'n':'amc13OrnMismatch' , 'r':'T1.STATUS.AMC%02i.COUNTERS.AMC13_ORN_MISMATCH'        , 'expand':1  },
    {'n':'eventCounterJumps', 'r':'T1.STATUS.AMC%02i.COUNTERS.EVN_ERRORS_AT_LINK_INPUT'  , 'expand':1, 'warn':'>0'  },
    {'n':'crcErrors'        , 'r':'T1.STATUS.AMC%02i.BP_CRC_ERR'                         , 'err':'>0' },
  ]
}

urosmetrics = {
  'root':[                                                     
    {'n':'fwVersion'             , 'r':'ctrl.id.algorev'              , 'postpro':lambda x:(x&0xFFFFFF) },
    {'n':'fwTypeOK'              , 'r':'ctrl.id.algorev'              , 'postpro':lambda x:int((x>>24)&0xFF==2), 'err':'!=1' },
  ],                                              
  'TTC':[                                         
    {'n':'L1ACounter'            , 'r':'ttc.csr.stat1.evt_ctr'            , 'noraise': True    , 'postpro':lambda x:x-1},
    {'n':'BunchCounter'          , 'r':'ttc.csr.stat0.bunch_ctr'          , 'noraise': True    },
    {'n':'OrbitCounter'          , 'r':'ttc.csr.stat2.orb_ctr'            , 'noraise': True    },
    {'n':'SingleBitErrors'       , 'r':'ttc.csr.stat3.single_biterr_ctr'  , 'noraise': True    , 'err':'>0' },
    {'n':'DoubleBitErrors'       , 'r':'ttc.csr.stat3.double_biterr_ctr'  , 'noraise': True    , 'err':'>0' },
    {'n':'IsClock40Locked'       , 'r':'ctrl.csr.stat.clk40_lock'         , 'noraise': True    , 'err':'==0'},
    {'n':'IsBC0Locked'           , 'r':'ttc.csr.stat0.bc0_lock'           , 'noraise': True    , 'err':'==0'},
    {'n':'StopCounter'           , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 0   },
    {'n':'StartCounter'          , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 1   },
    {'n':'OC0Counter'            , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 3   },
    {'n':'ResyncCounter'         , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 4   },
    {'n':'EC0Counter'            , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 5   },
    {'n':'BC0Counter'            , 'r':'ttc.cmd_ctrs.ctr'                 , 'noraise': True    , 'muxr':'ttc.cmd_ctrs.ctr_sel', 'muxv': 6   },
  ],                                                            
  'Readout':[                                         
    {'n':'signal_detect'         , 'r':'readout.stat.snap12_sd'               , 'postpro':hex },
    
    {'n':'resync_ctr'            , 'r':'readout.stat.resync.ctr'              , 'expand':1 },
    {'n':'resync_queued'         , 'r':'readout.stat.resync.queued'           },
    {'n':'resync_queued_max'     , 'r':'readout.stat.resync.queued_max'       },
    
    {'n':'l1a_ctr'               , 'r':'readout.stat.l1a_ctr'                 , 'expand':1 },
    {'n':'hdr_ctr'               , 'r':'readout.stat.header_ctr'              , 'expand':1 },
    {'n':'trl_ctr'               , 'r':'readout.stat.trailer_ctr'             , 'expand':1 },
    {'n':'val_ctr'               , 'r':'readout.stat.valid_ctr'               , 'expand':1 },
    {'n':'max_evt_size_ctr'      , 'r':'readout.stat.max_evt_size_ctr'        , 'rollover': True , 'warn':'>0' },
    {'n':'big_evt_size_ctr'      , 'r':'readout.stat.big_evt_size_ctr'        , 'rollover': True },
    
    {'n':'urosTTS'               , 'r':'readout.stat.TTS'                     , 'err':'==ERR','warn':'!=RDY', 'postpro' : lambda x: std_tts_states[x] if x in std_tts_states else 'N/A' },
    {'n':'tts_rdy_ctr'           , 'r':'readout.stat.tts_rdy_ctr'             , 'expand':1 },
    {'n':'tts_bsy_ctr'           , 'r':'readout.stat.tts_bsy_ctr'             , 'expand':1 },
    {'n':'tts_ofw_ctr'           , 'r':'readout.stat.tts_ofw_ctr'             , 'expand':1 },
    
    {'n':'l1afifo_evn'           , 'r':'readout.stat.l1afifo.evn'               },
    {'n':'l1afifo_occupancy'     , 'r':'readout.stat.l1afifo.occupancy'       },
    {'n':'l1afifo_occupancy_max' , 'r':'readout.stat.l1afifo.occupancy_max'   },
    
    {'n':'amc13Rdy'              , 'r':'readout.stat.amc13.rdy'               , 'err':'!=1'},
    {'n':'amc13_rdy_ctr'         , 'r':'readout.stat.amc13.rdy_ctr'           , 'expand':1 },
    {'n':'amc13_nrdy_ctr'        , 'r':'readout.stat.amc13.nrdy_ctr'          , 'expand':1 },
    {'n':'amc13_af_ctr'          , 'r':'readout.stat.amc13.af_ctr'            , 'expand':1 },
  ],                                              
  'Inputs':[                                                  
    {'n':'edgeshift_ctr'         , 'r':'readout.stat.rob.edgeshift_ctr'            , 'rollover': True },
    {'n':'unlock12_ctr'          , 'r':'readout.stat.rob.unlock12_ctr'             , 'rollover': True , 'warn':'>0' },
    {'n':'startstoperr_ctr'      , 'r':'readout.stat.rob.startstoperr_ctr'         , 'rollover': True , 'warn':'>0' },
    {'n':'parityerr_total_ctr'   , 'r':'readout.stat.rob.parityerr_total_ctr'      , 'rollover': True , 'warn':'>0' },
    {'n':'parityerr_nofix_ctr'   , 'r':'readout.stat.rob.parityerr_nofix_ctr'      , 'rollover': True , 'warn':'>0' },
    {'n':'valid_ctr'             , 'r':'readout.stat.rob.valid_ctr'                , 'rollover': True },
    {'n':'w32interrupt_ctr'      , 'r':'readout.stat.rob.w32interrupt_ctr'         , 'rollover': True , 'warn':'>0' },
    {'n':'w32valid_ctr'          , 'r':'readout.stat.rob.w32valid_ctr'             , 'rollover': True },
    {'n':'inevent_idles_ctr'     , 'r':'readout.stat.rob.inevent_idles_ctr'        , 'rollover': True , 'warn':'>0' },
    {'n':'header_ctr'            , 'r':'readout.stat.rob.header_ctr'               , 'rollover': True },
    {'n':'trailer_ctr'           , 'r':'readout.stat.rob.trailer_ctr'              , 'rollover': True },
    {'n':'hits_ctr'              , 'r':'readout.stat.rob.hits_ctr'                 , 'rollover': True },
    {'n':'errs_ctr'              , 'r':'readout.stat.rob.errs_ctr'                 , 'rollover': True },
    {'n':'unknown_robword_ctr'   , 'r':'readout.stat.rob.unknown_robword_ctr'      , 'rollover': True , 'warn':'>0' },
    {'n':'not_in_event_ctr'      , 'r':'readout.stat.rob.not_in_event_ctr'         , 'rollover': True , 'warn':'>0' },
    {'n':'nonconsecutive_ctr'    , 'r':'readout.stat.rob.nonconsecutive_ctr'       , 'rollover': True },
    {'n':'notrailer_ctr'         , 'r':'readout.stat.rob.notrailer_ctr'            , 'rollover': True , 'warn':'>0' },
    {'n':'badtrailer_evn_ctr'    , 'r':'readout.stat.rob.badtrailer_evn_ctr'       , 'rollover': True , 'warn':'>0' },
    {'n':'badtrailer_wrd_ctr'    , 'r':'readout.stat.rob.badtrailer_wrd_ctr'       , 'rollover': True , 'warn':'>0' },
    {'n':'fifo_ofw_losses_ctr'   , 'r':'readout.stat.rob.fifo_ofw_losses_ctr'      , 'rollover': True , 'warn':'>0' },
    {'n':'fifo_af_ctr'           , 'r':'readout.stat.rob.fifo_af_ctr'              , 'rollover': True , 'expand':1  },
    {'n':'wren_ctr'              , 'r':'readout.stat.rob.wren_ctr'                 , 'rollover': True },
    {'n':'header_bxnbad_ctr'     , 'r':'readout.stat.rob.header_bxnbad_ctr'        , 'rollover': True , 'warn':'>0' },
    {'n':'toomuchdata_ctr'       , 'r':'readout.stat.rob.toomuchdata_ctr'          , 'rollover': True , 'warn':'>0' },
    {'n':'headerlate_ctr'        , 'r':'readout.stat.rob.headerlate_ctr'           , 'rollover': True , 'warn':'>0' },
    {'n':'headerpurged_ctr'      , 'r':'readout.stat.rob.headerpurged_ctr'         , 'rollover': True , 'warn':'>0' },
    {'n':'hits2daq_ctr'          , 'r':'readout.stat.rob.hits2daq_ctr'             , 'rollover': True },
    {'n':'trailer2daq_ctr'       , 'r':'readout.stat.rob.trailer2daq_ctr'          , 'rollover': True },
    {'n':'trailer_read_ctr'      , 'r':'readout.stat.rob.trailer_read_ctr'         , 'rollover': True },
    
    {'n':'samples_1'             , 'r':'readout.stat.rob.samples_1'             , 'postpro':lambda x:'%08X'%x },
    {'n':'samples_2'             , 'r':'readout.stat.rob.samples_2'             , 'postpro':lambda x:'%08X'%x },
    {'n':'samples_3'             , 'r':'readout.stat.rob.samples_3'             , 'postpro':lambda x:'%08X'%x },
    {'n':'nowlock'               , 'r':'readout.stat.rob.nowlock'               , 'err':'==0'},
    {'n':'word12_data'           , 'r':'readout.stat.rob.word12_data'           , 'postpro':lambda x:bin(x)[2:].zfill(12)[::-1] },
    {'n':'word12_weak'           , 'r':'readout.stat.rob.word12_weak'           , 'postpro':lambda x:bin(x)[2:].zfill(12)[::-1] },
    {'n':'netedgeshift'          , 'r':'readout.stat.rob.netedgeshift'          , 'postpro':lambda x: x+127 if x<129 else x-129  },
    {'n':'max_ff_occupancy'      , 'r':'readout.stat.rob.max_ff_occupancy'      },

    {'n':'iserdes_output' },
    {'n':'unlock12_ctr_few_secs' , 'err': '!=0'     },
    {'n':'hits2daq_ctr_recent'   },
    {'n':'evs_lost_rx'           , 'warn':'!=0'    },
    {'n':'evs_lost_rx_recent'    , 'err': '!=0'     },
    {'n':'evs_lost_evb'          , 'warn':'!=0'    },
    {'n':'evs_lost_evb_recent'   , 'err': '!=0'     },
    {'n':'evs_lost_tot'          , 'warn':'!=0'    },
    {'n':'evs_lost_tot_recent'   , 'err': '!=0'     },
    {'n':'evs_lost_ratio'        , 'warn': '>0'     },
    {'n':'evs_lost_ratio_recent' , 'err': '>0'     },
  ]
}

op2fun = {
  '<=':lambda x,y: x<=y,
  '==':lambda x,y: x==y,
  '>=':lambda x,y: x>=y,
  '!=':lambda x,y: x!=y,
  '>':lambda x,y: x>y,
  '<':lambda x,y: x<y,
  }
sortops = sorted(op2fun, cmp=lambda x,y:len(y)-len(x))

def decodeCondition(cond):
  if cond == 'LHCFREQ': return {'f':lambda x:(x< 3.99e+07) or (x> 4.01e+07), 'txt':'< 3.99e+07 or > 4.01e+07'}
  if any([cond.find(op)==0 for op in sortops]):
    op = sortops[ [cond.find(op) for op in sortops].index(0) ]
    f = op2fun[ op ]
    ref = cond[len(op):]
    try: ref = int(ref,0)
    except:pass
    return {'f':lambda x: f(x,ref), 'txt':cond}
  print 'bad condition:',cond

################################################################################
## Metric related aux functions
################################################################################

def calculateComplexMetrics(board, boardData, boardmetrics):
  t = time.time()
  if boardmetrics == urosmetrics:
    # get global trailers delivered to amc13
    ReadoutBlock = [block for block in boardData['subs'] if block['id']=='Readout'][0]
    trlr_amc13 = [metric for metric in ReadoutBlock['subs'] if metric['id']=='trl_ctr'][0]['value']
    hdr_amc13  = [metric for metric in ReadoutBlock['subs'] if metric['id']=='hdr_ctr'][0]['value']
    
    InputsBlock = [block for block in boardData['subs'] if block['id']=='Inputs'][0]
    for chBlock in InputsBlock['subs']:
      # fetch number of trailers read and written by the event builder
      trlr_ch_read      = [metric for metric in chBlock['subs'] if metric['id']=='trailer_read_ctr'][0]['value']
      trlr_ch_written   = [metric for metric in chBlock['subs'] if metric['id']=='trailer2daq_ctr'][0]['value']

      # Build the complex metrics
      evsLostRxMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_rx'][0]
      evsLostRxMetric['value'] = trlr_amc13 - trlr_ch_read
      if evsLostRxMetric['value'] == -1 and hdr_amc13 == trlr_amc13 + 1:
        evsLostRxMetric['value'] = 0 # It's possible to read the registers when one channel has been processed but the event is yet unfinished
      evsLostRxRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_rx_recent'][0]
      evsLostRxRecentMetric['value'] = evsLostRxMetric['value'] - ( board.history[chBlock['id']][min(board.history[chBlock['id']].keys())]['evs_lost_rx'] if board.history[chBlock['id']] else 0 )
      
      evsLostEvbMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_evb'][0]
      evsLostEvbMetric['value'] = trlr_ch_read - trlr_ch_written
      evsLostEvbRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_evb_recent'][0]
      evsLostEvbRecentMetric['value'] = evsLostEvbMetric['value'] - ( board.history[chBlock['id']][min(board.history[chBlock['id']].keys())]['evs_lost_evb'] if board.history[chBlock['id']] else 0 )
      
      evsLostTotMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_tot'][0]
      evsLostTotMetric['value'] = evsLostRxMetric['value'] + evsLostEvbMetric['value']
      evsLostTotRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_tot_recent'][0]
      evsLostTotRecentMetric['value'] = evsLostRxRecentMetric['value'] + evsLostEvbRecentMetric['value']

      evsLostRatioMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_ratio'][0]
      evsLostRatioMetric['value'] = 1.*evsLostTotMetric['value']/trlr_amc13 if trlr_amc13 else 0.
      evsLostRatioRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='evs_lost_ratio_recent'][0]
      trlr_amc13_recent = trlr_amc13 - ( board.history[chBlock['id']][min(board.history[chBlock['id']].keys())]['trlr_amc13'] if board.history[chBlock['id']] else 0 )
      evsLostRatioRecentMetric['value'] = 1.*evsLostTotRecentMetric['value']/trlr_amc13_recent if trlr_amc13_recent else 0.
      
      unlockCtrMetric = [metric for metric in chBlock['subs'] if metric['id']=='unlock12_ctr'][0]
      unlockCtrRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='unlock12_ctr_few_secs'][0]
      unlockCtrRecentMetric['value'] = unlockCtrMetric['value'] - ( board.history[chBlock['id']][max(board.history[chBlock['id']].keys())]['unlock12_ctr'] if board.history[chBlock['id']] else 0 )
      
      hits2daq_ctr = [metric for metric in chBlock['subs'] if metric['id']=='hits2daq_ctr'][0]['value']
      hits2daqCtrRecentMetric = [metric for metric in chBlock['subs'] if metric['id']=='hits2daq_ctr_recent'][0]
      hits2daqCtrRecentMetric['value'] = hits2daq_ctr - ( board.history[chBlock['id']][max(board.history[chBlock['id']].keys())]['hits2daq_ctr'] if board.history[chBlock['id']] else 0 )
      
      # write the history contents for previous metrics
      board.history[chBlock['id']][t] = {}
      board.history[chBlock['id']][t]['evs_lost_rx'] = evsLostRxMetric['value']
      board.history[chBlock['id']][t]['evs_lost_evb'] = evsLostEvbMetric['value']
      board.history[chBlock['id']][t]['trlr_amc13'] = trlr_amc13
      board.history[chBlock['id']][t]['unlock12_ctr'] = unlockCtrMetric['value']
      board.history[chBlock['id']][t]['hits2daq_ctr'] = hits2daq_ctr
      
      # Create the iserdes_output metric
      iserdesMetric = [metric for metric in chBlock['subs'] if metric['id']=='iserdes_output'][0]
      debug  = int( [metric for metric in chBlock['subs'] if metric['id']=='samples_1'][0]['value'] , 16 )      
      debug += int( [metric for metric in chBlock['subs'] if metric['id']=='samples_2'][0]['value'] , 16 ) <<32  
      debug += int( [metric for metric in chBlock['subs'] if metric['id']=='samples_3'][0]['value'] , 16 ) <<64  
      isdq, separators = '', [0]*70
      for i in range(7):
        isdq = bin((debug>>(13*i+3))&0x3FF)[2:].zfill(10) + isdq
        offset = (debug>>(13*i))&0x7
        for j in range(2):
          pos1, pos2 = 69 - (10*i+offset-2+5*j), 69 - (10*i+offset+3+5*j)
          if pos1 in range(len(separators)): separators[pos1] = ']' if separators[pos1] != '[' else ']['
          if pos2 in range(len(separators)): separators[pos2] = '[' if separators[pos2] != ']' else ']['
      iserdesMetric['value'] = ''
      for i in range(70):
        iserdesMetric['value'] += isdq[i] + (separators[i] if separators[i] else '')
  else:
    evbBlock = [block for block in boardData['subs'] if block['id']=='evb'][0]
    for timeType in ['run', 'ready', 'busy', 'oos', 'warn']:
      ctrValue = [metric for metric in evbBlock['subs'] if metric['id']==(timeType+'Time')][0]['value']
      [metric for metric in evbBlock['subs'] if metric['id']==(timeType+'TimeSecs')][0]['value'] = ctrValue / 200e6
    runTimeCtr = [metric for metric in evbBlock['subs'] if metric['id']==('runTime')][0]['value']
    runTimeCtrInc = ( runTimeCtr - board.history[-1][max(board.history[-1].keys())]['runTime'] ) if board.history[-1] else 0

    SLinkExpressBlock = [block for block in boardData['subs'] if block['id']=='SLinkExpress'][0]
    
    events = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('events')][0]['value']
    words = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('words')][0]['value']
    averageEventSizeMetric = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('averageEventSize')][0]
    averageEventSizeMetric['value'] = words /128. / events if events else 0.
    eventsRecent = ( events - board.history[-1][min(board.history[-1].keys())]['events'] ) if board.history[-1] else 0
    wordsRecent = ( words - board.history[-1][min(board.history[-1].keys())]['words'] ) if board.history[-1] else 0
    averageEventSizeRecentMetric = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('averageEventSizeRecent')][0]
    averageEventSizeRecentMetric['value'] = wordsRecent /128. / eventsRecent if eventsRecent else 0.
    
    bpTimeCtr = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('backPressureTime')][0]['value']
    [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('backPressureTimeSecs')][0]['value'] = bpTimeCtr / 200e6
    bpTimeCtrInc = ( bpTimeCtr - board.history[-1][max(board.history[-1].keys())]['backPressureTime'] ) if board.history[-1] else 0
    
    bpTimeRecentMetric = [metric for metric in SLinkExpressBlock['subs'] if metric['id']==('backPressureTimeRecent')][0]
    bpTimeRecentMetric['value'] = 1. * bpTimeCtrInc / runTimeCtrInc if runTimeCtrInc else 0.

    board.history[-1][t] = {}
    board.history[-1][t]['runTime'] = runTimeCtr
    board.history[-1][t]['backPressureTime'] = bpTimeCtr
    board.history[-1][t]['events'] = events
    board.history[-1][t]['words'] = words
    
def createPersistentData(board, metrics):
  board.rollover_data = {}
  for block in metrics:
    board.rollover_data[block] = {}
    if block not in ['Inputs', 'AMCPorts']:
      board.rollover_data[block] = {metric['n']:[0,0,False] for metric in metrics[block] if 'rollover' in metric}
    else:
      board.rollover_data[block] = {}
      for i in board.activeInputs():
        board.rollover_data[block][i] = {metric['n']:[0,0,False] for metric in metrics[block] if 'rollover' in metric}

  board.history = {}
  for i in board.activeInputs() + [-1]: # -1 used for the metrics that are not input-channel-dependent
    board.history[i] = {}

def pruneHistory(board, timeout):
  t = time.time()
  for channel in board.history:
    board.history[channel] = { ts:board.history[channel][ts] for ts in board.history[channel] if t-ts < timeout }

def getMetric(board, block, metric, channel=None, mode = 'getFresh'):
  # mode can be "structure", "getFresh", "prefetch", "getStored"

  result = { 'id':metric['n'] }

  # No value-related tasks for structure or complex metrics that do not involve register-reading
  if mode == 'structure' or 'r' not in metric: return result
  
  # get the register value, either fresh or old, either int or uhal object, when needed
  try:
    readfunc = lambda reg: board.read(
        reg,
        nodispatch = (mode == 'prefetch'),
        fresh=(mode in ['getFresh','prefetch']), 
        storeSuffix=(str(channel) if channel!=None else '') + (str(metric['muxv']) if 'muxr' in metric else '') )
    if 'muxr' in metric and mode in ['getFresh','prefetch']: board.write(metric['muxr'], metric['muxv'])
    val_lo = readfunc(metric['r']+'_LO') if 'expand' in metric else readfunc(metric['r'])
    val_hi = readfunc(metric['r']+'_HI') if 'expand' in metric else 0
  
    if mode != 'prefetch':
      # Calculate metric value
      val = (int(val_hi)<<32) + int(val_lo)
      if 'postpro' in metric: 
        val = metric['postpro'](val)
      elif 'rollover' in metric:
        # calculate and store rollover value if not yet set
        if metric['rollover'] == True: 
          metric['rollover'] = ( bin(board.getNode(metric['r']).getMask()).count('1') if 'expand' not in metric else
              (bin(board.getNode(metric['r']+'_LO').getMask()) + bin(board.getNode(metric['r']+'_HI').getMask())).count('1') )
        rodata = board.rollover_data[block][metric['n']] if block != 'Inputs' else board.rollover_data[block][channel][metric['n']]
        if  ( ( (val - rodata[0]) % 2**metric['rollover'] ) > 0.4 * 2**metric['rollover'] # jump from last stored value bigger than x% of the rollover depth
              and rodata != [0,0, False] ): # initial situation in case started in the middle of the run with values already high
          rodata[2] = True
        if val < rodata[0]:  rodata[1] += 1
        rodata[0] = val
        val += rodata[1] * 2**metric['rollover']
        if rodata[2]: result['overflow'] = True
      result['value'] = val
  
  except:
    if 'noraise' in metric:
      print 'Ignoring read exception due to noraise'
      traceback.print_exc()
      result['value'] = -1
    else:
      raise

  return result

def getMetrics(board, boardmetrics, onlyStructure = False):
  # prefetch the values
  if not onlyStructure:
    board.freezeStats(1)
    for block in boardmetrics:
      if block not in ['Inputs', 'AMCPorts']:
        for metric in boardmetrics[block]:
          getMetric(board, block, metric, mode = 'prefetch')
      else:
        for channel in board.activeInputs():
          board.statsFromChan(channel)
          if block =='AMCPorts':  # only for amc13; for uros, statsFromChan already does the prefetch by doing a block read
            for metric in boardmetrics[block]:
              getMetric(board, block, metric, channel=channel, mode = 'prefetch')
    board.dispatch()
    board.freezeStats(0)
  
  # do the postprocessing and all the rest
  subs0 = []
  for block in sorted(boardmetrics): # sorted has implications for later assignment of unique ids for metrics
    subs1 = []
    if block not in ['Inputs', 'AMCPorts']:
      for metric in boardmetrics[block]:
        subs1 += [ getMetric(board, block, metric, mode='structure' if onlyStructure else 'getStored') ]
    else:
      for channel in board.activeInputs():
        subs2 = []
        for metric in boardmetrics[block]:
          subs2 += [ getMetric(board, block, metric, channel=channel, mode='structure' if onlyStructure else 'getStored') ]
        subs1 += [ {'id':channel, 'subs':subs2} ]
        if onlyStructure and 'slotMap' in board.getParams():
          mapInfo = dtromap.decode_uros_map(
              board.s.hw[board.context['crate']]['amc13'].getParams()['fed'], 
              board.getParams()['slotMap'], 
              channel,
              board.context['slot'] )
          if mapInfo != None:
            subs1[-1]['map'] = mapInfo
            subs1[-1]['map']['uros'] = '[%s.%i]'%(board.context['id'], channel)
    subs0 += [ {'id':block, 'subs':subs1} ]
  result = {'id':board.context['id'], 'subs':subs0 }
  
  if not onlyStructure:
    pruneHistory(board, complexMetricsHistoryTimeout)
    calculateComplexMetrics(board, result, boardmetrics)
  
  return result

flatMetrics = {}
for metric in ( [ metric for block in urosmetrics for metric in urosmetrics[block] ] + 
                [ metric for block in amc13metrics for metric in amc13metrics[block] ] ):
  if metric['n'] in flatMetrics:
    raise Exception('Found metric with non-unique id: %s'%metric['n'])
  else:
    flatMetrics[metric['n']] = metric

def addEWtxts(block):
  block.update( {severity+'txt':decodeCondition(flatMetrics[block['id']][severity])['txt'] 
                  for severity in ['warn','err'] if block['id'] in flatMetrics and severity in flatMetrics[block['id']] } )
  if 'subs' in block and block['subs']:
    for sub in block['subs']:
      addEWtxts(sub)

def fillColor(block):
  if 'color' in block: pass
  elif 'value' in block:
    if    'err'   in flatMetrics[block['id']] and decodeCondition(flatMetrics[block['id']]['err'])['f'](block['value']): block['color'] = 3
    elif  'warn'  in flatMetrics[block['id']] and decodeCondition(flatMetrics[block['id']]['warn'])['f'](block['value']): block['color'] = 2
    elif  'warn'  in flatMetrics[block['id']] or 'err' in flatMetrics[block['id']]: block['color'] = 1
    else: block['color'] = 0
  elif 'subs' in block and block['subs']:
    for sub in block['subs']:
      fillColor(sub)
    if any( ['color' in sub for sub in block['subs'] ] ):
      block['color'] = max( [ sub['color'] for sub in block['subs'] if 'color' in sub ] )
  
  # <-------- This code controlled Inputs metrics color propagation when colors where assigned while creating complex metrics ------------->
  ## # Set complex propagation of Inputs color as a function of thresholds
  ## numErr  = len([chBlock for chBlock in InputsBlock['subs'] if chBlock['color'] == 3])
  ## numWarn = len([chBlock for chBlock in InputsBlock['subs'] if chBlock['color'] == 2])
  ## numCh = len( InputsBlock['subs'] )
  ## if numErr > numCh * .1:                       InputsBlock['color'] = 3
  ## elif numErr != 0 or numWarn > numCh * .1:     InputsBlock['color'] = 2
  ## else:                                         InputsBlock['color'] = 1

def addUnid(block, preamble = ''):
  block['unid']=preamble
  if 'subs' in block:
    block['subsAreGroup'] = any(['subs' in sub for sub in block['subs'] ])
    block['subsAreFinal'] = not block['subsAreGroup']
    for i in range(len(block['subs'])):
      addUnid( block['subs'][i] , preamble+'-'+str(i) )
  else:
    block['subs']=0 #because mustache goes up in hierarchy to look for non-existent variables
  
  
eorw = {2:'\n (!) ',3:'\n (x) '}
def prepareStatus(block, status, preamble = ''):
  if block['id'] == 'l1aCount' and block['value'] > status['num_events']:
    status['num_events'] = block['value']
  if block['id'] == 'L1Arate' and status['L1Arate'] == -1:
    status['L1Arate'] = block['value']
  if block['id'] == 'averageEventSize':
    status['avgEvtSize'] += block['value']
    status['avgEvtSizeFED'] += ('/' if status['avgEvtSizeFED'] else '') + '%.2f'%(block['value'])
  if block['id'] == 'averageEventSizeRecent':
    status['avgEvtSizeRecent'] += block['value']
    status['avgEvtSizeRecentFED'] += ('/' if status['avgEvtSizeRecentFED'] else '') + '%.2f'%(block['value'])
  if block['id'] == 'backPressureTimeRecent':
    status['backPressureFED'] += ('/' if status['backPressureFED'] else '') + '%.1f'%(block['value'])
  if 'value' in block:
    status['values'][block['unid']] = str(block['value']) + ('++' if 'overflow' in block else '')
  if 'color' in block: status['colors'][block['unid']]=block['color']
  if 'value' in block and 'color' in block and block['color']>1 :
    whatlist = ('general-errwarns' if '.Inputs.' not in preamble else 'inputs')
    if whatlist == 'inputs': whatlist += '-errs' if block['color']==3 else '-warns'
    if status[whatlist] and preamble in status[whatlist][-1]['id']:
      preamble = 'beginwhite%sendwhite'%preamble
    status[whatlist] += [ {'id':preamble + block['id'],'unid':block['unid']} ]
    if 'errtxt' in block: status[whatlist][-1]['errtxt'] = block['errtxt']
    if 'warntxt' in block: status[whatlist][-1]['warntxt'] = block['warntxt']
  if 'subs' in block and block['subs']:
    subsColStr = {2:'', 3:''}
    for sub in block['subs']:
      if 'color' in sub and sub['color']>1:
        subsColStr[sub['color']] += eorw[sub['color']] + str(sub['id']) + ( ' = %s'%str(sub['value']) if 'value' in sub else '' )
      prepareStatus(sub, status, preamble = '' if block['id']=='system' else preamble+str(block['id'])+'.')
    if block['unid'].count('-') in [2,3,4]: status['summary'][block['unid']] = str(block['id']) + subsColStr[3] + subsColStr[2] 

def analyzeRunDegr(block, parentBlock, result, unid2map):
  if block['id'] in ['evs_lost_tot_recent', 'evs_lost_ratio_recent']:
    byMetric = 'byRate' if block['id'] == 'evs_lost_tot_recent' else 'byRatio'
    result[byMetric]['totalSeen'] += 1
    isBad = ( (block['value'] > .1) if byMetric=='byRatio' else # 10% of recent ratio
              (block['value'] > complexMetricsHistoryTimeout * 100000 * .1) # 10% of the nominal hit rate (100kHz) --> 10kHz during the integration time
            )
    if isBad:
      [w, s] = unid2map[parentBlock['unid']]['phys'][:2]
      if s == 13: s = 4
      if s == 14: s = 10
      result[byMetric]['bad'][w][s-1] += 1
        
  elif 'subs' in block and block['subs']:
    for sub in block['subs']:
      analyzeRunDegr(sub, block, result, unid2map)
  

################################################################################
## UROS Cell Class definition
################################################################################

class UROSCell(object):
  def __init__(self, key, db_partKey=0, db_configKey=0, db_configVers=0):
    self.s = loadkey(key, db_partKey, db_configKey, db_configVers)
    self.timeOfCreation = time.time()

  def configure(self):
    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for amc13 in amc13s: amc13.resetCmd()
    for amc13 in amc13s: amc13.configTTCCmd()
    for tm7 in tm7s: tm7.configureCmd()
    for amc13 in amc13s: amc13.configDAQCmd()
  
  def start(self):
    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for tm7 in tm7s: tm7.startCmd()
    for amc13 in amc13s: amc13.startCmd()

  def stop(self):
    s = self.s
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    tm7s = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    
    for amc13 in amc13s: amc13.stopCmd()
    for tm7 in tm7s: tm7.stopCmd()
  
  def resetPersistentMonitoringData(self):
    s = self.s
    # for rollover and "historic" metrics
    procs = [proc for crate in s.hw for proc in s.hw[crate]['processors'] ]
    amc13s = [s.hw[crate]['amc13'] for crate in s.hw]
    for board in procs: createPersistentData(board, urosmetrics)
    for board in amc13s: createPersistentData(board, amc13metrics)

  def fetchMonitorData(self, onlyStructure = False):
    s = self.s
    result = []
    for crate in s.hw:
      result += [ { 'id':crate, 'subs':[] } ]
      for i in range(1,13):
        if i in s.hw[crate]['slots']:
          result[-1]['subs'] += [ getMetrics(s.hw[crate]['slot%i'%i], urosmetrics, onlyStructure = onlyStructure) ]
        else:
          result[-1]['subs'] += [ {'id':'amc%i'%i,'AMCexists':False} ] # empty slot needed for crate diagram
      result[-1]['subs'] += [ getMetrics(s.hw[crate]['amc13'], amc13metrics, onlyStructure = onlyStructure) ]
    result = {
      'id':'system',
      'AMCexists':True,# default AMCexists value for slots that do exist
      'subs':result,
      'timeOfCreation': self.timeOfCreation,
      } 
    addUnid(result)
    if onlyStructure: addEWtxts(result) #add txts
    return result
  
  def isRunningDegraded(self, monitorData, unid2map):
    result = { 'byRate':  {'totalSeen': 0, 'bad':{w:[0]*12 for w in range(-2,3)} },
               'byRatio': {'totalSeen': 0, 'bad':{w:[0]*12 for w in range(-2,3)} } }
    analyzeRunDegr(monitorData, None, result, unid2map)
    
    for byMetric in result:
      result[byMetric]['maxBadInSector'] = max([   value for w in result[byMetric]['bad'] for value in result[byMetric]['bad'][w]   ])
      result[byMetric]['maxBadInWheel'] = max([ sum(result[byMetric]['bad'][w]) for w in result[byMetric]['bad'] ])
      result[byMetric]['degraded'] = result[byMetric]['maxBadInSector']>=9 or result[byMetric]['maxBadInWheel']>=15
    return result['byRate']['degraded'], result
    
  def prepareWebData(self, monitorData):
    fillColor(monitorData)
    status = {'values': {} , 'colors': {}, 'summary':{}, 
              'inputs-warns': [], 'inputs-errs': [], 'general-errwarns': [],
              'num_events': 0, 'L1Arate':-1, 'avgEvtSize':0, 'avgEvtSizeFED':'', 'avgEvtSizeRecent':0, 'avgEvtSizeRecentFED':'', 'backPressureFED':'',
              'timestamp':int(time.time()), 'timeOfCreation':self.timeOfCreation}
    prepareStatus(monitorData, status)
    status['avgEvtSize'] = '%.2f'%status['avgEvtSize']
    status['avgEvtSizeRecent'] = '%.2f'%status['avgEvtSizeRecent']
    return status
  
  def prepareLogData(self, monitorData):
    tables = {'amc13':[], 'AMCPorts':[], 'uros':[], 'Inputs':[] }
  
    for (boardTbl, inputBlk, boardList) in [
          ('amc13', 'AMCPorts', [crate['subs'][12] for crate in monitorData['subs'] if ('AMCexists' not in crate['subs'][12] or crate['subs'][12]['AMCexists']) ] ),
          ('uros', 'Inputs', [crate['subs'][slotNum] for crate in monitorData['subs'] for slotNum in range(12) if ('AMCexists' not in crate['subs'][slotNum] or crate['subs'][slotNum]['AMCexists']) ] ),
          ]:
          
      for board in boardList:
        headers = ['boardID'] + [ '%s.%s'%(block['id'],metric['id']) for block in board['subs'] if block['id']!=inputBlk for metric in block['subs'] ]
        if not tables[boardTbl]:
          tables[boardTbl] += [ headers ]
        elif tables[boardTbl][0] != headers:
          raise Exception("Preparing Log Data error: new board's header list is not the same as the first's")
  
        tables[boardTbl] += [ [ board['id'] ] + [ metric['value'] for block in board['subs'] if block['id']!=inputBlk for metric in block['subs'] ] ]
        
        
        for input in next( block for block in board['subs'] if block['id']==inputBlk )['subs']:
          headers = ['inputNum'] + [ metric['id'] for metric in input['subs'] ]
          if not tables[inputBlk]:
            tables[inputBlk] += [ headers ]
          elif tables[inputBlk][0] != headers:
            raise Exception("Preparing Log Data error: new input's header list is not the same as the first's")
        
          tables[inputBlk] += [ [ '%s.%i'%(board['id'],input['id']) ] + [ metric['value'] for metric in input['subs'] ] ]

    log = ''
    for table in ['amc13', 'AMCPorts', 'uros', 'Inputs']:
      log += '\nTable %s\n'%table
      for row in tables[table]:
        log += '\t'.join( map(str,row) ) + '\n'
    
    return log

  def prepareDcsInitData(self, initData):
    def getunidmap(block, unid2map):
      if 'map' in block:
        unid2map[block['unid']] = block['map']
      elif 'subs' in block and block['subs']:
        for sub_block in block['subs']:
          getunidmap(sub_block, unid2map)

    unid2map = {}
    getunidmap(initData, unid2map)
    return unid2map
  
  def prepareDcsData(self, monitorData):
    metrics2save = ['hits2daq_ctr', 'hits2daq_ctr_recent', 'evs_lost_ratio', 'evs_lost_ratio_recent']
    def getRelevantMetrics(block, unid2metrics):
      if block['id'] in metrics2save:
        chanUnid = block['unid'].rsplit('-',1)[0]
        if chanUnid not in unid2metrics: unid2metrics[chanUnid] = {}
        unid2metrics[chanUnid][block['id']] = block['value']
      elif 'subs' in block and block['subs']:
        for sub_block in block['subs']:
          getRelevantMetrics(sub_block, unid2metrics)

    unid2metrics = {}
    getRelevantMetrics(monitorData, unid2metrics)
    
    return unid2metrics
    
    