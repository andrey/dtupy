################################################################################
## The UROS driver wrapper plus some helpers and the transitions
################################################################################

import time
import uros, tm7common
from dtupy.bcolors import bcolors

class UROS(uros.UROSController):
  isDAQsource = True
  fw_type_identifier = 0x02
  
  read                      = tm7common.read
  write                     = tm7common.write
  getNode                   = tm7common.getNode
  reg_num2name              = tm7common.reg_num2name
  gth_debuginfo             = tm7common.gth_debuginfo
  amc13link_reset_duration  = tm7common.amc13link_reset_duration
  ttc_err_ctr_clear         = tm7common.ttc_err_ctr_clear
  ttc_ctr_clear             = tm7common.ttc_ctr_clear
  measfreq                  = tm7common.measfreq
  ttc_stats                 = tm7common.ttc_stats
  verifyBoardTypeVersion    = tm7common.verifyBoardTypeVersion
  
  def __init__(self, device):
    super(UROS, self).__init__(device)
    self.storedReads = {}
    # following lines calculate the data necessary to store prefetched data into storedReads from a readBlock
    self.robRegAddrMask = { reg:(
            self.hw().getNode(reg).getAddress()-self.hw().getNode('readout.stat.rob').getAddress(), #address
            self.hw().getNode(reg).getMask(), #shift right
            max([bitnum for bitnum in range(32) if (self.hw().getNode(reg).getMask() & (2**bitnum -1)) == 0]) #shift right
            ) for reg in self.hw().getNodes('readout\.stat\.rob\..*') }
    self.robRegFrom = self.hw().getNode('readout.stat.rob').getAddress()
    self.robRegNum = max([self.robRegAddrMask[reg][0] for reg in self.robRegAddrMask])+1

  def calculateDisabledChannels(self, params_override = {}):
    params = self.getParams(params_override)
    self.disabled_channels = 0
    for mtp in range(1,7):
      disable_mtp = params['disable_mtp_%i'%mtp] if 'disable_mtp_%i'%mtp in params else params['disable_mtp']
      self.disabled_channels += (disable_mtp&0xFFF)<<((mtp-1)*12)
    
  def freezeStats(self, value):
    self.write("readout.conf.stat_freeze", value)
    
  def statsFromChan(self, channel):
    self.write("readout.conf.rob.stat_mux",channel) # contains dispatch
    blockData = self.hw().getClient().readBlock(self.robRegFrom, self.robRegNum)
    self.hw().dispatch()
    blockData = blockData.value()
    self.storedReads.update( { reg+str(channel):((blockData[self.robRegAddrMask[reg][0]]&self.robRegAddrMask[reg][1])>>self.robRegAddrMask[reg][2]) for reg in self.robRegAddrMask}  )
  
  def dispatch(self):
    self.hw().dispatch()
  
  def activeInputs(self):
    if not hasattr(self, 'disabled_channels'):
      self.calculateDisabledChannels()
    return [n for n in range(72) if not (self.disabled_channels>>n)&1]

    
  ################################################################################
  ## uros-level functions for cell operation
  ################################################################################
  
  def configureCmd(uros, params_override = {}):
    """Runs the configure transition"""
    params = uros.getParams(params_override)
    uros.calculateDisabledChannels(params_override)
    
    FWvers = uros.verifyBoardTypeVersion( params )
    
    uros.reset( 'external' )
    
    for rob in range(72):
      for reg in ['pd_inertia', 'pd_decay', 'idelay']:
        value = params['%s_%02i'%(reg,rob)] if '%s_%02i'%(reg,rob) in params else params[reg]
        uros.write( 'readout.conf.rob.%s_%02i'%(reg,rob), value )
    
    uros.write( 'readout.conf.rob.ctr_ena',             0 )
    uros.write( 'readout.conf.rob.max_inevent_idles',   params['max_inevent_idles'] )
    
    uros.write( 'readout.conf.dteb.max_words_per_ch',   params['max_words_per_ch'] )
    uros.write( 'readout.conf.dteb.max_words_per_ev',   params['max_words_per_ev'] )
    uros.write( 'readout.conf.dteb.max_hdr_delay',      params['max_hdr_delay'] )
    uros.write( 'readout.conf.dteb.rob_evn_inc',        params['rob_evn_inc'] )
    uros.write( 'readout.conf.dteb.enforce_bxn',        params['enforce_bxn'] )
    uros.write( 'readout.conf.dteb.status_mask',        params['dteb_status_mask'] )
    for mtp in range(1,7):
      uros.write('readout.conf.dteb.disable_ch_%i'%mtp, ( uros.disabled_channels>>(12*(mtp-1)) ) & 0xFFF )
    uros.write( 'readout.conf.dteb.dummydata_mul',      params['dummydata_mul'] )
  
    uros.write( 'readout.conf.dteb.big_evt_size_ctr_threshold',   params['big_evt_size_ctr_threshold'] )
    uros.write( 'readout.conf.rob.af_thres',            params['robff_af_thres'] )
    
    boardID = ((FWvers&0xFF)<<8) + (params['slotMap']&0xF)
    uros.write( 'readout.conf.boardID',                 boardID )
    uros.write( 'readout.conf.userData',                params['userData'] )
    uros.write( 'readout.conf.bcn_incr',                params['tm7BcnIncr'])
    uros.write( 'readout.conf.tts_thresholds.rdy2ofw',  params['tts_threshold_0'] )
    uros.write( 'readout.conf.tts_thresholds.ofw2rdy',  params['tts_threshold_1'] )
    uros.write( 'readout.conf.tts_thresholds.ofw2bsy',  params['tts_threshold_2'] )
    uros.write( 'readout.conf.tts_thresholds.bsy2ofw',  params['tts_threshold_3'] )
    #  0 <= val <= 15: the duration is 2**val-1 in ipbus clock cycles
    uros.write( 'readout.conf.amc13.rst_len',           2 )
  
  
  def startCmd(uros):
    """Runs the start transition"""
    
    uros.write( 'readout.conf.ctr_rst', 1)
    uros.write( 'readout.conf.rob.ctr_ena', 1)
    uros.write( 'readout.conf.ctr_rst', 0)
    
  def stopCmd(uros):
    """Runs the stop transition"""
    
    uros.write( 'readout.conf.rob.ctr_ena', 0)
  
  def stopCtrs(uros):
    uros.write( 'readout.conf.rob.ctr_ena', 0)
  def resetCtrs(uros):
    uros.write( 'readout.conf.ctr_rst', 1)
    uros.write( 'readout.conf.ctr_rst', 0)
  def startCtrs(uros):
    uros.write( 'readout.conf.rob.ctr_ena', 1)
