#!/bin/env python

from pprint import pprint
import sys

latency = 500
window = 450

#dumpfile = '20181217_amc13dump1.txt'
dumpfile = sys.argv[1]

with open(dumpfile,'r') as f:
  dump = f.read()

def parseEvent(reall):
  realldump = reall.split('+--------------+------------------+\n')[1]
  wordlist = [int(line.split('|')[2].strip(),16) for line in realldump.split('\n') if '|' in line]

  eventdata = {}

  eventdata['id'] = int((wordlist[0]>>32)&0xFFFFFF)
  eventdata['bx'] = int((wordlist[0]>>20)&0xFFF)
  eventdata['orbit'] = int((wordlist[1]>>4)&0xFFFFFFFF)
  eventdata['size'] = int((wordlist[-1]>>32)&0xFFFFFF)

  eventdata['tptimer'] = int((wordlist[4]>>(32+12))&0xFFFF)
  eventdata['lasttpbx'] = int((wordlist[4]>>(32+0))&0xFFF)


  payload = wordlist[5:-3]

  hits = []
  tps = []
  for hw in payload:
    if int(hw>>60) == 1:
      hits += [{'data':int(hw>>30)&0x3FFFFFFF}]
      hits += [{'data':int(hw&0x3FFFFFFF)}]
    elif int(hw>>62) == 2:
      tps += [{'word1':hw}]
    elif int(hw>>62) == 3:
      tps[-1]['word2'] = hw

  hits = [hit for hit in hits if hit['data'] != 0x3FFFFFFF]

  for hit in hits:
    hit['ch_id'] = (hit['data']>>17)&0x1FF
    hit['bx'] = (hit['data']>>5)&0xFFF
    hit['fine'] = (hit['data']>>0)&0x1F
    hit['bxdiff'] = hit['bx']-eventdata['bx']
    if hit['bxdiff'] >= 0: hit['bxdiff'] -= 3564
    hit['str'] = 'bx=%i.%i (%i) wrtEvBx=%i ch=%i (%i.%i) '%(hit['bx'], hit['fine'], hit['bx']*32 + hit['fine'], hit['bxdiff'], hit['ch_id'], hit['ch_id']%4, hit['ch_id']/4 )
  for tp in tps:
    tp['st']        = int((tp['word1']>>60)&0x3)
    tp['sl']        = int((tp['word1']>>58)&0x3)
    tp['bx']        = int((tp['word1']>>46)&0xFFF)
    tp['tpfine']    = int((tp['word1']>>41)&0x1F)
    tp['quality']   = int((tp['word1']>>35)&0x3F)
    tp['slope']     = 1.*int((tp['word1']>>16)&0x7FFF)/4096
    tp['position']  = 1.*int((tp['word1']>>0)&0xFFFF)/4
    
    tp['chi2']      = int((tp['word2']>>0)&0x1F)
    tp['index']     = int((tp['word2']>>5)&0x7)
    tp['lateral']   = [ int((tp['word2']>>(8 +i*1))&0x1 ) for i in range(4) ]
    tp['hitsused']  = [ int((tp['word2']>>(12+i*1))&0x1 ) for i in range(4) ]
    tp['hittimes']  = [ int((tp['word2']>>(16+i*4))&0xF ) for i in range(4) ]
    tp['hitchs']    = [ int((tp['word2']>>(32+i*7))&0x3F) for i in range(4) ]
    
    tp['str'] = 'bx=%i.%i (%i)'%(tp['bx'], tp['tpfine'], tp['bx']*32 + tp['tpfine'])
    tp['str'] += '    q=%i, slope=%.03f, position=%.02f, chi2=%i'%(tp['quality'], tp['slope'], tp['position'], tp['chi2'])
    tp['str'] += '\nlaterality=%s, hits_used=%s, hit_times=%s, hit_chs=%s'%(tp['lateral'], tp['hitsused'], str(tp['hittimes']), str(tp['hitchs']))
    

  eventdata['hits'] = hits
  eventdata['tps'] = tps

  return eventdata


def printEvent(eventdata):
  print '-'*80
  print 'EVid = %i, BXid = %i, size = %i (hits = %i)'%(eventdata['id'],eventdata['bx'],eventdata['size'], len(eventdata['hits']))
  print 'tptimer = %i, lastTPbx = %i'%(eventdata['tptimer'],eventdata['lasttpbx'])
  print 'Hits:'
  for hit in eventdata['hits']:
    print '  - ' + hit['str']
  print 'Trigger primitives:'
  for tp in eventdata['tps']:
    print '  - '+ tp['str'].replace('\n','\n    ')




realllist = dump.split('|  FED ID      |')[1:]
eventdatas = map(parseEvent, realllist)
map(printEvent, eventdatas)
print '-'*80

chhisto = {}
differentCHhisto = {}
hitshisto = {}
hitswithmorethan4channels = []
overflowcount = 0
bxid_tpbx_histo = {}

for ed in eventdatas:
  evchs = []
  for hit in ed['hits']:
    ch = hit['ch_id']
    if ch not in chhisto: chhisto[ch] = 0
    chhisto[ch] += 1
    evchs+= [ch]
  uniqueCHs = len(set(evchs))
  if uniqueCHs not in differentCHhisto: differentCHhisto[uniqueCHs] = 0
  differentCHhisto[uniqueCHs] += 1
  if uniqueCHs > 4: hitswithmorethan4channels += [ed]

  if len(evchs) not in hitshisto: hitshisto[len(evchs)] = 0
  hitshisto[len(evchs)] += 1

  if ed['tptimer']== 1023: overflowcount += 1
  else:
    bxid_tpbx = ed['bx'] - ed['lasttpbx']
    if bxid_tpbx <0: bxid_tpbx += 3564
    if bxid_tpbx not in bxid_tpbx_histo: bxid_tpbx_histo[bxid_tpbx] = 0
    bxid_tpbx_histo[bxid_tpbx] += 1





print 'number of total hits in each channel'
pprint(chhisto)
print 'number of event with this total number of active different channels'
pprint(differentCHhisto)
print 'number of event with this total number of hits'
pprint(hitshisto)

print 'overflow count = %i'%overflowcount
print 'histogram of differences from latTPbx to BXid'
pprint(bxid_tpbx_histo)

#print 'dump of events with more than 4 chs active'
#for ed in hitswithmorethan4channels:
#    printEvent(ed)


