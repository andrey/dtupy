#!/bin/env python

import os, sys
from dtupy.systembuilder import loadkey
from dtupy.unpacker import unpack
from pprint import pprint
import time

labels = ['q','time', 'bx', 'ch0', 'ch1', 'ch2', 'ch3', 't0', 't1', 't2', 't3', 'laterality0', 'laterality1', 'laterality2', 'laterality3', 'position', 'slope', 'chi2' ]
lines = []
with open(os.path.expandvars('${DTUPY_ETC}/ab7/outputdata.txt'), 'r') as f:
  lines = f.readlines()

lines = [ line.strip().split(',') for line in lines ]

def str2num( text ):
  return int(text) if not '.' in text else float(text)

lines = [ {labels[i]:str2num(line[i].strip()) for i in range(len(labels))} for line in lines  if len(line) == len(labels)]

lines = lines[:-1] if len(lines)%2 == 1 else lines

s = loadkey('ab7_slice_jm')
ab7 = s.hw.slice.slot11
amc13 = s.hw.slice.amc13


#debugdata = {}
#
#for regname in ab7.hw().getNodes('technical_trigger\.stat\.debug_counters\.phi3\..*\..*'):
#  shortname = regname[len('technical_trigger.stat.debug_counters.phi3.'):]
#  value = ab7.read(regname)
#  debugdata[shortname] = [ value ]

counter = 0

time2bx = True

line = lines[2]
#for line in [line for pair in zip(lines[:len(lines)/2], lines[len(lines)/2:]) for line in pair]:
for line in lines:
  for offset in range(8):
    #counter += 1
    #if counter > 10: break

    print '-'*50
    print '-'*50
    print 'Line from outputdata.txt with offset', offset
    print line['q'], line['time'], line['bx'], '[', line['ch0'], line['ch1'], line['ch2'], line['ch3'], '], [', line['t0'], line['t1'], line['t2'], line['t3'], '], [', line['laterality0'], line['laterality1'], line['laterality2'], line['laterality3'], '],', line['position'], line['slope'], line['chi2']
    print '(', line['q'], line['time'] + offset*32, line['bx']+offset, '[', line['ch0']+offset, line['ch1']+offset, line['ch2']+offset, line['ch3']+offset, '], [',  line['t0']+ offset*32, line['t1']+ offset*32, line['t2']+ offset*32, line['t3']+ offset*32, '], [', line['laterality0'], line['laterality1'], line['laterality2'], line['laterality3'], '],', line['position'], line['slope'], line['chi2'], ')'

    params = { 'testMode_enable':1 }
    used_arrivalBXs = []
    for ch in range(4):
      params['testMode_hit%i_ch'%ch] = (line['ch%i'%ch]+offset)*4+ch #if ch!=3 else 20
      if not time2bx:
        params['testMode_hit%i_BX'%ch] = line['t%i'%ch]/32 + offset
        params['testMode_hit%i_fine'%ch] = line['t%i'%ch]%32
      else:
        params['testMode_hit%i_BX'%ch] = line['t%i'%ch]/25 + offset
        params['testMode_hit%i_fine'%ch] = int( ( line['t%i'%ch]%25 ) * 30./25 ) + 1

      arrivalBX = params['testMode_hit%i_BX'%ch] + 100 + offset
      while arrivalBX in used_arrivalBXs:
        arrivalBX += 1
      used_arrivalBXs += [ arrivalBX ]#, arrivalBX-1, arrivalBX-2 ,  arrivalBX+1 , arrivalBX+2 ]
      params['testMode_hit%i_arrivalBX'%ch] = arrivalBX
      
      print '  injected hit',ch,'ch',params['testMode_hit%i_ch'%ch], 'bx', params['testMode_hit%i_BX'%ch], 'fine', params['testMode_hit%i_fine'%ch], '@bx',arrivalBX

    ab7.testModeConfigure(params)

    rawinputhits = ab7.read('technical_trigger.stat.debug_counters.phi3.IncomingRawHits.count_in')
    numtries_init = 4
    numtries = numtries_init
    while ab7.read('technical_trigger.stat.debug_counters.phi3.IncomingRawHits.count_in') < rawinputhits + 4 and numtries > 0:
      if numtries < numtries_init:
        print "retrying to launch trigger"
      numtries -= 1
      ab7.testModeTrigger()
      

    #print '-'*50
    #print '-- output for injection with offset',offset
    #print '-'*50
    #for i in range(5):
    #  amc13.readUnpackPrintEvent('ab7')
    
    for i in range(5):
      evt = amc13.readEvent()
      if evt:
        evt = unpack(evt, 'ab7')
        print '-'*10
        print 'Event @BX=%i, tptimer=%i, lasttpbx=%i, numtps=%i, numhits=%i'%(evt['BX_id'], evt['AMCs'][0]['tptimer'], evt['AMCs'][0]['lasttpbx'], len(evt['AMCs'][0]['tps']), len(evt['AMCs'][0]['hits']))
        for tp in evt['AMCs'][0]['tps']:
          print tp['quality'], tp['bx']*32 + tp['tpfine'], '---', tp['hitchs'], tp['hittimes'], tp['lateral'], tp['position'], tp['slope'], tp['chi2']
      
    #debugdata[offset] = {}
    print '-'*10
    print 'Debug regs'
    for shortname in ['tps.ff.rden_ctr_lo', 'ttgen.valid_ctr', 'ttgen.l1asgen_ctr'] + ['ttgen.tp%i'%i for i in range(7)]:
      regname = 'readout.stat.' + shortname
      value = ab7.read(regname)
      if 'ttgen.tp' in shortname:
        value = '%i (%i -> %i)'%( (value>>12)&0x1FFFF, (value>>17)&0xFFF, value&0xFFF )
      print value, '\t', shortname
    print
    for prefix in ['IncomingRawHits', 'RawSegmentCandidate', 'SegmentCandidate', 'SegmentPostDuplicate', 'SegmentAfterAnalysis', 'MainParams', 'OutgoingSegments']:
      for suffix in ['.count_in','.count_out']:
        shortname = prefix + suffix
        regname = 'technical_trigger.stat.debug_counters.phi3.' + shortname;
        value = ab7.read(regname)
        print value, '\t', shortname
      #  debugdata[shortname] += [ value ]
    
    time.sleep(.02)
    #a = raw_input('Press enter to continue with next hit batch')

#for key in sorted(debugdata.keys()):
#  print key
#  print debugdata[key]
    