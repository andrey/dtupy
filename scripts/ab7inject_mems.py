#!/bin/env python

import os, sys
from dtupy.systembuilder import loadkey
from dtupy.unpacker import unpack
from pprint import pprint
import time

labels = ['q','time', 'bx', 'ch0', 'ch1', 'ch2', 'ch3', 't0', 't1', 't2', 't3', 'laterality0', 'laterality1', 'laterality2', 'laterality3', 'position', 'slope', 'chi2' ]
lines = []
with open(os.path.expandvars('${DTUPY_ETC}/ab7/outputdata.txt'), 'r') as f:
  lines = f.readlines()
lines = [ line.strip().split(',') for line in lines ]
def str2num( text ):
  return int(text) if not '.' in text else float(text)
lines = [ {labels[i]:str2num(line[i].strip()) for i in range(len(labels))} for line in lines  if len(line) == len(labels)]
lines = lines[:-1] if len(lines)%2 == 1 else lines

s = loadkey('ab7_slice_jm')
ab7 = s.hw.slice.slot11
amc13 = s.hw.slice.amc13

hits2write = []

for line in lines[:1]: # actually I'm not using the line here, just making up the info
  for ch in range(4):
    # creating fake event in the 20th cell of each layer, bx 1000, with an arbitrary fine count
    channel = 20 * 4 + ch
    bx = 2000 + ch
    fine = 5*(ch+1)
    arrivalBX = bx + 100
    orbit = 0
    hits2write += [ {'ch': channel, 'bx': bx, 'fine':fine, 'arrivalBX':arrivalBX, 'orbit':orbit} ]
    
ab7.testModeEnable( 0 )
ab7.testMode_inject_mem( hits2write )
ab7.testModeEnable( 1 )

time.sleep(.1)

tps  = ab7.testMode_readout_mem()

for i in range(len(tps)):
  print '*'*50
  print i
  pprint(tps[i])
