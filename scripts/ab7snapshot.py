#!/bin/env python

import os, sys
from dtupy.systembuilder import loadkey
from dtupy.unpacker import unpack
from pprint import pprint
import time

s = loadkey('active')
ab7 = s.hw.madrid.slot11
amc13 = s.hw.madrid.amc13

ab7.write( 'technical_trigger.conf.bx_time_tolerance',   30  )
ab7.write( 'technical_trigger.conf.chisqr_threshold',    3  )
ab7.write( 'technical_trigger.conf.bx_time_convert_ena', 1 )

ab7.debugModeSetup(mode='snapshot', value = 1)
a = raw_input('Press enter to stop the snapshot')

hits = ab7.debugMode_readout_mem(prims=False)
tps = ab7.debugMode_readout_mem(prims=True)

ab7.debugModeSetup(mode='snapshot', value = 0)


for i in range(len(hits)):
  hit = hits[i]
  print i,'hit@%i.%i: ch=%i bx=%i.%i(%ins)'%(hit['arrivalOC'], hit['arrivalBX'], hit['ch'], hit['bx'], hit['tdc'], int(25*(hit['bx']+(hit['tdc']-1)/30.)) )

print

for i in range(len(tps)):
  tp = tps[i]
  print i, 'tp@%i.%i: bx=%i(%ins), q=%i, lat=%s, hits:'%(tp['arrivalOC'], tp['arrivalBX'], tp['bx_id'], tp['main_params']['bx_time'], tp['main_params']['quality'], tp['main_params']['laterality_combination']),
  for j in range(4):
    srchit = tp['main_params']['hits'][j]
    if srchit['dt_time']['valid']:
      print '%i.%i@%i, '%(j,srchit['channel_id'], srchit['dt_time']['tdc_time']),
  print

