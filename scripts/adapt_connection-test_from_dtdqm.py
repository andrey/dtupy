with open('/nfshome0/dtdqm/dtupy/TM7_sw_repo/mp7/tests/etc/mp7/connections-test.xml','r') as f:
    orig = f.read()

def noxmlcomments(txt):
    if '<!--' not in txt:
        return txt
    halves = txt.split('<!--',1)
    if '-->' in halves[1]:
         return halves[0] + noxmlcomments(halves[1].split('-->',1)[1])
    return halves[0]

lines = [line for line in noxmlcomments(orig).split('\n')  if 'PSEPC' in line or 'YB' in line]

with open('connections-tm7.xml','w') as f:
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<connections>\n\n')
    for l in lines:
        f.write(l.split('address_table')[0])
        f.write('address_table="file:///opt/cactus/etc/tm7/addrtab/tm7_infra.xml" />\n')
    f.write('\n</connections>\n')
