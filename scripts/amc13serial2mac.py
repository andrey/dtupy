for sn in [200,206,219,220,221,407]:
  nsn = 511 - sn
  fixed_part = "080030f30"
  var_part = (((sn>>8)&0x1)<< 10) + (((sn>>6)&0x3)<<7) + (nsn&0x3F)

  print 'SN#%i'%sn, 'T1: ' + fixed_part + '%03x'%(var_part + (1<<6)), 'T2: ' + fixed_part + '%03x'%(var_part ), 'T2old: ' + fixed_part + '%03x'%(var_part &0x1FF)
