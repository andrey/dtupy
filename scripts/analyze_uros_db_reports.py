#!/usr/bin/env python

import zlib
import cx_Oracle
from pprint import pprint


def analyze(rawdata):
  tables = {'amc13':{}, 'AMCPorts':{}, 'uros':{}, 'Inputs':{}}
  rawdata = rawdata.split('\n')
  
  currentTable = ''
  fieldNames = []
  for line in rawdata:
    if 'Table' in line:
      currentTable = line.split()[1]
      fieldNames = []
    elif not fieldNames:
      fieldNames = line.split()[1:]
    elif line:
      tables[currentTable][line.split()[0]] = dict(zip(fieldNames, line.split()[1:]))
  
  print tables['amc13']['amc13']['evb.l1aCount'], tables['uros']['112']['Readout.l1a_ctr']

      


username = 'cms_dt_elec_conf'
service = 'cms_omds_lb'
password = 'cmsonr2008'


con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
con.autocommit = 1
cur = con.cursor()

cur.execute('select RUN, TIME_STAMP from W_MON_DATA order by TIME_STAMP asc' ,{} )
rows = cur.fetchall()
snapshotList = {}

for row in rows:
  if row[0] not in snapshotList: snapshotList[row[0]] = []
  snapshotList[row[0]] += [ row[1] ]

del snapshotList[max(snapshotList.keys())]

for row in sort

rawdataList = {}
for run in snapshotList:
  cur.execute('select RAW_DATA from W_MON_DATA where TIME_STAMP=:ts' ,{ 'ts':snapshotList[run][-4] } )
  rawdataList[run] = cur.fetchone()[0].read()
  
cur.close()
con.close()

for run in rawdataList:
  print run, 
  analyze(rawdataList[run])

exit()




runs = {}
chans = {}
totalEventsBySlot = { slot:0 for slot in range(1,13) }
minrun = 1e100
maxrun = 0


def analyze(lines):
  
  global runs, chans, totalEventsBySlot

  if len(lines) != 170:
    return False
    raise Exception('Number of lines != 170: %s'%filename)
  
  [date, time, bla0, state, bla1, bla2, run] = lines[0].split()
  if [bla0, bla1, bla2] != ['State:', 'Run', 'Number:']:
    raise Exception('incorrect line 1 of file: %s'%filename)
    return False
  run = int(run)
  runs[run] = {'endtime': date + ' ' + time, 'rawdata':{}}
  
  if lines[1] != 'slot\tmtp\tch\tdBval\tulck\tpar(bad)\tstartstop\tphase\thdr\th-t\tint\tidl\tout\tunk\tevn\twrd\tntr\tjumps\thits\tTDCerr\tffofw\tbxbd\tmuch\tlate\tpurg\thits2daq':
    return False
  
  lines = [l.strip() for l in lines[2:] if l.strip()]
  
  if len(lines) != 168:
    raise Exception('number of lines not 168: %s'%filename)
    return False
  
  headersBySlot = { slot:{} for slot in range(1,13) }
  nChansBySlot = { slot:0 for slot in range(1,13) }
  for l in lines:
    if len(l.split()) != 26:
      raise Exception('number of lines not 168: %s, %s'%(filename, l))
      return False
    [slot, mtp, ch, dBval, ulck, par_bad, startstop, phase, hdr, h_t, interr, idl, out, unk, evn, wrd, ntr, jumps, hits, TDCerr, ffofw, bxbd, much, late, purg, hits2daq] = l.split()
    [slot, mtp, ch] = map(int, [slot, mtp, ch])
    runs[run]['rawdata'][(slot,12*(mtp-1)+ch)] = {'dBval':dBval, 'ulck':ulck, 'par_bad':par_bad, 'startstop':startstop, 'phase':phase, 'hdr':hdr, 'h_t':h_t, 'int':interr, 'idl':idl, 'out':out, 'unk':unk, 'evn':evn, 'wrd':wrd, 'ntr':ntr, 'jumps':jumps, 'hits':hits, 'TDCerr':TDCerr, 'ffofw':ffofw, 'bxbd':bxbd, 'much':much, 'late':late, 'purg':purg, 'hits2daq':hits2daq}
    if hdr!='nan': hdr = int(hdr)
    if hdr not in headersBySlot[slot]:
      headersBySlot[slot][hdr] = 0
    headersBySlot[slot][hdr] += 1
    nChansBySlot[slot] +=1
  
  mostCommonHdrBySlot = {slot:sorted(headersBySlot[slot].keys(), key=lambda k:headersBySlot[slot][k], reverse=True)[0] 
                          for slot in headersBySlot if headersBySlot[slot]}
  numEventsBySlot = { slot:(0 if mostCommonHdrBySlot[slot] == 'nan' or headersBySlot[slot][mostCommonHdrBySlot[slot]] < .25*nChansBySlot[slot] 
                            else mostCommonHdrBySlot[slot]) 
                      for slot in mostCommonHdrBySlot}
  if not all( [numEventsBySlot[slot]>0 for slot in numEventsBySlot] ): return False
  #print run
  for slot in numEventsBySlot:
    totalEventsBySlot[slot] += numEventsBySlot[slot]
    #if not numEventsBySlot[slot]:
    #  print 'slot %i, mostCommonHdr %s, shared by %i chans, of a total of %i chans'%(slot, str(mostCommonHdrBySlot[slot]), headersBySlot[slot][mostCommonHdrBySlot[slot]], nChansBySlot[slot] )
  
  print run
  for chan in runs[run]['rawdata']:
    if not chan in chans: chans[chan] = {'headers':0, 'trailers':0, 'trailerRatio':0}
    try: 
      chanheaders = int(runs[run]['rawdata'][chan]['hdr']) if runs[run]['rawdata'][chan]['hdr'] != 'nan' else 0
      chantrailers = chanheaders - int(runs[run]['rawdata'][chan]['h_t'])
    except:
      chanheaders = 0
      chantrailers = 0
    chans[chan]['headers'] += chanheaders
    chans[chan]['trailers'] += chantrailers
    if chan not in [(7,65),(12,65)] and (chantrailers > numEventsBySlot[chan[0]] or chantrailers < numEventsBySlot[chan[0]] ):
      print '\t',chan, chantrailers - numEventsBySlot[chan[0]], 100.*chantrailers/numEventsBySlot[chan[0]], chantrailers, numEventsBySlot[chan[0]]
      print '\t',headersBySlot[chan[0]]
      print '\t',runs[run]['rawdata'][chan]
    try:
      hits = int(runs[run]['rawdata'][chan]['hits'] )
      hits2daq = int(runs[run]['rawdata'][chan]['hits2daq'] )
      if chan not in [(7,65),(12,65)] and ( hits > hits2daq ):
        print '\tHITS:', 1.*(hits - hits2daq)/hits, hits - hits2daq, hits, hits2daq
        print '\t',runs[run]['rawdata'][chan]
    except: pass
    chans[chan]['trailerRatio'] = 100.*chans[chan]['trailers']/totalEventsBySlot[chan[0]]
  return True


# de los no append tienen uno 0 lineas, varios 172 y casi todos 173
# total number of runs = 703
# total space occupied by gzip files = 3036515162
# Distribution of gzip runs by size (>10^n)
# {0: 0, 1: 0, 2: 0, 3: 0, 4: 34, 5: 282, 6: 313, 7: 74, 8: 0, 9: 0}
# Percentage of the total space covered by runs in the size interval:
# {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.068, 5: 4.3, 6: 30.0, 7: 65.6, 8: 0.0, 9: 0.0}
# ==> We keep files bigger than 1e6
sizethreshold = 10**7


goodones = 0

listdir = os.listdir('.')
reportFile = 'uros_rx_integrity_report_%i.txt'
appendFile = 'uros_rx_integrity_report_append_%i.txt.gz'
for run in range(300000, 306000):
  if reportFile%run in listdir and appendFile%run in listdir and os.stat(appendFile%run).st_size>sizethreshold:
    with open(reportFile%run, 'r') as f:
      lines = f.read().split('\n')
    
    #with gzip.open(appendFile%run, 'rb') as f:
    #  gztxt =f.seek(-3, os.SEEK_END)
    #print appendFile%run, len(gztxt)
    #lines = gztxt.split('=')[-1].split('\n')

    lines = [l for l in lines if l]
    if analyze(lines[1:]):
      goodones += 1
      minrun = min(minrun, run)
      maxrun = max(maxrun, run)
      
  
for chan in sorted( chans.keys(), key = lambda k: chans[k]['trailerRatio'], reverse = True ):
  print chan, '\t', chans[chan]['trailerRatio']
    
print goodones, minrun, maxrun
print totalEventsBySlot
print min([totalEventsBySlot[slot] for slot in totalEventsBySlot if totalEventsBySlot[slot]]), max([totalEventsBySlot[slot] for slot in totalEventsBySlot if totalEventsBySlot[slot]])
