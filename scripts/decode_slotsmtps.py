#!/bin/env python

import sys, os
from pprint import pprint
from dtupy.dtromap import *

htmlcode = '''<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <title>uROS vs OFCu vs ROS vs Physical mapper</title>
 <script>
var data = REPLACE_WITH_DATA;

function getFilter(){
  filter = []
  for(var item in data[0]){
    var filterInput = document.getElementById("f"+item.toString());
    if(filterInput!=null && filterInput.value!= "") filter[item] = filterInput.value;
    else filter[item] = null;
  }
  return filter;
}

function filterAndDisplay() {
  filter = getFilter();
  var tableBody = document.getElementById("tb");
  while(tableBody.firstChild) tableBody.removeChild(tableBody.firstChild);
  for(var row in data){
    doPrint = data[row].every(function(value, index){return filter[index]==null || filter[index]==value.toString();});
    if(doPrint){
      var tr = document.createElement('TR');
      for(var item in data[row]){
        var td = document.createElement('TD');
        td.appendChild(document.createTextNode(data[row][item].toString()));
        tr.appendChild(td);
      }
      tableBody.appendChild(tr);
    }
  }
}

function loaded() {
  var labelCells = document.getElementById("labels").children;
  for (i in labelCells) labelCells[i].innerHTML += '<br><input type="text" size="1" id="f'+i.toString()+'" oninput="filterAndDisplay()">';
  filterAndDisplay();
}
</script> 
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
</head>
 <body onload="loaded()"> 
 <h3>uROS vs OFCu vs ROS vs Physical mapper</h3>
<table style="width:100%">
  <thead>
    <tr>
      <th colspan="4">uROS map</th>
      <th colspan="4">OFCu map</th>
      <th colspan="5">ROS map</th>
      <th colspan="6">Physical map</th>
    </tr>
    <tr id="labels">
      <th>Crate</th><th>Slot</th><th>slotMap</th><th>Ch.</th>
      <th>Wh</th><th>Sector</th><th>MTP</th><th>Ch.</th>
      <th>Wh</th><th>Sector</th><th>MBx</th><th>Ch.</th><th>(0-24)</th>
      <th>Wh</th><th>Sector</th><th>St.</th><th>ROB</th><th>SL</th><th>Wires</th>
    </tr>
  </thead>
  <tbody id="tb">
  </tbody>
</table>
</body>
</html>'''


if len(sys.argv)!= 3:
  raise Exception('Please pass as first argument the name of the file containing the MTP connections, and output filename as second argument')

with open(sys.argv[1], 'r') as f:
  lines = f.readlines()

lines = map(lambda x:x.split('#')[0].strip(), lines)
lines = map(lambda x: x.split(), lines)
lines = [line for line in lines if line]

channels = []

if all([len(line)==6 for line in lines]):
  htmlcode = htmlcode.replace('<th colspan="4">uROS map</th>','<th colspan="3">uROS map</th>')
  htmlcode = htmlcode.replace('<th>slotMap</th>','')
  for line in lines:
    [crate, slot, input, wheel, sector, mtp] = [line[0]] + map(int,line[1:])
    for mtp_ch in range(12):
      ( ROS_sector, ROS_station, ROS_rob ) = mtp2ros( sector, mtp, mtp_ch )
      ROS_rob024 = ros_mb_024(ROS_station, ROS_rob)
      ( PHYS_sector, PHYS_station, PHYS_rob )  = ros2phys( ROS_sector, ROS_station, ROS_rob )
      ( SL, wire_min, wire_max ) = physwires( wheel, PHYS_sector, PHYS_station, PHYS_rob )
      channels += [ [crate, slot, (input-1)*12+mtp_ch, 
                     wheel, sector, mtp, mtp_ch,
                     wheel, ROS_sector, ROS_station, ROS_rob, ROS_rob024,
                     wheel, PHYS_sector, PHYS_station, PHYS_rob, SL, '%i-%i'%(wire_min, wire_max) ] ]

elif all([len(line)==3 for line in lines]):
  for line in lines:
    [crate, slot, slotMap] = [line[0]] + map(int,line[1:])
    for input in (range(1,7) if (slotMap-1)%6<4 else [1]):
      ( wheel, sector, mtp ) = slotMap2mtp(crate, slotMap, input)
      for mtp_ch in range(12):
        ( ROS_sector, ROS_station, ROS_rob ) = mtp2ros( sector, mtp, mtp_ch )
        ROS_rob024 = ros_mb_024(ROS_station, ROS_rob)
        ( PHYS_sector, PHYS_station, PHYS_rob )  = ros2phys( ROS_sector, ROS_station, ROS_rob )
        ( SL, wire_min, wire_max ) = physwires( wheel, PHYS_sector, PHYS_station, PHYS_rob )
        channels += [ [crate, slot, slotMap, (input-1)*12+mtp_ch, 
                       wheel, sector, mtp, mtp_ch,
                       wheel, ROS_sector, ROS_station, ROS_rob, ROS_rob024,
                       wheel, PHYS_sector, PHYS_station, PHYS_rob, SL, '%i-%i'%(wire_min, wire_max) ] ]
else:
  raise Exception('First argument file must contain either 3 (slotMap type mapping) or 6 columns (slice type mapping)')

############################################################

import json
data = json.dumps(channels, separators=(',',':')).replace('],[','],\n  [')
htmlcode = htmlcode.replace('REPLACE_WITH_DATA',data)
    
with open(sys.argv[2], 'w') as f:
  f.write(htmlcode)
#  f.write('slot\tch\twheel\tROS_sector\tROS_station\tROS_rob\tPHYS_sector\tPHYS_station\tPHYS_rob\n')
#  for channel_info in channels:
#    f.write('\t'.join(map(str,channel_info))+'\n')
