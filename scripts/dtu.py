#!/bin/env python

import argparse
from dtupy.cli import COMMANDS



if __name__ == '__main__':

    # create the top-level parser
    parser = argparse.ArgumentParser(prog='dtu.py')
    sp = parser.add_subparsers(metavar='command')
    for cmd in COMMANDS:
        cmd(sp)
    args = parser.parse_args()
    args.func(args)
    
    

