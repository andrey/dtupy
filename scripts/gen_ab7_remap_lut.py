#!/bin/env python

import sys

if len(sys.argv)!=3:
  print "please give name of the output file and the channels to enable:\n$ gen_ab7_remap_lut.py outputfilename.txt 64-95,112-127"
  exit()

  
if sys.argv[1][-4:] != '.txt':
  print "lut file must have txt extension"
  exit()
filename = sys.argv[1]


keepchannels_string = sys.argv[2]
keepchannels = []
for token in keepchannels_string.split(','):
  rangelimits = map(int,token.split('-'))
  if len(rangelimits) == 1: rangelimits = rangelimits * 2
  if len(rangelimits) == 2: rangelimits[1] += 1
  else: raise Exception('Channel ranges can only have one hyphen. This is wrong: %s'%token)
  keepchannels += range(*rangelimits)

print 'Writing remap lut to file %s as a transparent pass filter for channels: '%filename, keepchannels

remap_lut = [i if i in keepchannels else 0xFFFFFFFF for i in range(256)]
remap_lut = '\n'.join(['%X'%num for num in remap_lut])

with open(filename,'w') as f:
  f.write(remap_lut)
