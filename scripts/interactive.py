## load PYTHONSTARTUP (in case the interactive console was launched without loading it)
if __name__=='__main__':
    try:
        import os,imp
        foo = imp.load_source('pystartup',os.environ['PYTHONSTARTUP'])
        del os,imp,foo
    except:
        pass


import sys
from dtupy import systembuilder

key = sys.argv[1]

s = systembuilder.loadkey(key, maskCreationException = True)

print '[INFO] Done loading system described in key %s as global variable "s"'%key

if len(s.hw) == 1:
  globals().update(s.hw[s.hw.keys()[0]])
  print '[INFO] The following global variables were set, corresponding to the only crate ("%s") in this system: %s'%(s.hw.keys()[0], ', '.join(  sorted(s.hw[s.hw.keys()[0]].keys())  ))
  
