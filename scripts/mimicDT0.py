#!/usr/bin/env python

import os, random, traceback
import time, datetime
import urllib2
from dtupy.uroscell import UROSCell


# Should I keep going?
id = '%06i'%int(random.random()*1e6)

status = ''

oserrorcounter = 0

def goahead():
  global oserrorcounter
  stopCause = 0
  if oserrorcounter: oserrorcounter -= 1
  if 'mimic_disable' in os.listdir('.'):
    stopCause = 1
  for fn in os.listdir('.'):
    try:
      if 'mimic_onduty_' in fn and not 'id%s'%id in fn and (time.time()-os.path.getmtime(fn) < 50):
        stopCause = 2
      if 'mimic_onduty_' in fn and not 'id%s'%id in fn and (time.time()-os.path.getmtime(fn) > 50):
        os.remove(fn)
    except OSError:
      oserrorcounter += 2
  if oserrorcounter > 10:
    stopCause = 3
  if not stopCause:
    with open('mimic_onduty_id%s'%(id),'w') as f:
      f.write(' ')
  elif status != '': # if the script had already run once, this will cause it to stop --> report it
    with open('mimic_report.txt','a') as f:
      f.write(str(datetime.datetime.now()) + '\tExiting session of the mimic script (#%s) because an exit condition was met (%i)\n'%(id, stopCause))
    
  return stopCause == 0

try:
  while goahead():
    laststatus = status
    
    # Report start of new session of mimic script
    if status == '':
      with open('mimic_report.txt','a') as f:
        f.write(str(datetime.datetime.now()) + '\tStarting new session of the mimic script (#%s)\n'%id)
    
    # Retrieve the web page and mark errors of communication
    try:
      # 41000 -> minidaq, 40000 -> GR
      html = urllib2.urlopen('http://vmepc-s2g16-20-01.cms:40000/urn:xdaq-application:lid=11/').read()
      status = 'connectionAccepted'
    except urllib2.URLError:
      status = 'connectionRefused'
    except:
      status = 'exceptionInUrlOpen'
      
    if status in ['connectionRefused', 'exceptionInUrlOpen']:
      try:
        html = urllib2.urlopen('http://vmepc-s2g16-20-01.cms:41000/urn:xdaq-application:lid=11/').read()
        status = 'connectionAccepted'
      except:
        pass
    if status == 'connectionAccepted':
      status = 'HTTPRequestError' if 'HTTP Request Error' in html else 'HTTPRequestOK'

    if status == 'HTTPRequestOK':
      try:
        status = html.split('stateName')[1].split('\n')[3].strip() + '\t' + html.split('runNumber')[1].split('\n')[3].strip()
      except:
        status = 'HTMLParsingError'
    
    if status != laststatus:
      # Report change of status
      with open('mimic_report.txt','a') as f:
        f.write(str(datetime.datetime.now()) + '\t'+ status + '\n')

      # Run necessary transitions
      if 'Configured' in status:
          with open('mimic_report.txt','a') as f:
            f.write('Proceed to uros cell CONFIGURE transition\n')
          UROSCell('uros_noSlink').configure()
          with open('mimic_report.txt','a') as f:
            f.write('Proceed to uros cell START transition\n')
          UROSCell('uros_noSlink').start()
          with open('mimic_report.txt','a') as f:
            f.write('Finished START transition\n')

      # Update the mimic_status_XXX file
      for fn in os.listdir('.'):
        if 'mimic_status_' in fn:
          os.remove(fn)
      with open('mimic_status_'+status.replace('\t','_')+'_'+str(int(time.time())), 'w') as f:
        f.write(' ')
      
except:
  time.sleep(1)
  # this try inside the except is because not all backtraces are being written, and this should reveal what is the problem
  try:
    with open('mimic_report.txt','a') as f:
      f.write('Exception raised, backtrace should follow\n')
    with open('mimic_report.txt','a') as f:
      f.write(traceback.format_exc())
  except:
    time.sleep(1)
    with open('mimic_report.txt','a') as f:
      f.write(traceback.format_exc())
