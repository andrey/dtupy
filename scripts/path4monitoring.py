#!/bin/env python

import getpass
import os


username = getpass.getuser()

suffix1 = ''
while True:
  folder = '/tmp/' + username + str(suffix1)
  if os.path.exists(folder):
    if os.path.isdir(folder) and os.access(folder, os.W_OK):
      break
    else:
      suffix1 = suffix1 + 1 if suffix1 else 1
  else:
    os.mkdir(folder)
    break

suffix2 = ''
while True:
  folder = '/tmp/' + username + str(suffix1) + '/urosmonitor' + str(suffix2)
  if os.path.exists(folder):
    if os.path.isdir(folder) and os.access(folder, os.W_OK):
      break
    else:
      suffix2 = suffix2 + 1 if suffix2 else 1
  else:
    os.mkdir(folder)
    break

print folder