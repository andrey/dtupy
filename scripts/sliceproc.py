#!/bin/env python

from dtupy.uroscell import UROSCell
from dtupy.bcolors import bcolors
import os, sys, time, datetime, random
import traceback, cx_Oracle
import zlib, json
import requests
import xml.etree.ElementTree as ET
import socket
import getpass
import pprint



def log_message(message):
  try:
    message = str(datetime.datetime.now()) + '\t' + message
    reportfile = 'sliceproc_report.txt'
    if reportfile in os.listdir('.'):
      datereportfile = datetime.date.fromtimestamp( os.path.getmtime(reportfile) )
      if datereportfile != datetime.date.today():
        newreportfile = reportfile.replace('.','_%s.'%datereportfile)
        newreportfilecontent = ''
        with open(newreportfile, 'a') as fout, open(reportfile, 'r') as fin:
          newreportfilecontent = fin.read()
          fout.write(newreportfilecontent)
        with open(reportfile, 'w') as f:
          f.write('')
          
    with open(reportfile,'a') as f:
      f.write( message + '\n' )
  except:
    print 'Exception when writing to log file, error redirected to stderr while trying to log the message:'
    print message
    print 'This is the exception itself'
    traceback.print_exc()
    
def keepgoing(info):
  try:
    firstTime = 'id' not in info
    if firstTime:
      info['id'] = '%s_%s_%i_%06i'%( socket.gethostname(), getpass.getuser(), os.getpid(), int(random.random()*1e6) )
    
    stopCause = ''
    
    if 'sliceproc_disable' in os.listdir('.'):
      stopCause = 'sliceproc_disable file found in tmp folder'
      
    if stopCause:
      if not firstTime: # if the script had already run once, this will cause it to stop --> report it
        log_message( '[INFO] Exiting session of the monitor script (#%s) because an exit condition was met (%s)'%(info['id'], stopCause) )
    else:
      if firstTime:
        log_message( '[INFO] Starting monitor script with id#%s'%info['id'] )

    return stopCause == ''
  except:
    log_message('[ERROR] Exception raised in method keepgoing(); Stopping monitor script\n' + traceback.format_exc() )
    return False
    
runTypes = {'Global':40000, 'Local':41000}
def getinfo(info):
  ## Get the status from the xdaq application by SOAP
  headers = {
    'content-type': 'text/xml', 
    'Content-Description': 'SOAP Message', 
    'SOAPAction': 'urn:xdaq-application:class=SOAPStateMachine,instance=0' }

  body = '''
  <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Header>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
      <xdaq:WhatsYourStatus xmlns:xdaq="urn:xdaq-soap:3.0"/>
    </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>
  '''
  
  commErrCounter = 0
  for runType in runTypes:
    try:
      url = 'http://%s:%i'%('vmepc-s2g16-14-01', runTypes[runType])
      response = requests.post(url,data=body, headers=headers)
      response = ET.fromstring(response.content)[1][0].tag
      response = response.split('WhatsYourStatusResponse_')[1].split('_')
      response = { response[i]:response[i+1] for i in range(0,len(response),2) }
      for key in response:
        try: response[key] = int(response[key])
        except: pass

      if any([key not in info or response[key]!=info[key] for key in response]):
        response['State_change']=time.time()
      
      response['runType'] = runType
      info.update(response)
      break
    except:
      commErrCounter += 1

  if commErrCounter == len(runTypes):
    info['State']='CommError'
    info['runType']='N/A'
  
  #foldercontents = os.listdir('.')
  #info['write_files'] = 'write_files' in foldercontents

  
  
  
  
  
  
  
  
  
  
  
bits_bx = 12
bits_muonornot = 1
fw_version = 8
  
def readRams(uc):
  crate = uc.s.hw.slice
  result = {}
  for slotNum in crate.slots:
    result[slotNum] = {}
    for wheel in range(4):
      rb = crate['slot%i'%slotNum].hw().getClient().readBlock(0xd2000000+(wheel<<25),2**(bits_bx + bits_muonornot))
      crate['slot%i'%slotNum].hw().getClient().dispatch()
      result[slotNum][wheel] = map(int, rb)
  return result

def dump(histo):
  txt = '\n'
  for slotNum in histo:
    txt += '\nslot %i '%slotNum
    for wheel in range(4):
      txt += '\nwheel %i muon events per bx\n'%wheel
      for bxnum in range(2**bits_bx):
        txt += '%i\t'%histo[slotNum][wheel][bxnum]
      txt += '\nwheel %i non-muon events per bx\n'%wheel
      for bxnum in range(2**bits_bx):
        txt += '%i\t'%histo[slotNum][wheel][bxnum + (1<<bits_bx)]
    
  return txt
    
#######################################################  

period = 1
dataDumpPeriod = 300
lastLumi = -1
t0 = -1000

# Run information
info = {  'runNumber' : 0, 'State' : 'Unknown', 'runType':'N/A', 'partKey' : 0, 'configKey' : 0,
          'old_State':None, 'State_change' : 0, 
          'lastDataDump': 0, 'totalEvents':0
          }

histo0 = None
while keepgoing(info):
  while t0+period > time.time(): pass
  t0 = time.time()

  try:
    getinfo(info)
  except:
    log_message('[ERROR] Exception raised in method getinfo(); Re-trying...\n' + traceback.format_exc() )
    continue
  
  try:
    if info['old_State'] == None or ( info['old_State'] not in ['Running','Configured'] and info['State'] == 'Configured' ):
      print info['old_State'], info['State']
      log_message( '[INFO] Init/Configure, slice monitor for fw version %i'%fw_version )
      uc = UROSCell('uros_slice_lumi')
      uc.configure()
    if info['old_State'] == 'Configured' and info['State'] == 'Running':
      log_message( '[INFO] Start detected' )
      uc.start()
      histo0 = readRams(uc)
      info['lastDataDump']=t0
    if info['old_State'] in ['Running', 'Paused'] and info['State'] not in ['Running', 'Paused']:
      log_message( '[INFO] run %i ended: '%(info['runNumber'])) 

    info['old_State'] = info['State']

    # retrieve monitoring data from boards
    #############################################
    
    orbit_number = uc.s.hw.slice.slot4.read('readout.stat.tmp.timesupdiff') # this contains the orbit number in the modified fw
    
    if ((orbit_number >> 18) != lastLumi) and ((orbit_number & 0x3FFFF) > 0x1FFF):
      lastLumi = (orbit_number >> 18)
    else:
      continue
    
    if histo0 == None: histo0 = readRams(uc)
    histo = readRams(uc)
    for slotNum in histo:
      for wheel in histo[slotNum]:
        for i in range(len(histo[slotNum][wheel])):
          if (histo[slotNum][wheel][i] >> 30) != (lastLumi & 0x3) :
            print '[WARN] Supposed to be reading lumi section  0x%X, but last two bits are 0x%X, in slot %i, wheel %i, i %i'%(lastLumi, (histo[slotNum][wheel][i] >> 30), slotNum, wheel, i)
          histo[slotNum][wheel][i] = (histo[slotNum][wheel][i]>>15)&0x7FFF
    
    info['totalEvents'] = sum(histo[histo.keys()[0]][0])
    
    log_message( '[DATADUMP] run %i lumi section %i (%.1e events) ended: '%(info['runNumber'],lastLumi-1, info['totalEvents']) + dump(histo) ) 
    
    
  except:
    log_message( '[ERROR] Exception in hw access\n' + traceback.format_exc() )
    continue
  


