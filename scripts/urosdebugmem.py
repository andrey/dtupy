#!/bin/env python

from dtupy.systembuilder import loadkey
from dtupy.bcolors import bcolors
import os, time, select,sys


stopConditions = {
  'edgeshift      '.strip():   0  ,
  'unlock12       '.strip():   1  ,
  'startstoperr   '.strip():   2  ,
  'parityerr_total'.strip():   3  ,
  'parityerr_nofix'.strip():   4  ,
  'valid          '.strip():   5  ,
  'w32interrupt   '.strip():   6  ,
  'w32valid       '.strip():   7  ,
  'inevent_idles  '.strip():   8  ,
  'header         '.strip():   9  ,
  'trailer        '.strip():  10  ,
  'hits           '.strip():  11  ,
  'errs           '.strip():  12  ,
  'unknown_robword'.strip():  13  ,
  'not_in_event   '.strip():  14  ,
  'nonconsecutive '.strip():  15  ,
  'notrailer      '.strip():  16  ,
  'badtrailer_evn '.strip():  17  ,
  'badtrailer_wrd '.strip():  18  ,
  'fifo_ofw_losses'.strip():  19  ,
  'wren           '.strip():  20  ,
  'fifo_af        '.strip():  21  ,
}


s = loadkey('uros')
uros = s.hw['zero']['slot3']
ch = 5
stop = 'parityerr_total'
  
if len(sys.argv)>1 and 'read' in sys.argv[1]:
  print 'Waiting for debugmem to be triggered'
  while (uros.read("readout.stat.debugmem_port_0")>>31):
    time.sleep(.2)
  words = []
  advance = 0
  for i in range(512):
    advance = 1 - advance
    uros.write("readout.conf.rob.debugmem_advance",advance)
    readadvance = uros.read("readout.conf.rob.debugmem_advance")
    #time.sleep(.01)
    readword = 0
    for j in reversed(range(4)):
      readword = readword<<32
      readword += uros.read("readout.stat.debugmem_port_%i"%j)
    
    #print 'adv %i/%i'%(advance,readadvance), advance==readadvance, 
    #if words: print ((readword>>27)&0xF) == (((words[-1]>>27)&0xF) -1 )%16, ((readword>>27)&0xF)
    #else: print ((readword>>27)&0xF)
    words += [ readword ]
    
  lastword = 0
  repeatcount = 0
  forevercount = 0
  first = True
  prev_ctr, post_ctr, prev_rep_ctr, post_rep_ctr = 0,0,0,0
  print '\n\ndata raw_word12  weak_word12  lock toggle X good repeated'
  for w in words[::-1]:
    if not first and ((w>>27)&0xF) != ((forevercount +1) % 16):
      print 'forevercounterror(%i->%i)'%(forevercount, (w>>27)&0xF),
      if (w>>31)&1 : prev_rep_ctr += 1
      else: post_rep_ctr += 1
    if (w>>31)&1 : prev_ctr += 1
    else: post_ctr += 1
    forevercount = (w>>27)&0xF
    w = w & 0x7FFFFFFFFFFFFFFFFFFFFFF87FFFFFF
    if w != lastword or first: 
      if repeatcount:
        print 'x%i'%repeatcount,
        if repeatcount != 5: print '!!!!',

      ### samples decoding
      debug = (w>>32)&0x7FFFFFFFFFFFFFFFFFFFFFF
      isdq, separators = '', [0]*70
      for i in range(7):
        isdq = bin((debug>>(13*i+3))&0x3FF)[2:].zfill(10) + isdq
        offset = (debug>>(13*i))&0x7
        for j in range(2):
          pos1, pos2 = 69 - (10*i+offset-2+5*j), 69 - (10*i+offset+3+5*j)
          if pos1 > -1: separators[pos1] = ']' if separators[pos1] != '[' else ']['
          if pos2 > -1: separators[pos2] = '[' if separators[pos2] != ']' else ']['
      iserdes = '->'
      for i in range(70):
        iserdes += isdq[i] + (separators[i] if separators[i] else '')
      
      
      ## the rest
      word12 = hex(int( (w>>1)&0xFF ))[2:].zfill(2)
      print
      print (word12+'  ' if (w>>10)&1 else '  '+word12)if (w>>26)&1 else ' '*4,
      print '   ' + bin(w&0xFFF)[2:].zfill(12),
      print bin((w>>12)&0xFFF)[2:].zfill(12),
      print iserdes,
      print '  lock' if (w>>24)&1 else 'unlock',
      print '  |' if (w>>25)&1 else '|  ',
      print 'changed' if (w>>26)&1 else '=======', 
      print '   ' if (w>>31)&1 else 'x_x' ,
      repeatcount = 0
      lastword = w
    else:
      repeatcount = repeatcount + 1 if repeatcount else 2
    first = False
  
  print '\n\n prev_ctr = %i, prev_rep_ctr = %i, post_ctr = %i, post_rep_ctr = %i, post_different_words = %i'%(prev_ctr, prev_rep_ctr, post_ctr, post_rep_ctr, post_ctr - post_rep_ctr)
    
if len(sys.argv)>1 and 'start' in sys.argv[1]:
  uros.write("readout.conf.rob.debugmem_mux",ch)
  uros.write("readout.conf.rob.debugmem_trig",stopConditions[stop])
  uros.write("readout.conf.rob.debugmem_start",1)
  uros.write("readout.conf.rob.debugmem_start",0)
  uros.write("readout.conf.rob.debugmem_advance",0)
  print 'Started storage in debugmem for channel %i and stop condition "%s"'%(ch, stop)
