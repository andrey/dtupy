#!/bin/env python

import os
import cx_Oracle
import datetime
import smtplib

hostname = 'vmepc-s2g16-14-01'
max_time = 100

failure = ''

if os.system('ping -c 1 %s > /dev/null 2>&1'%hostname):
  failure += 'UROS CONTROL HOST DOWN? %s failed to respond to ping\n'%hostname


username = 'cms_dt_elec_conf'
service = 'cms_omds_lb'
password = 'cmsonr2008'

con = cx_Oracle.connect( '%s/%s@%s'%(username,password,service) )
con.autocommit = 1
cur = con.cursor()
cur.execute( 'select TIME_STAMP from W_MON_LIVE order by TIME_STAMP desc' )
mostrecentrecord = cur.fetchone()[0]
cur.close()
con.close()

td = datetime.datetime.now() - mostrecentrecord
age = (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6 # unfortunately hosts running python 2.6 cannot use td.total_seconds()

if age>max_time:
  failure += 'UROS MONITORING PROCESS DOWN? most recent row in db table W_MON_LIVE is too old: %i s > %i s\n'%(age, max_time)

# email options
smtpserver = "localhost"
sender = "uros host watchdog <dtdqm@vmepc-s2g16-14-01.localdomain>"
recipients = ["anavarro@cern.ch"]
subject = 'UROS watchdog error'

message = """\
From: %s
To: %s
Subject: %s

%s
"""
msg = message%(sender, ", ".join(recipients), subject, failure)

if failure:
  server = smtplib.SMTP(smtpserver)
  #server.set_debuglevel(3)
  server.sendmail(sender, recipients, msg)
  server.quit()

print msg  
